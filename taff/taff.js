(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
           //$('head').append('<link href="https://fonts.googleapis.com/css?family=Roboto&Suranna&display=swap" rel="stylesheet" type="text/css">');
           $('head').append('<link href="https://fonts.googleapis.com/css2?family=Suranna&display=swap" rel="stylesheet">');
           $('head').append('<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap" rel="stylesheet">');
           $('.signup-text > p').text('Keep up with our latest products and promotions by joining our mailing list. Don’t miss out. Register today.');
           $('.custom-container:contains("{{iframe-taff-contact-us}}")').html('<iframe height="699" title="Embedded Wufoo Form" allowtransparency="true" frameborder="0" scrolling="no" style="width:100%;border:none" src="https://techsembly.wufoo.com/embed/x1l0rr1u0rwkqfg/">');
           //$('.powered-by > div:nth-child(1)').html('© 2022 Textile and Fashion Federation Singapore. All Rights Reserved.</br/>');
           $('.promo-title').text('In the Spotlight');
           $('#refineBtn').text('Filter');
	   $('img[alt="techsemblylogoNEW"]').attr('src','https://media.graphcms.com/j6MMRu8SOiW8N8cmh5zk');
	   $(".dropdown-toggle").before('<img src="https://media.graphcms.com/resize=,width:25,height:20/a2bzDjtKSjiSNSzSIokF" style="margin-right:5px; margin-bottom:2px"/>');
	   $('.toggle-menu-cont .shipping li').first().html("Ship to ");
           $('.nav-search-field').attr("placeholder", "Search");
           $('.home-intro-container h2').text("About OneOrchard.Store");
           $('.sign-up-form input').attr("placeholder", "Enter your email address");
           $('img[alt="securepaymentbadge"]').attr("src","https://media.graphcms.com/wxOEWKgpRBqZDRYtvGm4");
           $('img[alt="paypal"]').attr('src','https://media.graphcms.com/MFJ7FXxkTL2k38lA4epv');
	   $('img[alt="visa"]').attr('src','https://media.graphcms.com/ZnEBjiO1RsKDLYdzqdxo');
	   $('img[alt="mastercard"]').attr('src','https://media.graphcms.com/iANG75BYQJqcgkyMWeAU');
	   $('.new-cust-note').text('First time shopping? Enter your email address to continue.')

	   $('.cards-desc').html('<img _ngcontent-serverapp-c5="" alt="paypal" src="https://media.graphcms.com/MFJ7FXxkTL2k38lA4epv" width="60" style="padding:2px"><img _ngcontent-serverapp-c5="" alt="visa" src="https://media.graphcms.com/ZnEBjiO1RsKDLYdzqdxo" width="60" style="padding:2px"><img _ngcontent-serverapp-c5="" alt="mastercard" src="https://media.graphcms.com/iANG75BYQJqcgkyMWeAU" width="60" style="padding:2px">');
           $('.success-title').text("We've received your order!");

           setInterval(function() {
    		    $('.vendor-banner:not(.appeared)')
        	    .addClass('appeared')
        	        .each(function() {
		         $('.vendor-banner.akosee').css('background-image','url(https://media.graphcms.com/pLZNNjwwQW8JUGTqkV97)');
		         $('.vendor-banner.anna-rainn').css('background-image','url(https://media.graphcms.com/3tYAhmvwT1SFhPRa5ye1)');
		         $('.vendor-banner.ans-ein').css('background-image','url(https://media.graphcms.com/Ws32gSGwQb6HDsC16XHo)');
		         $('.vendor-banner.bells-birds').css('background-image','url(https://media.graphcms.com/i3fbljx2QzGXQgASMHDy)');
		         $('.vendor-banner.carrie-k').css('background-image','url(https://media.graphcms.com/epNhEU3nRjuZ1jEX96rl)');
		         $('.vendor-banner.forbidden-hill').css('background-image','url(https://media.graphcms.com/U58iu06gTCD1SBKM6NcH)');
		         $('.vendor-banner.ginlee-studio').css('background-image','url(https://media.graphcms.com/9r27ZclQW2rkTuq3d8dw)');
		         $('.vendor-banner.hher').css('background-image','url(https://media.graphcms.com/Qmr3EmsTrmoxqB7Qydde)');
		         $('.vendor-banner.josee-p').css('background-image','url(https://media.graphcms.com/y769ZHAYTOSvAgk2qar2)');
		         $('.vendor-banner.lai-chan').css('background-image','url(https://media.graphcms.com/L3z6WO23RQG7D6u0UjKZ)');
		         $('.vendor-banner.ling-wu').css('background-image','url(https://media.graphcms.com/Mj8CKM53STyTb7OXGKhv)');
		         $('.vendor-banner.minor-miracles').css('background-image','url(https://media.graphcms.com/wDFop5vySEqAHB0xkv75)');
		         $('.vendor-banner.nena').css('background-image','url(https://media.graphcms.com/dIsjjYfkTruTpim0ducg)');
		         $('.vendor-banner.oeteo').css('background-image','url(https://media.graphcms.com/s8sk2bHjQCSfgOqxfjfy)');
		         $('.vendor-banner.r-y-e').css('background-image','url(https://media.graphcms.com/IKafLz7R9a18t5fuxU04)');
		         $('.vendor-banner.silvia-teh').css('background-image','url(https://media.graphcms.com/9T53FoWWQu225AhWoLie)');
		         $('.vendor-banner.techsemblytest').css('background-image','url()');
		         $('.vendor-banner.the-form').css('background-image','url(https://media.graphcms.com/CbfQx6RTSZCGkNDELxbW)');
		         $('.vendor-banner.the-lab-fragrances').css('background-image','url(https://media.graphcms.com/LlpIXLKJSDWP9Zm77FZZ)');
		         $('.vendor-banner.toufie').css('background-image','url(https://media.graphcms.com/mXNVuzskTuGRNc7P09pJ)');
		         $('.vendor-banner.weekend-sundries').css('background-image','url(https://media.graphcms.com/WdTr3Ai3Sk6soqcM4VBF)');
		         $('.vendor-banner.ying-the-label').css('background-image','url(https://media.graphcms.com/QjTQtSWoQgCfFh2efIix)');
		         $('.vendor-banner.hunter-and-boo').css('background-image','url(https://media.graphcms.com/v26IESx6QfeVUOGNdgMo)');
		         $('.vendor-banner.shirt-number-white').css('background-image','url(https://media.graphcms.com/7w2FV17QQoW9r7GTcEx5)');
		    });
           	  });

           setInterval(function() {
    		    $('.vendor-point-cont:not(.appeared)')
        	    .addClass('appeared')
        	        .each(function() {
		         $("a[routerlink='/delivery']").attr("href", "/pages/delivery");
		         $("a[routerlink='/returns']").attr("href", "/pages/returns");
		         $("a[routerlink='/terms-and-conditions']").attr("href", "/pages/terms-and-conditions");
		    });
           	  });
           $('.success-title').text("We've received your order!");
		setInterval(function() {
    		    $('.product-info .made-by:not(.appeared)')
        	    .addClass('appeared')
        	        .each(function() {
		         $('.product-info .made-by').html(function(index, html) {return html.replace('Lovingly made in Singapore', '');});
                         $('.product-info .made-by').html(function(index, html) {return html.replace('Lovingly made in Malaysia', '');});
		         $('.product-info .made-by br').css('display','none');
		    });
           	  });
		setInterval(function() {
    		    $('.variant-container:not(.appeared)')
        	    .addClass('appeared')
        	        .each(function() {
		        $(".variant-container .Size .option-label").after('<span style="margin-left:10px">(<a href="/pages/size-guide">Size Guide</a>)</span>');
		    });
           	  });
 		setInterval(function() {
    		    $("img[src$='https://media.graphcms.com/cIypYEbWSoHgbeeUfZkQ']:not(.appeared)")
        	    .addClass('appeared')
        	        .each(function() {
        	   	$("img[src$='https://media.graphcms.com/cIypYEbWSoHgbeeUfZkQ']").before('<div style="clear:both"><a href="javascript:history.back()"><< Return to Product</a></div>');
		    });
           	  });
 		setInterval(function() {
    		    $('.cards-desc:not(.appeared)')
        	    .addClass('appeared')
        	        .each(function() {
                        $('.cards-desc').html('<img _ngcontent-serverapp-c5="" alt="paypal" src="https://media.graphcms.com/MFJ7FXxkTL2k38lA4epv" width="60" style="padding:2px"><img _ngcontent-serverapp-c5="" alt="visa" src="https://media.graphcms.com/ZnEBjiO1RsKDLYdzqdxo" width="60" style="padding:2px"><img _ngcontent-serverapp-c5="" alt="mastercard" src="https://media.graphcms.com/iANG75BYQJqcgkyMWeAU" width="60" style="padding:2px">');
		    });
           	  });
 		setInterval(function() {
      $('.user-container .balance-form').closest('.user-container:not(.d-none)').addClass('d-none');
      $('.user-container .ts-balance-form').closest('.user-container:not(.d-none)').addClass('d-none');
    		    $('img[alt="securepaymentbadge"]:not(.appeared)')
        	    .addClass('appeared')
        	        .each(function() {
                        $('img[alt="securepaymentbadge"]').attr("src","https://media.graphcms.com/wxOEWKgpRBqZDRYtvGm4");
		    });
            $('.vendor-micro-cont .vendor-intro .vendor-logo').each(function(){
                if(!$(this).parent('div').hasClass('vendor-logo-cont')){
                    $(this).parent('div').addClass('vendor-logo-cont');
                }
            });
            $('.vendor-micro-cont .container.vendor-contact-form-cont').each(function(){
                if(!$(this).children('.vendor-form-inner').length){
                    $(this).wrapInner($("<div class='vendor-form-inner'>"));
                }
            });
            $('.vendor-micro-cont .container.vendor-contact-form-cont .vendor-form-inner').each(function(){
                $(this).find('h2.vendor-heading').prependTo($(this).closest(".vendor-contact-form-cont"));
            });
            $('.footer a:contains("@")').each(function(){
              var string_email= $(this).html();
              var result_email = string_email.substring(string_email.indexOf(':') + 1);
              var email_link = "mailto:" + result_email;
              if(!$(this).hasClass('email-link')){
                $(this).addClass('email-link');
                $(this).attr('href', email_link);
              }
            });
        });
        });
    });
})();
