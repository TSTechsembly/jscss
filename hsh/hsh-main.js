function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }
    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });
    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}
function addBottomFooter() {
  var footerHtml = '<div class="col-12 footer-bottom-container px-0 pt-3 pt-lg-5 d-none d-lg-block"><!-- START FooterCookieLinks COMPONENT -->'+
    '<div data-react-component="CookieLinks" class="w-100"><div class="footer-cookie-links-container w-100">'+
      '<ul class="d-lg-inline-flex align-items-center mb-0 pl-0"><li><a href="https://www.peninsula.com/en/global-pages/data-privacy-and-security" title="Data Privacy and Security Policy" target="_blank">Data Privacy and Security Policy</a></li><li><a href="https://www.peninsula.com/en/accessibility-policy-statement" title="Accessibility Policy Statement (US Properties)" target="_blank">Accessibility Policy Statement (US Properties)</a></li>'+
      '<li><!-- CookiePro Cookies Settings button start --><button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</button><!-- CookiePro Cookies Settings button end --></li></ul>'+
      '</div></div>'+
      '<!-- END FooterCookieLinks COMPONENT -->'+
  '</div>';
  var elem = $(".footer .copyright-cont")[0];
  elem.insertAdjacentHTML('beforebegin', footerHtml);
}
function addMobBottomFooter() {
  var footerMobHtml = '<div class="container custom-container d-lg-none"><div class="col-12 footer-bottom-container px-0 pt-3 pt-lg-5"><!-- START FooterCookieLinks COMPONENT -->'+
    '<div data-react-component="CookieLinks" class="w-100"><div class="footer-cookie-links-container w-100">'+
      '<ul class="d-lg-inline-flex align-items-center mb-0 pl-0"><li><a href="https://www.peninsula.com/en/global-pages/data-privacy-and-security" title="Data Privacy and Security Policy" target="_blank">Data Privacy and Security Policy</a></li><li><a href="https://www.peninsula.com/en/accessibility-policy-statement" title="Accessibility Policy Statement (US Properties)" target="_blank">Accessibility Policy Statement (US Properties)</a></li>'+
      '<li><!-- CookiePro Cookies Settings button start --><button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</button><!-- CookiePro Cookies Settings button end --></li></ul>'+
      '</div></div>'+
      '<!-- END FooterCookieLinks COMPONENT -->'+
  '</div></div>';
  var elem = $(".footer .copyright-cont.d-lg-none")[0];
  elem.insertAdjacentHTML('beforebegin', footerMobHtml);
}
(function() {
  var script = document.createElement("SCRIPT");
  script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
  script.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script);
      var newStyle = document.createElement('style');
      newStyle.appendChild(document.createTextNode("\
        @font-face {\
          font-family: 'futura-pt';\
          src:url('https://use.typekit.net/af/9b05f3/000000000000000000013365/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3') format('woff2');\
          url('https://use.typekit.net/af/9b05f3/000000000000000000013365/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3') format('woff');\
          url('https://use.typekit.net/af/9b05f3/000000000000000000013365/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3') format('opentype');\
        }\
        @font-face {\
          font-family: 'minion-pro';\
          src: url('https://use.typekit.net/af/1286c7/0000000000000000000151d6/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3') format('woff2');\
          url('https://use.typekit.net/af/1286c7/0000000000000000000151d6/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3') format('woff');\
          url('https://use.typekit.net/af/1286c7/0000000000000000000151d6/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3') format('opentype');\
        }\
      "));
    document.head.appendChild(newStyle);

    //amenities patch
      var ee = document.createElement('script');
      ee.src = 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js';
      ee.async = true;
      document.head.appendChild(ee);

      var ew = document.createElement('link');
      ew.href = 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css';
      //document.head.appendChild(ew);

      var ew = document.createElement('script');
      ew.src = 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js';
      ew.async = true;
      document.head.appendChild(ew);

      var ef = document.createElement('link');
      ef.href = 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.css';
      ef.rel = 'stylesheet';
      document.head.appendChild(ef);

    //end amenities patch
    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };
    checkReady(function($) {
      $(function() {
        $('header .mob-top-links .mob-search-link').click(function() {
          if(!$('nav#sidebar #mobile-nav-sub').hasClass('d-block')){
            $('nav#sidebar #mobile-nav-sub').addClass('d-block');
            if(!$('nav#sidebar #mobile-nav-sub').parent('nav#sidebar').hasClass('active')) {
              $('nav#sidebar #mobile-nav-sub').parent('nav#sidebar').addClass('active');
              $('.mob-overlay').addClass('active');
            }
            if($('nav#sidebar #mobile-nav-sub').parent('nav#sidebar').attr('ng-reflect-ng-class') != 'active' ) {
              $('nav#sidebar #mobile-nav-sub').parent('nav#sidebar').attr('ng-reflect-ng-class','active');
            }
          }
          else if(($('nav#sidebar').hasClass('active')) && (!$('nav#sidebar #mobile-nav-sub').hasClass('d-block'))){
            $('nav#sidebar #mobile-nav-sub').addClass('d-block');
          }
          else if(($('nav#sidebar').hasClass('active')) && ($('nav#sidebar #mobile-nav-sub').hasClass('d-block'))){
                $('nav#sidebar #mobile-nav-sub').removeClass('d-block');
            }
        });
        $('#sidebarCollapse').click(function() {
            if(!$('nav#sidebar').hasClass('active')){
               $('.mob-overlay').removeClass('active');
            }
        });

        $(window).on('load', function(){
          setTimeout(function () {
            var attr = $('body').attr('data-pagetype');

            // For some browsers, `attr` is undefined; for others,
            // `attr` is false.  Check for both.
            if (typeof attr !== 'undefined' && attr !== false) {
              //console.log(attr);
            }
            if(attr === 'product' && $('header .head-links li .cart-qty').length){
              $(document).scrollTop( $(".cart-bag").offset().top );
              console.log('items added to cart');
            }
          }, 2000);
        });

        waitForElm('.sidebar-widget .list-group .list-group-holder .widget-holder ul').then((elem) => {
          var monthNames = [
            'january', 'february', 'march', 'april', 'may', 'june',
            'july', 'august', 'september', 'october', 'november', 'december'
          ];

          $('.sidebar-widget .list-group .list-group-holder .widget-holder ul li div').each(function() {
            var childClasses = $(this).attr('class').split(' ');
            var lastClass = childClasses[childClasses.length - 1];
            lastClass = lastClass.substring(lastClass.indexOf('-') + 1);
            let isMatch = monthNames.includes(lastClass);
            if(isMatch) {
              $(this).closest('ul').addClass('months-ul');
              $(this).parent('li').addClass(lastClass);
            }
          });
        }).catch((error) => { });

        waitForElm('.checkout-footer').then((elm) => {
          let hsh_store = window.location.pathname.split("/")[1];
          if (hsh_store == 'hongkong') {
            $('.checkout-footer img[alt="securepaymentbadge"]').attr('src','https://static.techsembly.com/6aQPAvWYMYws62LB9HfF7dbp');
            //$('.checkout-footer img[alt="securepaymentbadge"]').removeAttr('width');
          }
        }).catch((error) => {});

        waitForElm('footer').then((elm) => {
          addBottomFooter();
          addMobBottomFooter();
        }).catch((error) => {});

        /*waitForElm('.select-form.form-group input[placeholder="Select Date"]').then((elm) => {
          let host_store = window.location.pathname.split("/")[1];
          let lastSegment = window.location.href.split('/').pop();
          let datesForDisable = $('.delivery-text div').html();
          let daysToDisable = $('.detail-holder div').html();
          $('.select-form.form-group input[placeholder="Select Date"]').each(function(){
            $(this).addClass('date-appeared');
            $(this).prop("readonly", true);
            $(this).datepicker({
                minDate: daysToDisable,
                format: 'dd-mm-yyyy',
                autoclose: true,
                startDate:daysToDisable,
                todayHighlight: true,
                datesDisabled: datesForDisable

                }).on("changeDate", function (e) {
                $(this)[0].dispatchEvent(new Event("input", { bubbles: true }));
            });
          });
          console.log(host_store);
          console.log(lastSegment);
        }).catch((error) => {});*/

        setTimeout(function() {
          $('<div  class="spacing mobile-nav-sub mob-search-cont d-lg-none pt-2" id="mobile-nav-sub"><div  class="mobile-search-mini-form col-12 px-0"></div></div>').insertBefore('nav#sidebar .mob-nav');
          $('#mobile_search_mini_form').appendTo('nav#sidebar #mobile-nav-sub .mobile-search-mini-form');
          $('.footer .add-widget').each(function() {
            if(!$(this).find('.social-new-widget').length) {
              $(this).append('<div clas="footer-widget social-new-widget"><div class="widget-content pt-0"><div class="d-flex pay-methods"><div class="meth-link mr-3"><img alt="mastercard" src="https://bitbucket.org/TSTechsembly/jscss/raw/087bc2c77bcfcd91b305e9ce233ca67018b4aa03/hsh/images/mastercard2-hsh.svg"></div><div class="meth-link mr-3"><img alt="amex" src="https://bitbucket.org/TSTechsembly/jscss/raw/087bc2c77bcfcd91b305e9ce233ca67018b4aa03/hsh/images/amex-hsh.svg"></div><div class="meth-link mr-3"><img alt="visa" src="https://bitbucket.org/TSTechsembly/jscss/raw/087bc2c77bcfcd91b305e9ce233ca67018b4aa03/hsh/images/visa-hsh.svg"></div></div><div class="pt-4 intouch-note"><span>The Peninsula Hotels have<br>selected <a href="https://www.techsembly.com">Techsembly</a> to help you<br> choose, purchase and receive<br> your gift vouchers<br><br> Techsembly&#39;s secure checkout is<br> provided by <a href="https://www.stripe.com/">Stripe</a> and your<br> payment details are never<br> stored.</span><div><img class="mr-2" height="30" src="https://bitbucket.org/TSTechsembly/jscss/raw/087bc2c77bcfcd91b305e9ce233ca67018b4aa03/hsh/images/shield-hsh.svg" width="30"><span>Secured by SSL</span></div></div></div></div>');
            }
          });
          $('.footer .mobile-footer').each(function() {
            if(!$(this).find('.social-new-widget').length) {
              $(this).append('<div clas="footer-widget social-new-widget"><div class="widget-content pt-0"><div class="d-flex pay-methods"><div class="meth-link mr-3"><img alt="mastercard" src="https://bitbucket.org/TSTechsembly/jscss/raw/087bc2c77bcfcd91b305e9ce233ca67018b4aa03/hsh/images/mastercard2-hsh.svg"></div><div class="meth-link mr-3"><img alt="amex" src="https://bitbucket.org/TSTechsembly/jscss/raw/087bc2c77bcfcd91b305e9ce233ca67018b4aa03/hsh/images/amex-hsh.svg"></div><div class="meth-link mr-3"><img alt="visa" src="https://bitbucket.org/TSTechsembly/jscss/raw/087bc2c77bcfcd91b305e9ce233ca67018b4aa03/hsh/images/visa-hsh.svg"></div></div><div class="pt-4 intouch-note"><span>The Peninsula Hotels have<br> selected <a href="https://www.techsembly.com">Techsembly</a> to help you<br> choose, purchase and receive<br> your gift vouchers<br><br> Techsembly&#39;s secure checkout is<br> provided by <a href="https://www.stripe.com/">Stripe</a> and your<br> payment details are never<br> stored.</span><div><img class="mr-2" height="30" src="https://bitbucket.org/TSTechsembly/jscss/raw/087bc2c77bcfcd91b305e9ce233ca67018b4aa03/hsh/images/shield-hsh.svg" width="30"><span>Secured by SSL</span></div></div></div></div>');
            }
          });
          $('.header #sidebar').each(function() {
            if(!$(this).children('side-topbar').length){
              $(this).append('<ul class="side-topbar"><li><a href="#">The Peninsula Hotels</a></li><li><a href="#">The Peninsula Hong Kong</a></li><li><div class="trans-wrap"><img src="https://gifts.peninsula.com/images/world.svg"><select><option>English</option></select></div></li></ul>');
            }
          });
          $('.trans-wrap select').change(function(){
            let hsh_store = window.location.pathname.split("/")[1];
            if (($(this).val() == 'en') && (hsh_store == 'tokyo')) {
              window.location.href = "https://gift.peninsula.com/tokyo-en";
            }
            if (($(this).val() == 'jp') && (hsh_store == 'tokyo-en')) {
              window.location.href = "https://gift.peninsula.com/tokyo";
            }
          });
        }, 2000);
        setInterval(function() {
          hrefurl=$(location).attr("href");
            last_part=hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
            //console.log(last_part);
            if (last_part == 'wishlist'){
               if (!$('body').attr('data-pagetype','wishlist')){
                   $('body').attr('data-pagetype','wishlist');
               }
               /*desktop*/
               if(!$('header.header .head-links li a img[alt="wishlist"]').attr('src','https://gifts.peninsula.com/images/heart-active.svg')) {
                  $('header.header .head-links li a img[alt="wishlist"]').attr('src','https://gifts.peninsula.com/images/heart-active.svg');
               }
               /*mobile*/
               if(!$('nav#sidebar ul.user-links li a img[alt="wishlist"]').attr('src','https://gifts.peninsula.com/images/heart-active.svg')) {
                  $('nav#sidebar ul.user-links li a img[alt="wishlist"]').attr('src','https://gifts.peninsula.com/images/heart-active.svg');
               }
            };
          $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#personalise.personalise .row > div:first-child').removeClass('col-lg-10').addClass('col-lg-12');
          $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#personalise.personalise .row div.col-lg-2').addClass('d-none');
          /*var logo_img = $('.header .logo a img').attr('src');
          //console.log(logo_img);
          $('.checkout-container.complete .blue-light-bg img:not(.modified)').addClass('modified').attr('src', logo_img);*/
          $('.home-slider.bestsellers-slider .custom-container .d-md-block').removeClass('d-none');
          // $('.product-detail-container .product-holder .product-detail .product-info .product-heading .product-rating a.wishlistImage img').attr('src', 'https://gifts.peninsula.com/images/heart-active.svg');
          $('header.header .search .search-item-form .nav-search-field').attr('placeholder', 'Explore');
          $('.mobile-search-box .nav-search-field').attr('placeholder', 'Explore');
          $('.product-rating a.wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://gifts.peninsula.com/images/heart-active.svg');
          $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]').attr('src', 'https://bitbucket.org/TSTechsembly/jscss/raw/087bc2c77bcfcd91b305e9ce233ca67018b4aa03/hsh/images/edit.svg');
          $('.shipping-form-container .delivery-methods-cont a img[alt="edit"]').attr('src', 'https://bitbucket.org/TSTechsembly/jscss/raw/087bc2c77bcfcd91b305e9ce233ca67018b4aa03/hsh/images/edit.svg');
          $('.shipping-form-container .exist-address-wrapper .card a.edit-address img[alt="edit"]').attr('src', 'https://bitbucket.org/TSTechsembly/jscss/raw/087bc2c77bcfcd91b305e9ce233ca67018b4aa03/hsh/images/edit.svg');
          $('.products-container .products-holder-main .products-holder').removeClass('pt-5').addClass('pt-2');
          var host_name = window.location.host;
          var host_store = window.location.pathname.split("/")[1];
          // var host_store = host_name.substr(0, host_name.indexOf('.'));
          var hotels_link = 'https://www.peninsula.com';
          var hotels_string = 'The Peninsula Hotels';
          var store_name = '';
          var site_link = '#';
          var site_lang = '';
          var lastSegment = window.location.href.split('/').pop();
          //console.log(lastSegment);
          if (host_store == 'hongkong') {
            site_link = 'https://www.peninsula.com/en/hong-kong/5-star-luxury-hotel-kowloon';
            store_name = 'THE PENINSULA HONG KONG';
          }
          else if (host_store == 'newyork') {
            site_link = 'https://www.peninsula.com/en/new-york/5-star-luxury-hotel-midtown-nyc';
            store_name = 'THE PENINSULA NEW YORK';
          }
          else if (host_store == 'beverlyhills') {
            site_link = 'https://www.peninsula.com/en/beverly-hills/5-star-luxury-hotel-beverly-hills';
            store_name = 'THE PENINSULA BEVERLY HILLS';
          }
          else if (host_store == 'tokyo') {
            hotels_string = 'ザペニンシュラホテルズ';
            site_link = 'https://www.peninsula.com/ja/tokyo/5-star-luxury-hotel-ginza';
            store_name = 'ザ・ペニンシュラ東京';
            site_lang = '日本語';
          }
          else if (host_store == 'hshptk-en') {
            site_link = 'https://www.peninsula.com/ja/tokyo/5-star-luxury-hotel-ginza';
            store_name = 'THE PENINSULA TOKYO';
          }
          else if (host_store == 'paris') {
            hotels_string = 'Hôtels The Peninsula';
            hotels_link = 'https://www.peninsula.com/fr/default';
            site_link = 'https://www.peninsula.com/fr/paris/5-star-luxury-hotel-16th-arrondissement';
            store_name = 'THE PENINSULA PARIS';
            site_lang = 'French';
          }
          else if (host_store == 'paris-en') {
            site_link = 'https://www.peninsula.com/fr/paris/5-star-luxury-hotel-16th-arrondissement';
            store_name = 'THE PENINSULA PARIS';
          }
          else if (host_store == 'bangkok') {
            store_name = 'THE PENINSULA BANGKOK';
          }
          else if (host_store == 'chicago') {
            site_link = 'https://www.peninsula.com/en/chicago/5-star-luxury-hotel-downtown-chicago';
            store_name = 'THE PENINSULA CHICAGO';
          }
          else if (host_store == 'manila') {
            store_name = 'THE PENINSULA MANILA';
          }
          else if (host_store == 'london') {
            site_link = 'https://www.peninsula.com/en/london'
            store_name = 'THE PENINSULA LONDON';
          }
          //console.log(host_store);
          $('header.header .shipping li:first-child:not(.modified)').addClass('modified').html('Storefront');
          $('header.header .shipping li a#dropdownMenuButton:not(.active-store)').addClass('active-store').html(store_name);
          $('header.header .shipping li .dropdown .dropdown-menu').each(function(){
            if(!$(this).hasClass('cities-dd')){
              $(this).addClass('cities-dd');
              $(this).append('<div class="hotels-links-cont">'+
              '<div class="hongkong"><a class="dropdown-item hidestore" translate="" href="https://gift.peninsula.com/hongkong">Hong Kong</a></div>'+
              '<div class="tokyo"><a class="dropdown-item hidestore" translate="" href="https://gifts.peninsula.com/tokyo">Tokyo</a></div>'+
              '<div class="newyork"><a class="dropdown-item hidestore" translate="" href="https://gifts.peninsula.com/newyork">New York</a></div>'+
              '<div class="chicago"><a class="dropdown-item hidestore" translate="" href="https://gifts.peninsula.com/chicago">Chicago</a></div>'+
              '<div class="beverlyhills"><a class="dropdown-item hidestore" translate="" href="https://gifts.peninsula.com/beverlyhills">Beverly Hills</a></div>'+
              '<div class="paris"><a class="dropdown-item hidestore" translate="" href="https://gift.peninsula.com/paris">Paris</a></div>'+
              '</div>');
            }
          });
          $('#sidebar ul.list-unstyled.user-links').each(function(){
            if(!$(this).hasClass('user-mob-links-hsh')){
              $(this).addClass('user-mob-links-hsh');
              $(this).append('<li>'+
              '<div class="d-flex align-items-center">'+
              '<div class="dropdown pref_dropdown w-100">'+
              '<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownMenuLink_mob">'+store_name+'</a>'+
              '<div aria-labelledby="dropdownMenuLink" class="dropdown-menu" id="cities-dd" x-placement="top-start">'+
              '<div class="hongkong"><a class="dropdown-item hidestore" translate="" href="https://gift.peninsula.com/hongkong">Hong Kong</a></div>'+
              '<div class="tokyo"><a class="dropdown-item hidestore" translate="" href="https://gifts.peninsula.com/tokyo">Tokyo</a></div>'+
              '<div class="newyork"><a class="dropdown-item hidestore" translate="" href="https://gifts.peninsula.com/newyork">New York</a></div>'+
              '<div class="chicago"><a class="dropdown-item hidestore" translate="" href="https://gifts.peninsula.com/chicago">Chicago</a></div>'+
              '<div class="beverlyhills"><a class="dropdown-item hidestore" translate="" href="https://gifts.peninsula.com/beverlyhills">Beverly Hills</a></div>'+
              '<div class="paris"><a class="dropdown-item hidestore" translate="" href="https://gift.peninsula.com/paris">Paris</a></div>'+
              '<div><a class="dropdown-item hidestore" translate="" href="https://hshppr.techsembly.com/" style="display: none;">MANILA</a></div>'+
              '</div>'+
              '</div>'+
              '</div>'+
              '</li>');
            }
          });
          $('.trans-wrap select').each(function() {
            if(!$(this).hasClass('modified')){
              if (host_store == 'paris') {
                $(this).addClass('modified');
                $(this).find('option').html(site_lang);
              }
              if (host_store == 'tokyo') {
                $(this).addClass('modified');
                $(this).find('option').html(site_lang);
                $(this).append('<option value="en">ENGLISH</option>');
              }
              if (host_store == 'tokyo-en') {
                $(this).addClass('modified');
                $(this).append('<option value="jp">日本語</option>');
              }
            }
          });

          $('.wrapper #content').each(function(){
            if(!$(this).children('.top-header').length){
              $(this).prepend('<div class="top-header"><div class="container-fluid"><div class="row"><div class="col-lg-6 col-12 d-flex"><div class="store-name"><a href="'+hotels_link+'">'+hotels_string+'</a></div><div class="store-branch"><a href="'+site_link+'">'+store_name+'</a></div></div><div class="col-lg-6 col-12"><div class="trans-wrap"><img src="https://gifts.peninsula.com/images/world.svg"><select><option>English</option></select></div></div></div></div></div>');
            }
          });
          //$('.sidebar-widget .filter-toggler#main-menu-2').each(function(){
            if (host_store == 'hongkong') {
              /*$('#checkboxLabel4:not(.d-none)').addClass('d-none');
              $('#checkboxLabel4').next(':not(.d-none)').addClass('d-none');*/
              $('#checkboxLabel3').closest('li:not(.d-none)').addClass('d-none');
              $('#checkboxLabel4').closest('li:not(.d-none)').addClass('d-none');
            }
          //});
          $('#sidebar .BorderBottom').removeClass('BorderBottom-grey');
          $('.related-products .custom-container .container').each(function() {
            if(!$(this).hasClass('recent-container')){
              $(this).addClass('recent-container');
              $(this).prepend('<h3 class="text-center">Recently Viewed</h3>');
            }
          });
          $('.promo-holder .promo-content').each(function() {
            if(!$(this).children('.more-link').length){
              $(this).append('<a href="#" class="more-link d-flex align-items-center">Explore More <img width="26" height="26" src="https://gifts.peninsula.com/images/arrow-right.svg"></a>')
            }
          });
          $('.nav-ship-details .shipping-form-container').each(function(){
            if(!$(this).children('.purchase-note').length){
              if (host_store == 'hongkong') {
                //$(this).prepend('<div class="purchase-note pb-2 mb-1">If you are purchasing a studio 54 events ticket please fill out the shipping address as a billing address.</div>');
              }
            }
          });
          $(".select-form.form-group input[placeholder='Please add your desired delivery date']").each(function(){
              var days;
              var today = new Date();
              var enday = new Date(Date.now() + (days * 24 * 60 * 60 * 1000));
              var tdd;
              var tmm;
              var tyyyy;
              var targetDate;
              var edd = enday.getDate();
              var emm = enday.getMonth() + 1;
              var eyyyy = enday.getFullYear();

              if(!$(this).hasClass('modified')){
                  if (host_store == 'chicago-amenities' && lastSegment == 'milk-and-cookies-030937' ) {
                    $(this).addClass('modified');
                    //days = 2;
                    /*tdd = today.getDate() + 2;

                    if (tdd < 10) {
                      tdd = '0' + tdd;
                    }
                    if (tmm < 10) {
                      tmm = '0' + tmm;
                    }
                    if (edd < 10) {
                      edd = '0' + edd;
                    }
                    if (emm < 10) {
                      emm = '0' + emm;
                    }
                    today = tyyyy + '-' + tmm + '-' + tdd;*/
                    // enday = eyyyy + '-' + emm + '-' + edd;
                    //$(this).attr("min", today);
                    //$(this).attr("max", enday);
                    //$(this).attr("placeholder", "mm/dd/yyyy");
                    today.setDate(today.getDate() + 2);

                    tdd = ("0" + today.getDate()).slice(-2);
                    tmm = ("0" + (today.getMonth()+1)).slice(-2); // 0 is January, so we must add 1
                    tyyyy = today.getFullYear();

                    targetDate = tyyyy + '-' + tmm + '-' + tdd;

                    $(this).attr("min", targetDate);

                    $(this).attr("placeholder", "mm/dd/yyyy");
                  }
                  if (host_store == 'chicago-amenities' && lastSegment == 'chocolate-covered-strawberries-030964' ) {
                    $(this).addClass('modified');
                    today.setDate(today.getDate() + 1);

                    tdd = ("0" + today.getDate()).slice(-2);
                    tmm = ("0" + (today.getMonth()+1)).slice(-2); // 0 is January, so we must add 1
                    tyyyy = today.getFullYear();

                    targetDate = tyyyy + '-' + tmm + '-' + tdd;

                    $(this).attr("min", targetDate);

                    $(this).attr("placeholder", "mm/dd/yyyy");
                  }
                  if (host_store == 'chicago-amenities' && lastSegment == 'chocolate-covered-strawberries-with-bottle-of-champagne-030965' ) {
                    $(this).addClass('modified');
                    today.setDate(today.getDate() + 1);

                    tdd = ("0" + today.getDate()).slice(-2);
                    tmm = ("0" + (today.getMonth()+1)).slice(-2); // 0 is January, so we must add 1
                    tyyyy = today.getFullYear();

                    targetDate = tyyyy + '-' + tmm + '-' + tdd;

                    $(this).attr("min", targetDate);

                    $(this).attr("placeholder", "mm/dd/yyyy");
                  }
                  if (host_store == 'chicago-amenities' && lastSegment == 'celebration-cake-030976' ) {
                    $(this).addClass('modified');
                    today.setDate(today.getDate() + 3);

                    tdd = ("0" + today.getDate()).slice(-2);
                    tmm = ("0" + (today.getMonth()+1)).slice(-2); // 0 is January, so we must add 1
                    tyyyy = today.getFullYear();

                    targetDate = tyyyy + '-' + tmm + '-' + tdd;

                    $(this).attr("min", targetDate);

                    $(this).attr("placeholder", "mm/dd/yyyy");
                  }
                  if (host_store == 'chicago-amenities' && lastSegment == 'celebration-cake-and-bottle-of-champagne-030977' ) {
                    $(this).addClass('modified');
                    today.setDate(today.getDate() + 3);

                    tdd = ("0" + today.getDate()).slice(-2);
                    tmm = ("0" + (today.getMonth()+1)).slice(-2); // 0 is January, so we must add 1
                    tyyyy = today.getFullYear();

                    targetDate = tyyyy + '-' + tmm + '-' + tdd;

                    $(this).attr("min", targetDate);

                    $(this).attr("placeholder", "mm/dd/yyyy");
                  }
                  if (host_store == 'chicago-amenities' && lastSegment == 'children-s-tic-tac-toe-030978' ) {
                    $(this).addClass('modified');

                    today.setDate(today.getDate() + 3);

                    tdd = ("0" + today.getDate()).slice(-2);
                    tmm = ("0" + (today.getMonth()+1)).slice(-2); // 0 is January, so we must add 1
                    tyyyy = today.getFullYear();

                    targetDate = tyyyy + '-' + tmm + '-' + tdd;

                    $(this).attr("min", targetDate);

                    $(this).attr("placeholder", "mm/dd/yyyy");
                  }
              }
          });

          //amenities date patch
          $('.select-form.form-group input[placeholder="Select Date"]').each(function(){
            let datesForDisable = $('.delivery-text div').html();
            let daysToDisable = $('.detail-holder div').html();
            if(!$(this).hasClass('date-appeared')){
              $(this).addClass('date-appeared');
              $(this).prop("readonly", true);
              $(this).datepicker({
                  minDate: daysToDisable,
                  format: 'dd-mm-yyyy',
                  autoclose: true,
                  startDate:daysToDisable,
                  todayHighlight: true,
                  datesDisabled: datesForDisable

                  }).on("changeDate", function (e) {
                  $(this)[0].dispatchEvent(new Event("input", { bubbles: true }));
              });
            }
          });
          //end amenities date patch
          $(".select-form.form-group input[placeholder='Date of birth']").each(function(){
            if(!$(this).hasClass('dob-appeared')){
              $(this).addClass('dob-appeared');
              $(this).removeAttr('min');
              let currDate = new Date();
              let twentyYearsAgo = new Date();
              twentyYearsAgo.setFullYear(currDate.getFullYear() - 18);

              // Format the date as yyyy-mm-dd
              let formattedDate = `${twentyYearsAgo.getFullYear()}-${(twentyYearsAgo.getMonth() + 1).toString().padStart(2, '0')}-${twentyYearsAgo.getDate().toString().padStart(2, '0')}`;

              //$(this).attr('max', formattedDate);
              let alert_msg;
              if (host_store == 'tokyo-en'){
                alert_msg = 'The consumption of alcohol by individuals under the age of 20 is prohibited by law. Alcohol sales are not permitted to individuals under the age of 20.';
              }
              else if (host_store == 'tokyo') {
                alert_msg = '20歳未満の者の飲酒は法律で禁止されています。 20歳未満の者に対しては酒類を販売しません。';
              }

              $(this).on("change", function (e) {
                $(this)[0].dispatchEvent(new Event("input", { bubbles: true }));
                const selectedDate = new Date(e.target.value);
                const currentDate = new Date();
                const age = currentDate.getFullYear() - selectedDate.getFullYear();

                const birthMonth = selectedDate.getMonth();
                const currentMonth = currentDate.getMonth();

                const birthDay = selectedDate.getDate();
                const currentDay = currentDate.getDate();

                /*if ((birthMonth < currentMonth) || (birthMonth === currentMonth && birthDay <= currentDay)) {
                  // If the birth date has already occurred this year or is upcoming
                  console.log("InValid age:", age);
                } else {
                  age--; // Adjust age if birth date is yet to occur this year
                  console.log("Valid age:", age);
                }*/
                if (host_store == 'tokyo-en' || host_store == 'tokyo'){
                  if (age >= 20) {
                    // Age is valid, proceed
                    console.log("Valid age:", age);
                    $('.personalise-form .order-btn').removeAttr('disabled');
                  } else {
                    // Age is less than 20, show an error message or take appropriate action
                    console.log("Invalid age:", age);
                    alert(alert_msg);
                    $('.personalise-form .order-btn').attr('disabled','disabled');
                  }
                }
              });
            }
          });




          $('.shipping-form-container h1.breadcrumb-heading:contains("Shipping Details")').each(function() {
            if(!$(this).closest('shipping-form-container').hasClass('modified')){
              $(this).closest('shipping-form-container').addClass('modified');
              $(this).html('Choose Shipping Details');
            }
          });
          if (host_store == 'hongkong') {
            $('.shipping-form-container h1.breadcrumb-heading:contains("Shipping Details")').each(function() {
            if(!$(this).closest('shipping-form-container').hasClass('modified')){
              $(this).closest('shipping-form-container').addClass('modified');
              $(this).html('Choose Delivery Details');
              }
            });
            $('.delivery-methods-cont .card .card-body .row-head:contains("Ship to")').each(function() {
            if(!$(this).closest('card-row').hasClass('modified')){
              $(this).closest('card-row').addClass('modified');
              $(this).html('Deliver to');
            }
            });
            $('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc:contains("Shipping")').each(function() {
              if(!$(this).closest('sub-total-item').hasClass('modified')){
                $(this).closest('sub-total-item').addClass('modified');
                $(this).html('Delivery');
              }
            });
          }
          $('body.auth-user .user-container .ts-balance-form').closest('.user-container:not(.d-none)').addClass('d-none');

          $('.footer a:contains("+")').each(function(){
            var string_phone= $(this).html();
            var result_phone;
            if(string_phone.indexOf(":")>=0){
              result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
            }
            else {
              result_phone = string_phone;
            }
            var phone_no = "tel:" + result_phone;
            if(!$(this).hasClass('tel-link')){
              $(this).addClass('tel-link');
              $(this).attr('href', phone_no);
            }
          });
          $('.footer a:contains("@peninsula.com")').each(function(){
            var string_email= $(this).html();
            var result_email;
            if(string_email.indexOf(":")>=0){
              result_email = string_email.substring(string_email.indexOf(':') + 1);
            }
            else {
              result_email = string_email;
            }
            var email_link = "mailto:" + result_email;
            if(!$(this).hasClass('email-link')){
              $(this).addClass('email-link');
              $(this).attr('href', email_link);
            }
          });
          var year = new Date().getFullYear();
          $('.footer .copyright-cont .powered-by .powered-link-cont span:last-child:not(.d-none)').addClass('d-none');
          $('.footer .copyright-cont .powered-by .powered-link-cont span:not(.modified)').addClass('modified').html("&#169; "+year+" The Peninsula Hotels.  All Rights Reserved.");
          $('.footer .footer-widget').each(function() {
            if(!$(this).closest('div.col-lg-12').hasClass('add-widget')){
                $(this).closest('div.col-lg-12').addClass('add-widget');
              }
          });
        }, 1000);
      });
    });
})();
