(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
            $('head').append('<link rel="preconnect" href="https://fonts.gstatic.com"><link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">');
            $('head').append('<link rel="preconnect" href="https://fonts.gstatic.com"><link href="https://fonts.googleapis.com/css2?family=Crimson+Text:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&display=swap" rel="stylesheet">');
            $('head').append('<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">');
            //$('header.header').prepend('<div class="container-fluid top-header py-1"><div class="row spacing align-items-center justify-content-center flex-wrap text-center py-sm-1">Free Shipping Across All Orders in Singapore!</div></div>');
            setTimeout(function () {
              $('header.header .search .search-item-form .nav-search-field').attr('placeholder','');
              $('.mobile-search-box .nav-search-field').attr('placeholder','');
              //$('.home-banner .banner-heading').prepend('<div class="banner-title text-uppercase">Personalised </div>');
              //$('.home-banner .banner-heading').append('<div class="sub-head">Beautiful, Unique and Personal Gifts for your little ones.</div>');
              //$('.gift-section .gifts-content').prepend('<div class="gifts-title text-uppercase">Personalize your gifts</div>');
              $('.sign-up-section .sign-up-container .sign-up-form input[type="email"]').attr('placeholder','');
              $('.footer .copyright-cont .pay-methods-avail').append('<div class="meth-link new"><img alt="apple-pay" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/apple_pay-f6db0077dc7c325b436ecbdcf254239100b35b70b1663bc7523d7c424901fa09.svg" width="55"></div>'+
              '<div class="meth-link new"><img alt="master-card" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/master-173035bc8124581983d4efa50cf8626e8553c2b311353fbf67485f9c1a2b88d1.svg" width="55"></div>'+
              '<div class="meth-link new"><img alt="paypal" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/paypal-49e4c1e03244b6d2de0d270ca0d22dd15da6e92cc7266e93eb43762df5aa355d.svg" width="55"></div>'+
              '<div class="meth-link new"><img alt="visa" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/visa-319d545c6fd255c9aad5eeaad21fd6f7f7b4fdbdb1a35ce83b89cca12a187f00.svg" width="55"></div>');
               $('<div class="d-flex align-items-center pt-2"><a class="footer-cp-link" href="https://techsembly.com/" target="_blank"><img class="copy-right-img" src="https://media.graphcms.com/gwNQKBJfTHCU2KGOiUCH", width="160"/></a></div>').insertAfter($(".powered-link-cont"));
            }, 2000);
            setInterval(function () {
                if(!$('header .top-header').closest('body').hasClass('top-head-exist')){
                    $('header .top-header').closest('body').addClass('top-head-exist');
                }
                if(!$('header.header .logo').parent('div').hasClass('modified')){
                    $('header.header .logo').parent('div').addClass('modified').removeClass('col-md-4');
                }
                $('.home-intro h2').each(function(){
                    if(!$(this).hasClass('store-title')){
                        $(this).addClass('store-title');
                        $(this).html('OUR STORY');
                    }
                });
                $('#mailchip-email').closest('div:not(.mail-cont)').addClass('mail-cont flex-row').removeClass('flex-column flex-md-row');
                $('.footer .copyright-cont .pay-methods-avail ').each(function(){
                    if(!$(this).hasClass('modified')){
                        $(this).addClass('modified');
                        $(this).find('.meth-link:not(.new)').addClass('d-none');
                    }
                });
                if(!$('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').hasClass('social-widget')){
                    $('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').addClass('social-widget');
                    $('.social-widget').append('<a class="btn btn-primary btn-contact d-none d-lg-flex justify-content-center align-items-center mt-1" href="/contact">CONTACT</a>')
                }
            });
        });
    });
})();
