function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }
    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });
    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

function addRelatedProductHead() {
  var heading = '<h1 class="productHeading">More Like This</h1>';
  var relProdCont = $('.related-products .custom-container .container')[0];
  relProdCont.insertAdjacentHTML('afterbegin', heading);
}

function addLoginCartNote() {
  var loginCartNote = "<div class='login-note'>Please note this login is for Siam Hotel Thailand gifting platform only.<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}

function addGuestCheckoutNote() {
  var checkoutNote = "<div class='checkout-note text-center w-100 pt-4'>You can create account after checkout<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}

function addcopyrightText() {
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="container-fluid"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-12 col-lg-6 tc-rights-reserved pl-0">Techsembly© '+copyrightYear+'</div></div></div>';
  var elem = $(".checkout-footer")[0];
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}

function addBackToHomeBtn() {
  var copyrightHtml = '<div class="d-flex justify-content-center items-center backToHomeBtn"><a href="https://the-siam-hotel-resort.techsembly.com">BACK TO HOME</a></div>'
  var elem = $(".order-confirm-wrapper .card-footer")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}
function addcopyrightToComplete() {
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="card-footer-right"><a href="#">© '+copyrightYear+' The Siam Hotel</a></div>'
  var elem = $(".order-confirm-wrapper .card-footer .col-12")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}
(function () {
  var script = document.createElement("SCRIPT");
  script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
  script.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script);

  // document.head.appendChild(newStyle);
  // Poll for jQuery to come into existence
  var checkReady = function (callback) {
    if (window.jQuery) {
      callback(jQuery);
    } else {
      window.setTimeout(function () {
        checkReady(callback);
      }, 20);
    }
  };
  checkReady(function ($) {
    $(function () {

      waitForElm('.related-products .custom-container .container').then((elm) => {
        addRelatedProductHead();
      }).catch((error) => {});

      waitForElm('.cart-right-cont').then((elm) => {
        addLoginCartNote();
        addGuestCheckoutNote();
      }).catch((error) => {});
      waitForElm('app-footer-checkout').then((elm) => {
        addcopyrightText()
      }).catch((error) => {});
      waitForElm('.order-confirm-wrapper .card-footer').then((elm) => {
        addBackToHomeBtn()
      }).catch((error) => {});
      waitForElm('.order-confirm-wrapper .card-footer .col-12').then((elm) => {
        addcopyrightToComplete()
      }).catch((error) => {});

      $("app-product-listing .mob-btn-group").parent().addClass('product-listing-head');
      setTimeout(function () {
        $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');

      }, 2000);
      setInterval(function () {

        $(".head-links #profile-icon img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/N1a9pWdoY3F15JDbLG9yBz1H");
        $(".head-links #wishlist-heart img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/baowjEJb46uwHtQWkz9bvGvQ");
        $(".head-links #cart-bag img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/D1Ea4qHGMKpyqxp5NtmrmBMM");

        // add to wishlist icon working and image change
        $('.product-detail-container .wishlistImage:has(img[alt="add-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
          $('.product-detail-container .wishlistImage img').attr('src', 'https://static.techsembly.com/CjTTaTZoFsDJ8eZPgocLfp6H');
        });

        $('.product-detail-container .wishlistImage:has(img[alt="added-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
          $('.product-detail-container .wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://static.techsembly.com/NKgd1i56tfzkNF2BYnXNQYPw');
        });


        $('.products-container .products-holder-main .products-holder .product-column .product-list .product-content .product-rating a.wishlistImage img').each(function(){
          if($(this).attr('src') != 'https://static.techsembly.com/CjTTaTZoFsDJ8eZPgocLfp6H') {
            $(this).attr('src','https://static.techsembly.com/CjTTaTZoFsDJ8eZPgocLfp6H');
          }
          if(($(this).attr('alt')=='added-to-wishlist') && ($(this).attr('src') != 'https://static.techsembly.com/NKgd1i56tfzkNF2BYnXNQYPw')) {
            $(this).attr('src','https://static.techsembly.com/NKgd1i56tfzkNF2BYnXNQYPw');
          }
        });


        // $('.product-detail-container .product-detail .wishlistImage img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/CjTTaTZoFsDJ8eZPgocLfp6H");
        $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
        $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');
        $('.custom-container.cart-container .card.vendor-items-holder .card-body .edit-item-new img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/YxPJAMPgqxXvH5fRahZcRoS9");
        $('.custom-container.checkout .shipping-form-container .card-header .edit-address img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/YxPJAMPgqxXvH5fRahZcRoS9");
        $('.delivery-methods-cont .contact-card .card-body .card-row .edit-contact img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/YxPJAMPgqxXvH5fRahZcRoS9");
        $('.delivery-methods-cont .contact-card .card-body .card-row .edit-address img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/YxPJAMPgqxXvH5fRahZcRoS9");
        // $('.checkout .password-note:not(.modified)').addClass('modified').html('Must be at least 6 characters including one special character');
        $('.checkout-container.complete .order-confirm-top img:not(.modified)').addClass('modified completeLogo').attr('src', 'https://static.techsembly.com/v245ngRjM7vNxBuUESsXq7NY').attr('width', '125');
         $("header.header .logo").closest(".col-4:not(.modified)").addClass('modified mobile-logo-cont');
        $('.custom-container.cart-container').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'cart')) {
            $(this).closest('body').attr('data-pagetype', 'cart');
          }
        });
        hrefurl=$(location).attr("href");
        last_part=hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
        currentUrlSecondPart = window.location.pathname.split('/')[1];
        if (currentUrlSecondPart == 'catalogsearch'){
         if (!$('body').attr('data-pagetype','search_results')){
           $('body').attr('data-pagetype','search_results');
         }
        }
        $('.shipping-form-container .form-control#last-name').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).closest('.col-md-6').addClass('last-name-cont');
          }
        });
        $('.products-container .products-holder-main .products-holder .product-column').each(function () {
          if ($(this).closest('.products-holder').hasClass('pt-5')) {
            $(this).closest('.products-holder').removeClass('pt-5').addClass('pt-2');
          }
        });
        $('.product-detail-container .personalise#personalise>div.row').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified productDesc');
          }
        });
        $('.home-banner h1.banner-heading div:contains("SIAM SUITE")').each(function () {
          if (!$(this).closest('.home-banner').hasClass('siam-suit-banner')) {
            $(this).closest('.home-banner').addClass('siam-suit-banner');
          }
        });

        $('.shipping-form-container .breadcrumb-heading:contains("Billing Details")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $('.shipping-form-container .address-heading').addClass('modified chngedHeading').html('Or Add New Billing Details');
          }
        });

        $('.home-slider .custom-container h2.section-title:contains("THE SIAM EXPERIENCES")').each(function () {
          if (!$(this).closest('.home-slider').hasClass('experience-slider')) {
            $(this).closest('.home-slider').addClass('experience-slider');
          }
        });
        $('.home-banner h1.banner-heading div:contains("RIVERSIDE INFINITY POOL")').each(function () {
          if (!$(this).closest('.home-banner').hasClass('pool-banner')) {
            $(this).closest('.home-banner').addClass('pool-banner');
          }
        });

        $('.product-detail-container .personalise-form .form-group .order-btn').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('BUY NOW');
          }
        });
        $('.footer .footer-widget .widget-content .social-links a img[alt="twitter"]').attr('src', 'https://static.techsembly.com/RjdNFDJxZh4t2xRMXtaEpmvd');
        $('.footer .footer-widget .widget-content .social-links a img[alt="fb"]').attr('src', 'https://static.techsembly.com/Jm7GqQeAB11HKTjQu2Cq3HNa');
        $('.footer .footer-widget .widget-content .social-links a img[alt="instagram"]').attr('src', 'https://static.techsembly.com/cPjF8V25fZR32gvbYyNUnAhX');
        $('.footer .footer-widget .widget-content .social-links').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).append('<li><a href="https://www.youtube.com/channel/UCBBT1M5snzIVFUf56e-XZvg"><img alt="youtube" width="20" src="https://static.techsembly.com/ErCi1fkN42BB55qb1SMyXvQa"></a></li>');
          }
        });
        $('.footer .footer-widget .widget-title:contains("KEEP IN TOUCH")').each(function () {
          if (!$(this).closest('.footer-widget').hasClass('text-right')) {
            $(this).closest('.footer-widget').addClass('text-right');
            $('.footer-widget.text-right .widget-title').html("Follow us");
          }
        });
        $('.footer .footer-widget .widget-content ul li a:contains("EXPLORE OUR WORLD")').each(function () {
          if (!$(this).closest('.footer-widget').hasClass('widget-col-1')) {
            $(this).closest('.footer-widget').addClass('widget-col-1');
          }
        });
        $('.footer .footer-widget .widget-content ul li a:contains("INFORMATION")').each(function () {
          if (!$(this).closest('.footer-widget').hasClass('widget-col-2')) {
            $(this).closest('.footer-widget').addClass('widget-col-2');
          }
        });
        $('.custom-container.cart-container .btn.continue-btn-new').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).closest(".col-12").addClass('modified pl-md-0');
            $(this).html('Return to shopping');
          }
        });
        $('.custom-container.cart-container .order-summary-cont .btn.btn-primary:contains("Proceed to Checkout")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Continue Checkout');
          }
        });
         $('.payment-form-container .checkbox-cont .checkbox-note:contains("Same as delivery address")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Same as shipping address');
          }
        });
         $('.shipping-form-container .order-shipping-container .delivery-dropdowns-cont .form-group').each(function () {
          if ($(this).closest(".col-12").hasClass('px-md-3')) {
            $(this).closest(".col-12").removeClass('px-md-3');
            $(this).closest(".col-12").addClass('modified pr-md-3');
          }
        });
        // $('.custom-container.checkout .btn.btn-submit').each(function () {
        //   if (!$(this).hasClass('modified')) {
        //     $(this).addClass('modified');
        //     $(this).html('Add & Continue Checkout');
        //   }
        // });

        $('.custom-container.cart-container').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'cart')) {
            $(this).closest('body').attr('data-pagetype', 'cart');
          }
        });
        $('.custom-container.checkout').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'checkout')) {
            $(this).closest('body').attr('data-pagetype', 'checkout');
          }
        });
        $('.checkout-container.complete .section-cont.customer-note').each(function () {
          if (!$(this).hasClass('total-delivery')) {
            $(this).addClass('total-delivery');
            var newTxt = 'Your order is being confirmed and we will be shipping it soon';
            $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function (_, html) {
              return html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">' + newTxt + '</div>')
            });
          }
        });

        $('.checkout-container.complete .msg-cont .msg-inner').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            var newTxt = 'The Siam Hotel Team';
            $(".checkout-container.complete .msg-cont .msg-inner:contains('We’ll send you shipping confirmation when your item(s) are on the way! We appreciate your business, and hope you enjoy your purchase.')").html(function (_, html) {
              return html.replace(/(The Siam)/g,newTxt)
            });
          }
        });
      })
    })
  })
})();
