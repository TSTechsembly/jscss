function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }
        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });
        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
function addLoginCartNote(){
  var loginCartNote = "<div class='login-note'>Please note this login is for Patina Maldives, Fari Islands gifting platform only.<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}
function addLoginCartDesc(){
  var loginCartNote = "<div class='login-note'>If you do not have an account please use guest checkout. You will be given the option to save account information later in the checkout process<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}
function addcopyrightText(){
  var copyrightHtml = '<div class="container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-6 ts-copyright"><span><a href="https://techsembly.com/" target="_blank">Techsembly&copy;</a></span><span class="ml-1">2022</span></div><div class="col-6 tc-rights-reserved text-right pr-0"></div></div></div></div>'
  var elem = $(".checkout-footer")[0]
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}
function addGuestCheckoutNote(){
  var checkoutNote = "<div class='checkout-note text-center w-100'>You can create account after checkout<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}
function addcompleteText(){
  var addcompleteText = '<div class="card-footer-right"><span>&copy; 2022 Capella Bangkok</span></div>'
  var textComplete = $(".checkout-container.complete .card-footer.blue-light-bg .col-12")[0]
  textComplete.insertAdjacentHTML('beforeend', addcompleteText)
}
function addRelatedProductHead() {
  var heading = '<h1 class="productHeading">More Like This</h1>';
  var relProdCont = $('.related-products .custom-container .container')[0];
  relProdCont.insertAdjacentHTML('afterbegin', heading);
}
function addBackToHomeBtn() {
  var copyrightHtml = '<div class="d-flex justify-content-center items-center backToHomeBtn"><a href="https://patina-maldives.techsembly.com/">BACK TO HOME</a></div>'
  var elem = $(".order-confirm-wrapper .card-footer")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}
function addcopyrightToComplete() {
  var copyrightHtml = '<div class="card-footer-right"><a href="#">© 2022 Pitana Maldives</a></div>'
  var elem = $(".order-confirm-wrapper .card-footer .col-12")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}

(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);
    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
          font-family: 'Lato';\
          src: url('https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap') format('woff');\
      }\
      @font-face {\
        font-family: 'cormorantregular';\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7e19db5aff6156fc597b0a8eff57cd9d1117fdc4/she-her/fonts/cormorant/cormorant-regular-webfont.woff2') format('woff2');\
      }\
    "));
    document.head.appendChild(newStyle);
    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };
    checkReady(function($) {
      $(function() {
        $(window).on('load', function(){
          setTimeout(function () {

          }, 2000);
        });
        waitForElm('app-footer-checkout').then((elm) => {
          addcopyrightText()
        }).catch((error) => {});
        waitForElm('.related-products .custom-container .container').then((elm) => {
          addRelatedProductHead();
        }).catch((error) => {});
        waitForElm('.cart-right-cont').then((elm) => {
          addLoginCartNote();
          addGuestCheckoutNote();
        }).catch((error) => {});
        waitForElm('.order-confirm-wrapper .card-footer').then((elm) => {
          addBackToHomeBtn()
        }).catch((error) => { });
        waitForElm('.order-confirm-wrapper .card-footer .col-12').then((elm) => {
          addcopyrightToComplete()
        }).catch((error) => { });

        setTimeout(function () {
          $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
          hrefurl=$(location).attr("href");
          last_part=hrefurl.substr(hrefurl.lastIndexOf('#') + 1);
          //console.log(last_part);
          if (last_part == 'details'){
             $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#personalize-tab').removeClass('active');
             $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#personalize-tab').removeClass('show');
             $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#personalise').removeClass('active');
             $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#personalise').removeClass('show');
             $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#details-tab').addClass('active');
             $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#details').addClass('active');
             $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#details').addClass('show');
             $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#details').addClass('in');
          };

        }, 2000);
        setInterval(function() {
          $('header .mob-top-links li a img[alt="cart"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/9EJKCBNCPKrZMtkFjwDRFvMQ');
          $('header .mob-top-links li a img[alt="search image"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/tQDL2ZhkncY7pxJAypvmuZug');
          $(".head-links .profile-icon img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/wbPXLUYEXKwmFD65rqhdZ83x");
	        $(".head-links .wishlist-heart img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/syLetY21dabE7bDMy1pBoxRp");
	        $(".head-links .cart-bag img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/9EJKCBNCPKrZMtkFjwDRFvMQ");

          $('.nav-search-field:not(.modified)').addClass('modified').attr("placeholder", "");
          $('header.header .menu-holder>li>div').each(function(){
            if(!$(this).children().length){
              $(this).addClass('empty');
            }
          });

          $('.products-container .products-holder-main').removeClass('pt-lg-2');
          $('.products-container .products-holder-main .products-holder').removeClass('pt-5');
          $('.products-holder-main .products-holder .product-column .product-list a.wishlistImage img[alt="add-to-wishlist"]').attr('src','https://static.techsembly.com/syLetY21dabE7bDMy1pBoxRp');
          $('.products-holder-main .products-holder .product-column .product-list a.wishlistImage img[alt="added-to-wishlist"]').attr('src','https://static.techsembly.com/ixYEzYnojmBc3WcArN5qvdY2');
          $('.product-detail-container .product-holder .product-detail .product-info .product-heading .product-rating a.wishlistImage img[alt="add-to-wishlist"]').attr('src','https://static.techsembly.com/syLetY21dabE7bDMy1pBoxRp');
          $('.product-detail-container .product-holder .product-detail .product-info .product-heading .product-rating a.wishlistImage img[alt="added-to-wishlist"]').attr('src','https://static.techsembly.com/ixYEzYnojmBc3WcArN5qvdY2');
          $('.wishlist-container .products-container .products-holder .product-column .product-list .product-content .product-rating .wishlistImage img[alt="added-to-wishlist"]').attr('src','https://static.techsembly.com/ixYEzYnojmBc3WcArN5qvdY2');

          $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/uPjNo3rYoN3JJt36hPiFPuUn');
          $('.delivery-methods-cont .card .card-body .text-right a img[alt="edit"]:not(.edit-img)').addClass('edit-img').attr('src', 'https://static.techsembly.com/uPjNo3rYoN3JJt36hPiFPuUn');
          $('.custom-container.checkout a img[alt="edit"]:not(.edit-img)').addClass('edit-img').attr('src', 'https://static.techsembly.com/uPjNo3rYoN3JJt36hPiFPuUn');

          $('.nav-ship-details .breadcrumb-heading:not(.modified)').addClass('modified').html('Billing Details');
          $('.shipping-form-container .breadcrumb-heading:not(.modified)').addClass('modified').html('Select shipment Method');
          $('.custom-container.cart-container .vendor-items-cont a.edit-item-new img[alt="edit"]').attr('src','https://static.techsembly.com/uPjNo3rYoN3JJt36hPiFPuUn');
          $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
          $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');

          $('.auth-user .checkout .shipping-form h1.breadcrumb-heading:contains("Billing Details")').each(function() {
            if(!$(this).closest('.shipping-form').hasClass('shipp-details')){
              $(this).closest('.shipping-form').addClass('shipp-details');
              $(this).html('Choose Billing Details');
            }
          });
          $('.nav-checkout-delivery .shipping-form-container .form-group .btn.btn-submit:contains("Continue Checkout")').each(function() {
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Continue to payment');
            }
          });
          $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
            if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('buy now');
            }
          });
          $('.custom-container.cart-container .vendor-items-cont .card .card-header h2.vendor-heading .delivered-by').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Fulfilled by')
            }
          });
          $('.custom-container.checkout .delivery-methods-cont .vendor-cards-holder .card .card-header .vendor-title span:contains("Delivered by")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified').removeClass('mr-1');
              $(this).html('Fulfilled by')
            }
          });

          $('.custom-container.cart-container a.btn.continue-btn-new').each(function() {
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Return to shopping');
            }
          });
          $('app-product-detail app-related-products[title="More items from"]').each(function(){
            if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).find('h3').html('More Like This');
            }
          });

          $('.footer .footer-widget .widget-title:contains("KEEP IN TOUCH")').each(function() {
            if(!$(this).closest('.footer-widget').hasClass('uppercase')){
              $(this).closest('.footer-widget').addClass('uppercase');
              $('.footer-widget.uppercase .widget-title').html("Follow Us");
            }
          });

          $('.footer .footer-widget .widget-content ul li a').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              $(this).attr('target', '_blank');
            }
          });
          $('.footer .mobile-footer .footer-widget ul li a').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              $(this).attr('target', '_blank');
            }
          });
          // $('.checkout-container.complete .section-cont.customer-note').each(function(){
          //   if(!$(this).hasClass('total-delivery-modified')){
          //     $(this).addClass('total-delivery-modified');
          //     var firstPart = $(this).find('br')[0].previousSibling.nodeValue;

          //     var newTxt = 'Your order is being confirmed and we will be shipping it soon';
          //     $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function(_, html) {
          //      return  html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">'+newTxt+'</div>')
          //     });

          //     var secondPart = $(this).find('.del-content').html();

          //     $(this).html('<div class="salutation-cont d-none">'+firstPart+'</div><div class="del-content pt-0">'+secondPart+'</div>');
          //   }
          // });

          $('.social-links img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/WgR2bZ3ZY2JTkR3GydD8Lvgn');
          $('.social-links img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/gHyZehrHwmpDiovL6GdMZQ1v');
          $('.social-links img[alt="linkedin"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/nsmZMaVqNWcSYNaAkdrvZG7e');
          $('.social-links img[alt="pinterest"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/2q4UEpdxCPEr3orRr5AgjE1L');
          $('.social-links img[alt="fb"]').closest('li:not(.fb-link-cont)').addClass('fb-link-cont');
          // $('.social-links img[alt="instagram"]').closest('li:not(.insta-link-cont)').addClass('insta-link-cont').insertBefore($('.social-links .fb-link-cont'));
          // $('.social-link .insta-link-cont:not(:first-child)').remove();
        })
      })
    })
})();
