function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }
        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });
        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
function addLoginCartNote(){
  var loginCartNote = "<div class='login-note'>Please note this login is for Capella Ubud gift card platform<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}
function addGuestCheckoutNote(){
  var checkoutNote = "<div class='checkout-note text-center w-100'>If you do not have an account please use guest checkout. You will be given the option to save account information later in the checkout process<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}
function addPaymentNote(){
  var paymentNote = "<div class='payment-note w-100 pb-3'>Please note that payments above IDR Rp 9,999,999.99 are not supported by American Express<div>";
  var elemPayMethodCont = $('.custom-container.checkout .payment-form-container .payments-methods-cont')[0];
  elemPayMethodCont.insertAdjacentHTML('beforeend', paymentNote);
}
function addcopyrightText(){
  var copyrightHtml = '<div class="container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-6 ts-copyright"><span><a href="https://techsembly.com/" target="_blank">Techsembly&copy;</a></span><span class="ml-1">2022</span></div><div class="col-6 tc-rights-reserved text-right pr-0"></div></div></div></div>'
  var elem = $(".checkout-footer")[0]
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}
function addcompleteText(){
  var addcompleteText = '<div class="card-footer-right"><span>&copy; 2022 Capella Ubud</span></div>'
  var textComplete = $(".checkout-container.complete .card-footer.blue-light-bg .col-12")[0]
  textComplete.insertAdjacentHTML('beforeend', addcompleteText)
}
(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);
    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
          font-family: 'Calibre-Regular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/0248a0a0a1f01eacb7f4e564bdbf5c1b1ad9ed9e/capella/fonts/calibre/calibre-regular.woff') format('woff');\
          font-weight: 400;\
          font-style: normal;\
      }\
      @font-face {\
          font-family: 'Calibre-Medium';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/0248a0a0a1f01eacb7f4e564bdbf5c1b1ad9ed9e/capella/fonts/calibre/calibre-medium.woff') format('woff');\
          font-weight: 500;\
          font-style: normal;\
      }\
      @font-face {\
          font-family: 'Calibre-Light';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/0248a0a0a1f01eacb7f4e564bdbf5c1b1ad9ed9e/capella/fonts/calibre/calibre-light.woff') format('woff');\
          font-weight: 300;\
          font-style: normal;\
      }\
      @font-face {\
          font-family: 'Goudy Regular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/8e6bcc5fda15f6aac1b7420ae6755823c4ba0a5e/capella/fonts/goudy-regular/36B697_1_0.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/8e6bcc5fda15f6aac1b7420ae6755823c4ba0a5e/capella/fonts/goudy-regular/36B697_1_0.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/8ca4741bcb6f81a45e4694689dec65c6292ce05b/capella/fonts/goudy-regular/36B697_1_0.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/8ca4741bcb6f81a45e4694689dec65c6292ce05b/capella/fonts/goudy-regular/36B697_1_0.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/8ca4741bcb6f81a45e4694689dec65c6292ce05b/capella/fonts/goudy-regular/36B697_1_0.woff2') format('woff2');\
          font-weight: 400;\
      }\
      @font-face {\
          font-family: 'Goudy Light';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/09f93dd9d580eccf519654691f087d28c4793ce4/capella/fonts/goudy-light/36B697_0_0.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/09f93dd9d580eccf519654691f087d28c4793ce4/capella/fonts/goudy-light/36B697_0_0.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/04325497a631886b9e8057843aade47f859451b3/capella/fonts/goudy-light/36B697_0_0.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/09f93dd9d580eccf519654691f087d28c4793ce4/capella/fonts/goudy-light/36B697_0_0.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/09f93dd9d580eccf519654691f087d28c4793ce4/capella/fonts/goudy-light/36B697_0_0.woff2') format('woff2');\
          font-weight: 300;\
      }\
    "));
    document.head.appendChild(newStyle);
    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };
    checkReady(function($) {
      $(function() {
        waitForElm('app-footer-checkout').then((elm) => {
          addcopyrightText()
        }).catch((error) => {});
        waitForElm('.cart-right-cont').then((elm) => {
          addLoginCartNote();
          addGuestCheckoutNote();
        }).catch((error) => {});
        waitForElm('.payment-form-container .payments-methods-cont').then((elm) => {
          addPaymentNote();
        }).catch((error) => {});
        waitForElm('.checkout-container.complete').then((elm) => {
          addcompleteText();
        }).catch((error) => {});
        $(window).on('load', function(){
          setTimeout(function () {
            var attr = $('body').attr('data-pagetype');

            // For some browsers, `attr` is undefined; for others,
            // `attr` is false.  Check for both.
            if (typeof attr !== 'undefined' && attr !== false) {
              //console.log(attr);
            }
            if(attr === 'product' && $('header .head-links li .cart-qty').length){
              $(document).scrollTop( $("#cart-bag").offset().top );
              console.log('items added to cart');
            }
          }, 2000);
        });
        setTimeout(function () {
          $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
          hrefurl=$(location).attr("href");
          last_part=hrefurl.substr(hrefurl.lastIndexOf('#') + 1);
          //console.log(last_part);
          if (last_part == 'details'){
             $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#personalize-tab').removeClass('active');
             $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#personalize-tab').removeClass('show');
             $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#personalise').removeClass('active');
             $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#personalise').removeClass('show');
             $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#details-tab').addClass('active');
             $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#details').addClass('active');
             $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#details').addClass('show');
             $('.product-detail-container .product-holder .product-order .tab-content .tab-pane#details').addClass('in');
          };
        }, 2000);
        setInterval(function() {
          /*$('.product-detail-container .product-detail .product-info .product-heading h1:contains("x 10 VOUCHERS")').each(function(){
            if(!$(this).closest('.product-holder').hasClass('tenx-pro')){
              $(this).closest('.product-holder').addClass('tenx-pro');
              $('.product-detail-container .tenx-pro .personalise-form .order-input').val('10');
              $('.product-detail-container .tenx-pro .personalise-form .order-input').removeClass('ng-untouched');
              $('.product-detail-container .tenx-pro .personalise-form .order-input').addClass('ng-touched');
              $('.product-detail-container .tenx-pro .personalise-form .order-input').addClass('ng-dirty');
            }
          });
          $('.custom-container.cart-container .cart-item.new .item-details .item-name:contains("x 10 VOUCHERS")').each(function(){
            if(!$(this).closest('.cart-item').hasClass('tenx-pro')){
              $(this).closest('.cart-item').addClass('tenx-pro');
            }
          });*/
          $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
          $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');
          $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
            if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('add to cart');
            }
          });
          $('app-product-detail app-related-products[title="More items from"]').each(function(){
            if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).find('h3').html('Other Gift Ideas');
            }
          });
          $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/uPjNo3rYoN3JJt36hPiFPuUn');
          $('.custom-container.cart-container .vendor-items-cont a img[alt="gift"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/vg23BokHkdx4WrwPDtVBsAGu');
          $('.delivery-methods-cont .card .card-body .text-right a img[alt="edit"]:not(.edit-img)').addClass('edit-img').attr('src', 'https://static.techsembly.com/uPjNo3rYoN3JJt36hPiFPuUn');
          $('.custom-container.checkout a img[alt="edit"]:not(.edit-img)').addClass('edit-img').attr('src', 'https://static.techsembly.com/uPjNo3rYoN3JJt36hPiFPuUn');
          $('.custom-container.cart-container').each(function(){
            if (!$(this).closest('body').attr('data-pagetype','cart')){
              $(this).closest('body').attr('data-pagetype','cart');
            }
          });
          $('.custom-container.cart-container .cart-item.new .pro-badges-cont .pro-badge span:contains("GiveX Product")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Digital Product');
            }
          });

          $('.custom-container.cart-container .card .cart-login-cont #guest-btn span:contains("Continue to Shipping")').each(function() {
            if(!$(this).hasClass('guest-login')){
              $(this).addClass('guest-login');
              $(this).html('Continue to Checkout');
            }
          });
          $('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc:contains("Shipping")').each(function() {
            if(!$(this).hasClass('total-delivery')){
              $(this).addClass('total-delivery');
              $(this).html('Delivery');
            }
          });
          $('.custom-container.checkout .delivery-methods-cont .card .card-body .shipping-totals-cont .total-caption:contains("Shipping:")').each(function() {
            if(!$(this).hasClass('total-delivery')){
              $(this).addClass('total-delivery');
              $(this).html('Delivery:');
            }
          });
          $('.custom-container.checkout .delivery-methods-cont .card .card-body .row-head:contains("Ship to")').each(function() {
            if(!$(this).hasClass('total-delivery')){
              $(this).addClass('total-delivery');
              $(this).html('Delivery');
            }
          });
          $('.custom-container.checkout .payment-form-container .checkbox-cont .checkbox-note:contains("Same as shipping address")').each(function() {
            if(!$(this).hasClass('total-delivery')){
              $(this).addClass('total-delivery');
              $(this).html('Same as delivery address');
            }
          });
          $('.checkout-container.complete .section-cont.customer-note').each(function(){
            if(!$(this).hasClass('total-delivery')){
              $(this).addClass('total-delivery');
              var newTxt = 'Your order is being confirmed. You will receive an email with further information';
              $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function(_, html) {
                 return  html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-string">'+newTxt+'</div>')
              });
            }
          });
          $('.checkout-container.complete .shipping-data .shipping-row .pr-md-3:contains("Address:")').each(function(){
            if(!$(this).hasClass('total-delivery')){
              $(this).addClass('total-delivery');
              $(this).html('Delivery:');
            }
          });
          $('.checkout-container.complete .shipping-data .shipping-row .pr-md-3:contains("Shipping:")').each(function(){
            if(!$(this).hasClass('total-delivery')){
              $(this).addClass('total-delivery');
              $(this).html('Delivery:');
            }
          });
          $('.checkout-container.complete').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).append('<a class="home-btn text-center d-block" href="/singapore/">Back to Home Page</a>');
            }
          });

          $('.cart-item.new .underline:contains("Add Gift Options")').each(function(){
            if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Add Gift Message');
            }
          });
          $('.login-signup .custom-container .breadcrumb-heading:contains("Log In")').each(function() {
            if(!$(this).hasClass('log-modified')){
              $(this).addClass('log-modified');
              $(this).html('Capella Ubud Gift Card Platform Log In');
            }
          });
          $('.custom-container.cart-container .btn.continue-btn-new').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Return to shopping');
              }
          });
          $('.nav-ship-details .breadcrumb-heading:not(.modified)').addClass('modified').html('Customer Details');
          $('.shipping-form-container .form-control#last-name').each(function(){
            if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).closest('.col-md-6').addClass('last-name-cont');
            }
          });
          $('.auth-user .checkout .shipping-form h1.breadcrumb-heading:contains("Shipping Details")').each(function() {
            if(!$(this).closest('.shipping-form').hasClass('shipp-details')){
              $(this).closest('.shipping-form').addClass('shipp-details');
              $(this).html('Choose Shipping Details');
            }
          });
          $('.custom-container.checkout .payment-form-container .form-group .card-images-holder .cards-desc').each(function() {
            if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Accepted Cards - Visa, Mastercard, and American Express.');
            }
          });

          $('.checkout .checkout-container.complete a:contains("Customer Support")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).attr('href','mailto:capella@techsembly.com');
            }
          });

          $('.footer .footer-widget .widget-title:contains("KEEP IN TOUCH")').each(function() {
            if(!$(this).closest('.footer-widget').hasClass('capitalize')){
              $(this).closest('.footer-widget').addClass('capitalize');
              $('.footer-widget.capitalize .widget-title').html("Keep In Touch");
            }
          });
          $('.footer .copyright-cont .pay-methods-avail ').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).find('.meth-link:not(.new)').addClass('d-none');
                  $(this).append('<div class="meth-link new"><img alt="amex" src="https://static.techsembly.com/KJEMbwBjwG4ETKdppQtbq5Z2" width="52"></div>'+
                  '<div class="meth-link new"><img alt="master-card" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/master-173035bc8124581983d4efa50cf8626e8553c2b311353fbf67485f9c1a2b88d1.svg" width="52"></div>'+
                  '<div class="meth-link new"><img alt="visa" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/visa-319d545c6fd255c9aad5eeaad21fd6f7f7b4fdbdb1a35ce83b89cca12a187f00.svg" width="52"></div>');
              }
          });
          $('.products-container .products-holder-main').removeClass('pt-lg-2');
          $('.products-container .products-holder-main .products-holder').removeClass('pt-5');
          $('.products-holder .product-column .product-content .d-flex').addClass('flex-row-reverse');
          $('.products-container .products-holder-main .products-holder .product-list .product-image').removeClass('mb-2');
          $('.footer .copyright-cont .powered-by').removeClass('col-lg-6 text-center align-items-center').addClass('col-lg-9 pr-0');
          $('.products-holder-main .products-holder .product-column .product-list a.wishlistImage img[alt="add-to-wishlist"]').attr('src','https://static.techsembly.com/MRXTrxS1t1kZoypfHTa8JMtF');
          // $('.product-pricing.w-100').appendTo('.product-title');
          $('.user-container .balance-form').closest('.user-container:not(.d-none)').addClass('d-none');

          $('.user-container .user-heading:contains("Check TS Gift Card Balance")').each(function(){
            if(!$(this).hasClass('gift-card-heading')){
              $(this).addClass('gift-card-heading');
              $(this).html('Check Gift Card Balance');
            }
          });
          $('.user-container .user-heading:contains("My TS Card Balance")').each(function(){
            if(!$(this).hasClass('gift-card-heading')){
              $(this).addClass('gift-card-heading');
              $(this).html('My Gift Card Balance');
            }
          });
          $('.ts-balance-form label[for="gift-card-no"]:not(.modified)').addClass('modified').html('Gift card number');
          $('.ts-balance-form input[name="gift-card-no"]:not(.modified)').addClass('modified').attr('placeholder','Enter gift card number');

          $('.footer a:contains("+66")').each(function(){
            var string_phone= $(this).html();
            var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
            var phone_no = "tel:" + result_phone;
            if(!$(this).hasClass('tel-link')){
              $(this).addClass('tel-link');
              $(this).attr('href', phone_no);
            }
          });
          $('.footer a:contains("@")').each(function(){
            var string_email= $(this).html();
            var result_email = string_email.substring(string_email.indexOf(':') + 1);
            var email_link = "mailto:" + result_email;
            if(!$(this).hasClass('email-link')){
              $(this).addClass('email-link');
              $(this).attr('href', email_link);
            }
          });
          $('.footer a:contains("Privacy")').each(function(){
            if(!$(this).hasClass('privacy-link')){
              $(this).addClass('privacy-link');
              $(this).attr('target', '_blank');
            }
          });
          $('.footer a:contains("Terms")').each(function(){
            if(!$(this).hasClass('tnc-link')){
              $(this).addClass('tnc-link');
              $(this).attr('target', '_blank');
            }
          });
          $('.social-links img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/y9GmmRooddrjN2GGuJR6mM3E');
          $('.social-links img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/47oUX2zXenv2WXMQhVCokcdc');
          $('.social-links img[alt="line"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/Nif33zqKFpemkqXk2mc94ysU');
          $('.social-links img[alt="fb"]').closest('li:not(.fb-link-cont)').addClass('fb-link-cont');
          $('.social-links img[alt="instagram"]').closest('li:not(.insta-link-cont)').addClass('insta-link-cont').insertBefore($('.social-links .fb-link-cont'));
          $('.social-link .insta-link-cont:not(:first-child)').remove();
          $('.product-rating a.wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://static.techsembly.com/PNab13cniRH8L8b2wgi35DMc');
        })
      })
    })
})();
