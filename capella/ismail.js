function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }

        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
function addLoginCartNote(){
  var loginCartNote = "<div class='login-note'>Please note this login is for Capella Bangkok gifting platform only. <div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}
function addGuestCheckoutNote(){
  var checkoutNote = "<div class='checkout-note text-center w-100'>You can create account after checkout<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}
function addcopyrightText(){
  var copyrightHtml = '<div class="container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-6 ts-copyright"><span><a href="https://techsembly.com/" target="_blank">Techsembly©</a></span><span class="ml-1">2021</span></div><div class="col-6 tc-rights-reserved text-right pr-0"></div></div></div></div>'
  var elem = $(".checkout-footer")[0]
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}
function addcompleteText(){
  var addcompleteText = '<div class="card-footer-right"><span>© 2021 Capella</span></div>'
  var textComplete = $(".checkout-container.complete .card-footer.blue-light-bg .col-12")[0]
  textComplete.insertAdjacentHTML('beforeend', addcompleteText)
}
(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
          font-family: 'Calibre-Regular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/20858334f659d0c98de31abb3f25341768531aeb/capella/fonts/calibre/Calibre-Regular.ttf');\
      }\
      @font-face {\
          font-family: 'Calibre-Medium';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/fcf974593b5d851af458246669f619a93fbd8056/capella/fonts/calibre/Calibre-Medium.ttf');\
      }\
      @font-face {\
          font-family: 'OPTIwtcGoudy-Regular':\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/f7bc807b6c5f65a8064e3199a9851cfa7da793a9/capella/fonts/goudy-wtc/OPTIwtcGoudy-Regular.ttf');\
      }\
    "));

    document.head.appendChild(newStyle);
    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };
    checkReady(function($) {
      $(function() {
        waitForElm('app-footer-checkout').then((elm) => {
          addcopyrightText()
        }).catch((error) => {});

        waitForElm('.cart-right-cont').then((elm) => {
          addLoginCartNote();
          addGuestCheckoutNote();
        }).catch((error) => {});

        waitForElm('.checkout-container.complete').then((elm) => {
          addcompleteText();
        }).catch((error) => {});

        setTimeout(function () {
          $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
        }, 2000);
        setInterval(function() {
          $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
          $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');
          $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('buy now');
              }
          });
          $('app-product-detail app-related-products[title="More items from"]').each(function(){
            if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).find('h3').html('More Like This');
            }
          });
          $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/uPjNo3rYoN3JJt36hPiFPuUn');
          $('.custom-container.cart-container .vendor-items-cont a img[alt="gift"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/vg23BokHkdx4WrwPDtVBsAGu');
          $('.delivery-methods-cont .card .card-body .text-right a img[alt="edit"]:not(.edit-img)').addClass('edit-img').attr('src', 'https://static.techsembly.com/uPjNo3rYoN3JJt36hPiFPuUn');
          $('.custom-container.checkout a img[alt="edit"]:not(.edit-img)').addClass('edit-img').attr('src', 'https://static.techsembly.com/uPjNo3rYoN3JJt36hPiFPuUn');
          $('.custom-container.cart-container').each(function(){
            if (!$(this).closest('body').attr('data-pagetype','cart')){
              $(this).closest('body').attr('data-pagetype','cart');
            }
          })
          $('.custom-container.cart-container .btn.continue-btn-new').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Return to shopping');
              }
          });
          $('.shipping-form-container .form-control#last-name').each(function(){
            if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).closest('.col-md-6').addClass('last-name-cont');
            }
          });
          $('.footer .footer-widget .widget-title:contains("KEEP IN TOUCH")').each(function(){
            if(!$(this).closest('.footer-widget').hasClass('capitalize')){
              $(this).closest('.footer-widget').addClass('capitalize');
              $('.footer-widget.capitalize .widget-title').html("Keep In Touch");
            }
          });
          $('.footer .copyright-cont .pay-methods-avail ').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).find('.meth-link:not(.new)').addClass('d-none');
                  $(this).append('<div class="meth-link new"><img alt="apple-pay" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/apple_pay-f6db0077dc7c325b436ecbdcf254239100b35b70b1663bc7523d7c424901fa09.svg" width="52"></div>'+
                  '<div class="meth-link new"><img alt="master-card" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/master-173035bc8124581983d4efa50cf8626e8553c2b311353fbf67485f9c1a2b88d1.svg" width="52"></div>'+
                  '<div class="meth-link new"><img alt="paypal" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/paypal-49e4c1e03244b6d2de0d270ca0d22dd15da6e92cc7266e93eb43762df5aa355d.svg" width="52"></div>'+
                  '<div class="meth-link new"><img alt="visa" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/visa-319d545c6fd255c9aad5eeaad21fd6f7f7b4fdbdb1a35ce83b89cca12a187f00.svg" width="52"></div>');
              }
          });
          $('.products-container .products-holder-main').removeClass('pt-lg-2');
          $('.products-container .products-holder-main .products-holder').removeClass('pt-5');
          $('.products-holder .product-column .product-content .d-flex').addClass('flex-row-reverse');
          $('.products-container .products-holder-main .products-holder .product-list .product-image').removeClass('mb-2');
          $('.footer .copyright-cont .powered-by').removeClass('col-lg-6 text-center align-items-center').addClass('col-lg-9 pr-0');
          $('.products-holder-main .products-holder .product-column .product-list a.wishlistImage img[alt="add-to-wishlist"]').attr('src','https://static.techsembly.com/MRXTrxS1t1kZoypfHTa8JMtF');
          // $('.product-pricing.w-100').appendTo('.product-title');
        })
      })
    })
})();
