LoadFonts();

// make loader
// append loade to body document.body.append()

var head = document.getElementsByTagName("HEAD")[0];
var link = document.createElement("link");
link.rel = "stylesheet";
link.type = "text/css";
link.href = "http://localhost/skp/QxC.css";
head.appendChild(link);

window.addEventListener("load", function () {
    //
    var pageloaded = false;
    setTimeout(() => {
        if (screen.width >= "991.98") {
            if (!pageloaded) {
                pageloaded = true;
                console.warn("Page Loaded");

                const currentPage = this.window.location.href;
                const check = currentPage.search("catalogsearch");
                const vendor = currentPage.search("vendor");

                console.log(check);

                if (check > -1) {
                    searchPage();
                } else if (document.body.getAttribute("data-pagetype") == "product") {
                    ProductPage();
                } else if (document.body.getAttribute("data-pagetype") == "category") {
                    Supplements();
                } else if (window.location.pathname === "/") {
                    HomePage();
                } else if (window.location.href === "https://shop.fitnessfirst.com.sg/signin-signup") {
                    SignInPage();
                } else if (window.location.href === "https://shop.fitnessfirst.com.sg/cart") {
                    CartPage();
                } else if (window.location.href === "https://shop.fitnessfirst.com.sg/wishlist") {
                    WishlistPage();
                } else if (window.location.href === "https://shop.fitnessfirst.com.sg/checkout") {
                    CheckoutPage();
                } else if (
                    window.location.href === "https://shop.fitnessfirst.com.sg/user/orders" ||
                    window.location.href === "https://shop.fitnessfirst.com.sg/user/account" ||
                    window.location.href === "https://shop.fitnessfirst.com.sg/user/edit" ||
                    window.location.href === "https://shop.fitnessfirst.com.sg/user/create-address" ||
                    window.location.href === "https://shop.fitnessfirst.com.sg/user/wishlist" ||
                    window.location.href === "https://shop.fitnessfirst.com.sg/pages/returns" ||
                    vendor > -1 ||
                    window.location.href === "https://shop.fitnessfirst.com.sg/pages/delivery"
                ) {
                    UserPage();
                }
            }
        } else {
            if (!pageloaded) {
                pageloaded = true;
                console.warn("Page Loaded");
                // document.get Loader delete / Display None
                if (document.body.getAttribute("data-pagetype") == "product") {
                    SmallProductPage();
                } else if (document.body.getAttribute("data-pagetype") == "category") {
                    SmallSupplements();
                } else if (window.location.pathname === "/") {
                    SmallHome();
                } else if (window.location.href === "https://shop.fitnessfirst.com.sg/cart") {
                    SmallCartPage();
                } else if (window.location.href === "https://shop.fitnessfirst.com.sg/wishlist") {
                    SmallWishlistPage();
                } else if (window.location.href === "https://shop.fitnessfirst.com.sg/checkout") {
                    SmallCheckoutPage();
                } else if (window.location.href === "https://shop.fitnessfirst.com.sg/signin-signup") {
                    SmallSignInPage();
                } else if (window.location.href === "https://shop.fitnessfirst.com.sg/signin-signup") {
                }
            }
        }
    }, 2000);
});

function LoadFonts() {
    var junction_font = new FontFace(
        "Junction Regular",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-Light.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-Bold",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-Bold.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-ExtraBold",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-ExtraBold.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-Thin",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-Thin.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-Medium",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-Medium.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-UltraItalic",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-UltraItalic.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-Ultra",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-Ultra.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-ThinItalic",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-ThinItalic.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-Regular",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-Regular.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-MediumIt",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-MediumIt.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-Medium",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-Medium.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-LightItalic",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-LightItalic.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });

    var junction_font = new FontFace(
        "SohoGothicPro-BoldItalic",
        "url(https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/fonts/sohogothic/SohoGothicPro-BoldItalic.otf)"
    );
    junction_font
        .load()
        .then(function (loaded_face) {
            console.warn("fontloaded", loaded_face);
            document.fonts.add(loaded_face);
        })
        .catch(function (error) {
            console.log("Error Loading Font");
        });
}

// Pages

function HomePage() {

    let customContainer = document.getElementsByClassName("gift-section")[0].lastElementChild;
    customContainer.classList.add("widthSetter");
    let profileImg = document.getElementsByClassName("profile-icon")[0];
    profileImg.firstElementChild.src = `https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/user.svg`;

    let wishlistHeart = document.getElementsByClassName("wishlist-heart")[0];
    wishlistHeart.firstElementChild.src = `https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/heart.svg`;

    let cartImg = document.getElementsByClassName("cart-bag")[0];
    cartImg.firstElementChild.src = `https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/bag.svg`;
    // SetHeader();
    SearchIcon(
        "https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/search.svg",
        "SearchBox-White"
    );
    ChangingPaymentCards();
    PoweredBy();
    AdjustFooterLinkArea();
    SignUpSetup();
    FooterLogo();
    document.getElementsByClassName("select-region")[0].lastElementChild.style.display = "none";

    let fotterListForm = document.getElementsByClassName("select-region")[0].firstElementChild.lastElementChild;
    fotterListForm.remove();

    OurProductContent();
}

function SmallHome() {
    let profileImg = document.getElementById("profile-icon");

    profileImg.firstElementChild.classList.add("Working");
    const d = new Date();
    let copyrightYear = d.getFullYear();

    MobileIconChange();
    OurProductContent();
    /*document.getElementsByClassName("pay-methods-avail")[1].innerHTML += `
  <div class="meth-link">
    <img
      class=""
      alt="amex"
      width="60"
      src="https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/visa.svg"
    />
   </div>`;
    var links = document.getElementsByClassName("meth-link");
    links[3].firstElementChild.src =
        "https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/apple-pay.svg";
    links[4].firstElementChild.src =
        "https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/master-card.svg";
    links[5].firstElementChild.src =
        "https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/paypal.svg";*/
    homeSlider.firstElementChild.firstElementChild.classList.remove("d-none");
    let powerBy = document.getElementsByClassName("powered-by")[1].firstElementChild;
    powerBy.innerHTML = '© '+copyrightYear+' Fitness First. All Rights Reserved';
    var poweredByRemove = document.getElementsByClassName("powered-link-cont")[1].lastElementChild;
    poweredByRemove.style.display = "none";
    let copyRightContent = document.getElementsByClassName("copyright-cont")[1].firstElementChild;
    copyRightContent.style.padding = "0px";
    document.getElementsByClassName("powered-by")[1].innerHTML +=
        "<img id='footerLogo' name='footerLogo' src='https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/techsembly-logo.svg'>";
}

function productprice(obj) {
    var size = 0,
        key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
}

function sizeObj(obj) {
    return Object.keys(obj).length;
}

function Supplements() {
    // NotificationBar();
    // SetHeader();
    // SetMenuLinkBlack();
    //OurProducts();


    let copyRightContent1 = document.getElementsByClassName("copyright-cont")[0].firstElementChild;
    copyRightContent1.style.padding = "0px";

    // console.clear();
    // let searchBtn = document.querySelector(".head-links");
    // console.log(searchBtn);
    // let clone = searchBtn.cloneNode(true);
    // console.log(clone);
    // alert(document.querySelectorAll(".head-links"));
    // document.querySelector(".head-links").prepend(clone);

    SearchIcon(
        "https://bitbucket.org/TSTechsembly/jscss/raw/e651d8ddd5d877b0f7123196be21f1487ec83bdb/fft/images/search-dark.svg",
        "SearchBox-Black"
    );
    AdjustFooterLinkArea();

    MoreProductsDescription();
    ChangingPaymentCards();
    PoweredBy();
    SignUpSetup();
    FooterLogo();
    document.getElementsByClassName(
        "footer-widget"
    )[3].lastElementChild.firstElementChild.lastElementChild.style.display = "none";
}

function SmallSupplements() {
    // NotificationBar();
    MobileIconChange();

    //OurProducts();
    MoreProductsDescription();
    PoweredBy();
    ChangingPaymentCards();
    FooterLogo();
    OurProductContent();
}

function ProductPage() {
    // if (window.location.href === "https://shop.fitnessfirst.com.sg/strong-protein-023629") {
    //   ProductPage2();
    //   return;
    // }

    // NotificationBar();
    // SetHeader();
    // SetMenuLinkBlack();
    //ProductTitleDesTabs();

    SearchIcon(
        "https://bitbucket.org/TSTechsembly/jscss/raw/e651d8ddd5d877b0f7123196be21f1487ec83bdb/fft/images/search-dark.svg",
        "SearchBox-Black"
    );

    // ProductTitleDes();

    let productDetailCont = document.querySelector(".detail-text");
    var cln = productDetailCont.cloneNode(true);
    //document.getElementById("askseller-tab").firstElementChild.textContent = "Ask Question";

    //let detailTabBtn = document.getElementById("details-tab");


    /*if (productDetailCont.querySelector(".xyz") !== null) {
        document.querySelector(".product-detail-container").appendChild(cln);
        document.querySelectorAll(".show-more-cont")[1].remove();

        // productDetailCont.innerHTML += document.getElementById("details").innerHTML;
        document.getElementById("details-tab").remove();
        document.getElementById("askseller-tab").remove();
    }*/

    document.getElementById("add-to-basket-btn").textContent = "BUY NOW";

    MoreProductsDescription();
    //OurProducts();
    ChangingPaymentCards();

    AdjustFooterLinkArea();

    PoweredBy();

    SignUpSetup();

    document.getElementsByClassName(
        "footer-widget"
    )[3].lastElementChild.firstElementChild.lastElementChild.style.display = "none";
    FooterLogo();
    JQ();

    return;
    // let copyRightContent1 = document.getElementsByClassName("copyright-cont")[0].firstElementChild;
    // copyRightContent1.style.padding = "0px";

    // document.getElementsByTagName("app-breadcrumbs")[0].style.display = "none";

    // let productHolder = document.getElementsByClassName("product-holder")[0];
    // productHolder.innerHTML =
    //   `
    //         <div class="product-breadcrum">
    //         <h2>FITNESS FIRST / FEATURED / BOOSTER</h2>
    //         </div>
    //         ` + productHolder.innerHTML;

    // document.getElementById("details-tab").firstElementChild.remove();
    // document.getElementById("delivery-tab").firstElementChild.remove();
    // document.getElementById("item-quantity").value = 2;
    // document.getElementById("add-to-basket-btn").textContent = "BUY NOW";

    // AddCarusrel();

    // document.getElementsByClassName("copyright-cont")[0].firstElementChild;
    // document.querySelector("#item-quantity").value = 1;
}

function SmallProductPage() {
    // if (window.location.href === "https://shop.fitnessfirst.com.sg/strong-protein-023629") {
    //   SmallProductPage2();
    //   return;
    // }
    // NotificationBar();
    MobileIconChange();
    //document.getElementById("add-to-basket-btn").innerHTML = "BUY NOW";

    // ProductTitleDes();
    //ProductTitleDesTabs();
    //OurProducts();
    MoreProductsDescription();
    PoweredBy();
    ChangingPaymentCards();
    FooterLogo();
    JQ();

    //document.getElementById("askseller-tab").firstElementChild.textContent = "Ask Question";
    //document.getElementById("delivery-tab").remove();

    let productDetailCont = document.querySelector(".detail-text");
    var cln = productDetailCont.cloneNode(true);
    /*if (productDetailCont.querySelector(".xyz") !== null) {
      document.querySelector(".product-detail-container").appendChild(cln);
      document.querySelectorAll(".show-more-cont")[1].remove();
        document.getElementById("details-tab").remove();
        document.getElementById("askseller-tab").remove();
    }*/

    // document.getElementsByTagName("app-related-products")[0].style.display =
    //   "none";
}

function ProductPage2() {
    // SetHeader();
    // SearchIcon("https://bitbucket.org/TSTechsembly/jscss/raw/e651d8ddd5d877b0f7123196be21f1487ec83bdb/fft/images/search-dark.svg", "SearchBox-Black");
    // SetMenuLinkBlack();
    // ProductTitleDes();

    document.getElementsByTagName("app-breadcrumbs")[0].style.display = "none";

    // let productHolder = document.getElementsByClassName("product-holder")[0];
    // productHolder.innerHTML =
    //   `
    //         <div class="product-breadcrum">
    //         <h2>FITNESS FIRST / FEATURED / BOOSTER</h2>
    //         </div>
    //         ` + productHolder.innerHTML;

    // document.getElementById("details-tab").firstElementChild.remove();
    // document.getElementById("delivery-tab").firstElementChild.remove();
    // document.getElementById("add-to-basket-btn").innerHTML = "BUY NOW";

    // console.log(document.querySelector("#item-quantity"));
    // document.querySelector("#item-quantity").value = 1;
    // document.getElementById("add-to-basket-btn").textContent = "BUY NOW";

    let productDetailCont = document.querySelector(".product-detail-container");
    if (productDetailCont.querySelector(".xyz") !== null) {
        productDetailCont.innerHTML += document.getElementById("details").innerHTML;
        document.getElementById("details-tab").firstElementChild.remove();
        //document.getElementById("delivery-tab").firstElementChild.remove();
        document.querySelector("#item-quantity").value = 1;
    }
    else {
      //document.getElementById("delivery-tab").remove();
    }

    document.getElementById("add-to-basket-btn").textContent = "BUY NOW";

    MoreProductsDescription();
    OurProducts();
    ChangingPaymentCards();

    AdjustFooterLinkArea();

    PoweredBy();

    SignUpSetup();

    document.getElementsByClassName(
        "footer-widget"
    )[3].lastElementChild.firstElementChild.lastElementChild.style.display = "none";
    FooterLogo();
    JQ();
}

function SmallProductPage2() {
    // NotificationBar();
    MobileIconChange();
    // ProductTitleDes();
    // ProductTitleDesTabs();

    document.getElementById("details-tab").firstElementChild.remove();
    //document.getElementById("delivery-tab").firstElementChild.remove();
    document.getElementById("add-to-basket-btn").innerHTML = "BUY NOW";

    //ProductTitleDesTabs();
    //OurProducts();
    MoreProductsDescription();
    PoweredBy();
    ChangingPaymentCards();
    FooterLogo();
    // document.getElementsByTagName("app-related-products")[0].style.display =
    //   "none";
}

function SignInPage() {
    // NotificationBar();
    // SetHeader();
    // SetMenuLinkBlack();
    SearchIcon(
        "https://bitbucket.org/TSTechsembly/jscss/raw/e651d8ddd5d877b0f7123196be21f1487ec83bdb/fft/images/search-dark.svg",
        "SearchBox-Black"
    );

    let breadcrum_label = document.querySelector(".default-breadcrumb");
    breadcrum_label.classList.add("signin_breadcrumb");

    SignUpSetup();
    // OurProducts();
    AdjustFooterLinkArea();
    PoweredBy();

    ChangingPaymentCards();
    FooterLogo();
    document.getElementsByClassName(
        "footer-widget"
    )[3].lastElementChild.firstElementChild.lastElementChild.style.display = "none";
}

function CartPage() {
    // NotificationBar();
    // SetHeader();
    // SetMenuLinkBlack();
    SearchIcon(
        "https://bitbucket.org/TSTechsembly/jscss/raw/e651d8ddd5d877b0f7123196be21f1487ec83bdb/fft/images/search-dark.svg",
        "SearchBox-Black"
    );

    let breadcrum_label = document.querySelector(".default-breadcrumb");
    breadcrum_label.classList.add("signin_breadcrumb");

    SignUpSetup();
    // OurProducts();
    AdjustFooterLinkArea();
    PoweredBy();

    ChangingPaymentCards();
    FooterLogo();
    document.getElementsByClassName(
        "footer-widget"
    )[3].lastElementChild.firstElementChild.lastElementChild.style.display = "none";
}

function WishlistPage() {
    // NotificationBar();
    // SetHeader();
    // SetMenuLinkBlack();
    SearchIcon(
        "https://bitbucket.org/TSTechsembly/jscss/raw/e651d8ddd5d877b0f7123196be21f1487ec83bdb/fft/images/search-dark.svg",
        "SearchBox-Black"
    );

    let breadcrum_label = document.querySelector(".default-breadcrumb");
    breadcrum_label.classList.add("signin_breadcrumb");

    SignUpSetup();
    // OurProducts();
    AdjustFooterLinkArea();
    PoweredBy();

    ChangingPaymentCards();
    FooterLogo();
    document.getElementsByClassName(
        "footer-widget"
    )[3].lastElementChild.firstElementChild.lastElementChild.style.display = "none";
}

function CheckoutPage() {
    // SetHeader();
    // SetMenuLinkBlack();
    SearchIcon(
        "https://bitbucket.org/TSTechsembly/jscss/raw/e651d8ddd5d877b0f7123196be21f1487ec83bdb/fft/images/search-dark.svg",
        "SearchBox-Black"
    );

    let breadcrum_label = document.querySelector(".default-breadcrumb");
    breadcrum_label.classList.add("signin_breadcrumb");

    SignUpSetup();
    // OurProducts();
    AdjustFooterLinkArea();
    PoweredBy();

    ChangingPaymentCards();
    FooterLogo();
    document.getElementsByClassName(
        "footer-widget"
    )[3].lastElementChild.firstElementChild.lastElementChild.style.display = "none";
}

function SmallCartPage() {
    MobileIconChange();
}

function SmallWishlistPage() {
    MobileIconChange();
}

function SmallCheckoutPage() {
    MobileIconChange();
}

function SmallSignInPage() {
    MobileIconChange();
    PoweredBy();
    ChangingPaymentCards();
    FooterLogo();
}
function searchPage() {
    // SetHeader();
    // SetMenuLinkBlack();
    SearchIcon(
        "https://bitbucket.org/TSTechsembly/jscss/raw/e651d8ddd5d877b0f7123196be21f1487ec83bdb/fft/images/search-dark.svg",
        "SearchBox-Black"
    );
    ChangingPaymentCards();

    AdjustFooterLinkArea();

    PoweredBy();

    SignUpSetup();

    document.getElementsByClassName(
        "footer-widget"
    )[3].lastElementChild.firstElementChild.lastElementChild.style.display = "none";
    FooterLogo();
}

function UserPage() {
    // SetHeader();
    // SetMenuLinkBlack();
    SearchIcon(
        "https://bitbucket.org/TSTechsembly/jscss/raw/e651d8ddd5d877b0f7123196be21f1487ec83bdb/fft/images/search-dark.svg",
        "SearchBox-Black"
    );
    ChangingPaymentCards();

    AdjustFooterLinkArea();

    PoweredBy();

    SignUpSetup();

    document.getElementsByClassName(
        "footer-widget"
    )[3].lastElementChild.firstElementChild.lastElementChild.style.display = "none";
    FooterLogo();
}
// Edits

function SetHeader() {
  if (screen.width >= "991.98") {
    var menuContent = document.getElementsByClassName("toggle-menu-cont")[0];
    var logo = document.getElementsByClassName("logo")[0].parentElement;
    var menuHolder = document.getElementsByClassName("menu-holder")[0].parentElement;
    menuHolder.firstElementChild.classList.add("UpperNav");

    menuContent.innerHTML = logo.innerHTML;
    logo.innerHTML = menuHolder.innerHTML;

    var menuHolder2 = document.getElementsByClassName("menu-holder")[1];
    menuHolder2.style.display = "none";
    var menuHolder = (document.getElementsByClassName("menu-holder")[1].style.display = "none");

    if(window.location.pathname === "/"){
      let headerAddClass = document.getElementById("header");
      headerAddClass.classList.add("home_header");
    }
    else{
      SetMenuLinkBlack()
    }
  }
}

function SearchIcon(imageSrc, color) {
    const searchIconDiv = document.createElement("li");
    // searchIconDiv.setAttribute("class", "search_btn");
    searchIconDiv.innerHTML = `<div class="dropdown add-to-basket">
<a >
<img
class="search_button"
alt="cart"
height="25"
width="25"
src=${imageSrc}
/>
</a>
</div>`;

    const gg = document.querySelector(".head-links");
    gg.prepend(searchIconDiv);

    document.querySelector(".search-item-form").classList.toggle("d-none");
    document.querySelector(".search-item-form").style.display = "block";
    document.querySelector(".search_button").addEventListener("click", function () {
        document.querySelector(".search-item-form").classList.toggle("d-none");
    }); // search-item-form
}

function FooterLogo() {
    document.getElementsByClassName("powered-by")[0].innerHTML +=
        "<img id='footerLogo' name='footerLogo' src='https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/techsembly-logo.svg'>";
}

function PoweredBy() {
    const d = new Date();
    let copyrightYear = d.getFullYear();
    let powerBy = document.getElementsByClassName("powered-by")[0].firstElementChild;
    powerBy.innerHTML = '©' +copyrightYear+' Fitness First. All Rights Reserved';

    var poweredByRemove = document.getElementsByClassName("powered-link-cont")[0].lastElementChild;
    poweredByRemove.style.display = "none";

    let copyRightContent = document.getElementsByClassName("copyright-cont")[0].firstElementChild;
    copyRightContent.style.padding = "0px";
}

function ChangingPaymentCards() {
    /*document.getElementsByClassName("pay-methods-avail")[0].innerHTML += `
    <div class="meth-link">
      <img
        class=""
        alt="amex"
        width="60"
        src="https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/visa.svg"
      />
    </div>`;

    var links = document.getElementsByClassName("meth-link");

    links[0].firstElementChild.src =
        "https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/apple-pay.svg";
    links[1].firstElementChild.src =
        " https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/master-card.svg";
    links[2].firstElementChild.src =
        "https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/paypal.svg";*/
}

function AdjustFooterLinkArea() {
    document.getElementsByClassName("footer-widget")[4].innerHTML += `
    <button class="btn btn-primary" style="background-color: #da1a32">
      CONTACT
    </button>
  `;
}

function SignUpSetup() {
    /*let formButton = document.getElementById("mailchip-submit-btn");
    formButton.style.textTransform = "uppercase";

    let formInput = document.getElementById("mailchip-email");
    formInput.style.margin = "0px";
    formInput.placeholder = "";*/
}

function SetMenuLinkBlack() {
    let menuCat = document.querySelectorAll(".main-category");
    for (let i = 0; i < menuCat.length; i++) {
        menuCat[i].style.color = "black";
    }
}

function AddCarusrel() {
    document.getElementsByClassName("product-carousel-desktop")[0].innerHTML = `
  <div id="product_wrapper">
  <div id="product_carousel">
    <div id="product_content">
      <img
        src="https://shop.fitnessfirst.com.sg/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBemFGQWc9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--07d9606c980be0a94393dabb2486dbc6555e45dd/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lOTmpBd2VEWXdNRDRHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--f339283b759c99fba5daae533311105d5b018d0e/energy-booster-performance-2.jpg"
        class="product_item"
      />
      <img
        src="https://shop.fitnessfirst.com.sg/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBeXlGQWc9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--8f8e41a74768ec77b01a2d943b728672c23fcb1f/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lOTmpBd2VEWXdNRDRHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--f339283b759c99fba5daae533311105d5b018d0e/strong-protein-chocolate-vanilla-muscle-2.jpg"
        class="product_item"
      />
      <img
        src="https://shop.fitnessfirst.com.sg/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBeStGQWc9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--8e5251b02399b7b94cae5968ebee3fa4063872f8/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lOTmpBd2VEWXdNRDRHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--f339283b759c99fba5daae533311105d5b018d0e/health-protein-chocolate-vanilla-wellbeing-1.jpg"
        class="product_item"
      />
      <img
        src="https://shop.fitnessfirst.com.sg/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBeStGQWc9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--8e5251b02399b7b94cae5968ebee3fa4063872f8/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lOTmpBd2VEWXdNRDRHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--f339283b759c99fba5daae533311105d5b018d0e/health-protein-chocolate-vanilla-wellbeing-1.jpg"
        class="product_item"
      />
    </div>
  </div>
  <button id="product_prev">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
    >
      <path fill="none" d="M0 0h24v24H0V0z" />
      <path d="M15.61 7.41L14.2 6l-6 6 6 6 1.41-1.41L11.03 12l4.58-4.59z" />
    </svg>
  </button>
  <button id="product_next">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
    >
      <path fill="none" d="M0 0h24v24H0V0z" />
      <path d="M10.02 6L8.61 7.41 13.19 12l-4.58 4.59L10.02 18l6-6-6-6z" />
    </svg>
  </button>
</div>
    `;
}

function NotificationBar() {
    let notificationBar = document.getElementById("content");
    notificationBar.innerHTML =
        `
  <div
      class="notificationbar"
      style="background-color: #232323; text-align: center; color: #ffffff"
    >
      Due to Covid-19 some deliveries might be delayed. Click here to reach
      representative
    </div>
    ` + notificationBar.innerHTML;
}

function ProductTitleDes() {
    let productDetail = document.getElementsByClassName("product-order")[0];
    productDetail.innerHTML =
        `
    <div class="product_right_box">
      <div class="product_right_box_title">
        <h3 class="product_right_box_head3">V-FROM TRAINER</h3>
        <img
          class="product_right_box_img"
          src="https://cdn-saas.techsembly.com/packs/./storefront/fonts/heart.svg"
        />
      </div>
      <div class="product_right_box_title2">
        <span class="product_right_box_sp">
          <p class="product_right_box_para">Vitruvivan</p>
        </span>
      </div>
    </div>
    ` + productDetail.innerHTML;
}

function ProductTitleDesTabs() {
    /*document.getElementById("details-tab").firstElementChild.innerHTML = "Specification";
    document.getElementById("delivery-tab").firstElementChild.innerHTML = "Contact Brand";*/

    // document.getElementById("add-to-basket-btn").textContent = "BUY NOW";
}

function OurProducts() {
    let AppNews = document.getElementsByTagName("app-new-newsletter")[0];
    AppNews.innerHTML =
        `
  <section class="home-intro">
      <div class="container home-intro-container">
        <h2 class="text-center">WHY OUR PRODUCTS</h2>
        <div
          class="intro-content"
          style="background-color: ; text-align: center; color: #000000"
        >
          <div>
            <p style="color: #000000">
              Sharpen your training efforts with ingredients that target
              specific health and nutritional needs, with scientifically backed
              research - Innermost collections are free of GMO, soy and gluten,
              low in sugar and mostly vegan, and come as a bundle of 3 items
              that complement each other to support your training.
            </p>
          </div>
        </div>
      </div>
    </section>
  ` + AppNews.innerHTML;
}

function MoreProductsDescription() {
    let productDescription = document.getElementsByClassName("product-title");
    let productDSize = productprice(productDescription);
    /*for (let i = 0; i < productDSize; i++) {
        productDescription[i].innerHTML =
            productDescription[i].innerHTML +
            `
        <div class="product_description">
          <p>Developed to maximise muscle</p>
        </div>
        `;
    }*/
}

function MobileIconChange() {
    let header = document.getElementsByClassName("mob-top-links")[0];
    let headerIcons = header.children;
    for (let index = 0; index < headerIcons.length; index++) {
        if (headerIcons[index].firstElementChild.className == "p-2 d-inline mob-search-link") {
            headerIcons[
                index
            ].firstElementChild.firstElementChild.src = `https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/search.svg`;
        } else if (headerIcons[index].firstElementChild.className == "pl-2 py-1 pr-1 d-inline profile-icon") {
            headerIcons[
                index
            ].firstElementChild.firstElementChild.src = `https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/user.svg`;
        } else if (headerIcons[index].firstElementChild.className == "px-2 py-1 d-inline wishlist-heart") {
            headerIcons[
                index
            ].firstElementChild.firstElementChild.src = `https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/heart.svg`;
        } else if (headerIcons[index].firstElementChild.firstElementChild.className == "p-1 mob-cart-btn cart-bag") {
            headerIcons[
                index
            ].firstElementChild.firstElementChild.firstElementChild.src = `https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/bag.svg`;
        }
    }
}

function OurProductContent() {
    let introContent = document.getElementsByClassName("intro-content")[0];
    introContent.firstElementChild.firstElementChild.classList.add("intro_content");
}

function JQ() {
    $(document).ready(function () {
        console.log('test');
        $("#accordion i").click(function () {
            //slide up all the link lists
            //console.log('accordion clicked')
            $(this).removeClass('expanded');
            $(this).html('+');
            $("#accordion ul div").slideUp(200);
            $("#accordion ul i img").attr(
                "src",
                "https://bitbucket.org/TSTechsembly/jscss/raw/89328e0454b22f0fdcb3cda1dc263c26c9b90fc8/fft/images/plus-circle.svg"
            );

            //slide down the link list below the h3 clicked - only if its closed
            if (!$(this).next().is(":visible")) {
                $(this).addClass('expanded');
                $(this).html('-');
                $(this).next().slideDown(200);
                $(this).parent().prev().find('i').removeClass('expanded');
                $(this).parent().prev().find('i').html('+');
                $(this).parent().next().find('i').removeClass('expanded');
                $(this).parent().next().find('i').html('+');
                $(this)
                    .children("img")
                    .attr(
                        "src",
                        "https://bitbucket.org/TSTechsembly/jscss/raw/89328e0454b22f0fdcb3cda1dc263c26c9b90fc8/fft/images/minus-circle.svg"
                    );
            }
        });
    });

    $(function () {
        //FastClick.attach(document.body);
    });
}

function waitForEl(selector, callback){
    var poller1 = setInterval(function(){
        var elem = document.getElementsByClassName(selector)[0];
        if(elem === undefined){
            return;
        }
        clearInterval(poller1);
        callback(elem)
    },100);
}

waitForEl("menu-holder", SetHeader)

/*Javascript by Imran */
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
    console.log(c)
  }
  return "";
}
var currency;
function getCookies() {
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    var lastIndex = c.lastIndexOf('=');
    var cVal = c.slice(lastIndex + 1);
    var cName = c.slice(0, lastIndex);
    var lastUnderScore = cName.lastIndexOf('_');
    const res_cname = cName.slice(lastUnderScore + 1);
    if (res_cname == 'curency'){
      currency = cVal;
    }
    //console.log(res_cname);
    console.log(currency);
  }
  return "";
}


function JQ_new() {
    // $("#accordion i").click(function () {
    //   console.log('i clicked')
    // });
  //var currency = getCookie("curency");
  getCookies()
  setInterval(function () {
    $('.notificationbar').each(function(){
        if(!$(this).hasClass('modified')){
            $(this).addClass('modified');
            $(this).html(function(i,o) {
              return o.replace('Click here','<a href="https://www.fitnessfirst.com.sg/contact-us">Click here</a>');
            });
        }
    });
    if(!$('header.header .toggle-menu-cont').hasClass('col-lg-3')){
        $('header.header .toggle-menu-cont').addClass('col-lg-3').removeClass('col-lg-4');
    }
    if(!$('header.header .menu-holder.UpperNav').parent('div').hasClass('col-lg-6')) {
      $('header.header .menu-holder.UpperNav').parent('div').addClass('col-lg-6').removeClass('col-lg-4');
    }
    if(!$('header.header .menu-links-cont').hasClass('col-lg-3')){
        $('header.header .menu-links-cont').addClass('col-lg-3').removeClass('col-lg-4');
    }

    if (document.body.getAttribute("data-pagetype") != "home") {
      if (!$('app-newsletter').find('.home-intro').length) {
        $('app-newsletter').prepend('<section class="home-intro">'+
          '<div class="container home-intro-container">'+
            '<h2 class="text-center">WHY OUR PRODUCTS</h2>'+
            '<div class="intro-content" style="background-color: ;text-align: center;color: #000000">'+
              '<div>'+
                '<p style="color: #000000">'+
                'Staying true to Fitness First purpose - '+
                '"We are the fitness leaders who inspire people to go further'+
                'in life", we&apos;re always looking for new ways to support and inspire '+
                'you and not just in the gym. You may train confidently' +
                'with our certified fitness coaches who are the most experienced in '+
                'Singapore to achieve your goals. Partnering with Innermost and Vitruvian, we aims to support you in your fitness and wellness journey no matter what your goal is in life or where you are.</p>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</section>');
      }
    }
    $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#details-tab h4:not(.modified)').addClass('modified').html('Specification');
    $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#askseller-tab h4:not(.modified)').addClass('modified').html('Ask Question');
    $('.product-detail-container .personalise-form .form-group .order-btn#add-to-basket-btn:not(.modified)').addClass('modified').html('BUY NOW');

    $('.partners-note p:not(.modified)').addClass('modified').each(function () {
      $(this).text('Thank you for your enquiry. We will get back to you as soon as possible.');
    });
    let productDetailCont = $('.product-detail-container .product-holder .product-order .tab-content .detail-text');

    if (productDetailCont.find("div.xyz").length) {
      productDetailCont.closest('.product-detail-container:not(.variable-product)').addClass('variable-product');
      if(!productDetailCont.hasClass('moved')) {
        productDetailCont.addClass('moved');
        productDetailCont.appendTo('.product-detail-container');
        productDetailCont.find(".show-more-cont").remove();
        // productDetailCont.innerHTML += document.getElementById("details").innerHTML;
        $("#details-tab").remove();
        $("#askseller-tab").remove();
        $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#delivery-tab h4:not(.modified)').addClass('modified').html('Ingredients');
      }
    }
    $('app-related-products[title="More items from"] h3').each(function(){
      if(!$(this).hasClass('modified')){
          $(this).addClass('modified');
          $(this).html('You might like these products too');
      }
    });

    //console.log(currency);
    $('.related-products .product-content .product-pricing p').each(function(){
      if(!$(this).find('.ccy').length) {
        $(this).prepend("<span class='ccy mr-1'>"+ currency + "</span>");
      }
    });
    $('.wishlist-container .product-content .product-pricing p').each(function(){
      if(!$(this).find('.ccy').length) {
        $(this).prepend("<span class='ccy mr-1'>"+ currency + "</span>");
      }
    });
    $('.user-container .balance-form').closest('.user-container:not(.d-none)').addClass('d-none');
    $('.user-container .ts-balance-form').closest('.user-container:not(.d-none)').addClass('d-none');
    $('.user-heading .wishlist-link').closest('.user-heading:not(.d-none)').addClass('d-none');
    $('.user-wrapper .user-container .order-items-details .order-item .contact-btn:contains("Contact Seller")').each(function(){
      if(!$(this).hasClass('modified')){
          $(this).addClass('modified');
          $(this).attr('href','https://www.fitnessfirst.com.sg/contact-us');
      }
    });
    $('.user-wrapper .user-container .order-items-details .order-item .contact-btn:contains("GLO Support"):not(.d-none)').addClass('d-none');
    $('.cart-item#line-item-26056 .item-details input[type="number"]:not(.disabled)').addClass('disabled').attr('disabled','disabled');
    $('.custom-container.checkout-footer img[alt="securepaymentbadge"]').each(function(){
      if(!$(this).hasClass('modified')){
        $(this).addClass('modified');
        $(this).attr('src', 'https://bitbucket.org/TSTechsembly/jscss/raw/16a0a611352f081284a718684aab2e466e9dece5/fft/images/secure-payments.png');
      }
    });
    $('.footer .copyright-cont .pay-methods-avail ').each(function(){
        if(!$(this).hasClass('modified')){
            $(this).addClass('modified');
            $(this).find('.meth-link:not(.new)').addClass('d-none');
            $(this).append('<!--div class="meth-link new"><img alt="apple-pay" src="https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/apple-pay.svg" width="52"></div-->'+
            '<div class="meth-link new"><img alt="master-card" src="https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/master-card.svg" width="52"></div>'+
            '<!--div class="meth-link new"><img alt="paypal" src="https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/paypal.svg" width="52"></div-->'+
            '<div class="meth-link new"><img alt="visa" src="https://bitbucket.org/TSTechsembly/jscss/raw/9670cb0e131095e18d81028c869edfeafc600b99/fft/images/visa.svg" width="52"></div>');
        }
    });
    $(".select-form.form-group input[placeholder='Preferred-date']").each(function(){
        if(!$(this).hasClass('testing')){
            $(this).addClass('testing');
            var days = 7;
            var today = new Date();
            var enday = new Date(Date.now() + (days * 24 * 60 * 60 * 1000));
            var tdd = today.getDate() + 7;
            var tmm = today.getMonth() + 1;
            var tyyyy = today.getFullYear();
            var edd = enday.getDate();
            var emm = enday.getMonth() + 1;
            var eyyyy = enday.getFullYear();
            if (tdd < 10) {
              tdd = '0' + tdd;
            }
            if (tmm < 10) {
              tmm = '0' + tmm;
            }
            if (edd < 10) {
              edd = '0' + edd;
            }
            if (emm < 10) {
              emm = '0' + emm;
            }
            today = tyyyy + '-' + tmm + '-' + tdd;
            // enday = eyyyy + '-' + emm + '-' + edd;
            $(".testing").attr("min", today);
            $(".testing").attr("max", enday);
            $(".testing").attr("placeholder", "mm/dd/yyyy");
        }
      });
      $('.product-detail-container .personalise-form .form-group label[for="Preferred-date"]').each(function(){
        if(!$(this).hasClass('modified')){
            $(this).addClass('modified');
            $(this).html('Preferred Date: *');
        }
      });
      $(".mat-form-field-label").each(function(){
        if($(this).attr('aria-owns')) {
          $(this).removeAttr('aria-owns');
        }
      });
    });


}
if ( ! window.deferAfterjQueryLoaded ) {
    window.deferAfterjQueryLoaded = [];
    Object.defineProperty(window, "$", {
        set: function(value) {
            window.setTimeout(function() {
                $.each(window.deferAfterjQueryLoaded, function(index, fn) {
                    fn();
                });

            }, 0);
            Object.defineProperty(window, "$", { value: value });
        },

        configurable: true
    });
}

window.deferAfterjQueryLoaded.push(function() {
  JQ_new();
  JQ();
});
