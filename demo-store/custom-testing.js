function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }
        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });
        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
function addPromoDropDowns(){
  var promoDropDowns = '<div class="col-12 d-flex justify-content-between flex-wrap promo-options-cont px-0">'+
  '<div class="col-12 col-md-6 pl-md-0 form-group">'+
  '<select class="form-control" name="bank_name" id="bank_name">'+
  '<option disabled="disabled" selected="selected" value="">Select Bank</option>'+
  '<option value="citi">CITI Bank</option>'+
  '<option value="ocbc">OCBC</option>'+
  '<option value="gourmet">Hotel Gourmet</option>'+
  '</select>'+
  '</div>'+
  '<div class="col-12 col-md-6 pr-md-0 form-group">'+
  '<select class="form-control" name="card_type" id="card_type">'+
  '<option disabled="disabled" selected="selected" value="">Select Card Type</option>'+
  '<option value="visa">Visa</option>'+
  '<option value="master-card">Master Card</option>'+
  '<option value="others">Others</option>'+
  '</select>'+
  '</div>'+
  '</div>';
  var elem = $(".payments-methods-cont")[0];
  elem.insertAdjacentHTML('afterbegin', promoDropDowns);
}
function addScript(){
  var scriptTag = '<script>'+
    'var form_id = "jquery_form";'+

    'var data = { "access_token": "5f1f7hhgvy9ekwe3l90izr9v"};'+

    'function onSuccess() {'+
        'window.location = window.location.pathname + "?message=Email+Successfully+Sent%21&isError=0";'+
    '}'+

    'function onError(error) {'+
        // remove this to avoid redirect
        'window.location = window.location.pathname + "?message=Email+could+not+be+sent.&isError=1";'+
    '}'+

    'var sendButton = $("#" + form_id + " [name='+'send'+']");'+

    'function send() {'+
        'sendButton.val("Sending…");'+
        'sendButton.prop("disabled",true);'+

        'var subject = $("#" + form_id + " [name='+'subject'+']").val();'+
        'var message = $("#" + form_id + " [name='+'text'+']").val();'+
        'data["subject"] = subject;'+
        'data["text"] = message;'+

        '$.post("https://postmail.invotes.com/send", data, onSuccess).fail(onError);'+

        'return false;'+
    '}'+

    'sendButton.on('+'click'+', send);'+

    'var $form = $("#" + form_id);'+
    '$form.submit(function( event ) {'+
        'event.preventDefault();'+
    '});'+
  '</script>';
  var elemBody = $("body")[0];
  elemBody.insertAdjacentHTML('beforeend', scriptTag);
}
function redirect(){
  var type = document.getElementById('type').value;
  var location_cont = document.getElementById('location').value;
  if (type == "Anklets" && location_cont == "Asia") {
        location = 'https://ahsun-demo.techsembly.com/buttsaab?filters=%5B%7B%2240%22:%5B%22Anklets%22%5D,%22112%22:%5B%22%20Asia%22%5D%7D,%22%22%5D&price=%5B0,955,%22SGD%22%5D';
  }
  else {

  }
  return false;
}
function redirect2(){
  location = 'https://demo-store5.techsembly.com/sigven/';
  return false;
}
(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    /*setTimeout(function() {
      document.getElementsByTagName("body")[0].append(
        '<script>'+
          'var form_id = "jquery_form";'+

          'var data = { "access_token": "5f1f7hhgvy9ekwe3l90izr9v"};'+

          'function onSuccess() {'+
              'window.location = window.location.pathname + "?message=Email+Successfully+Sent%21&isError=0";'+
          '}'+

          'function onError(error) {'+
              // remove this to avoid redirect
              'window.location = window.location.pathname + "?message=Email+could+not+be+sent.&isError=1";'+
          '}'+

          'var sendButton = $("#" + form_id + " [name='+'send'+']");'+

          'function send() {'+
              'sendButton.val("Sending…");'+
              'sendButton.prop("disabled",true);'+

              'var subject = $("#" + form_id + " [name='+'subject'+']").val();'+
              'var message = $("#" + form_id + " [name='+'text'+']").val();'+
              'data["subject"] = subject;'+
              'data["text"] = message;'+

              '$.post("https://postmail.invotes.com/send", data, onSuccess).fail(onError);'+

              'return false;'+
          '}'+

          'sendButton.on('+'click'+', send);'+

          'var $form = $("#" + form_id);'+
          '$form.submit(function( event ) {'+
              'event.preventDefault();'+
          '});'+
        '</script>'
      );
  }, 500);*/

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
          waitForElm('.payment-form-container .payments-methods-cont').then((elm) => {
            addPromoDropDowns();
            $('select#card_type').on('change', function() {
              var bank_name = $('select#bank_name').val();
              var promo_input = $('.payment-form-container input#promo-code');
              var promo_code;
              if (bank_name == 'citi' && this.value == 'visa') {
                promo_code = 'citi 15%';
                alert('Please apply promo code ' + promo_code);
                //promo_input.val(promo_code);
              }
              else if (bank_name == 'ocbc' && this.value == 'master-card') {
                promo_code = 'ocbc 20%';
                alert('Please apply promo code ' + promo_code);
                //promo_input.val(promo_code);
              }
              else if (bank_name == 'gourmet' && this.value == 'others') {
                promo_code = 'gourmet 25%';
                alert('Please apply promo code ' + promo_code);
                //promo_input.val(promo_code);
              }

              if (promo_code != '') {
                //promo_input.blur();
                /*promo_input.closest('.form-group').removeClass('collapse');
                promo_input.closest('form').removeClass('ng-pristine ng-untouched').addClass('ng-touched ng-dirty');
                promo_input.removeClass('ng-pristine ng-untouched').addClass('ng-touched ng-dirty');*/
                //promo_input.closest('form').find('.apply-btn').removeAttr('disabled');
              }
            });
          }).catch((error) => {});

          // code for test form submission with prevent default
          waitForElm('#my-form').then((elm) => {
           var form = document.getElementById("my-form");

           async function handleSubmit(event) {
            event.preventDefault();
            var status = document.getElementById("my-form-status");
            var data = new FormData(event.target);
            fetch(event.target.action, {
              method: form.method,
              body: data,
              headers: {
                'Accept': 'application/json'
              }
            }).then(response => {
              if (response.ok) {
                status.innerHTML = "Thanks for your submission!";
                form.reset();
                location = 'https://demo-store5.techsembly.com/sigven/pages/thank-you';
              } else {
                response.json().then(data => {
                  if (Object.hasOwn(data, 'errors')) {
                    status.innerHTML = data["errors"].map(error => error["message"]).join(", ")
                  } else {
                    status.innerHTML = "Oops! There was a problem submitting your form"
                  }
                })
              }
            }).catch(error => {
              status.innerHTML = "Oops! There was a problem submitting your form"
            });
          }
          form.addEventListener("submit", handleSubmit)

        }).catch((error) => {});



          /*waitForElm('#delivery-options-popup').then((elm) => {
            var radios = document.querySelectorAll('#delivery-options-popup .radio-container input[type="radio"]');

            for (const radioButton of radios) {
              radioButton.addEventListener('change', showSelected);
            }

            function showSelected(e) {
                console.log(e);
                if (this.checked) {

                  var val = this.value;
                  //setTimeout(function() {
                    for (var j = 0; j < radios.length; j++) {
                        if (radios[j].value == val) {
                            //radios[j].setAttribute("checked", "checked");
                            radios[j].checked = true;
                            console.log(radios[j]);
                            //break;
                        }
                        else {
                          //radios[j].setAttribute("checked", "");
                          radios[j].checked = false;
                        }
                    }
                    console.log(val);

                  //}, 200);
                }
            }
            /*$('#delivery-options-popup input[name^="opt-delivery-"]').change(function() {
              var val = $(this).val();
              console.log(val);
              $('#delivery-options-popup input[name^="opt-delivery-"]').val(val);
            });*/
          /*}).catch((error) => {});*/
          //$( "#form-test" ).each(function(){
            /*var form_id = "form-test";
            var data = {
                "access_token": "5f1f7hhgvy9ekwe3l90izr9v" // sent after you sign up
            };

            function onSuccess() {
                // remove this to avoid redirect
                window.location = window.location.pathname + "?message=Email+Successfully+Sent%21&isError=0";
            }

            function onError(error) {
                // remove this to avoid redirect
                window.location = window.location.pathname + "?message=Email+could+not+be+sent.&isError=1";
            }
            var values = form_id.serializeArray();

            var inputs = {};
            $.each(values, function(k, v){
                inputs[v.name]= v.value;
            });
            var sendButton = $("#submitBtn");
            function send() {
                sendButton.val('Sending…');
                sendButton.prop('disabled',true);

                var subject = 'Form Data';
                var message = JSON.Stringify(inputs);

                data['subject'] = subject;
                data['text'] = message;

                $.post('https://postmail.invotes.com/send',
                    data,
                    onSuccess
                ).fail(onError);

                return false;
            }
            sendButton.on('click', send);
            var $form = $("#" + form_id);
             $form.submit(function( event ) {
                 event.preventDefault();
             });*/




          /*$( "#form-test" ).submit(function(event) {
            event.preventDefault();
            var values = $(this).serializeArray();

            var form = $(this);
            var actionUrl = form.attr('action');
            var inputs = {};
            $.each(values, function(k, v){
                inputs[v.name]= v.value;
            });
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: inputs,
                success: function(data)
                {
                  console.log(data);
                }
            });
          });*/

          setTimeout(function() {

          }, 2000);

          setInterval(function() {
            $(".select-form.form-group input[placeholder='Preferred-date ']").each(function(){
                if(!$(this).hasClass('testing')){
                    $(this).addClass('testing');
                    var days = 7;
                    var today = new Date();
                    var enday = new Date(Date.now() + (days * 24 * 60 * 60 * 1000));
                    var tdd = today.getDate() + 7;
                    var tmm = today.getMonth() + 1;
                    var tyyyy = today.getFullYear();
                    var edd = enday.getDate();
                    var emm = enday.getMonth() + 1;
                    var eyyyy = enday.getFullYear();
                    if (tdd < 10) {
                      tdd = '0' + tdd;
                    }
                    if (tmm < 10) {
                      tmm = '0' + tmm;
                    }
                    if (edd < 10) {
                      edd = '0' + edd;
                    }
                    if (emm < 10) {
                      emm = '0' + emm;
                    }
                    today = tyyyy + '-' + tmm + '-' + tdd;
                    // enday = eyyyy + '-' + emm + '-' + edd;
                    $(".testing").attr("min", today);
                    $(".testing").attr("max", enday);
                    $(".testing").attr("placeholder", "mm/dd/yyyy");
                }
            });
            $('#delivery-options-popup').each(function(){
              var radios = document.querySelectorAll('#delivery-options-popup .radio-container input[type="radio"]');

              for (const radioButton of radios) {
                radioButton.addEventListener('change', showSelected);
              }

              function showSelected(e) {
                  console.log(e);
                  if (this.checked) {

                    var val = this.value;
                    var clickEvent = new Event('click');
                    //setTimeout(function() {
                      for (var j = 0; j < radios.length; j++) {
                          if (radios[j].value == val) {
                              //radios[j].setAttribute("checked", "checked");
                              radios[j].checked = true;
                              radios[j].dispatchEvent(clickEvent);
                              console.log(radios[j]);
                              //break;
                          }
                          else {
                            //radios[j].setAttribute("checked", "");
                            radios[j].checked = false;
                          }
                      }
                      console.log(val);

                    //}, 200);
                  }
              }
            });


          });


        });
    });
})();
