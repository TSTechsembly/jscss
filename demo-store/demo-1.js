function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }
        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });
        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
(function() {
  var script = document.createElement("SCRIPT");
  script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
  script.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script);

  let timeout;

  function myFunction() {
    timeout = setTimeout(alertFunc, 3000);
  }

  function alertFunc() {
    console.log("test demo 1");
  }

  myFunction();
  // Poll for jQuery to come into existence
  var checkReady = function(callback) {
      if (window.jQuery) {
          callback(jQuery);
      }
      else {
          window.setTimeout(function() { checkReady(callback); }, 20);
      }
  };

  // Now lets do something
  checkReady(function($) {
      $(function() {
        var href = window.location.href;
        var anchor = href.substring(href.indexOf("#") + 1);
        var div = '#'+anchor;
        console.log(div);
        setTimeout(function() {
          $('html, body').animate({
             scrollTop: $(div).offset().top
          }, 2000);
          return false;
        }, 1000);

        waitForElm('.payment-form-container').then((elm) => {
          $('input[formcontrolname="enableCheckout"]').closest('.card:not(.enableCheckout-holder)').addClass('enableCheckout-holder');
          $('input[formcontrolname="enableCheckout"]')[0].click();
          $('.enableCheckout-holder').insertAfter($(".card-details-cont")[0]);


        }).catch((error) => { });

        setInterval(function() {
          $("[id='select-tag-Type of Room'] option:contains(1NIGHT)").each(function() {
            if (!$(this).hasClass('modified-1')) {
              $(this).addClass('modified-1');
              $(this).html($(this).html().replace(/1NIGHT/g, ' '));
            }
          });
          $("[id='select-tag-Type of Room'] option:contains(2NIGHTS)").each(function() {
            if (!$(this).hasClass('modified-2')) {
              $(this).addClass('modified-2');
              $(this).html($(this).html().replace(/2NIGHTS/g, ' '));
            }
          });
          //$('.enableCheckout-holder').insertAfter($(".card-details-cont")[0]);
        });

      });
  });

})();
