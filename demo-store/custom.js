function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }
        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });
        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
function addPromoDropDowns(){
  var promoDropDowns = '<div class="col-12 d-flex justify-content-between flex-wrap promo-options-cont px-0">'+
  '<div class="col-12 col-md-6 pl-md-0 form-group">'+
  '<select class="form-control" name="bank_name" id="bank_name">'+
  '<option disabled="disabled" selected="selected" value="">Select Bank</option>'+
  '<option value="citi">CITI Bank</option>'+
  '<option value="ocbc">OCBC</option>'+
  '<option value="gourmet">Hotel Gourmet</option>'+
  '</select>'+
  '</div>'+
  '<div class="col-12 col-md-6 pr-md-0 form-group">'+
  '<select class="form-control" name="card_type" id="card_type">'+
  '<option disabled="disabled" selected="selected" value="">Select Card Type</option>'+
  '<option value="visa">Visa</option>'+
  '<option value="master-card">Master Card</option>'+
  '<option value="others">Others</option>'+
  '</select>'+
  '</div>'+
  '</div>';
  var elem = $(".payments-methods-cont")[0];
  elem.insertAdjacentHTML('afterbegin', promoDropDowns);
}
(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
          waitForElm('.payment-form-container .payments-methods-cont').then((elm) => {
            addPromoDropDowns();
            var putCursorAtEnd = function(jQueryInput) {
              var input = jQueryInput[0];
              // works for Chrome, FF, Safari, IE9+
              if (input.setSelectionRange) {
                var len = jQueryInput.val().length;
                input.setSelectionRange(len, len);
              }
            };
            $('select#card_type').on('change', function(e) {
              var bank_name = $('select#bank_name').val();
              var promo_input = $('.payment-form-container input#promo-code');
              var promo_code;
              if (bank_name == 'citi' && this.value == 'visa') {
                promo_code = 'citi 15%';
                alert('Please apply promo code ' + promo_code + ' by clicking apply button ');
                promo_input.val(promo_code);
                /*if (!promo_input.is(":focus")) {
                  e.preventDefault();
                  // but this also cancels the focus, so we must trigger that manually
                  promo_input.focus();
                  putCursorAtEnd(promo_input);
                }*/
              }
              else if (bank_name == 'ocbc' && this.value == 'master-card') {
                promo_code = 'ocbc 20%';
                alert('Please apply promo code ' + promo_code + ' by clicking apply button ');
                promo_input.val(promo_code);
              }
              else if (bank_name == 'gourmet' && this.value == 'others') {
                promo_code = 'gourmet 25%';
                alert('Please apply promo code ' + promo_code + ' by clicking apply button ');
                promo_input.val(promo_code);
              }

              if (promo_code != '') {
                //promo_input.blur();
                promo_input.closest('.form-group').removeClass('collapse');
                promo_input.closest('form').removeClass('ng-pristine ng-untouched').addClass('ng-touched ng-dirty');
                promo_input.removeClass('ng-pristine ng-untouched').addClass('ng-touched ng-dirty');

                setTimeout(function() {
                  promo_input.focus();

                  var ev = new Event('input');

                  document.getElementById('promo-code').value = document.getElementById('promo-code').value + ' ';
                  document.getElementById('promo-code').dispatchEvent(ev);

                  //putCursorAtEnd(promo_input);
                  //promo_input.val(promo_input.val() + ' ');
                }, 200);
                //promo_input.closest('form').find('.apply-btn').removeAttr('disabled');
              }
            });

          }).catch((error) => {});

          setTimeout(function() {

          }, 2000);

          setInterval(function() {
            $(".select-form.form-group input#Preferred-date").each(function(){
                if(!$(this).hasClass('testing')){
                    $(this).addClass('testing');
                    var days = 7;
                    var today = new Date();
                    var enday = new Date(Date.now() + (days * 24 * 60 * 60 * 1000));
                    var tdd = today.getDate() + 7;
                    var tmm = today.getMonth() + 1;
                    var tyyyy = today.getFullYear();
                    var edd = enday.getDate();
                    var emm = enday.getMonth() + 1;
                    var eyyyy = enday.getFullYear();
                    if (tdd < 10) {
                      tdd = '0' + tdd;
                    }
                    if (tmm < 10) {
                      tmm = '0' + tmm;
                    }
                    if (edd < 10) {
                      edd = '0' + edd;
                    }
                    if (emm < 10) {
                      emm = '0' + emm;
                    }
                    today = tyyyy + '-' + tmm + '-' + tdd;
                    // enday = eyyyy + '-' + emm + '-' + edd;
                    $(".testing").attr("min", today);
                    $(".testing").attr("max", enday);
                    $(".testing").attr("placeholder", "mm/dd/yyyy");
                }
            });

          });
        });
    });
})();
