function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }
    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });
    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }

    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
          $('head').append('<link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@400;500;600;700&display=swap" rel="stylesheet"></link>');
          $('head').append('<link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@400;500;600;700&family=Open+Sans:wght@300;400;500;600;700&display=swap" rel="stylesheet"></link>');

          setInterval(function () {

            // header
            $('.shipping #dropdownMenuButton').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).text('SGD');
              }
            });

            $('.search .btn-search').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                // $(this).find('img').attr('src', 'https://static.techsembly.com/5wsERQMeaQDqjXUBNiiG7JtD')
                $(this).append('<img class="search-icon" alt="search image" src="https://static.techsembly.com/5wsERQMeaQDqjXUBNiiG7JtD">');
              }
            });


            $('#profile-icon-sign-partial').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).append('<img class="profile-icon" alt="search image" src="https://static.techsembly.com/QsbArgHaQV5XLYgwuFP9SB68">');
              }
            });


            $('#wishlist-heart-cart-partial').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).append('<img class="wishlist-icon" alt="search image" src="https://static.techsembly.com/YDG7QxxMkm1RP7797m9FEosj">');
              }
            });


            $('#cart-bag-partial').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).append('<img class="cart-icon" alt="search image" src="https://static.techsembly.com/snTBRNTLWKuZP2XBE9hCVWuq">');
              }
            });

            $('.promo-section .promo-box .promo-content').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).append('<span class="multi-banner-discover">DISCOVER MORE</span>');
              }
            });

            $('.home-banner, .gift-section a, .promo-section .promo-box').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).prepend('<div class="banner-overlay"></div>');
              }
            });

            // footer social icons

            $('.footer-widget .social-links').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).append(`
                <li class="social-list"><img class="facebook" alt="search image" src="https://static.techsembly.com/fDCpqHhhukKAjvdYvp5kpLk9"></li>
                <li class="social-list"><img class="instagram" alt="search image" src="https://static.techsembly.com/K1JstsS8BACX7H9chFQ4J45W"></li>
                <li class="social-list"><img class="linkedin" alt="search image" src="https://static.techsembly.com/ojwFaAVRLu8a1gVnXbYVQqYc"></li>
                `);

              }
            });

            // owl arrows

            $('.owl-carousel .owl-nav button').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).text('').append('<img class="owl-next-icon" alt="search image" src="https://static.techsembly.com/Vj5g2B1cqes1ZcFgo4LdNLww">');
              }
            });

            $('#details-tab').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).parent().addClass('d-none');
              }
            });
            $('.custom-container.cart-container .vendor-items-cont .edit-item-new').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).text('').append('<img class="edit-icon" alt="search image" src="https://static.techsembly.com/NPYRdytipVjtSV2vKtzLvrV6">');
              }
            });

            $('.edit-contact, .edit-address').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).find('img').attr('src','https://static.techsembly.com/NPYRdytipVjtSV2vKtzLvrV6');
              }
            });

            $('.cart-item.new .item-qty-cont .item-dec').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).text('').append('<img class="edit-icon" alt="search image" src="https://static.techsembly.com/Vj5g2B1cqes1ZcFgo4LdNLww">');
              }
            });

            $('.cart-item.new .item-qty-cont .item-inc').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).text('').append('<img class="edit-icon" alt="search image" src="https://static.techsembly.com/Vj5g2B1cqes1ZcFgo4LdNLww">');
              }
            });


            $('.custom-container.cart-container .login-cart-panel').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).prepend('<span class="cart-login-note">Please note this login is for Goodwood Park Singapore gifting platform only.</span>');
              }
            });

            $('.guest-form-cont').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).append('<a href="#" class="cart-account-note-link"><span class="cart-account-note">You can create account after checkout</span></a>');
              }
            });

            $('.custom-container.cart-container .vendor-items-cont .card .card-header .flag-cont span').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).text('Shipping From Singapore').prepend('<img class="cart-flag-img" alt="search image" src="https://static.techsembly.com/H5awccKu5kT2uWL51Yff7Y43"> ');
              }
            });





            $('.custom-container.cart-container .continue-btn-new').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).text('Return TO shopping');
              }
            });

            $('app-related-products').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
               var related_title = $(this).attr('title');
               if(related_title === "Recently Viewed"){
                $(this).addClass('d-none');
               }
              }
            });

            $('.address-form.new #country option[selected="selected"]').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).text('Country');
              }
            });

            $('.address-form.new #region').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).attr('placeholder','Unit/Apartment No. (if applicable)');
              }
            });

            $('.address-form.new #phone').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).attr('placeholder','Phone Number');
              }
            });

            // copyright chectout packag


            // $('app-cart footer').each(function () {
            //   if (!$(this).hasClass('modified-footer')) {
            //     $(this).addClass('modified');
            //     $(this).parent().next().addClass('modified-footer');
            //     $('.footer.modified-footer .custom-container').html('<div class="col-lg-12"><span class="modified-copyright">Techsembly&copy; 2021 </span></div>');
            //     console.log('a')
            //   }
            // });


            // $('.footer .custom-container').each(function () {
            //   if (!$(this).hasClass('modified-footer')) {
            //     $(this).addClass('modified');
            //     $(this).parent('.footer').addClass('modified-footer');
            //     $(this).html('<div class="col-lg-12"><span>Techsembly&copy; 2021 </span></div>');
            //   }
            // });

              $('.checkout .shipping-form-container').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).append('<div class="col-lg-12 p-0 modified-copyright-col"><span class="modified-copyright">Techsembly&copy; 2021 </span></div>');
              }
            });

            // checkout usd remove

          $('.cart-container .unit-price:contains("USD"),.cart-container .unit-total-price:contains("USD"), .cart-container .cart-total:contains("USD")').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              $(this).html($(this).html().split("USD").join(""));
            }
          });


          // $('.cart-container .more-items-product-prices:not(:contains("USD"))').each(function () {
          //   if (!$(this).hasClass('modified')) {
          //     $(this).addClass('modified');
          //     $(this).prepend('USD');
          //   }
          // });

          $('.delivery-methods-cont .vendor-cards-holder .card .order-item .item-price:contains("USD"), .delivery-methods-cont .vendor-cards-holder .card .shipping-totals-cont .total-row .total-row-val:contains("USD")').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              $(this).html($(this).html().split("USD").join(""));
            }
          });


          });

        });
    });

})();
