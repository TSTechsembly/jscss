function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }
    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });
    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {

          waitForElm('input#user-gift-amount').then((elm) => {
            var productSKU = parseInt($('.product-detail-container .product-holder .product-sku').html());



            //if (productSKU == '029589') {
              document.getElementById("user-gift-amount").addEventListener('focusout', function (evt) {
                var min = '200';
                var max = '20000';
                evt.target.setAttribute("value",evt.target.value);
                evt.target.setAttribute("min", '200');
                evt.target.setAttribute("max", '20000');
                var amount = evt.target.value;

                if (amount < 200) {
                  if (!$('.user-amount-holder .alert-danger.new').length) {
                    $('.user-amount-holder').append('<div class="alert-danger new">Minimum gift card amount is SGD $ 200</div>')
                  }
                  else {
                    $('.user-amount-holder').find('.alert-danger.new').removeClass('d-none');
                  }
                  $('.user-amount-holder').find('.alert-danger.new2').addClass('d-none');
                  $('.user-amount-holder').find('.alert-secondary.new').addClass('d-none');
                }
                else if (amount > 20000) {
                  if (!$('.user-amount-holder .alert-danger.new2').length) {
                    $('.user-amount-holder').append('<div class="alert-danger new2">Maximum gift card amount is SGD $ 20000</div>');
                  }
                  else {
                    $('.user-amount-holder').find('.alert-danger.new2').removeClass('d-none');
                  }
                  $('.user-amount-holder').find('.alert-danger.new').addClass('d-none');
                  $('.user-amount-holder').find('.alert-secondary.new').addClass('d-none');
                }
                else if (amount < 200 || amount > 20000) {
                   document.getElementById("user-gift-amount").classList.add('ng-invalid');
                   document.getElementById("user-gift-amount").classList.remove('ng-valid');
                   if (!$('.user-amount-holder .alert-secondary.new').length) {
                     $('.user-amount-holder').append('<div class="alert-secondary new">Please select the gift card value from SGD $ 200 to SGD $ 20000</div>')
                   }
                   else {
                     $('.user-amount-holder').find('.alert-secondary.new').removeClass('d-none');
                   }
                   $('.user-amount-holder').find('.alert-danger.new').addClass('d-none');
                   $('.user-amount-holder').find('.alert-danger.new2').addClass('d-none');
                }
                else if (amount > 200 || amount < 20000) {
                   document.getElementById("user-gift-amount").classList.remove('ng-invalid');
                }
              });
            //}
          }).catch((error) => {});

           setInterval(function() {
             var cardAmt = $('#user-gift-amount').val();
             console.log(cardAmt);
             $('#user-gift-amount').each(function(){
               if(!$(this).closest('fieldset').hasClass('user-amount-holder')){
                 $(this).closest('fieldset').addClass('user-amount-holder');
               }
             });
               $('.product-detail-container .alert-danger:contains("Minimum gift card amount is SGD $ 100")').each(function(){
                 if(!$(this).hasClass('modified')){
                   $(this).addClass('modified d-none');
                   //$(this).html('Minimum gift card amount is SGD $ 200');
                 }
               });


               $('.product-detail-container .alert-danger:contains("Maximum gift card amount is SGD $ 10000")').each(function(){
                 if(!$(this).hasClass('modified')){
                   $(this).addClass('modified d-none');
                   //$(this).html('Maximum gift card amount is SGD $ 20000');
                 }
               });

               $('.product-detail-container .alert-secondary:contains("SGD $ 100 to SGD $ 10000")').each(function(){
                 if(!$(this).hasClass('modified')){
                   $(this).addClass('modified d-none');
                   //$(this).html('Please select the gift card value from SGD $ 200 to SGD $ 20000');
                 }
               });

           });
        });
    });
})();
