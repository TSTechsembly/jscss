setInterval(function() {
  jQuery('.breadcrumb-item.last:contains(" TEST - Overnight Stay Gift Card  "):not(.target-product-appears)').addClass('target-product-appears').each(function() {

      $("[id='select-tag-Type of Room']").prop('disabled', true);

      $("[id='select-tag-Number of Nights']").change(function() {
      $("[id='select-tag-Type of Room'] option:eq(0)").prop('selected', true);

      var nights
      nights = $( "[id='select-tag-Number of Nights'] option:selected" ).text();

      	if (nights==="2 Nights "){

      		$("[id='select-tag-Type of Room'] option:contains(1NIGHT)").each(function() {
      		    $(this).prop('disabled', true).prop('hidden', true);
      		});

      		$("[id='select-tag-Type of Room'] option:contains(2NIGHTS)").each(function() {
                  $(this).prop('disabled', false).prop('hidden', false);
                  $(this).html($(this).html().replace(/2NIGHTS/g, ' '));
                  $(this).removeClass('d-none');
                  $("[id='select-tag-Type of Room'] option:contains(1NIGHT)").addClass('d-none');
      		});
      	}

      	if (nights==="1 Night ") {
      		$("[id='select-tag-Type of Room'] option:contains(2NIGHTS)").each(function() {
      		$(this).prop('disabled', true).prop('hidden', true);
      		});

      		$("[id='select-tag-Type of Room'] option:contains(1NIGHT)").each(function() {
                  $(this).prop('disabled', false).prop('hidden', false);
                  $(this).html($(this).html().replace(/1NIGHT/g, ' '));
                  $(this).removeClass('d-none');
                  $("[id='select-tag-Type of Room'] option:contains(2NIGHTS)").addClass('d-none');
      		});
      	}

      $("[id='select-tag-Type of Room']").prop('disabled', false);
      })


   });
}, 250);
