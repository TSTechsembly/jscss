(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {

            setTimeout(function () {

            }, 2000);
            setInterval(function () {
              $('.footer a:contains("(831)")').each(function(){
                var phone_no = "tel:" + $(this).html();
                if(!$(this).hasClass('tel-link')){
                  $(this).addClass('tel-link');
                  $(this).attr('href', phone_no);
                }
              });
              $('.footer a:contains("@quaillodge.com")').each(function(){
                var email_link = "mailto:" + $(this).html();
                if(!$(this).hasClass('email-link')){
                  $(this).addClass('email-link');
                  $(this).attr('href', email_link);
                }
              });
            });
        });
    });
})();
