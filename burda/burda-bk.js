function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }
    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });
    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

function addHomeBannerDiscover() {
  var discoverMoreBtn = '<div class="discover-btn-cont pb-2"><a class="btn btn-primary discover-btn mb-1">DISCOVER MORE</a></div>';
  var homeBanner = $('.home-banner .banner-heading')[0];
  homeBanner.insertAdjacentHTML('beforeend', discoverMoreBtn);
}

function addPromoHead() {
  var promoHead = '<h2 class="section-title text-center text-uppercase">Dining Experiences</h2>';
  var elemPromoHolder = $('.promo-section .promo-holder')[0];
  elemPromoHolder.insertAdjacentHTML('beforebegin', promoHead);
}

function addLoginCartNote() {
  var loginCartNote = "<div class='login-note'>Please note this login is for Lifestyle Asia gifting platform only.<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}

function addGuestCheckoutNote() {
  var checkoutNote = "<div class='checkout-note text-center w-100 pt-4'>If you do not have an account please use guest checkout. You will be given the option to save account information later in the checkout process<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}

function addcopyrightText() {
  var copyrightHtml = '<div class="container-fluid"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-12 col-lg-6 tc-rights-reserved pl-0">Techsembly© 2022</div></div></div>'
  var elem = $(".checkout-footer")[0]
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}

function addcopyrightToComplete() {
  var copyrightHtml = '<div class="card-footer-right"><a rhef="#">© 2022 Lifestyle Asia</a></div>'
  var elem = $(".order-confirm-wrapper .card-footer .col-12")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}

function addBackToHomeBtn() {
  var copyrightHtml = '<div class="d-flex justify-content-center items-center backToHomeBtn"><a href="https://burda-luxury.techsembly.com/">BACK TO HOME</a></div>'
  var elem = $(".order-confirm-wrapper .card-footer")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}

function addVendorBannerHead() {
  var discoverMoreBtn = '<div class="vendor-banner-content"><h1>Burda Luxury</h1><p><img src="https://static.techsembly.com/tcDrwCNwT71p7YtuEFpqAokf"/>1 The Knolls Sentosa Island, Singapore 098297</p><a class="dics-more" href="#">Discover More</a></div>';
  var homeBanner = $('.vendor-micro-cont .vendor-banner.burda-luxury')[0];
  homeBanner.insertAdjacentHTML('beforeend', discoverMoreBtn);
}

function addViewMoreLink(link) {
  var viewMoreLink = '<a class="ml-2 viewMoreLink" href="' + link + '">View More</a>'
  var elem = $(".vendor-micro-cont .container#best-seller-cont")[0]
  elem.insertAdjacentHTML('beforeend', viewMoreLink)
}

function redirect() {
  var type_val = document.getElementById('pro_type').value;
  var location_cont = document.getElementById('location').value;
  var site_url = window.location.pathname;

  if (type_val != "" && location_cont != "") {
    location = site_url + '?filters=%5B%7B%2240%22:%5B%22' + type_val + '%22%5D,%22112%22:%5B%22%20' + location_cont + '%22%5D%7D';
    //location = site_url+'?filters='+type_val+'&'+location_cont+'';
  } else { }
  return false;
}
(function () {
  var script = document.createElement("SCRIPT");
  script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
  script.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script);


  var newStyle = document.createElement('style');
  newStyle.appendChild(document.createTextNode("\
    @font-face {\
      font-family: 'Optima';\
      src: url('https://bitbucket.org/TSTechsembly/jscss/raw/81ec4b7eef2f7717f8b0ab24a7c00732adffded2/burda/fonts/optima/OPTIMA.woff') format('woff'); \
      font-style: normal;\
      font-weight: normal;\
    }\
    @font-face {\
      font-family: 'Optima Italic';\
      src: url('https://bitbucket.org/TSTechsembly/jscss/raw/81ec4b7eef2f7717f8b0ab24a7c00732adffded2/burda/fonts/optima/Optima_Italic.woff') format('woff'); \
      font-weight: normal;\
    }\
    @font-face {\
      font-family: 'Optima Medium';\
      src: url('https://bitbucket.org/TSTechsembly/jscss/raw/33ba165ceb52d337f9e57af47edd2dff535ee41a/burda/fonts/optima/Optima_Medium.woff') format('woff'); \
    }\
    @font-face {\
      font-family: 'Optima Bold';\
      src: url('https://bitbucket.org/TSTechsembly/jscss/raw/33ba165ceb52d337f9e57af47edd2dff535ee41a/burda/fonts/optima/OPTIMA_B.woff') format('woff'); \
    }\
    @font-face {\
      font-family: 'Roboto', sans-serif;\
      src: url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap'); \
    }\
    "));

  document.head.appendChild(newStyle);

  // Poll for jQuery to come into existence
  var checkReady = function (callback) {
    if (window.jQuery) {
      callback(jQuery);
    } else {
      window.setTimeout(function () {
        checkReady(callback);
      }, 20);
    }
  };

  // Now lets do something
  checkReady(function ($) {
    $('head').append('<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">');

    $(function () {
      waitForElm('.home-banner .banner-heading').then((elm) => {
        addHomeBannerDiscover();
      }).catch((error) => { });

      waitForElm('.promo-section').then((elm) => {
        addPromoHead();
      }).catch((error) => { });
      waitForElm('.cart-right-cont').then((elm) => {
        addLoginCartNote();
        addGuestCheckoutNote();
      }).catch((error) => { });
      waitForElm('app-footer-checkout').then((elm) => {
        addcopyrightText()
      }).catch((error) => { });
      waitForElm('.order-confirm-wrapper .card-footer').then((elm) => {
        addBackToHomeBtn()
      }).catch((error) => { });
      waitForElm('.order-confirm-wrapper .card-footer .col-12').then((elm) => {
        addcopyrightToComplete()
      }).catch((error) => { });
      waitForElm('.vendor-micro-cont .vendor-banner.burda-luxury').then((elm) => {
        addVendorBannerHead()
      }).catch((error) => { });

      setTimeout(function () {
        $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
      }, 2000);

      setTimeout(function () {

        var vendor_map;
        var lastSegment;
        var url = window.location.href;
        var vendor_url = url.split('/');

        lastSegment = vendor_url.pop() || vendor_url.pop();
        // console.log(lastSegment);
        var bestSellerTitleInt;
        var prosLinkInt;

        if (lastSegment == 'burda-luxury-UTSs') {
          vendor_map = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8909.953615060807!2d-0.14831632212806356!3d51.50566242702753!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760528e7cb6aa7%3A0x3f53197818cb259!2s7-12%20Half%20Moon%20St%2C%20London%20W1J%207BH%2C%20UK!5e0!3m2!1sen!2s!4v1663744460868!5m2!1sen!2s" width="524" height="402" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';



          $('.vendor-micro-cont .container#best-seller-cont .vendor-heading').each(function () {

            bestSellerTitleInt = "Popular Experiences By Burda Luxury";
            prosLinkInt = $(this).find("a:last-child").attr("href");
            // console.log(prosLinkInt);
            if (prosLinkInt != null && (!$(this).hasClass('modified-ajax'))) {
              $(this).html('<b>' + bestSellerTitleInt + '</b>');
              $(this).addClass('modified-ajax');
              $(this).closest('.vendor-heading').addClass('text-center');
              addViewMoreLink(prosLinkInt);
            }
          });
        }
        if (lastSegment == '137-pillars-bangkok-dTiV') {

          vendor_map = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.691456086347!2d100.57064601534829!3d13.73712079035689!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29eff20aad00d%3A0xd5b772ac352f1fc5!2s137%20Pillars%20Suites%20%26%20Residences%20Bangkok!5e0!3m2!1sen!2s!4v1663747312055!5m2!1sen!2s" width="524" height="402" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

          $('.vendor-micro-cont .container#best-seller-cont .vendor-heading').each(function () {

            bestSellerTitleInt = "Popular Experiences By 137 Pillars Suites & Residences Bangkok";
            prosLinkInt = $(this).find("a:last-child").attr("href");
            // console.log(prosLinkInt);
            if (prosLinkInt != null && (!$(this).hasClass('modified-ajax'))) {
              $(this).html('<b>' + bestSellerTitleInt + '</b>');
              $(this).addClass('modified-ajax');
              $(this).closest('.vendor-heading').addClass('text-center');
              addViewMoreLink(prosLinkInt);
            }
          });



        }
        if (lastSegment == '137-pillars-chiang-mai-xtmb') {

          vendor_map = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3777.139648299373!2d99.0019789154017!3d18.79192888725148!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30da3ab029df1c7d%3A0x8e31e674b1e14cfc!2s137%20Pillars%20House%20Chiang%20Mai!5e0!3m2!1sen!2s!4v1663752524401!5m2!1sen!2s" width="524" height="402" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

          $('.vendor-micro-cont .container#best-seller-cont .vendor-heading').each(function () {

            bestSellerTitleInt = "Popular Experiences By 137 Pillars House Chiang Mai";
            prosLinkInt = $(this).find("a:last-child").attr("href");
            // console.log(prosLinkInt);
            if (prosLinkInt != null && (!$(this).hasClass('modified-ajax'))) {
              $(this).html('<b>' + bestSellerTitleInt + '</b>');
              $(this).addClass('modified-ajax');
              $(this).closest('.vendor-heading').addClass('text-center');
              addViewMoreLink(prosLinkInt);
            }
          });

        }

        if (lastSegment == 'burda-jw-marriott-hotel-bangkok-wyoH') {

          vendor_map = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d180520.50592345523!2d100.5026314832153!3d13.753593848732727!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29ee677aa7f5b%3A0xde920c956c943df!2sJW%20Marriott%20Hotel%20Bangkok!5e0!3m2!1sen!2s!4v1663754292621!5m2!1sen!2s" width="524" height="402" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';


          $('.vendor-micro-cont .container#best-seller-cont .vendor-heading').each(function () {

            bestSellerTitleInt = "Popular Experiences By JW Marriott Hotel Bangkok";
            prosLinkInt = $(this).find("a:last-child").attr("href");
            // console.log(prosLinkInt);
            if (prosLinkInt != null && (!$(this).hasClass('modified-ajax'))) {
              $(this).html('<b>' + bestSellerTitleInt + '</b>');
              $(this).addClass('modified-ajax');
              $(this).closest('.vendor-heading').addClass('text-center');
              addViewMoreLink(prosLinkInt);
            }
          });

        }

        if (lastSegment == 'burda-kimpton-maa-lai-bangkok-DflT') {
          vendor_map = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.682208879349!2d100.54125431534831!3d13.737679990356556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29ed964965fbb%3A0x507b29b8ac81a847!2s78%20Soi%20Ton%20Son%2C%20Khwaeng%20Lumphini%2C%20Khet%20Pathum%20Wan%2C%20Krung%20Thep%20Maha%20Nakhon%2010330%2C%20Thailand!5e0!3m2!1sen!2s!4v1663754687023!5m2!1sen!2s" width="524" height="402" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

          $('.vendor-micro-cont .container#best-seller-cont .vendor-heading').each(function () {

            bestSellerTitleInt = "Popular Experiences By Kimpton Maa-Lai Bangkok";
            prosLinkInt = $(this).find("a:last-child").attr("href");
            // console.log(prosLinkInt);
            if (prosLinkInt != null && (!$(this).hasClass('modified-ajax'))) {
              $(this).html('<b>' + bestSellerTitleInt + '</b>');
              $(this).addClass('modified-ajax');
              $(this).closest('.vendor-heading').addClass('text-center');
              addViewMoreLink(prosLinkInt);
            }
          });

        }

        if (lastSegment == 'burda-crimson-resort-and-spa-boracay-qLiS') {

          vendor_map = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3902.8853594827815!2d121.9085573147974!3d11.982433691510124!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33a53c715d5c9e37%3A0x9de0ac6e5d1ff390!2sCrimson%20Resort%20and%20Spa%20Boracay!5e0!3m2!1sen!2s!4v1664541242527!5m2!1sen!2s" width="524" height="402" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

          $('.vendor-micro-cont .container#best-seller-cont .vendor-heading').each(function () {

            bestSellerTitleInt = "Popular Experiences By Crimson Resort & Spa Boracay";
            prosLinkInt = $(this).find("a:last-child").attr("href");
            // console.log(prosLinkInt);
            if (prosLinkInt != null && (!$(this).hasClass('modified-ajax'))) {
              $(this).html('<b>' + bestSellerTitleInt + '</b>');
              $(this).addClass('modified-ajax');
              $(this).closest('.vendor-heading').addClass('text-center');
              addViewMoreLink(prosLinkInt);
            }
          });

        }

        if (lastSegment == 'burda-sindhorn-kempinski-hotel-bangkok-jAiI') {
          vendor_map = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.697956360845!2d100.54175411534833!3d13.73672769035718!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29ed8ff94b18f%3A0xd0ac5e54f9a0996a!2s80%20Soi%20Ton%20Son%2C%20Khwaeng%20Lumphini%2C%20Khet%20Pathum%20Wan%2C%20Krung%20Thep%20Maha%20Nakhon%2010330%2C%20Thailand!5e0!3m2!1sen!2s!4v1663755569054!5m2!1sen!2s" width="524" height="402" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

          $('.vendor-micro-cont .container#best-seller-cont .vendor-heading').each(function () {

            bestSellerTitleInt = "Popular Experiences By Sindhorn Kempinski Hotel Bangkok";
            prosLinkInt = $(this).find("a:last-child").attr("href");
            // console.log(prosLinkInt);
            if (prosLinkInt != null && (!$(this).hasClass('modified-ajax'))) {
              $(this).html('<b>' + bestSellerTitleInt + '</b>');
              $(this).addClass('modified-ajax');
              $(this).closest('.vendor-heading').addClass('text-center');
              addViewMoreLink(prosLinkInt);
            }
          });


        }

        if (lastSegment == 'burda-the-siam-hotel-kJVo') {
          vendor_map = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3874.962431865486!2d100.50380281534865!3d13.781138290328423!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29bdf83ff0f6f%3A0x8246602fcc2926de!2sThe%20Siam%20Hotel!5e0!3m2!1sen!2s!4v1663755916046!5m2!1sen!2s" width="524" height="402" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

          $('.vendor-micro-cont .container#best-seller-cont .vendor-heading').each(function () {

            bestSellerTitleInt = "Popular Experiences By The Siam Hotel Bangkok";
            prosLinkInt = $(this).find("a:last-child").attr("href");
            // console.log(prosLinkInt);
            if (prosLinkInt != null && (!$(this).hasClass('modified-ajax'))) {
              $(this).html('<b>' + bestSellerTitleInt + '</b>');
              $(this).addClass('modified-ajax');
              $(this).closest('.vendor-heading').addClass('text-center');
              addViewMoreLink(prosLinkInt);
            }
          });

        }

        $('.vendor-about-section .map-cont').append(vendor_map);




      }, 3000);
      setInterval(function () {

        // Product detail page Wishlist heart icons change upon click
        $('.product-detail-container .wishlistImage:has(img[alt="add-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
          $('.product-detail-container .wishlistImage img').attr('src', 'https://static.techsembly.com/zTQeSnpEvtpkqMXdmBJYWMqX');
        });

        $('.product-detail-container .wishlistImage:has(img[alt="added-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
          $('.product-detail-container .wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://static.techsembly.com/nnWu3VhLKu6QQwo9YahRgEho');
        });

        // Product detail page related products Wishlist heart icons change upon click
        $('.related-products  a.wishlistImage img').each(function () {
          if ($(this).attr('src') != 'https://static.techsembly.com/zTQeSnpEvtpkqMXdmBJYWMqX') {
            $(this).attr('src', 'https://static.techsembly.com/zTQeSnpEvtpkqMXdmBJYWMqX');
          }
          if (($(this).attr('alt') == 'added-to-wishlist') && ($(this).attr('src') != 'https://static.techsembly.com/nnWu3VhLKu6QQwo9YahRgEho')) {
            $(this).attr('src', 'https://static.techsembly.com/nnWu3VhLKu6QQwo9YahRgEho');
          }
        });

        // Category page Wishlist heart icons change upon click
        $('.products-container .products-holder-main .products-holder .product-column .product-list .product-content .product-rating a.wishlistImage img').each(function () {
          if ($(this).attr('src') != 'https://static.techsembly.com/zTQeSnpEvtpkqMXdmBJYWMqX') {
            $(this).attr('src', 'https://static.techsembly.com/zTQeSnpEvtpkqMXdmBJYWMqX');
          }
          if (($(this).attr('alt') == 'added-to-wishlist') && ($(this).attr('src') != 'https://static.techsembly.com/nnWu3VhLKu6QQwo9YahRgEho')) {
            $(this).attr('src', 'https://static.techsembly.com/nnWu3VhLKu6QQwo9YahRgEho');
          }
        });
        // wishlist page heart icon
        $('.wishlist-container .wishlistImage:has(img[alt="added-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
          $('.wishlist-container .wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://static.techsembly.com/nnWu3VhLKu6QQwo9YahRgEho');
        });

        $('.product-detail-container .personalise#personalise>div.row').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified productDesc');
          }
        });
        $('app-product-detail app-related-products[title="More items from"]').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).find('h3').html('More Like This');
          }
        });
        if (!$('header.header .toggle-menu-cont').hasClass('col-3')) {
          $('header.header .toggle-menu-cont').addClass('col-3').removeClass('col-1 col-sm-2 col-md-2');
        }
        if (!$('header.header .menu-links-cont').hasClass('col-3')) {
          $('header.header .menu-links-cont').addClass('col-3').removeClass('col-md-6 col-sm-5 col-7');
        }
        if ($('header.header .logo').parent('div').hasClass('col-sm-5')) {
          $('header.header .logo').parent('div').removeClass('col-4 col-sm-5');
          $('header.header .logo').parent('div').addClass('col-6 logo-cont');
        }
        $('header.header .menu-holder>li>div').each(function () {
          if (!$(this).children().length) {
            $(this).addClass('empty');
          }
        });
        $('.featured-vendors-area .promo-holder .promo-box .promo-content').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).append('<div class="discover-btn-cont pb-2"><a class="btn btn-primary discover-btn mb-1">DISCOVER MORE</a></div>');
          }
        });
        $('.sign-up-section .sign-up-container').each(function () {
          if (!$(this).hasClass('wrapped')) {
            $(this).addClass('wrapped');
            $(this).wrap('<div class="sign-up-inner-section"></div>');
          }
        });
        $('.custom-container.cart-container').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'cart')) {
            $(this).closest('body').attr('data-pagetype', 'cart');
          }
        });
        $('.products-container .products-holder-main .vendor-details').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'vendor-results')) {
            $(this).closest('body').attr('data-pagetype', 'vendor-results');
          }
        });
        $('.custom-container.checkout').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'checkout')) {
            $(this).closest('body').attr('data-pagetype', 'checkout');
          }
        });
        $('.custom-container.login-signup').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'account')) {
            $(this).closest('body').attr('data-pagetype', 'account');
          }
        });
        $('.container.user-container').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'user')) {
            $(this).closest('body').attr('data-pagetype', 'user');
          }
        });
        $('.footer .footer-widget .widget-content ul li a:contains("About")').each(function () {
          if (!$(this).closest('.footer-widget').hasClass('widget-col-1')) {
            $(this).closest('.footer-widget').addClass('widget-col-1');
          }
        });
        $('.footer .footer-widget .widget-title:contains("FOLLOW US")').each(function () {
          if (!$(this).closest('.footer-widget').hasClass('text-right')) {
            $(this).closest('.footer-widget').addClass('text-right');
            $('.footer-widget.text-right .widget-title').html("Follow us");
          }
        });
        $('.footer .footer-widget .widget-content ul li a:contains("BURDALUXURY")').each(function () {
          if (!$(this).closest('.footer-widget').hasClass('widget-col-2')) {
            $(this).closest('.footer-widget').addClass('widget-col-2');
          }
        });
        $('.home-slider .custom-container h2.section-title:contains("Top Experiences")').each(function () {
          if (!$(this).closest('.home-slider').hasClass('experience-slider')) {
            $(this).closest('.home-slider').addClass('experience-slider');
          }
        });
        $('.home-slider .custom-container h2.section-title:contains("Trending")').each(function () {
          if (!$(this).closest('.home-slider').hasClass('trending-slider')) {
            $(this).closest('.home-slider').addClass('trending-slider');
          }
        });
        $('.checkout-copyright-cont').each(function () {
          if (!$(this).closest('.container-fluid').hasClass('modified')) {
            $(this).closest('.container-fluid').addClass('my-checkout-footer modified');
          }
        });
        $('.product-detail-container .personalise-form .form-group .order-btn').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Book');
          }
        });
        $('.custom-container.cart-container .order-summary-cont .btn.btn-primary:contains("Proceed to Checkout")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Continue Checkout');
          }
        });
        $('.shipping-form-cont .btn-submit, .custom-container.checkout .shipping-form-container .btn.btn-submit:contains("Add & Continue Checkout")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Continue To Delivery');
          }
        });
        $('.custom-container.checkout .shipping-form-container .btn.btn-submit:contains("Continue Checkout")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Continue To Payment');
          }
        });
        $('.custom-container.login-signup h1.breadcrumb-heading').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('My gift cards');
          }
        });
        $('.shipping-form-container .breadcrumb-heading:contains("Shipping")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified').html("Shipping Details");
          }
        });
        $('.shipping-form-container .breadcrumb-heading:contains("Billing")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified').html("Billing Details");
          }
        });
        $('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc:contains("Subtotal")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified').html("Subtotal:");
          }
        });
        $('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc:contains("Shipping")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified').html("Shipping:");
          }
        });
        $('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc:contains("Tax Inclusive of prices")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified').html("Tax Inclusive of prices:");
          }
        });
        $('.checkout .payment-form-container .section-heading:contains("Details")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified').html("Billing Details:");
          }
        });

        $('.custom-container.login-signup .left-panel .panel-heading').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Existing Gift Card Account');
          }
        });
        $('.custom-container.login-signup .right-panel .panel-heading').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('New Gift Card Account');
          }
        });
        $('.custom-container .login-signup-tabs-cont form.login-form .forget-psw-link').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Forgot Password?');
          }
        });
        $('form.signup-form .btn.submit').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Create Gift card Account');
          }
        });
        $('.payment-form-container .checkbox-cont .checkbox-note:contains("Same as delivery address")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Same as shipping address');
          }
        });

        $('.shipping-form-container .order-shipping-container .delivery-dropdowns-cont .form-group').each(function () {
          if ($(this).closest(".col-12").hasClass('px-md-3')) {
            $(this).closest(".col-12").removeClass('px-md-3');
            $(this).closest(".col-12").addClass('modified pr-md-3');
          }
        });
        $('.home-slider h2.section-title div:contains("Trending")').each(function () {
          if (!$(this).closest('.home-banner').hasClass('trending-title')) {
            $(this).closest('.home-banner').addClass('trending-title');
          }
        });
        $('.container.user-container h1.user-heading').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('My Gift Card Account');
          }
        });
        $('.user-container .balance-form').closest('.user-container:not(.d-none)').addClass('d-none modified');
        $('.user-container .ts-balance-form').closest('.user-container:not(.d-none)').addClass('d-none modified');
        $('.user-container .addresses-holder').closest('.user-container:not(.d-none)').addClass('d-none modified');

        $('.user-container .wishlist-link').closest('.user-heading:not(.d-none)').addClass('d-none modified');
        $('.user-container .wishlist-link').closest('.user-container:not(.modified)').addClass('modified signout-cont');


        $('.user-container .user-heading:contains("My Orders")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified my-gifts-head').html("My Gift Cards");
          }
        });

        $('.my-gifts-head').each(function () {
          if (!$(this).closest('div.user-container').hasClass('modified')) {
            $(this).closest('.user-container').addClass('modified my-gifts-cont');
          }
        });

        hrefurl = $(location).attr("href");
        last_part = hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
        currentUrlSecondPart = window.location.pathname.split('/')[1];

        if (currentUrlSecondPart == 'catalogsearch') {
          if (!$('body').attr('data-pagetype', 'search_results')) {
            $('body').attr('data-pagetype', 'search_results');
          }
        }
        if (currentUrlSecondPart == 'vendors') {
          if (!$('body').attr('data-pagetype', 'vendors')) {
            $('body').attr('data-pagetype', 'vendors');
          }
        }
        if (last_part == 'wishlist') {
          if (!$('body').attr('data-pagetype', 'wishlist')) {
            $('body').attr('data-pagetype', 'wishlist');
          }
        }

        $('.filters-form-cont').each(function () {
          if (!$(this).closest('.container').hasClass('custom-form-container')) {
            $(this).closest('.container').addClass('custom-form-container');
          }
        });
        $('.checkout-container.complete .section-cont.customer-note').each(function () {
          if (!$(this).hasClass('total-delivery')) {
            $(this).addClass('total-delivery');
            var newTxt = 'Your order is being confirmed. You will receive  email with further information.';
            $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function (_, html) {
              return html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">' + newTxt + '</div>')
            });
          }
        });
        $('.checkout-container.complete .msg-cont .msg-inner').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            var newTxt = 'Lifestyle Asia Team';
            $(".checkout-container.complete .msg-cont .msg-inner:contains('We&apos;ll send you shipping confirmation when your item(s) are on the way! We appreciate your business, and hope you enjoy your purchase.')").html(function (_, html) {
              return html.replace(/(Burda Luxury)/g, newTxt)
            });
          }
        });
        $('.checkout-container.complete .shipping-data .shipping-row div:contains("Address")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Delivery Address:');
          }
        });
        $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
        $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');
        $('.custom-container.cart-container .card.vendor-items-holder .card-body .edit-item-new img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/P6TRcFR4hD1kUiyYrtTgLuFx");
        $('.cart-item.new .pro-badges-cont .pro-badge img[alt="digital-item"]:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/yYNkUjs55AcnAvnFMBgy3SA9");
        $('.cart-item.new .pro-badges-cont .pro-badge img[alt="food-item"]:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/t2HpPjkazBMW6Gyigo9pY9qG");
        $(".footer .footer-widget:last-child .widget-title:not(.modified)").addClass("modified").addClass('modified').html("FOLLOW US");
        $('.footer .footer-widget .social-links li a img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/FneppAS2xpG6eVKqo4nJXbqF');
        $('.footer .footer-widget .social-links li a img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/vsG1xxjDDbDJuTF8FumaUTmr');
        $('.footer .footer-widget .social-links li a img[alt="twitter"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/WNCD7Y6ZkRvT4ywxSU3dkyDN');

        $('.footer .footer-widget .social-links li a img[alt="fb"]').closest('li:not(.modified)').addClass('modified order-2');
        $('.footer .footer-widget .social-links li a img[alt="instagram"]').closest('li:not(.modified)').addClass('modified order-3');

        $('.delivery-methods-cont .contact-card .card-body .card-row .edit-contact img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/P6TRcFR4hD1kUiyYrtTgLuFx");
        $('.custom-container.checkout .shipping-form-container .card-header .edit-address img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/P6TRcFR4hD1kUiyYrtTgLuFx");
        $('.delivery-methods-cont .contact-card .card-body .card-row .edit-address img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/P6TRcFR4hD1kUiyYrtTgLuFx");
        // $('.checkout .password-note:not(.modified)').addClass('modified').html('Must be at least 6 characters including one special character');
        $('.checkout-container.complete .order-confirm-top img:not(.modified)').addClass('modified completeLogo').attr('src', ' https://static.techsembly.com/wcpKYjmcFmfWCAfr5Gii8Bnv').attr('width', '224.57');
        $('.custom-container.cart-container .card .cart-login-cont .checkout-desc-cont img[alt="lock"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/VSSp7RrWXYwn7HFCukYdYQZ2');

        $('meta[name="viewport"]:not(.modified)').addClass('modified').attr("content", "width=device-width, initial-scale=1, maximum-scale=1");
        //vendor microsite js
        $('.vendor-micro-cont .vendor-menu-cont').each(function () {
          if (!$(this).closest('.container').hasClass('vendor-menu-container')) {
            $(this).closest('.container').addClass('vendor-menu-container');
          }
        });
        $('.vendor-micro-cont.dt-view .vendor-logo').each(function () {
          if (!$(this).closest('div').hasClass('vendor-logo-cont')) {
            $(this).closest('div').addClass('vendor-logo-cont');
            $(this).closest('div').removeClass('pb-5');
          }
        });
        $('.vendor-micro-cont .container .vendor-heading b').each(function () {
          if (!$(this).children('a.collapse-link').length) {
            $(this).prepend('<a class="collapse-link collapsed" data-toggle="collapse" aria-expanded="false"><i class="fa fa-plus" aria-hidden="true"></i></a>');
          }
        });
        $('.vendor-micro-cont .container.vendor-point-cont#delivery ').each(function () {
          if ($(this).find('.collapse-link').attr('href') != '#del-desc') {
            $(this).find('.collapse-link').attr('href', '#del-desc');
            $(this).find('.collapse-link').attr('aria-controls', 'del-desc');
          }
          if ($(this).children('.vendor-point-desc').attr('id') != 'del-desc') {
            $(this).children('.vendor-point-desc').attr('id', 'del-desc');
          }
        });
        $('.vendor-micro-cont .container.vendor-point-cont#delivery p.mb-4').each(function () {
          if (!$(this).children('span.address-line').length) {
            var addHtml = $(this).html();
            var newAddHtml = 'Ships from <span class="address-line">' + addHtml.substring(13) + '</span>';
            //console.log(newAddHtml);
            $(this).html(newAddHtml);
          }
        });
        $('.vendor-micro-cont .container .vendor-heading b a.collapse-link').each(function () {
          if (!$(this).hasClass('collapsed')) {
            $(this).find('i.fa').addClass('fa-minus').removeClass('fa-plus');
          } else {
            $(this).find('i.fa').addClass('fa-plus').removeClass('fa-minus');
          }
        });
        $('.vendor-micro-cont .container .vendor-heading b:contains("Returns")').each(function () {
          if (!$(this).closest('.container').hasClass('returns-cont')) {
            $(this).closest('.container').addClass('returns-cont');
            $(this).html("Policies")
          }
        });
        $('.vendor-micro-cont .container.vendor-point-cont.returns-cont ').each(function () {
          if ($(this).find('.collapse-link').attr('href') != '#return-desc') {
            $(this).find('.collapse-link').attr('href', '#return-desc');
            $(this).find('.collapse-link').attr('aria-controls', 'return-desc');
          }
          if ($(this).children('.vendor-point-desc').attr('id') != 'return-desc') {
            $(this).children('.vendor-point-desc').attr('id', 'return-desc');
          }
        });
        $('.vendor-micro-cont .container .vendor-heading b:contains("Terms")').each(function () {
          if (!$(this).closest('.container').hasClass('tnc-cont')) {
            $(this).closest('.container').addClass('tnc-cont');
          }
        });
        $('.vendor-micro-cont .container.vendor-point-cont.tnc-cont ').each(function () {
          if ($(this).find('.collapse-link').attr('href') != '#tnc-desc') {
            $(this).find('.collapse-link').attr('href', '#tnc-desc');
            $(this).find('.collapse-link').attr('aria-controls', 'tnc-desc');
          }
          if ($(this).children('.vendor-point-desc').attr('id') != 'tnc-desc') {
            $(this).children('.vendor-point-desc').attr('id', 'tnc-desc');
          }
        });
        $('.vendor-micro-cont .container #return-desc div:last-child').each(function () {
          if (!$(this).children('span.address-line').length) {
            var addReturnHtml = $(this).html();
            var newAddReturnHtml = addReturnHtml.substring(3);
            //console.log(newAddReturnHtml);
            $(this).html('<span class="address-line">' + newAddReturnHtml + '</span>');
          }
        });

        $('.vendor-micro-cont .container.vendor-point-cont ').each(function () {
          if (!$(this).children('.vendor-point-desc').length) {
            //$(this).children().not('h2.vendor-heading ').wrapAll('<div class="vendor-point-desc collapse" />');
            $(this).wrapInner($("<div class='vendor-point-desc collapse'>"));
          }
        });

        $('.vendor-micro-cont .container.vendor-point-cont .vendor-point-desc').each(function () {
          if (!$(this).find('h2.vendor-heading').hasClass('moved')) {
            $(this).find('h2.vendor-heading').addClass('moved');
            $(this).find('h2.vendor-heading').prependTo($(this).closest(".vendor-point-cont"));
          }
        });


      });


    });
  });
})();
