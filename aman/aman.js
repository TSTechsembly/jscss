function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }
        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });
        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
function addMobMenuTop() {
  var mobMenuTop = '<div class="col-12 mob-menu-top">'+
    '<div class="col-12 mob-men-top-inner d-flex align-items-center justify-content-between px-0">'+
      '<div class="mob-menu-top-left col-6 pl-0">'+
        '<a class="d-block" href="/">'+
          '<img class="" alt="logo" width="121" src="https://static.techsembly.com/sD4RbQZeV5iSTjZiDNajxS4j">'+
        '</a>'+
      '</div>'+
      '<div class="mob-menu-top-right text-right col-6 pr-0">'+
        '<a class="mob-close-link d-block" href="#">'+
          '<img class="" alt="X" width="19" src="https://static.techsembly.com/rMp7Rb6xE8uPHhFCRtsepiZW">'+
        '</a>'+
      '</div>'+
    '</div>'+
  '</div>';
  var elemSidebar = $('nav#sidebar')[0];
  elemSidebar.insertAdjacentHTML('afterbegin', mobMenuTop);
}
function addInspireSection(){
  var inspireSection = '<section class="inspired-section bg-white">'+
    '<div class="col-12 px-1 px-lg-3 py-2">'+
      '<div class="col-12 d-flex flex-wrap align-items-center py-4 px-3">'+
        '<div class="col-12 col-lg-2 pl-lg-0 pt-2 pt-lg-0 text-center text-lg-left">'+
          '<h2 class="inspired-heading">Get inspired</h2>'+
        '</div>'+
        '<div class="col-12 col-lg-7 text-center text-lg-left px-0 px-lg-3 pr-lg-0">'+
          '<p>To receive updates about exclusive experiences, events, new destinations and more, please register your interest.</p>'+
        '</div>'+
        '<div class="col-12 col-lg-3 text-center text-lg-right px-3 pb-2 pr-lg-0 pb-lg-0 btn-signup-cont">'+
          '<a class="btn btn-primary btn-sign-up mt-4 mt-lg-0" target="_blank" href=" https://news.aman.com/home?_ga=2.210901149.941875855.1647911344-1599667154.1646098532">Sign Up</a>'+
        '</div>'+
      '</div>'+
    '</div>'+
    '<div class="col-12 px-1 px-lg-3">'+
      '<div class="col-12 px-3">'+
        '<hr class="border-stroke border-below-transparent margin-auto">'+
      '</div>'+
    '</div>'+
  '</section>';
  var elemFooter = $('footer.footer')[0];
  elemFooter.insertAdjacentHTML('beforebegin', inspireSection);
}
function addSocialLinks(){
  var extraLinks = '<!--li><a href="#" rel="nofollow"><img class="" alt="we-chat" width="24" src="https://static.techsembly.com/42B2VdRedrgcTo7USH628sB1"></a></li-->'+
  '<li><a href="https://www.youtube.com/c/AmanResortsHotelsResidences/" rel="nofollow"><img class="" alt="youtube" width="30" src="https://static.techsembly.com/2eoXMWUEt53dPVoKzMZYVHR1"></a></li>';
  var elemFooterSocial = $('.footer .footer-widget .social-links')[0];
  elemFooterSocial.insertAdjacentHTML('beforeend', extraLinks);
}
function addSuccessCopyRight() {
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var successCopyRight = '<div class="card-footer-right text-right"><span>Copyright '+copyrightYear+
  ', Aman Group S.a.r.l.</span></div>';
  var elemFooterLeftSocial = $('.custom-container.checkout-container.complete .card.order-confirm-wrapper .card-footer-left')[0];
  elemFooterLeftSocial.insertAdjacentHTML('afterend', successCopyRight);
}
(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
          font-family: 'Lyon-Regular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/544b857f022c257c0f2fe3bb4777b0df836f9a31/aman/fonts/lyon/LyonText-Regular-Web.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/544b857f022c257c0f2fe3bb4777b0df836f9a31/aman/fonts/lyon/LyonText-Regular-Web.woff2') format('woff2');\
          font-weight: 400;\
      }\
      @font-face {\
          font-family: 'Lyon-Light';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/544b857f022c257c0f2fe3bb4777b0df836f9a31/aman/fonts/lyon/LyonDisplay-Light-Web.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/544b857f022c257c0f2fe3bb4777b0df836f9a31/aman/fonts/lyon/LyonDisplay-Light-Web.woff2') format('woff2');\
          font-weight: 300;\
      }\
      @font-face {\
          font-family: 'Whitney Book':\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/5ab29bc1431ded8e2419bfc6a53e3db41a53c50e/aman/fonts/Whitney/whitney-book/Whitney-Book-Pro.otf');\
          font-weight: normal;\
      }\
      @font-face {\
          font-family: 'Whitney-Light';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/328050f0b3253260712248b776d5e32d43030635/aman/fonts/Whitney/Whitney-Light.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/328050f0b3253260712248b776d5e32d43030635/aman/fonts/Whitney/Whitney-Light.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/328050f0b3253260712248b776d5e32d43030635/aman/fonts/Whitney/Whitney-Light.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/328050f0b3253260712248b776d5e32d43030635/aman/fonts/Whitney/Whitney-Light.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/328050f0b3253260712248b776d5e32d43030635/aman/fonts/Whitney/Whitney-Light.woff2') format('woff2');\
          font-weight: normal;\
      }\
      @font-face {\
          font-family: 'Whitney-Medium';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b0a7552bd9b817f0f528af13188b458a47370a58/aman/fonts/Whitney/Whitney-Medium.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b0a7552bd9b817f0f528af13188b458a47370a58/aman/fonts/Whitney/Whitney-Medium.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0a7552bd9b817f0f528af13188b458a47370a58/aman/fonts/Whitney/Whitney-Medium.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0a7552bd9b817f0f528af13188b458a47370a58/aman/fonts/Whitney/Whitney-Medium.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0a7552bd9b817f0f528af13188b458a47370a58/aman/fonts/Whitney/Whitney-Medium.woff2') format('woff2');\
          font-weight: 500;\
      }\
      @font-face {\
          font-family: 'Whitney-Bold';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b0a7552bd9b817f0f528af13188b458a47370a58/aman/fonts/Whitney/Whitney-Bold.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b0a7552bd9b817f0f528af13188b458a47370a58/aman/fonts/Whitney/Whitney-Bold.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0a7552bd9b817f0f528af13188b458a47370a58/aman/fonts/Whitney/Whitney-Bold.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0a7552bd9b817f0f528af13188b458a47370a58/aman/fonts/Whitney/Whitney-Bold.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0a7552bd9b817f0f528af13188b458a47370a58/aman/fonts/Whitney/Whitney-Bold.woff2') format('woff2');\
          font-weight: bold;\
      }\
      @font-face {\
          font-family: 'theano_didotregular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/63f8a2e2c0ca9d219f6dc3bbaff0ae7dfa2d79de/aman/fonts/theano-didot/theanodidot-regular-webfont.woff2') format('woff2');\
      }\
    "));

    document.head.appendChild(newStyle);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
          waitForElm('nav#sidebar').then((elm) => {
            addMobMenuTop();
            $('.mob-close-link').click(function(){
              $('html').removeClass('scroll-hidden-html');
              $('body').removeClass('scroll-hidden');
              $('nav#sidebar').removeClass('active');
              $('.mob-overlay').removeClass('active');
            });
          }).catch((error) => {});

          waitForElm('footer.footer').then((elm) => {
            addInspireSection();
          }).catch((error) => {});

          waitForElm('.footer .footer-widget .social-links').then((elm) => {
            addSocialLinks();
          }).catch((error) => {});

          waitForElm('.custom-container.checkout-container.complete').then((elm) => {
            addSuccessCopyRight();
          }).catch((error) => {});


          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">');

          setTimeout(function () {
            //$('header.header .head-links').prepend('<li class="nav-item res-link-cont pr-2"><a href="#" class="btn btn-primary btn-reserve">Reserve</a></li>');
            //$('header.header .head-links').append('<li class="nav-item cart-qty-nav"><span class="">(</span><span class="cart-qty-new"></span><span class="">)</span></li>');

            //$('header.header .head-links li.wishlist-cont:not(.moved)').addClass('moved').insertBefore('$("header.header .head-links li.profile-cont")');
            $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
          }, 2000);
          setInterval(function() {
            $('.nav-search-field:not(.modified)').addClass('modified').attr("placeholder", "");
            $('header.header .menu-links-cont .search .search-item-form button img:not(.altered)').addClass('altered').attr('src', 'https://static.techsembly.com/HpAxA4Ahnb4b9Gmx7FHjjc2i');
            $('header.header .head-links').each(function(){
              if((!$(this).find('li.res-link-cont').length) && (!$(this).hasClass('res-linked'))){
                $(this).addClass('res-linked');
                $(this).prepend('<li class="nav-item res-link-cont pr-2"><a href="https://reservations.aman.com/?_ga=2.135458103.782340815.1648465756-1136217069.1625666010&_gac=1.127414527.1648209812.Cj0KCQjw0PWRBhDKARIsAPKHFGgGLYh4B5Im_ejlAZx_dO3ORLOFH7k44Bkzj2iH5DhQlP1IHrqHuJYaAiUwEALw_wcB&adult=2&arrive=2022-03-29&chain=16840&child=0&depart=2022-03-30&level=chain&locale=en-US&rooms=1" target="_blank" class="btn btn-primary btn-reserve">Reserve</a></li>');
              }
              if((!$(this).find('li.cart-qty-nav').length)  && (!$(this).hasClass('qty-moved'))){
                $(this).addClass('qty-moved');
                $(this).append('<li class="nav-item cart-qty-nav"><span class="">(</span><span class="cart-qty-new"></span><span class="">)</span></li>');
              }
            });
            $('header.header .head-links li a#profile-icon').closest('.nav-item:not(.profile-cont)').addClass('profile-cont');
            $('header.header .head-links li a#wishlist-heart').closest('.nav-item:not(.wishlist-cont)').addClass('wishlist-cont');
            $('header.header .mobile-menu#sidebarCollapse svg').each(function(){
                $(this).attr('width','28');
                $(this).attr('height','24');
                $(this).find('rect').attr('height','5');
            });
            if(!$('header.header .toggle-menu-cont').hasClass('col-4')){
                $('header.header .toggle-menu-cont').addClass('col-4').removeClass('col-1 col-sm-2 col-md-2');
            }
            if(!$('header.header .menu-links-cont').hasClass('col-4')){
                $('header.header .menu-links-cont').addClass('col-4').removeClass('col-md-6 col-sm-5 col-7');
            }
            $('header .mob-top-links li a img[alt="search image"]:not(.altered)').addClass('altered').attr('src', 'https://static.techsembly.com/eJeyJVSbYcjhdDXXdHFsgWdc');
            $('header.header .shipping li .dropdown .dropdown-menu').each(function(){
              if(!$(this).hasClass('ccy-dd')){
                $(this).addClass('ccy-dd');
                $(this).append('<div class="ccy-links-cont">'+
                '<div class="en"><a class="dropdown-item hidestore" href="https://gifts.aman.com/">EN</a></div>'+
                '<div class="jp"><a class="dropdown-item hidestore" href="https://gifts.aman.com/jp">JP</a></div>'+
                '</div>');
              }
            });
            $('#sidebar ul.list-unstyled.user-links').each(function(){
              if(!$(this).hasClass('user-mob-links')){
                $(this).addClass('user-mob-links');
                $(this).append('<li>'+
                '<div class="d-flex align-items-center">'+
                '<div class="dropdown pref_dropdown w-100">'+
                '<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownMenuLink_mob">EN</a>'+
                '<div aria-labelledby="dropdownMenuLink" class="dropdown-menu countries-links-cont pt-0 mt-0" x-placement="top-start">'+
                '<div class="en"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.aman.com/">EN</a></div>'+
                '<div class="jp"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.aman.com/jp">JP</a></div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</li>');
              }
            });
            $('header.header .head-links li a#cart-bag img[alt="cart"]:not(.altered)').addClass('altered').attr('src', 'https://static.techsembly.com/vVcamgZHQZbnRqC2YKtHN7fC');
            $('header .mob-top-links li a img[alt="cart"]:not(.altered)').addClass('altered').attr('src', 'https://static.techsembly.com/vVcamgZHQZbnRqC2YKtHN7fC');
            $('header.header .head-links li a#wishlist-heart img[alt="wishlist"]:not(.altered)').addClass('altered').attr('src', 'https://static.techsembly.com/dLn3cGqjECbxk7Rex2UFZE9T');
            $('header.header .head-links li a#profile-icon img[alt="user-profile"]:not(.altered)').addClass('altered').attr('src', 'https://static.techsembly.com/qnNvqwrptDoNbMj1tySCWuoM');
            $('header.header .head-links li .add-to-basket').each(function(){
              if ($(this).find('.cart-qty').length) {
                $(this).find('.cart-qty').addClass('moved').appendTo('.head-links .cart-qty-nav .cart-qty-new');
              }
            });
            //$('header.header .head-links li .add-to-basket .cart-qty:not(.moved)').addClass('moved').appendTo('.head-links .cart-qty-nav .cart-qty-new');
            $('.head-links .cart-qty-nav .cart-qty-new').each(function(){
              if(!$(this).find('.cart-qty').length && !$(this).hasClass('empty')) {
                $(this).addClass('empty');
                $(this).html('0');
              }
              else if ($(this).find('.cart-qty').length) {
                $(this).removeClass('empty');
              }
            });

            $('header.header .head-links li .add-to-basket .dropdown-menu h4:not(.altered)').addClass('altered').html('Just added to your cart');
            $('header .mob-top-links .add-to-basket .dropdown-menu h4:not(.altered)').addClass('altered').html('Just added to your cart');

            $('header.header .menu-holder>li>div').each(function(){
              if(!$(this).children().length){
                $(this).addClass('empty');
              }
            });

            $('header.header .menu-holder li a.main-category:contains("Destinations")').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('target','_blank');
              }
            });

            $('header.header .menu-holder li a.main-category:contains("Hotels & Resorts")').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('target','_blank');
              }
            });

            hrefurl=$(location).attr("href");
            last_part=hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
            currentUrlSecondPart = window.location.pathname.split('/')[1];

            if (currentUrlSecondPart == ''){
               if (!$('body').attr('data-pagetype','home')){
                 $('body').attr('data-pagetype','home');
               }
            }
            if (currentUrlSecondPart == 'catalogsearch'){
               if (!$('body').attr('data-pagetype','search_results')){
                   $('body').attr('data-pagetype','search_results');
               }
            }
            if (currentUrlSecondPart == 'vendors'){
               if (!$('body').attr('data-pagetype','vendors')){
                   $('body').attr('data-pagetype','vendors');
               }
            }
            if (last_part == 'wishlist'){
               if (!$('body').attr('data-pagetype','wishlist')){
                   $('body').attr('data-pagetype','wishlist');
               }
               if(!$('header.header .head-links li a img[alt="wishlist"]').attr('src','https://static.techsembly.com/CBkTkeuZxTaMWX8Ns38SKCQA')){
                  $('header.header .head-links li a img[alt="wishlist"]').attr('src','https://static.techsembly.com/CBkTkeuZxTaMWX8Ns38SKCQA');
               }
            }

            $('.wishlistImage:has(img[alt="add-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
                $('.wishlistImage img').attr('src', 'https://static.techsembly.com/dLn3cGqjECbxk7Rex2UFZE9T');
            });

            $('.wishlistImage:has(img[alt="added-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
                $('.wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://static.techsembly.com/CBkTkeuZxTaMWX8Ns38SKCQA');
            });

            if ($(".wishlist-container .products-container .products-holder i[aria-hidden='true']").length < 2){
              $('.wishlist-container .products-container .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
            }
            $('.address-form').find("input[formcontrolname=building]").each(function(){
                $(this).parent('div').addClass('building-cont');
            });
            $('.address-form').find("input[formcontrolname=postCode]").each(function(){
                $(this).parent('div').addClass('post-code-cont');
            });
            $('.address-form').find("input[formcontrolname=phone]").each(function(){
                if(!$(this).val()) {
                  //$(this).attr("placeholder", "Phone Number");
                }
                $(this).parent('div').addClass('phone-no-cont');
            });

            $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
            $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');
            $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Purchase');
              }
            });
            $('.related-products .custom-container').each(function(){
              if(!$(this).children('h3.text-center').length){
                $(this).prepend('<h3 class="text-center related-pros-heading">More Like This</h3>');
              }
            });
            $('app-product-detail app-related-products[title="More items from"]').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).find('h3').html('More Like This');
              }
            });
            $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/bQcH38XymqSDBKmsKN9Gy2n8');
            $('.custom-container.cart-container .vendor-items-cont a img[alt="gift"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/hYWZF1yrmjwyXbDHYw6CkHSn');

            $('.auth-user .checkout .shipping-form h1.breadcrumb-heading:contains("Shipping Details")').each(function() {
              if(!$(this).closest('.shipping-form').hasClass('shipp-details')){
                $(this).closest('.shipping-form').addClass('shipp-details');
                $(this).html('Choose Shipping Details');
              }
            });

            $('.shipping-form-container .exist-address-wrapper .card a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/bQcH38XymqSDBKmsKN9Gy2n8');

            $('.delivery-methods-cont .card .card-body .card-row a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/bQcH38XymqSDBKmsKN9Gy2n8');

            $('.payment-form-container .card-details-cont').each(function(){
              if(!$(this).children('h2.section-heading').length){
                $(this).prepend('<h2 class="section-heading">Card Details</h2>');
              }
            });
            $('.payment-form-container .input-cont img.pass-eye:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/L2sx85ckZHqJ8HXdDq9MNNpW');

            //$('.checkout .password-note:not(.modified)').addClass('modified').html('Must be atleast 6 characters');

            $('.custom-container.checkout .page-heading:contains("Success")').each(function(){
              if(!$(this).hasClass('d-none')){
                $(this).addClass('d-none');
              }
            });
            $('.checkout-container.complete .section-cont.customer-note').each(function(){
              if(!$(this).hasClass('total-delivery')){
                $(this).addClass('total-delivery');
                var newTxt = 'You order is being confirmed and we will be shipping it soon';
                $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function(_, html) {
                   return  html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">'+newTxt+'</div>')
                });
              }
            });

            $('.custom-container.checkout .checkout-tabs-cont .vendor-items-holder .vendor-order-details .order-item .qty').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newTxt = 'Qty';
                $(".qty:contains('Qty:')").html(function(_, html) {
                   return  html.replace(/(Qty:)/g, '<span class="updated-qty-string mr-1">'+newTxt+'</span>');
                });
              }
            })

            $('.custom-container.checkout-container.complete .msg-inner').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newTxt = 'The Aman Team';
                $(".msg-inner:contains('AMAN Hotels Resorts')").html(function(_, html) {
                   return  html.replace(/(AMAN Hotels Resorts)/g, '<div class="updated-string">'+newTxt+'</div>');
                });
              }
            });

            $('.user-container .ts-balance-form').closest('.user-container:not(.d-none)').addClass('d-none');

            $('.footer a:contains("+1")').each(function(){
              var string_phone= $(this).html();
              var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
              var phone_no = "tel:" + result_phone;
              if(!$(this).hasClass('tel-link')){
                $(this).addClass('tel-link');
                $(this).attr('href', phone_no);
              }
            });
            $('.footer a:contains("@")').each(function(){
              var string_email= $(this).html();
              var result_email = string_email.substring(string_email.indexOf(':') + 1);
              var email_link = "mailto:" + result_email;
              if(!$(this).hasClass('email-link')){
                $(this).addClass('email-link');
                $(this).attr('href', email_link);
              }
            });

            $('.footer a:contains("Gift-Card Support")').each(function(){
              var string_email2= 'aman@techsembly.com';
              var email_link2 = "mailto:" + string_email2;
              if(!$(this).hasClass('email-link')){
                $(this).addClass('email-link');
                $(this).attr('href', email_link2);
              }
            });

            $('.custom-container.checkout-container.complete .card.order-confirm-wrapper .card-footer a:contains("Customer Support")').each(function(){
              var string_email_add = 'aman@techsembly.com';
              var email_link_checkout = "mailto:" + string_email_add;
              if(!$(this).hasClass('email-link')){
                $(this).addClass('email-link');
                $(this).attr('href', email_link_checkout);
              }
            });

            $('.footer .mobile-footer .list-group .list-group-holder:nth-child(3) .list-group-item-action:not(.modified)').addClass('modified').html('More Destinations');

            if(!$('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').hasClass('social-widget')){
                $('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').addClass('social-widget');
                $('.social-widget .widget-title').html('Follow Us On:');
            }
            $('.social-links img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/txAx8LYqMqKfu61oj9nzxUkz');
            $('.social-links img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/a8pSywARZdfjTMWrrWxhDKyW');
            $('.social-links img[alt="line"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/b8xAXZBvQEHij6hKii27uacA');
            $('.social-links img[alt="twitter"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/F4B3wmaqEXoy454yDkXftCRN');
            $('.social-links img[alt="fb"]').closest('li:not(.fb-link-cont)').addClass('fb-link-cont');
            $('.social-links img[alt="instagram"]').closest('li:not(.insta-link-cont)').addClass('insta-link-cont');

            $('.footer .copyright-cont .pay-methods-avail ').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).find('.meth-link:not(.new)').addClass('d-none');
                    $(this).append('<div class="meth-link new"><img alt="amex" src="https://static.techsembly.com/h1Qp9Pzc8UxfarwwK27qFUDv" width="55"></div>'+
                    '<div class="meth-link new"><img alt="master-card" src="https://static.techsembly.com/fQDrfQrDmHzYwHv2wSX71tqx" width="55"></div>'+
                    '<div class="meth-link new"><img alt="visa" src="https://static.techsembly.com/3RrhdBRJYn9WjnW3C5buZuJR" width="55"></div>');
                }
            });
            $('.footer .mobile-footer .list-group .list-group-holder .footer-widget').each(function(){
              if(!$(this).children().length){
                $(this).closest('.list-group-holder').addClass('d-none');
              }
            });
          });
        });
    });
})();
