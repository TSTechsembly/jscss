function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }

        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
(function() {
  var script = document.createElement("SCRIPT");
  script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
  script.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script);

      var newStyle = document.createElement('style');
      newStyle.appendChild(document.createTextNode("\
        @font-face {\
          font-family: 'cormorantregular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7e19db5aff6156fc597b0a8eff57cd9d1117fdc4/she-her/fonts/cormorant/cormorant-regular-webfont.woff2') format('woff2');\
        }\
        @font-face {\
          font-family: 'cormorantbold';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7e19db5aff6156fc597b0a8eff57cd9d1117fdc4/she-her/fonts/cormorant/cormorant-bold-webfont.woff2') format('woff2');\
        }\
        @font-face {\
          font-family: 'cormorantbold_italic';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7e19db5aff6156fc597b0a8eff57cd9d1117fdc4/she-her/fonts/cormorant/cormorant-bolditalic-webfont.woff2') format('woff2');\
        }\
        @font-face {\
          font-family: 'cormorantitalic';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7e19db5aff6156fc597b0a8eff57cd9d1117fdc4/she-her/fonts/cormorant/cormorant-italic-webfont.woff2') format('woff2');\
        }\
        @font-face {\
        font-family: 'cormorantlight';\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7e19db5aff6156fc597b0a8eff57cd9d1117fdc4/she-her/fonts/cormorant/cormorant-light-webfont.woff2') format('woff2');\
        }\
        @font-face {\
          font-family: 'cormorantlight_italic';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7e19db5aff6156fc597b0a8eff57cd9d1117fdc4/she-her/fonts/cormorant/cormorant-lightitalic-webfont.woff2') format('woff2');\
        }\
        @font-face {\
          font-family: 'cormorantmedium';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7e19db5aff6156fc597b0a8eff57cd9d1117fdc4/she-her/fonts/cormorant/cormorant-medium-webfont.woff2') format('woff2');\
        }\
        @font-face {\
          font-family: 'cormorantmedium_italic';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7e19db5aff6156fc597b0a8eff57cd9d1117fdc4/she-her/fonts/cormorant/cormorant-mediumitalic-webfont.woff2') format('woff2');\
        }\
        @font-face {\
          font-family: 'cormorantsemibold';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7e19db5aff6156fc597b0a8eff57cd9d1117fdc4/she-her/fonts/cormorant/cormorant-semibold-webfont.woff2') format('woff2');\
        }\
        @font-face {\
          font-family: 'cormorantsemibold_italic';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7e19db5aff6156fc597b0a8eff57cd9d1117fdc4/she-her/fonts/cormorant/cormorant-semibolditalic-webfont.woff2') format('woff2');\
        }\
      "));
      document.head.appendChild(newStyle);
      // Poll for jQuery to come into existence
      var checkReady = function(callback) {
          if (window.jQuery) {
              callback(jQuery);
          }
          else {
              window.setTimeout(function() { checkReady(callback); }, 20);
          }
      };
      //custom js code checkReady
      checkReady(function($) {
        $(function() {
          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600&display=swap" rel="stylesheet">');
          $('head').append('<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">');

          //setTimeout
          setTimeout(function() {

          }, 2000);
          setInterval(function () {
            $('.bestsellers-slider .section-title:contains("Shop by Favourites")').each(function(){
              if(!$(this).closest('.bestsellers-slider').hasClass('home-category-slider')){
                $(this).closest('.bestsellers-slider').addClass('home-category-slider');
                $(this).html('<span>Shop by <em><b>Favourites</b></em></span>')
              }
            });
            $('.bestsellers-slider .section-title:contains("Follow us on Instagram")').each(function(){
              if(!$(this).closest('.bestsellers-slider').hasClass('home-social-slider')){
                $(this).closest('.bestsellers-slider').addClass('home-social-slider');
                $(this).html('Follow us on <span>Instagram</span>');
              }
            });
            $('.home-slider .section-title:contains("Designers")').each(function(){
              var str = $(this).html();
              str_1 = str.split(/\s(.+)/)[0];  //everything before the first space
              str_2 = str.split(/\s(.+)/)[1];  //everything after the first space
              if(!$(this).closest('.home-slider').hasClass('home-slider-designer')){
                $(this).closest('.home-slider').addClass('home-slider-designer');
                //$(this).closest('.home-slider').removeClass('bestsellers-slider');
                $(this).html('<span>'+str_1+' <em><b>'+str_2+'</b></em></span>');
              }
            });
            $('.home-slider-designer .owl-carousel .owl-nav button.owl-prev img:not(.altered)').addClass('altered').attr('src', 'https://bitbucket.org/TSTechsembly/jscss/raw/6bf0fd0cf0142d97e964f5e7ed887f341d176548/she-her/images/arrow-left.png');
            $('.home-slider-designer .owl-carousel .owl-nav button.owl-next img:not(.altered)').addClass('altered').attr('src', 'https://bitbucket.org/TSTechsembly/jscss/raw/6bf0fd0cf0142d97e964f5e7ed887f341d176548/she-her/images/arrow-right.png');
            $('.home-slider .section-title:contains("Shop by Personality")').each(function(){
              if(!$(this).closest('.home-slider').hasClass('home-slider-personality')){
                $(this).closest('.home-slider').addClass('home-slider-personality');
                $(this).html('<span>Shop by <em><b>Personality</b></em></span>');
              }
            });
            $('.wishlist-section').each(function(){
              if(!$(this).closest('.home-intro-container').hasClass('container-fluid')){
                $(this).closest('.home-intro-container').removeClass('container');
                $(this).closest('.home-intro-container').addClass('container-fluid');
              }
            });
            $('.review-slider').each(function(){
              if($(this).closest('.home-intro-container').hasClass('container')){
                $(this).closest('.home-intro-container').removeClass('container');
              }
            });
            $('.featured-client h2:not(.altered)').addClass('altered').html('They <em>talk</em> about <em>us</em>, <br>they <em>love</em> us');
            //$('.featured-client h2:not(.altered)').addClass('altered').html('she.her. jewellery has been seen in');

            $('.clients-list img[src="https://cdn-saas.techsembly.com/assets/new-home/mail_online-5f79112ec8d877803e569f60a6dcf343fbf9d3732671216aa25b9d48f8ec3edf.svg"]:not(.altered)').addClass('altered').attr('src','https://static.techsembly.com/f1dGpanbho6q71qaRuBTYJKR');

            $('.clients-list img[src="https://cdn-saas.techsembly.com/assets/new-home/expatliving-8e6cde199838abf2bf48d6581f54c42b98a1615576e9989d4fb10106d9602f48.svg"]:not(.altered)').addClass('altered').attr('src','https://static.techsembly.com/UJiqpSu7ZGjMP6uHZaw61tQT');

            $('.clients-list img[src="https://cdn-saas.techsembly.com/assets/new-home/singaporetatler-3ec5b3a6c9a192e047712fa59e9558720b9f9f2a12bdaea9fe725785db90cdd9.svg"]:not(.altered)').addClass('altered').attr('src','https://static.techsembly.com/9ohQH7yM8FHHmSeQ11fC7GYk');

            $('.clients-list img[src="https://cdn-saas.techsembly.com/assets/new-home/marieclaire-ac44db0aef16c06b69e69a22bd43c64b7c6f01f3ffdf6be529e2f6dedad0ff4e.svg"]:not(.altered)').addClass('altered').attr('src','https://static.techsembly.com/sFkFi3EQMdJRetHV64NxhXkS');

            $('.clients-list img[src="https://cdn-saas.techsembly.com/assets/new-home/womensweekly-57aa92816223debed5e0f077297f92740b1cb1870e27ce94593ad5db6a583071.svg"]:not(.altered)').addClass('altered').attr('src','https://static.techsembly.com/UMDa2e2Tpe5WekEcxt4sYJhT');

            $('.clients-list img[src="https://cdn-saas.techsembly.com/assets/new-home/the_business_times-1581402eacace3ff90954efb6d00941935235d7a42c53b6deb60a20e7a488cfb.svg"]:not(.altered)').addClass('altered').attr('src','https://static.techsembly.com/P2wt6UYZiiggBkw6CFHV8CT2');
            $('.sign-up-section:not(.moved)').addClass('moved').insertBefore('footer');
            $('.footer .copyright-cont .pay-methods-avail').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).find('.meth-link:not(.new)').addClass('d-none');
                    $(this).append('<!--div class="meth-link new"><img alt="apple-pay" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/apple_pay-f6db0077dc7c325b436ecbdcf254239100b35b70b1663bc7523d7c424901fa09.svg" width="52"></div-->'+
                    '<div class="meth-link new"><img alt="master-card" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/master-173035bc8124581983d4efa50cf8626e8553c2b311353fbf67485f9c1a2b88d1.svg" width="52"></div>'+
                    '<!--div class="meth-link new"><img alt="paypal" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/paypal-49e4c1e03244b6d2de0d270ca0d22dd15da6e92cc7266e93eb43762df5aa355d.svg" width="52"></div-->'+
                    '<div class="meth-link new"><img alt="visa" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/visa-319d545c6fd255c9aad5eeaad21fd6f7f7b4fdbdb1a35ce83b89cca12a187f00.svg" width="52"></div>');
                }
            });
            $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
                $(this).html('buy now');
            });
            //Product tabs
            $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#personalize-tab h4:not(.modified)').addClass('modified').html("Curators' Notes");
            $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#details-tab h4:not(.modified)').addClass('modified').html('PRODUCT DETAILS & MATERIAL');
            $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#delivery-tab h4:not(.modified)').addClass('modified').html('SHIPPING AND RETURN');
            $('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#askseller-tab h4:not(.modified)').addClass('modified').html('Ask the designer');
            $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').removeClass('col-lg-5').addClass('col-lg-7 product-inner modified');
            $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').removeClass('col-lg-7').addClass('col-lg-5 modified');
            $('.custom-container.cart-container .card.order-summary-cont .card-body .card-body-inner').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Delivery fees will be calculated during checkout.');
              }
            });
            $('.footer').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $('<div class="col-12 col-md-6 col-lg-4 px-0 acknowledge-cont"><h6 class="ack-heading">ACKNOWLEDGEMENT OF COUNTRY</h6><div class="ack-desc">We acknowledge Aboriginal and Torres Strait Islander peoples as the First Australians and Traditional Custodians of the lands where we live, learn and work</div></div>').insertBefore('.footer  .copyright-cont:not(.d-lg-none)');
              }
            });
            if(!$('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').hasClass('social-widget')){
                $('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').addClass('social-widget');
            }
            $('#content').each(function(){
                if(!$(this).children('.notificationbar').length){
                  $(this).prepend('<div class="notificationbar">10% off your first order | Subscribe Now</div>')
                }
            });
            $('.product-detail-container .product-holder .product-rating a.wishlistImage').each(function (){
              if(!$(this).parent('div').hasClass('heart-icon-left')){
                $(this).parent('div').addClass('heart-icon-left');
              }
            });
            $('.custom-container.cart-container .vendor-heading').each(function () {
                if (!$(this).children('span.delivered-by').length) {
                    $(this).prepend('<span class="delivered-by mr-2">Order from</span>');
                }
            });
            $('.product-detail-container .product-holder .product-rating a.wishlistImage img:not(.appeared)').addClass('appeared').attr('src', 'https://bitbucket.org/TSTechsembly/jscss/raw/152d30b6f77356012af4e5880ecb55e6abad76b5/she-her/images/heart.svg');
          });
        });
      });
})();
