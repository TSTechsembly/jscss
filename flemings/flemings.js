function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }
        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });
        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
function addcopyrightText(){
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-6 ts-copyright"><span><a href="https://techsembly.com/" target="_blank">Techsembly&copy;</a></span><span class="ml-1">'+copyrightYear+'</span></div><div class="col-6 tc-rights-reserved text-right pr-0"></div></div></div></div>';
  var elem = $(".checkout-footer")[0];
  elem.insertAdjacentHTML('afterend', copyrightHtml);
}
function addLoginCartNote(){
  var loginCartNote = "<div class='login-note'>Please note this login is for Flemings gifting platform only.<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}
function addGuestCheckoutNote(){
  var checkoutNote = "<div class='checkout-note text-center w-100'>You can create account after checkout<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}
(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);
    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
        font-family: 'theano_didotregular';\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/3c6282021694c1db812aaa7821d6379e0bdb6a01/flemings/fonts/theanodidot/theanodidot-regular-webfont.eot');\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/3c6282021694c1db812aaa7821d6379e0bdb6a01/flemings/fonts/theanodidot/theanodidot-regular-webfont.eot?#iefix') format('embedded-opentype'), \
             url('https://bitbucket.org/TSTechsembly/jscss/raw/3c6282021694c1db812aaa7821d6379e0bdb6a01/flemings/fonts/theanodidot/theanodidot-regular-webfont.ttf') format('truetype'),\
             url('https://bitbucket.org/TSTechsembly/jscss/raw/3c6282021694c1db812aaa7821d6379e0bdb6a01/flemings/fonts/theanodidot/theanodidot-regular-webfont.woff') format('woff'),\
             url('https://bitbucket.org/TSTechsembly/jscss/raw/3c6282021694c1db812aaa7821d6379e0bdb6a01/flemings/fonts/theanodidot/theanodidot-regular-webfont.woff2') format('woff2');\
        font-weight: normal;\
        font-style: normal;\
      }\
    "));
    document.head.appendChild(newStyle);
    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };
    checkReady(function($) {
      $(function() {
        waitForElm('app-footer-checkout').then((elm) => {
          addcopyrightText()
        }).catch((error) => {});
        waitForElm('.cart-right-cont').then((elm) => {
          addLoginCartNote();
          addGuestCheckoutNote();
        }).catch((error) => {});
        $('.sign-up-section').each(function() {
            if(!$(this).children('.container-overview').length){
                $('<div class="container-overview"><div class="overview-inner text-center"><h2 class="text-center">HOTEL OVERVIEW</h2><div class="intro-content"><p>Flemings Mayfair is one of the most iconic boutique hotels in Mayfair and of the few remaining independent, family owned and operated hotels in London. Part of Small Luxury Hotels of the World, this award-winning hotel offers unrivalled luxury accommodation in one of capital’s most prestigious neighbourhoods. Converted from thirteen Georgian townhouses that date from 1731, the hotel was founded by Robert Fleming in 1851, making it one of London&apos;s oldest established hotels.</p></div></div></div>').insertBefore($(this));
            }
        });
        $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400&display=swap" rel="stylesheet">');
        $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Questrial&display=swap" rel="stylesheet">');
        setInterval(function() {
            $('header.header .menu-holder').each(function(){
              if(!$(this).closest('.container').hasClass('wrapped')){
                $(this).closest('.container').addClass('wrapped');
                $(this).closest('.container').wrap('<div class="container-fluid main-menu-wrapper"></div>');
              }
            });
            $('header.header .head-links li a#cart-bag img[alt="cart"]').attr('src', 'https://static.techsembly.com/qkKfDaiNDADKayaknYdejz4c');
            $('header.header .head-links li a#wishlist-heart img[alt="wishlist"]').attr('src', 'https://static.techsembly.com/qNKdp6adcEkUr74erCE4BNPf');
            $('header.header .head-links li a#profile-icon img[alt="user-profile"]').attr('src', 'https://static.techsembly.com/KDArkU8zqW6M4zuhKtZeaewb');
            $('header.header .search .search-item-form button#search-bar-button img[alt="search image"]').attr('src', 'https://static.techsembly.com/CmbknKQJS7ApNu2QWoP7be4w');
            $('.promo-holder .promo-content').each(function() {
                if(!$(this).children('.more-link').length){
                  $(this).append('<a href="#" class="more-link d-flex align-items-center">Explore More</a>')
                }
            });
            $('.gift-section .custom-container .gifts-content .gifts-desc').each(function() {
                if(!$(this).children('.v-button').length){
                  $(this).append('<a href="#" class="v-button">Explore Gift Vouchers</a>')
                }
            });
            var categoryHeading = $('.product-container .breadcrumb-heading').text();
            var productText = $('.custom-container.product-container .product-text')[0];

            $('.products-container .products-holder-main').each(function(){
              if (categoryHeading == 'Skincare') {
                if(!$(this).children('.acc-container').length){
                  $(this).prepend('<div class="acc-container"><div class="ass-container-holder text-center"></div></div>');
                }
                if(!$('.ass-container-holder').children('.product-text').length){
                  $(productText).appendTo('.ass-container-holder');
                }
              }
            });
            $('.related-products .custom-container').each(function(){
              if(!$(this).children('h3.text-center').length){
                $(this).prepend('<h3 class="text-center related-pros-heading">More Like This</h3>');
              }
            });
            $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
                if(!$(this).hasClass('buy-btn')){
                    $(this).addClass('buy-btn')
                    $(this).html('Buy Now');
                }
            });
            $('.custom-container.cart-container .btn-default.continue-btn-new:contains("Continue shopping")').each(function(){
            if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Return To Shopping');
                }
            });
            $('.custom-container.cart-container .cart-login-cont .btn.submit:contains("SIGN IN & CHECKOUT")').each(function(){
            if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Sign In & Checkout');
                }
            });
            $('.custom-container.cart-container .vendor-items-cont .card .card-header .flag-cont span:contains("Shipping From United Kingdom")').each(function() {
                if(!$(this).closest('.flag-cont').hasClass('flag-icon')){
                    $(this).closest('.flag-cont').addClass('flag-icon');
                    $(this).html('<img class="mr-2" src="https://static.techsembly.com/nHmFaPrqhQQFcYNMVRRdjmpN"><span>Shipping From United Kingdom</span>');
                }
            });
            $('.custom-container.cart-container').removeClass('pt-3 pt-lg-4')
            $('.custom-container.login-signup .container').removeClass('custom-container');
            $('.custom-container.cart-container .cart-left-cont .container').removeClass('custom-container');
            $('.container.product-detail-container .container').removeClass('custom-container');
            $('.custom-container.product-container .container').removeClass('custom-container');
            $('.products-container .products-holder-main .products-holder').removeClass('pt-5');
            $('.products-container .products-holder-main .products-holder .product-column .product-list .product-content .d-flex').removeClass('mt-1');
            $('.container-fluid.BorderBottom.border-grey').addClass('d-none');
            $('.shipping-form-container .exist-address-wrapper .card a img[alt="edit"]').attr('src', 'https://static.techsembly.com/WbU3wGmhuZLNN6dqQC8Zdtwv');
            $('.delivery-methods-cont .card .card-body .card-row a img[alt="edit"]').attr('src', 'https://static.techsembly.com/WbU3wGmhuZLNN6dqQC8Zdtwv');
            $('.custom-container.cart-container .vendor-items-cont a img[alt="gift"]').attr('src', 'https://static.techsembly.com/e3zeUxayQorbspri5podsqkH');
            $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]').attr('src', 'https://static.techsembly.com/WbU3wGmhuZLNN6dqQC8Zdtwv');
            $('.related-products .related-products-holder .product-content .product-rating a.wishlistImage img[alt="add-to-wishlist"]').attr('src', 'https://static.techsembly.com/mshchPVjAi8mHf3mZMWhbNQV');
            $('.product-detail-container .product-holder .product-detail .product-info .product-heading .product-rating a.wishlistImage img[alt="add-to-wishlist"]').attr('src', 'https://static.techsembly.com/mshchPVjAi8mHf3mZMWhbNQV');

            $('.footer a:contains("+44")').each(function(){
              var string_phone= $(this).html();
              var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
              var phone_no = "tel:" + result_phone;
              if(!$(this).hasClass('tel-link')){
                $(this).addClass('tel-link');
                $(this).attr('href', phone_no);
              }
            });
            $('.footer a:contains("@")').each(function(){
              var string_email= $(this).html();
              var result_email = string_email.substring(string_email.indexOf(':') + 1);
              var email_link = "mailto:" + result_email;
              if(!$(this).hasClass('email-link')){
                $(this).addClass('email-link');
                $(this).attr('href', email_link);
              }
            });

            $('.footer .footer-widget .widget-title:contains("KEEP IN TOUCH")').each(function() {
                if(!$(this).closest('.footer-widget').hasClass('text-right')){
                  $(this).closest('.footer-widget').addClass('text-right');
                  $('.footer-widget.text-right .widget-title').html("Follow us on:");
                }
            });
            $('.sign-up-section .sign-up-container .sign-up-form .d-flex').removeClass('flex-column');
            $('.footer .footer-widget .widget-content .social-links a img[alt="twitter"]').attr('src', 'https://static.techsembly.com/N48Li22wSsVQtFLMZcCLC3WJ');
            $('.footer .footer-widget .widget-content .social-links a img[alt="fb"]').attr('src', 'https://static.techsembly.com/pH4TT8kSNys3NsK5setS5zu1');
            $('.footer .footer-widget .widget-content .social-links a img[alt="instagram"]').attr('src', 'https://static.techsembly.com/gLLYiGhzv6D5VtaXB39iLcfF');

        })
      })
    })
})();
