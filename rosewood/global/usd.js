function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }
    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });
    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}
function addAnnounceMentBar() {
  var announcementBar = '<div class="notificationbar">' +
    '<span>Our Commitment to Our Guests: <a href="#"><u>COVID-19</u></a></span>' +
    '<span class="cross-cont"><img width="12" src="https://static.techsembly.com/yApgKFNtUAA6Ae3oQVCNBMck"/></span>' +
    '</div>';
  var elemContent = $('body #content')[0];
  elemContent.insertAdjacentHTML('afterbegin', announcementBar);
}
function addSidebarCross() {
  var crossCont = '<div class="sidebar-cross-cont">' +
    '<div class="inner-cont"><img width="19" src="https://static.techsembly.com/yApgKFNtUAA6Ae3oQVCNBMck"/></div>' +
    '</div>';
  var sidebarContent = $('body #sidebar')[0];
  sidebarContent.insertAdjacentHTML('afterbegin', crossCont);
}
function addCCYToolTip() {
  var ccyToolTip = '<div class="ccy-tooltip-cont d-none d-lg-block"><div class="ccy-tooltip">' +
    '<div class="tooltip-desc">Simply choose your<br> preferred currency here.</div>' +
    '<span class="cross-cont">X</span>' +
    '</div></div>';
  var elemToggleMenuCont = $('header .row.spacing .toggle-menu-cont')[0];
  elemToggleMenuCont.insertAdjacentHTML('beforeend', ccyToolTip);
}
function addMenuLabel() {
  var menuLabel = '<span class="mob-menu-label text-uppercase">Menu</span>';
  var elemMenuButton = $('header.header .mobile-menu#sidebarCollapse')[0];
  elemMenuButton.insertAdjacentHTML('beforeend', menuLabel);
}
function addRecipientInfoTop() {
  var recipientInfo = "<div class='form-group recipient-info-top'><label class='control-label'>Recipient Details</label><div class='recipient-note'></div></div>";
  var elemRecNameInput = $('.product-detail-container .personalise-form .delivery-form .delivery-fields[formcontrolname="receipient_first_name"]')[0];
  elemRecNameInput.insertAdjacentHTML('beforebegin', recipientInfo);
}
function addLoginCartNote() {
  var loginCartNote = "<div class='login-note'>Please note this login is for Rosewood Hotels & Resorts Gift Cards & Global Experiences.<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}
function addPersonalDataNote() {
  var loginPersonalDataNote = "<div class='personal-data-note'>Your personal data will be processed in accordance with Rosewood Hotel Groupâ€™s <a href='https://www.rosewoodhotels.com/en/privacy-policy' target='_blank'>Privacy Policy</a>.<div>";
  var elemHeading = $('.custom-container .login-signup-tabs-cont .right-panel .signup-form')[0];
  elemHeading.insertAdjacentHTML('afterend', loginPersonalDataNote);
}
function addGuestCheckoutNote() {
  var checkoutNote = "<div class='checkout-note text-center w-100'>If you do not have an account please use guest checkout. You will be given the option to save account information later in the checkout process<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}
function addPaymentNotes() {
  var payNotesHtml = '<div class="col-12 col-lg-8 px-0 payment-notes-outer"><div class="col-12 payment-notes-cont p-4"><h4 class="notes-heading">Note:</h4>' +
    '<div class="notes-cont">' +
    '<p>Your purchase order is subject to Rosewood&apos;s <a href="https://www.rosewoodhotels.com/en/gift-cards-terms-and-conditions" target="_blank">Terms and Conditions</a> and our service provider Techsembly&apos;s <a href="pages/terms-conditions-gc-experiences" target="_blank">Terms and Conditions</a>. </p><br>' +
    '<p>By confirming your order you agree to Rosewood&apos;s <a href="https://www.rosewoodhotels.com/en/gift-cards-terms-and-conditions" target="_blank">Terms and Conditions</a> and Techsembly&apos;s <a href="pages/terms-conditions-gc-experiences" target="_blank">Terms and Conditions</a> and the processing of your personal data as described in Rosewood&apos;s <a href="https://www.rosewoodhotels.com/en/privacy-policy" target="_blank">Privacy Policy</a>. This purchase will display on my statement as  TS* Rosewood Hotels.</p><br>' +
    '<p>For assistance please contact Customer Service, 7 days a week, 24 hours a day. </p><br>' +
    '<p>UK Toll-free: +44 800 0488077<br>' +
    'US Toll-free: +1 888 2258452<br>' +
    'US Local: +1 203 5838588<br>' +
    'Hong Kong Local: +852 2319 4897<br>' +
    '</p>'
  '</div>' +
    '</div>' +
    '</div>';
  //var elem = $(".custom-container.checkout .payment-form-container .left-panel")[0];
  var elem = $(".checkout  .payment-form-container .payment-form")[0]
  elem.insertAdjacentHTML('beforeend', payNotesHtml);
}
function addSignUpSection() {
  var newSignupSection = '<section class="sign-up-section new">' +
    '<div  class="sign-up-container"><h2  class="mb-2">STAY CONNECTED</h2>' +
    '<div  class="signup-text text-center">' +
    '<p>Be the first to receive Rosewood news and exclusive offers</p>' +
    '<a href="https://rwhg-mkt-prod1-m.adobe-campaign.com/lp/RosewoodWebSubscriptions?languageCode=en&propertyCode=Brand&source=WEB&emailAddress=" class="btn btn-primary btn-signup" type="button" target="_blank">SIGN UP</a>' +
    '</div>' +
    '</div>' +
    '</section>';
  var elemFooter = $('footer.footer')[0];
  elemFooter.insertAdjacentHTML('beforebegin', newSignupSection);
}
function addcopyrightText() {
  const d = new Date();
  let copyrightYear = d.getFullYear();
  //console.log(copyrightYear);
  var copyrightHtml = '<div class="container-fluid"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-7 col-md-6 tc-rights-reserved pl-0">©' + copyrightYear +
    ' Rosewood Hotel Group</div><div class="col-5 col-md-6 ts-copyright text-right px-0"><span>Powered by</span><span class="ml-1"><a href="https://techsembly.com/" target="_blank">Techsembly</a></span></div></div></div>';
  var elem = $(".checkout-footer")[0];
  elem.insertAdjacentHTML('afterend', copyrightHtml);
}
(function () {
  var script = document.createElement("SCRIPT");
  script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
  script.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script);

  var script_fonts = document.createElement("SCRIPT");
  script_fonts.src = 'https://bitbucket.org/TSTechsembly/jscss/raw/7e4aac0e05611b48a233545953efd8cd679a4d6b/rosewood/global/fonts.js';
  script_fonts.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script_fonts);


  // Poll for jQuery to come into existence
  var checkReady = function (callback) {
    if (window.jQuery) {
      callback(jQuery);
    }
    else {
      window.setTimeout(function () { checkReady(callback); }, 20);
    }
  };


  // Now lets do something
  checkReady(function ($) {
    $(function () {
      $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">');
      waitForElm('body #content').then((elm) => {
        //addAnnounceMentBar();
        //$("body").addClass("announce-open");
      }).catch((error) => { });
      waitForElm('body #sidebar').then((elm) => {
        addSidebarCross();
      }).catch((error) => { });

      waitForElm('header.header .mobile-menu').then((elm) => {
        //addMenuLabel();
      }).catch((error) => { });

      waitForElm('header.header .toggle-menu-cont').then((elm) => {
        addCCYToolTip();
      }).catch((error) => { });

      waitForElm('.product-detail-container .personalise-form .delivery-form').then((elm) => {
        addRecipientInfoTop();
      }).catch((error) => { });

      waitForElm('.product-detail-container .breadcrumb').then((elm) => {
        // Find the "Experiences" word in the breadcrumbs
        var breadcrumbItems = $(".breadcrumb-item");
        breadcrumbItems.each(function () {
          var breadcrumbText = $(this).text().trim();
          if (breadcrumbText === "Experiences") {
            $("app-product-detail .container.product-detail-container").addClass("experiences");
            return false; // Stop iterating once found
          }
        });
      }).catch((error) => { });
      waitForElm('.product-detail-container .product-holder .product-sku').then((elm) => {
        var productSKU = $.trim($('.product-detail-container .product-holder .product-sku').html());
        if (productSKU =='041367' || productSKU =='041368' || productSKU =='041276' || productSKU =='041269') {
          $('.product-detail-container .product-holder').addClass('hide-last-thumb-modified');
        }
      });

      waitForElm('.product-detail-container .product-holder .product-sku').then((elm) => {
        var productSKU = $.trim($('.product-detail-container .product-holder .product-sku').html());
        if (productSKU == '038613') {
          if ($(".product-detail-container .product-holder").hasClass('digital')) {
            $("<div class='deliveryNote1'>We will send the gift card to your recipient’s inbox with a personalized message of your choice.</div>").insertBefore(".product-detail-container .personalise-form .delivery-form .recipient-info-top");
          }
          else if ($(".product-detail-container .product-holder").hasClass('physical')) {
            $("<div class='deliveryNote1'>We will send the gift card to your recipient’s address with a personalized message of your choice.</div>").insertBefore(".product-detail-container .personalise-form .delivery-form .recipient-info-top");
          }
        }
      }).catch((error) => { });

      waitForElm('.product-detail-container').then((elm) => {
        var productSKU = $.trim($('.product-detail-container .product-holder .product-sku').html());

        if (productSKU == '030113') {

          $("#item-quantity").focusout(function (e) {
            var unitProPrice = $('#product-price').html();
            var unitProPriceFloat = parseFloat(unitProPrice.replace(/[^0-9.]/g, ""));
            var unitProQty = $(this).val();
            var unitPriceCalc = unitProPriceFloat * unitProQty;
            console.log(unitPriceCalc);
            if (unitPriceCalc > 10000) {
              e.preventDefault();
              alert('You have exceeded the Gift Card daily transaction limit of <<USD 10,000>>');
              $('.product-detail-container .order-btn').attr('disabled', 'disabled');
              $('.product-detail-container .buy-btn').attr('disabled', 'disabled');
            }
            else {
              $('.product-detail-container .order-btn').removeAttr('disabled');
              $('.product-detail-container .buy-btn').removeAttr('disabled');
            }
          });

          $('select[title="select-tag-for-gift-card"]').on('change', function (e) {
            var unitProPrice = $('#product-price').html();
            var unitProPriceFloat = parseFloat(unitProPrice.replace(/[^0-9.]/g, ""));
            var unitProQty = $("#item-quantity").val();
            var unitPriceCalc = unitProPriceFloat * unitProQty;
            console.log(unitPriceCalc);
            if (unitPriceCalc > 10000) {
              e.preventDefault();
              alert('You have exceeded the Gift Card daily transaction limit of <<USD 10,000>>');
              $('.product-detail-container .order-btn').attr('disabled', 'disabled');
              $('.product-detail-container .buy-btn').attr('disabled', 'disabled');
            }
            else {
              $('.product-detail-container .order-btn').removeAttr('disabled');
              $('.product-detail-container .buy-btn').removeAttr('disabled');
            }
          });


          /*$('#item-quantity').attr('max','5');
          $("#item-quantity").focusout(function(e) {
            var val = $(this).val();
            var max = $(this).attr("max");
            if (max > 0 && val > max){
                e.preventDefault();
                alert('Max no. of products allowed is '+max+' per order');
                $(this).val(max);
            }
          });*/
        }
      }).catch((error) => { });

      waitForElm('input#user-gift-amount').then((elm) => {
        var productSKU = parseInt($('.product-detail-container .product-holder .product-sku').html());
        if (productSKU == '030113') {
          $('#user-gift-amount').focusout(function (e) {
            var unitProPrice = $('#product-price').html();
            var unitProPriceFloat = parseFloat(unitProPrice.replace(/[^0-9.]/g, ""));
            var unitProQty = $("#item-quantity").val();
            var unitPriceCalc = unitProPriceFloat * unitProQty;
            console.log(unitPriceCalc);
            if (unitPriceCalc > 10000) {
              e.preventDefault();
              alert('You have exceeded the Gift Card daily transaction limit of <<USD 10,000>>');
              $('.product-detail-container .order-btn').attr('disabled', 'disabled');
              $('.product-detail-container .buy-btn').attr('disabled', 'disabled');
            }
            else {
              $('.product-detail-container .order-btn').removeAttr('disabled');
              $('.product-detail-container .buy-btn').removeAttr('disabled');
            }
          });
        }
      }).catch((error) => { });

      waitForElm('#max-transaction-popup').then((elm) => {
        $('#max-transaction-popup .return-btn').click(function () {
          $('.container.custom-container.cart-container .cart-login-cont .btn.btn-primary').removeClass('disabled');
        });
      }).catch((error) => { });

      var values = [];
      var sum;
      var unitPrices = [];
      var unitPricesTotal;

      waitForElm('.custom-container.cart-container .cart-item.new').then((elm) => {
        $('.cart-item.new .item-details .item-inc').click(function () {
          $('.container.custom-container.cart-container .cart-login-cont .btn.btn-primary').addClass('disabled');
        });

      }).catch((error) => { });

      waitForElm('.cart-right-cont').then((elm) => {
        addLoginCartNote();
        addGuestCheckoutNote();
      }).catch((error) => { });

      waitForElm('.custom-container.checkout .payment-form-container').then((elm) => {
        addPaymentNotes()
      }).catch((error) => { });

      waitForElm('app-footer-checkout').then((elm) => {
        addcopyrightText()
      }).catch((error) => { });

      waitForElm('.custom-container .login-signup-tabs-cont .right-panel').then((elm) => {
        addPersonalDataNote()
      }).catch((error) => { });

      waitForElm('footer.footer').then((elm) => {
        addSignUpSection();
      }).catch((error) => { });

      waitForElm('.product-detail-container .personalise-form').then((elm) => {
        $("#gift-card-value-Select gift card value").change(function () {
          console.log($('#product-price').html());
        });
      }).catch((error) => { });

      // ========= added for rosewood retail site ==========
      waitForElm('.festive-outer-html-block').then((elem) => {
        $('.festive-outer-html-block').each(function () {
          $(this).parent().addClass('festive-outer-modified');
        });
      }).catch((error) => { });


      waitForElm('.ultimate-gift-card').then((elem) => {
        $('.ultimate-gift-card').each(function () {
          $(this).parent().addClass('ultimate-gift-card-outer');
        });
      }).catch((error) => { });
      waitForElm('.container.home-intro-container').then((elm) => {
        $($('.container.home-intro-container')[1]).addClass('discoverExp-modified');
      }).catch((error) => { });
      waitForElm('.home-banner .banner-heading a div').then((elm) => {
        $('.home-banner .banner-heading a div:contains("THE ULTIMATE GIFT")').each(function () {
          $(this).closest(".home-banner").addClass("home-gift-section");
        });
      }).catch((error) => { });
      waitForElm('.view-experiences-cta').then((elem) => {
        $('.view-experiences-cta').each(function () {
          $(this).parent().addClass('view-experiences-cta-outer');
        });
      }).catch((error) => { });
      waitForElm('.cta-btn').then((elem) => {
        $('.cta-btn').each(function () {
          $(this).attr('target', '_self');
        });
      }).catch((error) => { });
      waitForElm('#same-billing-address').then((elem) => {
        setTimeout(function () {
          let checkbox = document.getElementById("same-billing-address");
          // Create a new click event
          var clickEvent = new MouseEvent("click", {
            bubbles: true,
            cancelable: true,
            view: window
          });

          // Dispatch the click event on the checkbox
          checkbox.dispatchEvent(clickEvent);
          //checkbox.setAttribute("disabled", 'disabled');
        }, 200);
      }).catch((error) => { });

      waitForElm('.product-detail-container .product-holder .product-order .nav-tabs').then((elm) => {
        var segment_str = window.location.pathname; // return segment1/segment2/segment3/segment4
        var segment_array = segment_str.split('/');
        var last_segment = segment_array.pop();
        console.log(last_segment)
        if (last_segment == 'gift-card-030113') {
          $(".product-detail-container .product-holder .product-order .nav-tabs").each(function () {

            // if (!$(this).hasClass('d-none')) {
            $(this).addClass('d-none')
            // }
          });
        }
      })
      waitForElm('#sidebar .list-group-holder').then((elm) => {
        $('.list-group-holder .list-group-item-action').click(function () {
          let client_collapse_target = $(this).attr('href');
          let aria_exp = $(this).attr('aria-expanded');
          let target_div = client_collapse_target.substring(client_collapse_target.indexOf('#') + 1);
          //let target_div_elm = $(this).closest('.list-group-holder').find('#'+target_div);
          let target_div_elm = document.getElementById(target_div);
          console.log(target_div_elm.classList);
          setTimeout(() => {
            if ($(this).hasClass('down')) {
              target_div_elm.classList.remove('d-none');
            }
            else if (!$(this).hasClass('down')) {
              target_div_elm.classList.remove('show');
              target_div_elm.classList.add('d-none');
            }
          }, 300);
          //console.log(target_div)
        });
        $('.list-group-item-action').collapse({
          toggle: false
        })
      }).catch((error) => { });
      
      // waitForElm('#sidebar .mob-nav.accordian li.nav-item').then((elm) => {
      //   console.log("arrived in dom")
      //   $(".mob-nav .collapse-link").click(function () {
      //     // Get all collapse links
      //     console.log($(this));
      //     let clientCollapseLinks = document.querySelectorAll('.mob-nav .collapse-link');
      //     let clientCollapseDivs = document.querySelectorAll('.mob-nav .sub-menu-cont');

      //     // Add click event listener to each collapse link
      //     clientCollapseLinks.forEach(function (link) {
      //       link.addEventListener('click', function (event) {
      //         event.preventDefault(); // Prevent default link behavior
      //         console.log(this.getAttribute('data-target'))
      //         // Find the associated collapse element
      //         let client_collapse_target = this.getAttribute('data-target');

      //         let clientTargetId = client_collapse_target.substring(client_collapse_target.indexOf('#') + 1);
      //         let targetElement = document.getElementById(clientTargetId);

      //         // Toggle the 'collapsed' class on the target element
      //         if (targetElement && link.classList.contains('down')) {
      //           setTimeout(function () {
      //             targetElement.classList.remove('d-none');
      //             console.log("has down class")
      //           },200)
      //         }
      //         else {
      //           targetElement.classList.add('d-none');
      //           //link.setAttribute("aria-expanded", "false");
      //           //console.log('collapsed')
      //         }
      //         console.log(link.classList)
      //         clientCollapseLinks.forEach(function (element) {
      //           //console.log(element.getAttribute('aria-controls'));
      //           if (element.getAttribute('aria-controls') != clientTargetId) {
      //             element.classList.remove('down');
      //           }
      //         });
      //         clientCollapseDivs.forEach(function (element) {
      //           setTimeout(function () {
      //             if (element.id != clientTargetId) {
      //               //console.log(element.id);
      //               element.classList.remove('d-none');
      //             }
      //           }, 200);
      //         });
      //       });
      //     });

          
      //   });
      // }).catch((error) => { });

      // ========= added for rosewood retail site end ==========

      setTimeout(function () {
        $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
        $('.wishlist-container .products-container .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
      }, 1000);

      setInterval(function () {
                      // video js start
        var $video = $("#click"),
        mousedown = false;

        $video.on('mousedown', function () {
          mousedown = true;
        });

        $(window).on('mouseup', function () {
          mousedown = false;
        });

        $video.on('play', function () {
          $video.attr('controls', '');
          $('.playpause').addClass('hide');
        });

        $video.on('pause', function () {
          if (!mousedown) {
            $video.removeAttr('controls');
          }
          $('.playpause').removeClass('hide');
        });


        $('.video').parent().click(function () {
          if ($(this).children(".video").get(0).paused) {
            $(this).children(".video").get(0).play();
          }
        });
        
        if (!$('.video-main').hasClass('parent-modified')) {
          $('.video-main').parent().addClass('px-0');
          $('.video-main').addClass('parent-modified');
        }
        
        $(".notificationbar .cross-cont").click(function () {
          if (!$(".notificationbar").hasClass('d-none')) {
            $(".notificationbar").addClass('d-none');
            $("body").removeClass("announce-open");
          }
        });
        $(".ccy-tooltip-cont .cross-cont").click(function () {
          if (!$(".ccy-tooltip-cont").hasClass('d-lg-none')) {
            $(".ccy-tooltip-cont").removeClass('d-lg-block').addClass('d-lg-none');
          }
        });
        $('header.header .shipping li .dropdown .dropdown-menu').each(function () {
          if (!$(this).hasClass('countries-dd')) {
            $(this).addClass('countries-dd');
            $(this).append('<div class="countries-links-cont">' +
              '<div class="hkd"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://shop.rosewoodhotels.com/hkd">HKD</a></div>' +
              '<div class="usd"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://shop.rosewoodhotels.com/usd">USD</a></div>' +
              // '<div class="gbp"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://shop.rosewoodhotels.com/gbp">GBP</a></div>' +
              '<div class="eur"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://shop.rosewoodhotels.com/eur">EUR</a></div>' +
              // '<div class="cad"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://shop.rosewoodhotels.com/cad">CAD</a></div>' +
              '</div>');
          }
        });

        $('#sidebar ul.list-unstyled.user-links').each(function () {
          if (!$(this).hasClass('user-mob-links')) {
            $(this).addClass('user-mob-links');
            var activeCCY = $('header.header .shipping li a.dropdown-toggle').html();
            $(this).append('<li>' +
              '<div class="d-flex align-items-center">' +
              '<div class="dropdown pref_dropdown w-100">' +
              '<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownMenuLink_mob">' + activeCCY + '</a>' +
              '<div aria-labelledby="dropdownMenuLink" class="dropdown-menu countries-links-cont pt-0 mt-0" x-placement="top-start">' +
              '<div class="hkd"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://shop.rosewoodhotels.com/hkd">HKD</a></div>' +
              '<div class="usd"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://shop.rosewoodhotels.com/usd">USD</a></div>' +
              // '<div class="gbp"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://shop.rosewoodhotels.com/gbp">GBP</a></div>' +
              '<div class="eur"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://shop.rosewoodhotels.com/eur">EUR</a></div>' +
              // '<div class="cad"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://shop.rosewoodhotels.com/cad">CAD</a></div>' +
              '</div>' +
              '</div>' +
              '</div>' +
              '</li>');
          }
        });
        $(".head-links .profile-icon img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/gSKYD9Y6cMQH3NaEBUPkGFXr");
        $(".head-links .wishlist-heart img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/3WkfvrNVHgazZq8TytpGjAk6");
        $(".head-links .cart-bag img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/6jYH3dCJQpMuSxCH7ZMJL5u2");
        $(".header .menu-links-cont .search-item-form .btn-search img:not(.altered)").addClass('altered').attr("src", "https://static.techsembly.com/MMt3jVMDRfZy2r4goZ4UwZmQ");
        $('header .mob-top-links li a img[alt="search image"]:not(.altered)').addClass('altered').attr('src', 'https://static.techsembly.com/FPdNZj6KvAVStVB3PUekq4os');
        $('header .mob-top-links li a img[alt="cart"]:not(.altered)').addClass('altered').attr('src', 'https://static.techsembly.com/tX7ATTP9ShgxeKbzRhXJR9F5');
        $("header.header .head-links li .add-to-basket .dropdown-menu h4:not(.altered)").addClass('altered').html("SHOPPING BAG");
        $('header .mob-top-links .add-to-basket .dropdown-menu h4:not(.altered)').addClass('altered').html('SHOPPING BAG');
        $('header.header .mobile-menu#sidebarCollapse svg').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('height', '23.5');
            $(this).attr('width', '30');
            $(this).find('rect').attr('height', '8');
            $(this).find('rect:nth-child(1)').attr('y', '0');
            $(this).find('rect:nth-child(2)').attr('y', '35');
            $(this).find('rect:nth-child(3)').attr('y', '70');
          }
        });
        if (!$('header.header .toggle-menu-cont').hasClass('col-3')) {
          $('header.header .toggle-menu-cont').addClass('col-3').removeClass('col-1 col-sm-2 col-md-2');
        }
        if (!$('header.header .menu-links-cont').hasClass('col-3')) {
          $('header.header .menu-links-cont').addClass('col-3').removeClass('col-md-6 col-sm-5 col-7');
        }
        if ($('header.header .logo').parent('div').hasClass('col-sm-5')) {
          $('header.header .logo').parent('div').removeClass('col-4 col-sm-5');
          $('header.header .logo').parent('div').addClass('col-6 logo-cont');
        }
        $('header.header .menu-holder>li>div').each(function () {
          if (!$(this).children().length) {
            $(this).addClass('empty');
          }
        });

        $('header.header .head-links li a.profile-icon').closest('.nav-item:not(.profile-cont)').addClass('profile-cont');
        $('header.header .head-links li a.wishlist-heart').closest('.nav-item:not(.wishlist-cont)').addClass('wishlist-cont');
        $('header.header .head-links li a.cart-bag').closest('.nav-item:not(.cart-cont)').addClass('cart-cont');
        //$('header.header .head-links li a img[alt="user-profile"]').closest('.nav-item:not(.profile-cont)').addClass('profile-cont');
        //$('header.header .head-links li a img[alt="wishlist"]').closest('.nav-item:not(.wishlist-cont)').addClass('wishlist-cont');
        $('nav#sidebar ul li a img[alt="wishlist"]').closest('li:not(.wishlist-cont)').addClass('wishlist-cont');
        //$('header.header .head-links li a img[alt="cart"]').closest('.nav-item:not(.cart-cont)').addClass('cart-cont');

        hrefurl = $(location).attr("href");
        last_part = hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
        currentUrlSecondPart = window.location.pathname.split('/')[1];

        if (currentUrlSecondPart == 'catalogsearch') {
          if (!$('body').attr('data-pagetype', 'search_results')) {
            $('body').attr('data-pagetype', 'search_results');
          }
        }
        if (currentUrlSecondPart == 'vendors') {
          if (!$('body').attr('data-pagetype', 'vendors')) {
            $('body').attr('data-pagetype', 'vendors');
          }
        }
        if (last_part == 'wishlist') {
          if (!$('body').attr('data-pagetype', 'wishlist')) {
            $('body').attr('data-pagetype', 'wishlist');
          }
        }

        $('app-product-listing .product-container .product-select').closest('.row.mb-3:not(.breadcrumb-holder)').addClass('breadcrumb-holder');

        $('.products-container .products-holder-main .products-holder .product-column .product-list .product-content .product-rating a.wishlistImage img[alt="add-to-wishlist"]:not(.appeared)').addClass('appeared').attr('src', 'https://static.techsembly.com/u2nBLFoGVLEz56YcdK2G54Lb');
        $('.products-container .products-holder-main .products-holder .product-column .product-list .product-content .product-rating a.wishlistImage img[alt="added-to-wishlist"]:not(.appeared)').addClass('appeared').attr('src', 'https://static.techsembly.com/76vMMmLE3poX4WSFBv2frvhF');

        $('.mobile-nav .btn-transparent#refineBtn:not(.modified)').addClass('modified').html('Filter');

        $('.wishlist-container .products-container .products-holder .product-column .product-list .product-content .product-rating .wishlistImage img[alt="added-to-wishlist"]:not(.appeared)').addClass('appeared').attr('src', 'https://static.techsembly.com/76vMMmLE3poX4WSFBv2frvhF');

        $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
        $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');

        $('.product-detail-container .product-holder .product-detail .product-info .product-heading .product-rating a.wishlistImage img[alt="add-to-wishlist"]:not(.appeared)').addClass('appeared').attr('src', 'https://static.techsembly.com/u2nBLFoGVLEz56YcdK2G54Lb');
        $('.product-detail-container .product-holder .product-detail .product-info .product-heading .product-rating a.wishlistImage img[alt="added-to-wishlist"]:not(.appeared)').addClass('appeared').attr('src', 'https://static.techsembly.com/76vMMmLE3poX4WSFBv2frvhF');

        $('.product-detail-container .personalise-form .form-group label').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(".personalise-form .form-group label:contains('*')").html(function (_, html) {
              //return  html.replace(/\*/g, '<span class="updated-string">*</span>');
            });
          }
        });

        $('.personalise-form .form-group input[formcontrolname="user_gift_amount"] + .alert-secondary').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Please select the gift card value from USD$50 to USD$2,000 ');
          }
        });

        $('.product-detail-container .personalise-form .delivery-form .title:contains("Delivery Options")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Delivery Option');
          }
        });

        $('.product-detail-container .personalise-form .delivery-form .radio-container input[formcontrolname="delivery_mode"]').each(function () {
          if (!$(this).closest('.radio-holder').hasClass('modified')) {
            $(this).closest('.radio-holder').addClass('modified');
            $(this).closest('.radio-holder').find('span:contains("Digital")').html('E-Gift Card');
            $(this).closest('.radio-holder').find('span:contains("Physical")').html('Gift Card');
            if (navigator.platform.indexOf('Win') > -1) {
              $(this).closest('.radio-holder').addClass('win-radio-holder');
            }
          }
        });

        $('.product-detail-container .personalise-form .delivery-form .delivery-fields[formcontrolname="receipient_first_name"]').each(function () {
          var infoText;
          // var recipientInfo = "<div class='form-group recipient-info-top'><label class='control-label'>Recipient Details</label><div class='recipient-note'></div></div>";
          // if (!$('.product-detail-container .personalise-form .delivery-form').find('.recipient-info-top').length) {
          //   //$(recipientInfo).insertBefore($(this));
          // }
          var productSKU = $.trim($('.product-detail-container .product-holder .product-sku').html());
          if ($(this).closest('.product-holder').hasClass('digital')) {
            infoText = "Please share the name and email of the gift card recipient";

            if (!$(this).hasClass('digital-rec')) {
              $(this).addClass('digital-rec');
              $(this).removeClass('physical-rec');
              $('.recipient-note').html(infoText);
              if (productSKU == '038613') {
                $('.deliveryNote1').html("We will send the gift card to your recipient’s inbox with a personalized message of your choice.");
              }
            }
          }
          else if ($(this).closest('.product-holder').hasClass('physical')) {
            infoText = "Please share the name of the gift card recipient";
            if (!$(this).hasClass('physical-rec')) {
              $(this).addClass('physical-rec');
              $(this).removeClass('digital-rec');
              $('.recipient-note').html(infoText);
              if (productSKU == '038613') {
                $('.deliveryNote1').html("We will send the gift card to your recipient’s address with a personalized message of your choice.")
              }
            }
          }
        });

        $('.product-detail-container .personalise-form .delivery-form .delivery-fields[formcontrolname="message"]').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('placeholder', 'Write a Gift Message (optional)');
          }
        });

        $('.product-detail-container .personalise-form .delivery-form .notify:contains("Up to 200 characters")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Maximum 200 characters');
          }
        });

        $('.product-detail-container .product-holder .personalise-form .delivery-form .info-text').each(function () {
          var msg_txt;
          if ($(this).closest('.product-holder').hasClass('digital')) {
            msg_txt = "<b>Maximum US$2,000 per E-Gift card and US$10,000 per transaction.</b> If you would like to add a unique message for each card, please add each gift card separately to the shopping bag.";
            if (!$(this).hasClass('digital-info-text')) {
              $(this).addClass('digital-info-text');
              $(this).removeClass('physical-info-text');
              $(this).html(msg_txt);
            }
          }
          else if ($(this).closest('.product-holder').hasClass('physical')) {
            msg_txt = "<b>Maximum US$2,000 per card and US$10,000 per transaction.</b> Your message will apply to all Gift Cards to one single shipping address. If you would like to add a unique message for each card, please add each gift card separately to the shopping bag.";
            if (!$(this).hasClass('physical-info-text')) {
              $(this).addClass('physical-info-text');
              $(this).removeClass('digital-info-text');
              $(this).html(msg_txt);
            }
          }
        });

        $('.product-detail-container .personalise-form .form-group .order-btn').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('ADD TO BAG');
          }
        });

        $('.product-detail-container .personalise-form .form-group .buy-btn').each(function () {
          /*if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Proceed to Checkout');
          }*/
        });

        $('.product-detail-container .product-holder .share-link li:first-child span').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Share via');
          }
        });

        $('.cart-container .qty-col.d-lg-block .item-count').each(function () {
          if (!$(this).hasClass('counted')) {
            $(this).addClass('counted');
            var itemVal = parseInt($(this).html());
            values.push(itemVal);
            console.log(values);
            sum = values.reduce(function (a, b) { return a + b; }, 0);
            console.log(sum);

            /*if (sum > 5) {
              alert('More than 5 items not allowed in one order');
              $('.container.custom-container.cart-container .cart-login-cont .btn.btn-primary').addClass('disabled');
            }
            else {
              $('.container.custom-container.cart-container .cart-login-cont .btn.btn-primary').removeClass('disabled');
            }*/
          }
        });
        $('.cart-item.new').each(function () {
          if (($(this).attr('id') == 'line-item-30113') && !$(this).hasClass('givex-product')) {
            $(this).addClass('givex-product');
            var unitPrice = $(this).find('.unit-total-price').html();
            var unitPriceFloat = parseFloat(unitPrice.replace(/[^0-9.]/g, ""));
            var unitQty = parseInt($(this).find('.item-count').first().html());
            unitPrices.push(parseInt(unitPriceFloat));
            unitPricesTotal = unitPrices.reduce(function (a, b) { return a + b; }, 0);
            console.log(unitPrices);
            console.log(unitPricesTotal);
            if (unitPricesTotal > 10000) {
              alert('You have exceeded the Gift Card daily transaction limit of <<USD 10,000>>');
              $('.container.custom-container.cart-container .cart-login-cont .btn.btn-primary').addClass('disabled');
            }
            else {
              $('.container.custom-container.cart-container .cart-login-cont .btn.btn-primary').removeClass('disabled');
            }
          }
        });


        $('#cart-notification .modal-container.login-modal .modal-header .panel-heading').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Products May Be Removed From Your BAG');
          }
        });

        $('#cart-notification .modal-container.login-modal .modal-body p.info-text').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Proceed To Checkout enables you to have quick check-out for this Gift Card product only. If you wish to check-out with all items in your cart, please select View Bag.');
          }
        });

        $('.modal-popup#cart-notification .footer-btns .right-btn .btn-primary').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Buy Now');
          }
        });

        $('#cart-notification .footer-btns .view-cart').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).wrap('<div class="footer-btns-left"></div>');
            $(this).html('View Bag');
          }
        });

        $('.login-modal .modal-body .container .store-logo').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('src', 'https://static.techsembly.com/fBczckgz1TVHUUt9n7q5ipAZ');
          }
        });
        $('.login-modal .checkout-desc-cont img[alt="lock"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/jBL6KnUEhRKVEmP3ZHxVKsgG');

        $('.modal-popup form.guestCheckout-form input#guest-email:not(.modified)').addClass('modified').attr('placeholder', 'Email');

        $('.product-detail-container .product-holder .share-link li:nth-child(2) img:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/L7tKASTK87qxYn9nXPGHzoNi');
        $('.product-detail-container .product-holder .share-link li:nth-child(3) img:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/GibF5PZYHLiYBNPw2LxytzRs');
        $('.product-detail-container .product-holder .share-link li:nth-child(4) img:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/M1SGqx8gAag9yvYsoyi1yzmR');

        $('.modal-popup#max-transaction-popup .modal-content').each(function () {
          if (!$(this).closest('.col-12').hasClass('modal-content-outer')) {
            $(this).closest('.col-12').addClass('modal-content-outer');
          }
        });

        $('.modal-popup#max-transaction-popup .btns-cont .return-btn').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Return to BAG');
          }
        });

        $('.login-modal .modal-close img').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('src', 'https://static.techsembly.com/EcNuhtwmAyReJj2tJcPuynEW');
          }
        });

        $('.custom-container.cart-container').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'cart')) {
            $(this).closest('body').attr('data-pagetype', 'cart');
          }
        });

        $('.custom-container.cart-container .vendor-items-cont .card-header .vendor-heading .delivered-by').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Fulfilled By');
          }
        });


        $('.cart-item.new .item-details .item-qty-cont').each(function () {
          if (!$(this).closest('.col-lg-2').hasClass('qty-col')) {
            $(this).closest('.col-lg-2').addClass('qty-col');
          }
        });

        $('.cart-item.new .item-details .unit-total-price').each(function () {
          if (!$(this).closest('.col-lg-3').hasClass('sub-total-col')) {
            $(this).closest('.col-lg-3').addClass('sub-total-col');
          }
        });

        $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/sgo534ygqVc6EzhG8fxSB1Aa');

        $('.custom-container.checkout').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'checkout')) {
            $(this).closest('body').attr('data-pagetype', 'checkout');
          }
        });

        $('.custom-container.cart-container .btn.continue-btn-new').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Back to Shop');
          }
        });

        $('.shipping-form-container .exist-address-wrapper .card a img[alt="edit"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/T82dXsi6BExFik2ZLvP2zutF');

        $('.container.custom-container.checkout .btn#continue-to-delivery-and-billing').each(function () {
          if (!$(this).closest('.form-group').hasClass('submit-btn-cont')) {
            $(this).closest('.form-group').addClass('submit-btn-cont');
            $(this).html('CONTINUE');
          }
        });

        $('.custom-container.checkout .address-form.new .form-control[formcontrolname="phone"]').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('placeholder', '(Country Code) Phone Number');
          }
        });

        $('.address-form.new select[formcontrolname="country"] option:disabled').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Country/Region');
          }
        });

        $('.container.custom-container.checkout.physical .billing-heading:contains("Shipping Address")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Recipient Address');
          }
        });

        $('.delivery-methods-cont .vendor-cards-holder .card .order-item a img[alt="edit"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/T82dXsi6BExFik2ZLvP2zutF');

        $('.delivery-methods-cont .vendor-cards-holder .card .delivery-dropdowns-cont .label-email .recipent-info-edit img[alt="edit-recipient"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/T82dXsi6BExFik2ZLvP2zutF');

        $('.delivery-methods-cont .card .card-body .card-row a img[alt="edit"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/T82dXsi6BExFik2ZLvP2zutF');

        $('.custom-container.checkout .delivery-methods-cont .vendor-cards-holder h3.info-text:contains(Recipientâ€™s Info)').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html("Recipient's Info");
          }
        });

        $('.delivery-methods-cont .vendor-cards-holder .card .order-item a.edit-item-new').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            var newTxt = 'Edit Gift Card';
            $("a.edit-item-new:contains('Edit Product')").html(function (_, html) {
              return html.replace(/(Edit Product)/g, '<span class="updated-edit-string">' + newTxt + '</span>');
            });
          }
        });

        $(".custom-container.checkout .delivery-methods-cont .vendor-cards-holder .card .order-shipping-container label:contains('Gift Message')").each(function () {
          if (!$(this).closest('.col-12').hasClass('form-group')) {
            $(this).closest('.col-12').addClass('form-group mb-0');
          }
        });

        $(".custom-container.checkout .delivery-methods-cont .vendor-cards-holder .card .order-shipping-container span.limit:contains('character')").each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('200 characters maximum');
          }
        });

        $('app-checkout-delivery-v2 h1.breadcrumb-heading').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Delivery Method');
          }
        });

        $('.custom-container.checkout .delivery-methods-cont .vendor-cards-holder .card .card-header .vendor-title span:contains("Delivered by")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Fulfilled By');
          }
        });

        $('.custom-container.checkout .delivery-methods-cont .vendor-cards-holder .card .shipping-totals-cont .total-caption:contains("Shipping")').each(function () {
          if (!$(this).closest('.total-row').hasClass('shipping-total-cont')) {
            $(this).closest('.total-row').addClass('shipping-total-cont');
          }
        });

        $('.payment-form-container .card-details-cont').each(function () {
          if (!$(this).children('h2.section-heading').length) {
            $(this).prepend('<h2 class="section-heading">Card Details</h2>');
            $(this).append('<div class="card-note">We accept Visa, Mastercard and American Express.</div>');
          }
        });

        $('.custom-container.checkout .payment-form-container .section-heading:contains("Card Details")').each(function () {
          if (!$(this).closest('.section-cont').hasClass('cc-details-cont')) {
            $(this).closest('.section-cont').addClass('cc-details-cont');
          }
        });

        $('.custom-container.checkout .payment-form-container .section-heading:contains("Details")').each(function () {
          if (!$(this).closest('.section-cont').hasClass('billing-detail-cont')) {
            $(this).closest('.section-cont').addClass('billing-detail-cont');
          }
        });

        //$('.payment-form-container .input-cont.password img.pass-eye:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/f2VPyxYkpvRZWKi5vUM3hp8N');

        //$('.checkout .password-note:not(.modified)').addClass('modified').html('Must be at least 6 characters including one special character');

        $('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc:contains("Shipping")').each(function () {
          if (!$(this).closest('.sub-total-item').hasClass('shipping-total-cont')) {
            $(this).closest('.sub-total-item').addClass('shipping-total-cont');
          }
        });

        $('.custom-container.checkout .order-totals-container .promo-note').each(function () {
          if (!$(this).closest('.form-group').hasClass('promo-note-warpper')) {
            $(this).closest('.form-group').addClass('promo-note-warpper');
          }
        });

        $(".custom-container.checkout .payment-form-container .btn.btn-submit:not(.modified)").addClass('modified').html('COMPLETE PURCHASE');

        $('.checkout .checkout-container.complete .card.order-confirm-wrapper').each(function () {
          if (!$(this).parent('.col-12').hasClass('order-confirm-outer')) {
            $(this).parent('.col-12').addClass('order-confirm-outer');
          }
        });

        $('.checkout-container.complete .card-header img[alt="Rosewood Hotels logo"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/fBczckgz1TVHUUt9n7q5ipAZ');

        $('.checkout-container.complete .section-cont.customer-note').each(function () {
          if (!$(this).hasClass('total-delivery')) {
            $(this).addClass('total-delivery');
            var firstPart = $(this).find('br')[0].previousSibling.nodeValue;

            var newTxt = 'Your order is being confirmed. You will receive an email with further information.';
            $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function (_, html) {
              return html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">' + newTxt + '</div>')
            });

            var secondPart = $(this).find('.del-content').html();

            $(this).html('<div class="salutation-cont d-none">' + firstPart + '</div><div class="del-content pt-0">' + secondPart + '</div>');
          }
        });

        $('.checkout-container.complete.physical .shipping-data .shipping-row .col-6:contains("Address")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Delivery Address:');
          }
        });

        $('.checkout-container.complete.digital .shipping-data .shipping-row .col-6:contains("Address")').each(function () {
          if (!$(this).closest('.shipping-row').hasClass('digital-product-address')) {
            $(this).closest('.shipping-row').addClass('digital-product-address');
          }
        });

        $('.checkout-container.complete:not(.digital) .msg-cont .msg-inner').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('You will receive a shipping confirmation when your Gift Card(s) are on the way.');
          }
        });

        $('.checkout-container.complete.digital .msg-cont .msg-inner').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Your E-Gift will be delivered to the recipients inbox shortly.');
          }
        });

        /*$('.checkout .checkout-container.complete .card-footer-right').each(function(){
          if(!$(this).hasClass('modified')){
            $(this).addClass('modified');
            $(this).html('<span>RHR Gift Card Services, L.L.C., a Virginia limited liability company</span>');
          }
        });*/

        $('.checkout .checkout-container.complete .card-footer-left span:contains("Questions")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Need help? Contact our');
          }
        });

        $('.checkout .checkout-container.complete .card-footer a:contains("Customer Support")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('href', 'mailto:rosewood@techsembly.com');
          }
        });

        $('.checkout-container.complete .card-footer').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).append('<div class="col-12 text-center home-btn-cont"><a class="btn btn-primary home-btn text-center text-uppercase" href="https://rosewood-hotels-resorts-usd-new-retail.techsembly.com/">back to home</a></div>');
          }
        });

        $('body.auth-user .user-container .ts-balance-form').closest('.user-container:not(.d-none)').addClass('d-none');

        $('.user-container .user-heading:contains("Check TS Gift Card Balance")').each(function () {
          if (!$(this).hasClass('gift-card-heading')) {
            $(this).addClass('gift-card-heading');
            $(this).html('Check Gift Card Balance');
          }
        });

        $('.user-container .user-heading:contains("My TS Card Balance")').each(function () {
          if (!$(this).hasClass('gift-card-heading')) {
            $(this).addClass('gift-card-heading');
            $(this).html('My Gift Card Balance');
          }
        });
        $('.ts-balance-form label[for="gift-card-no"]:not(.modified)').addClass('modified').html('Gift card number');
        $('.ts-balance-form input[name="gift-card-no"]:not(.modified)').addClass('modified').attr('placeholder', 'Enter gift card number');

        $('.custom-container.login-signup').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'login-signup')) {
            $(this).closest('body').attr('data-pagetype', 'login-signup');
          }
        });

        $('form.signup-form label.newsletter-check span:not(.modified)').addClass('modified').html("I agree to receive news and special offers emails in relation to hotels, products and services from Rosewood Hotel Group, its affiliates and other businesses or properties it owns or manages.");

        $('.corp-gifts-cont').each(function () {
          if (!$(this).closest('body').hasClass('corp-page')) {
            $(this).closest('body').addClass('corp-page');
          }
        });

        $('.custom-container.footer-page-container .breadcrumb-heading').each(function () {
          if (!$(this).closest('.col-lg-6').hasClass('modified')) {
            $(this).closest('.col-lg-6').addClass('modified col-lg-12').removeClass('col-lg-6');
          }
        });

        $('.footer .footer-widget .widget-content ul li a').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('target', '_blank');
          }
        });

        $('.footer .mobile-footer .footer-widget ul li a').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('target', '_blank');
          }
        });
        $('.home-gift-section').each(function () {
          if (!$('.home-gift-section:eq(0)').hasClass('d-lg-block')) {
            $('.home-gift-section:eq(0)').addClass('d-none d-lg-block');
          }
          if (!$('.home-gift-section:eq(1)').hasClass('d-lg-block')) {
            $('.home-gift-section:eq(1)').addClass('d-lg-none');
          }
        });

        $(".footer .footer-widget:last-child .widget-title:not(.modified)").addClass("modified").addClass('modified').html("Follow Us");
        $('.footer .footer-widget .social-links li a img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/kVHWRiaCq6MCshDvQ775Lnjp');
        $('.footer .footer-widget .social-links li a img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/CCokyF4cPuvJMwsrktFtZYyS');
        $('.footer .footer-widget .social-links li a img[alt="twitter"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/94GN39JddxjD41TVJhpiFhWK');
        $('.footer .footer-widget .social-links li a img[alt="pinterest"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/mZ2LSsvAmjoPdhk7ahyzAhFQ');
        $('.footer .footer-widget .social-links:not(.modified)').append('<li><a href="https://www.youtube.com/channel/UClZ2O-efl09dZ2BypTw2kng" rel="nofollow"><img class="modified" alt="youtube" width="32" src="https://static.techsembly.com/1E4EQD8YiKiNMJKbiyicRTf3"></a></li>');
        $('.footer .footer-widget .social-links:not(.modified)').addClass('modified').append('<li><a href="https://www.rosewoodhotels.com/en/wechat" rel="nofollow"><img class="modified" alt="wechat" width="32" src="https://static.techsembly.com/1ADctJsvZbB5RM6oLN12h88F"></a></li>');
        $('header.header .menu-holder>li>div:not(.modified)').addClass('modified submenu');

        $('.footer a:contains("+1")').each(function () {
          var string_phone = $(this).html();
          var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
          var phone_no = "tel:" + result_phone;
          if (!$(this).hasClass('tel-link')) {
            $(this).addClass('tel-link');
            $(this).attr('href', phone_no);
          }
        });

        $('.footer a:contains("Gift Card Support")').each(function () {
          var string_email = 'rosewood@techsembly.com';
          var email_link = "mailto:" + string_email;
          if (!$(this).hasClass('email-link')) {
            $(this).addClass('email-link');
            $(this).attr('href', email_link);
          }
        });
        waitForElm('.notificationbar .cross-cont').then((elm) => {
          $(".notificationbar .cross-cont").click(function () {
            if (!$(".notificationbar").hasClass("d-none")) {
              $(".notificationbar").addClass("d-none");
              $("nav#sidebar").removeClass("announce-open");
            }
          });
        });
        waitForElm('.sidebar-cross-cont img').then((elm) => {
          $(".sidebar-cross-cont img").click(function () {
            if ($(".mob-overlay").hasClass('active')) {
              $("body").removeClass('scroll-hidden');
              $("html").removeClass('scroll-hidden-html');
              $("nav#sidebar").removeClass('active');
              $(".mob-overlay").removeClass("active");
            }
          });
        });
        $('.footer .powered-by').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            var new_txt = '©';
            var new_txt2 = '2023 Rosewood Hotel Group';
            var new_txt3 = 'Gongan Beian: 31010102004896';
            $(this).find("div:contains('©')").html(function (_, html) {
              return html.replace(/(©)/g, '<span class="updated-string">' + new_txt + '</span>');
            });
            /*$(this).find("div:contains('|')").html(function(_, html) {
               return  html.replace(/(|)/g, '<span class="pow-sep mx-lg-1">|</span>');
            });*/
            $(this).find("div:contains('2023 Rosewood Hotel Group')").html(function (_, html) {
              //return  html.replace(/(2023 Rosewood Hotel Group)/g, '<span class="updated-copyright">'+new_txt2+'<br class="d-lg-none"><span class="pow-sep mx-1 d-none d-lg-inline-block">|</span></span>');
            });
          }
        });

        $('footer .powered-by a').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('target', '_blank');
          }
        });

        $(".mat-form-field-label").each(function () {
          if ($(this).attr('aria-owns')) {
            $(this).removeAttr('aria-owns');
          }
        });

        /*$('.checkout .checkout-container.complete').each(function(){
          if($(this).length && ($(document).scrollTop() != 0)) {
            $(document).scrollTop( $("#content").offset().top );
            console.log('scrolled to top');
          }
        })*/


      });
      setInterval(function () {
        $('.product-heading h1:contains(GIFT CARD):not(.giftcardpresent)').addClass('giftcardpresent').each(function () {
          var term = $('.product-info .product-heading h1').text();
          $('.product-info .product-heading h1').text("CUSTOMIZE YOUR E-" + term);
          $('.radio-btn[value=physical]').click(function () {
            $('.product-info .product-heading h1').text("CUSTOMIZE YOUR " + term);
          });
          $('.radio-btn[value=digital]').click(function () {
            $('.product-info .product-heading h1').text("CUSTOMIZE YOUR E-" + term);
          });
          //$('<h1 _ngcontent-serverapp-c7="" class="title recipient">Recipient Details</h1><p class="recipientstatmentdigital" style="display:block">Please share the name and email of the gift card recipient</p>').insertAfter($('.radio-btn[value=digital]').parent().parent().parent());
          //$('.radio-btn[value=physical]').click(function() {
          $//('.recipientstatmentdigital').text("Please share the name of the gift card recipient");
          //});
          //$('.radio-btn[value=digital]').click(function() {
          //$('.recipientstatmentdigital').text("Please share the name and email of the gift card recipient");
          //});
          $('select[id="gift-card-value-Select gift card value"] option[value=34391]').text("Customize your card value");
        });
      }, 250);
    });
  });
})();
