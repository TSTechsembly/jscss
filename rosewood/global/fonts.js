(function() {

    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
          font-family: 'GothamUltra';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Ultra.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Ultra.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Ultra.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Ultra.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Ultra.woff2') format('woff2');\
      }\
      @font-face {\
          font-family: 'GothamBlack';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Black.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Black.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Black.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Black.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Black.woff2') format('woff2');\
      }\
      @font-face {\
          font-family: 'GothamBold';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Bold.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Bold.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Bold.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Bold.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Bold.woff2') format('woff2');\
      }\
      @font-face {\
          font-family: 'GothamLight';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Light.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Light.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Light.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Light.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Light.woff2') format('woff2');\
      }\
      @font-face {\
          font-family: 'GothamMedium';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Medium.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Medium.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Medium.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Medium.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Medium.woff2') format('woff2');\
      }\
      @font-face {\
          font-family: 'GothamBook';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Book.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Book.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Book.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Book.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/gotham/Gotham-Book.woff2') format('woff2');\
      }\
      @font-face {\
          font-family: 'Adobe-Caslon-Pro-regular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Regular.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Regular.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Regular.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Regular.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Regular.woff2') format('woff2');\
               font-weight: normal;\
               font-style: normal;\
      }\
      @font-face {\
          font-family: 'Adobe-Caslon-Pro-semibold';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Semibold.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Semibold.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Semibold.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Semibold.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Semibold.woff2') format('woff2');\
               font-weight: 600;\
               font-style: normal;\
      }\
      @font-face {\
          font-family: 'Adobe-Caslon-Pro-bold';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Bold.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Bold.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Bold.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Bold.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Bold.woff2') format('woff2');\
               font-weight: 700;\
               font-style: normal;\
      }\
      @font-face {\
          font-family: 'Adobe-Caslon-Pro-SemiboldItalic';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-SemiboldItalic.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-SemiboldItalic.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-SemiboldItalic.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-SemiboldItalic.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-SemiboldItalic.woff2') format('woff2');\
               font-weight: 600;\
               font-style: italic;\
      }\
      @font-face {\
          font-family: 'Adobe-Caslon-Pro-BoldItalic';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-BoldItalic.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-BoldItalic.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-BoldItalic.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-BoldItalic.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-BoldItalic.woff2') format('woff2');\
               font-weight: bold;\
               font-style: italic;\
      }\
      @font-face {\
          font-family: 'Adobe-Caslon-Pro-Italic';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Italic.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Italic.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Italic.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Italic.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/e7071cf8b67e11a1a06d6ff3c08895779634f093/rosewood/fonts/adobe-caslon-pro/ACaslonPro-Italic.woff2') format('woff2');\
               font-weight: normal;\
               font-style: italic;\
      }\
      @font-face {\
          font-family: 'brandon_grotesqueregular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/cca075c5ee5fda5ba3f3e5632aecda51069ec3aa/rosewood/fonts/brandon-grotesque/brandon_reg-webfont.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/cca075c5ee5fda5ba3f3e5632aecda51069ec3aa/rosewood/fonts/brandon-grotesque/brandon_reg-webfont.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/cca075c5ee5fda5ba3f3e5632aecda51069ec3aa/rosewood/fonts/brandon-grotesque/brandon_reg-webfont.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/cca075c5ee5fda5ba3f3e5632aecda51069ec3aa/rosewood/fonts/brandon-grotesque/brandon_reg-webfont.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/cca075c5ee5fda5ba3f3e5632aecda51069ec3aa/rosewood/fonts/brandon-grotesque/brandon_reg-webfont.woff2') format('woff2');\
          font-weight: normal;\
      }\
    "));
    document.head.appendChild(newStyle);

})();
