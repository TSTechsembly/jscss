function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }
        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });
        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
function addMenuLabel() {
  var menuLabel = '<span class="mob-menu-label text-uppercase">Menu</span>';
  var elemMenuButton = $('header.header .mobile-menu#sidebarCollapse')[0];
  elemMenuButton.insertAdjacentHTML('beforeend', menuLabel);
}
function addRecipientInfoTop(){
  var infoText;
  var productHolder = $('.product-detail-container .product-holder')[0];
  var recipientInfo = "<div class='form-group recipient-info-top'><label class='control-label'>Recipient Details</label><div class='recipient-note'></div></div>";
  var elemRecNameInput = $('.product-detail-container .personalise-form .delivery-form .delivery-fields[formcontrolname="receipient_first_name"]')[0];
  elemRecNameInput.insertAdjacentHTML('beforebegin', recipientInfo);
}
function addLoginCartNote(){
  var loginCartNote = "<div class='login-note'>Please note this login is for Rosewood gifting platform only.<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}
function addPersonalDataNote(){
  var loginPersonalDataNote = "<div class='personal-data-note'>Your personal data will be processed in accordance with Rosewood Hotel Groupâ€™s <a href='https://www.rosewoodhotels.com/en/privacy-policy' target='_blank'>Privacy Policy</a>.<div>";
  var elemHeading = $('.custom-container .login-signup-tabs-cont .right-panel .signup-form')[0];
  elemHeading.insertAdjacentHTML('afterend', loginPersonalDataNote);
}
function addGuestCheckoutNote(){
  var checkoutNote = "<div class='checkout-note text-center w-100'>If you do not have an account please use guest checkout. You will be given the option to save account information later in the checkout process<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}
function addPaymentNotes(){
  var payNotesHtml = '<div class="col-12 col-lg-8 px-0 payment-notes-outer"><div class="col-12 payment-notes-cont p-4"><h4 class="notes-heading">Note:</h4>'+
  '<div class="notes-cont">'+
  '<p>Your purchase order is subject to Rosewood&apos;s <a href="https://www.rosewoodhotels.com/en/gift-cards/terms-and-conditions" target="_blank">Terms and Conditions</a> and our service provider Techsembly&apos;s <a href="https://rwh-shop.techsembly.com/pages/terms-and-conditions" target="_blank">Terms and Conditions</a>. </p><br>'+
  '<p>By confirming your order you agree to Rosewood&apos;s <a href="https://www.rosewoodhotels.com/en/gift-cards/terms-and-conditions" target="_blank">Terms and Conditions</a> and Techsembly&apos;s <a href="https://rwh-shop.techsembly.com/pages/terms-and-conditions" target="_blank">Terms and Conditions</a> and the processing of your personal data as described in Rosewood&apos;s <a href="https://www.rosewoodhotels.com/en/privacy-policy" target="_blank">Privacy Policy</a>. This purchase will display on my statement as  TS* Rosewood Gift Card.</p><br>'+
  '<p>For assistance please contact Customer Service, 7 days a week, 24 hours a day. </p><br>'+
  '<p>UK Toll-free: +44 800 0488077<br>'+
  'US Toll-free: +1 888 2258452<br>'+
  'US Local: +1 203 5838588<br>'+
  'Hong Kong Local: +852 2319 4897<br>'+
  '</p>'
  '</div>'+
  '</div>'+
  '</div>';
  //var elem = $(".custom-container.checkout .payment-form-container .left-panel")[0];
  var elem = $(".checkout  .payment-form-container .payment-form")[0]
  elem.insertAdjacentHTML('beforeend', payNotesHtml);
}
function addSignUpSection(){
  var newSignupSection = '<section class="sign-up-section new">'+
  '<div  class="sign-up-container"><h2  class="mb-2">STAY CONNECTED</h2>'+
  '<div  class="signup-text text-center">'+
  '<p></p>'+
  '<a href="https://rwhg-mkt-prod1-m.adobe-campaign.com/lp/RosewoodWebSubscriptions" class="btn btn-primary btn-signup" type="button" target="_blank">SIGN UP</a>'+
  '</div>'+
  '</div>'+
  '</section>';
  var elemFooter = $('footer.footer')[0];
  elemFooter.insertAdjacentHTML('beforebegin', newSignupSection);
}
function addcopyrightText(){
  var copyrightHtml = '<div class="container-fluid"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-7 col-md-6 tc-rights-reserved pl-0">Â©2022 Rosewood Hotel Group</div><div class="col-5 col-md-6 ts-copyright text-right px-0"><span>Powered by</span><span class="ml-1"><a href="https://techsembly.com/" target="_blank">Techsembly</a></span></div></div></div>'
  var elem = $(".checkout-footer")[0]
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}
(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    var script_fonts = document.createElement("SCRIPT");
    script_fonts.src = 'https://bitbucket.org/TSTechsembly/jscss/raw/7e4aac0e05611b48a233545953efd8cd679a4d6b/rosewood/global/fonts.js';
    script_fonts.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script_fonts);


    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };


    // Now lets do something
    checkReady(function($) {
        $(function() {
          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">');
          waitForElm('header.header .mobile-menu').then((elm) => {
            addMenuLabel();
          }).catch((error) => {});

          waitForElm('.product-detail-container .personalise-form .delivery-form').then((elm) => {
            addRecipientInfoTop();
          }).catch((error) => {});

          waitForElm('.cart-right-cont').then((elm) => {
            addLoginCartNote();
            addGuestCheckoutNote();
          }).catch((error) => {});

          waitForElm('.custom-container.checkout .payment-form-container').then((elm) => {
            addPaymentNotes()
          }).catch((error) => {});

          waitForElm('app-footer-checkout').then((elm) => {
            addcopyrightText()
          }).catch((error) => {});

          waitForElm('.custom-container .login-signup-tabs-cont .right-panel').then((elm) => {
            addPersonalDataNote()
          }).catch((error) => {});

          waitForElm('footer.footer').then((elm) => {
            addSignUpSection();
          }).catch((error) => {});


          $(".sign-up-section .sign-up-container .sign-up-form .submit").html("<img src='https://static.techsembly.com/dT7gZrQPLcQMLTsbKgZUxMLL' alt='submit'>");
          $(".header .container.p-0.d-none.d-lg-block.d-xl-block").attr("id","myHeader");
          $(".sign-up-section .sign-up-container .sign-up-form input[type=email]").attr("placeholder","Enter Your Email");
          //$('meta[name=viewport]').attr('content','width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0');

          //$('.payment-form-container .checkbox-cont input[formcontrolname="sameBillShopAdd"]').removeClass('ng-valid');

          setTimeout(function() {

          }, 2000);

          setInterval(function() {
            $(".head-links #profile-icon img:not(.altered)").addClass('altered').attr("src","https://static.techsembly.com/jugLv3VVUT9nbZHejvZbccrk");
            $(".head-links #wishlist-heart img:not(.altered)").addClass('altered').attr("src","https://static.techsembly.com/T5SrjMfNjAAELrtpGzEjCn66");
            $(".head-links #cart-bag img:not(.altered)").addClass('altered').attr("src","https://static.techsembly.com/mnUCyo5cwev7XudggW6G6tnR");
            $(".header .menu-links-cont .search-item-form .btn-search img:not(.altered)").addClass('altered').attr("src","https://static.techsembly.com/CFpA9dTFYWyRX33r5gAGYnov");
            $('header .mob-top-links li a img[alt="search image"]:not(.altered)').addClass('altered').attr('src', 'https://static.techsembly.com/FPdNZj6KvAVStVB3PUekq4os');
            $('header .mob-top-links li a img[alt="cart"]:not(.altered)').addClass('altered').attr('src', 'https://static.techsembly.com/tX7ATTP9ShgxeKbzRhXJR9F5');
            $("header.header .head-links li .add-to-basket .dropdown-menu h4:not(.altered)").addClass('altered').html("SHOPPING BAG");
            $('header .mob-top-links .add-to-basket .dropdown-menu h4:not(.altered)').addClass('altered').html('SHOPPING BAG');
            $('header.header .mobile-menu#sidebarCollapse svg').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).attr('height','23.5');
                  $(this).attr('width','30');
                  $(this).find('rect').attr('height','8');
                  $(this).find('rect:nth-child(1)').attr('y','0');
                  $(this).find('rect:nth-child(2)').attr('y','35');
                  $(this).find('rect:nth-child(3)').attr('y','70');
              }
            });
            if(!$('header.header .toggle-menu-cont').hasClass('col-3')){
                $('header.header .toggle-menu-cont').addClass('col-3').removeClass('col-1 col-sm-2 col-md-2');
            }
            if(!$('header.header .menu-links-cont').hasClass('col-3')){
                $('header.header .menu-links-cont').addClass('col-3').removeClass('col-md-6 col-sm-5 col-7');
            }
            if($('header.header .logo').parent('div').hasClass('col-sm-5')){
                $('header.header .logo').parent('div').removeClass('col-4 col-sm-5');
                $('header.header .logo').parent('div').addClass('col-6 logo-cont');
            }
            $('header.header .menu-holder>li>div').each(function(){
              if(!$(this).children().length){
                $(this).addClass('empty');
              }
            });

            $('header.header .head-links li a#profile-icon').closest('.nav-item:not(.profile-cont)').addClass('profile-cont');
            $('header.header .head-links li a#wishlist-heart').closest('.nav-item:not(.wishlist-cont)').addClass('wishlist-cont');
            $('header.header .head-links li a#cart-bag').closest('.nav-item:not(.cart-cont)').addClass('cart-cont');
            //$('header.header .head-links li a img[alt="user-profile"]').closest('.nav-item:not(.profile-cont)').addClass('profile-cont');
            //$('header.header .head-links li a img[alt="wishlist"]').closest('.nav-item:not(.wishlist-cont)').addClass('wishlist-cont');
            $('nav#sidebar ul li a img[alt="wishlist"]').closest('li:not(.wishlist-cont)').addClass('wishlist-cont');
            //$('header.header .head-links li a img[alt="cart"]').closest('.nav-item:not(.cart-cont)').addClass('cart-cont');

            hrefurl=$(location).attr("href");
            last_part=hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
            currentUrlSecondPart = window.location.pathname.split('/')[1];

            if (currentUrlSecondPart == 'catalogsearch'){
               if (!$('body').attr('data-pagetype','search_results')){
                   $('body').attr('data-pagetype','search_results');
               }
            }
            if (currentUrlSecondPart == 'vendors'){
               if (!$('body').attr('data-pagetype','vendors')){
                   $('body').attr('data-pagetype','vendors');
               }
            }
            if (last_part == 'wishlist'){
               if (!$('body').attr('data-pagetype','wishlist')){
                   $('body').attr('data-pagetype','wishlist');
               }
            }

            $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
            $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');

            $('.product-detail-container .personalise-form .form-group label').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(".personalise-form .form-group label:contains('*')").html(function(_, html) {
                   //return  html.replace(/\*/g, '<span class="updated-string">*</span>');
                });
              }
            });

            $('.personalise-form .form-group input[formcontrolname="user_gift_amount"] + .alert-secondary').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Please select the gift card value from USD$50 to USD$2,000 ');
              }
            });

            $('.product-detail-container .personalise-form .delivery-form .title:contains("Delivery Options")').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Delivery Option');
              }
            });

            $('.product-detail-container .personalise-form .delivery-form .radio-container input[formcontrolname="delivery_mode"]').each(function(){
              if(!$(this).closest('.radio-holder').hasClass('modified')){
                  $(this).closest('.radio-holder').addClass('modified');
                  $(this).closest('.radio-holder').find('span:contains("Digital")').html('E-Gift Card');
                  $(this).closest('.radio-holder').find('span:contains("Physical")').html('Gift Card');
                  if (navigator.platform.indexOf('Win') > -1) {
                    $(this).closest('.radio-holder').addClass('win-radio-holder');
                  }
              }
            });

            $('.product-detail-container .personalise-form .delivery-form .delivery-fields[formcontrolname="receipient_first_name"]').each(function(){
              var infoText;
              var recipientInfo = "<div class='form-group recipient-info-top'><label class='control-label'>Recipient Details</label><div class='recipient-note'></div></div>";
              if (!$('.product-detail-container .personalise-form .delivery-form').find('.recipient-info-top').length) {
                //$(recipientInfo).insertBefore($(this));
              }
              if($(this).closest('.product-holder').hasClass('digital')){
                infoText = "Please share the name and email of the gift card recipient";

                if(!$(this).hasClass('digital-rec')){
                  $(this).addClass('digital-rec');
                  $(this).removeClass('physical-rec');
                  $('.recipient-note').html(infoText);
                }
              }
              else if($(this).closest('.product-holder').hasClass('physical')){
                infoText = "Please share the name of the gift card recipient";
                if(!$(this).hasClass('physical-rec')){
                  $(this).addClass('physical-rec');
                  $(this).removeClass('digital-rec');
                  $('.recipient-note').html(infoText);
                }
              }
            });

            $('.product-detail-container .personalise-form .delivery-form .delivery-fields[formcontrolname="message"]').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).attr('placeholder','Write a Gift Message (optional)');
              }
            });

            $('.product-detail-container .personalise-form .delivery-form .notify:contains("Up to 200 characters")').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Maximum 200 characters');
              }
            });

            $('.product-detail-container .product-holder .personalise-form .delivery-form .info-text').each(function(){
              var msg_txt;
              if($(this).closest('.product-holder').hasClass('digital')){
                msg_txt = "<b>Maximum US$2,000 per E-Gift card and US$10,000 per transaction.</b> If you would like to add a unique message for each card, please add each gift card separately to the shopping bag.";
                if(!$(this).hasClass('digital-info-text')){
                  $(this).addClass('digital-info-text');
                  $(this).removeClass('physical-info-text');
                  $(this).html(msg_txt);
                }
              }
              else if($(this).closest('.product-holder').hasClass('physical')){
                msg_txt = "<b>Maximum US$2,000 per card and US$10,000 per transaction.</b> Your message will apply to all Gift Cards to one single shipping address. If you would like to add a unique message for each card, please add each gift card separately to the shopping bag.";
                if(!$(this).hasClass('physical-info-text')){
                  $(this).addClass('physical-info-text');
                  $(this).removeClass('digital-info-text');
                  $(this).html(msg_txt);
                }
              }
            });

            $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('ADD TO BAG');
              }
            });

            $('.product-detail-container .personalise-form .form-group .buy-btn').each(function(){
              /*if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Proceed to Checkout');
              }*/
            });

            $('.product-detail-container .product-holder .share-link li:first-child span').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Share via');
              }
            });

            $('#cart-notification .modal-container.login-modal .modal-header .panel-heading').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Products May Be Removed From Your BAG');
              }
            });

            $('#cart-notification .modal-container.login-modal .modal-body p.info-text').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Proceed To Checkout enables you to have quick check-out for this Gift Card product only. If you wish to check-out with all items in your cart, please select View Bag.');
              }
            });

            $('.modal-popup#cart-notification .footer-btns .right-btn .btn-primary').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Buy Now');
              }
            });

            $('#cart-notification .footer-btns .view-cart').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).wrap('<div class="footer-btns-left"></div>');
                  $(this).html('View Bag');
              }
            });

            $('.login-modal .modal-body .container .store-logo').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).attr('src','https://static.techsembly.com/fBczckgz1TVHUUt9n7q5ipAZ');
              }
            });
            $('.login-modal .checkout-desc-cont img[alt="lock"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/jBL6KnUEhRKVEmP3ZHxVKsgG');

            $('.modal-popup form.guestCheckout-form input#guest-email:not(.modified)').addClass('modified').attr('placeholder','Email');

            $('.product-detail-container .product-holder .share-link li:nth-child(2) img:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/L7tKASTK87qxYn9nXPGHzoNi');
            $('.product-detail-container .product-holder .share-link li:nth-child(3) img:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/GibF5PZYHLiYBNPw2LxytzRs');
            $('.product-detail-container .product-holder .share-link li:nth-child(4) img:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/M1SGqx8gAag9yvYsoyi1yzmR');

            $('.modal-popup#max-transaction-popup .modal-content').each(function(){
              if(!$(this).closest('.col-12').hasClass('modal-content-outer')){
                $(this).closest('.col-12').addClass('modal-content-outer');
              }
            });

            $('.modal-popup#max-transaction-popup .btns-cont .return-btn').each(function(){
                if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Return to BAG');
                }
            });

            $('.login-modal .modal-close img').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).attr('src','https://static.techsembly.com/EcNuhtwmAyReJj2tJcPuynEW');
              }
            });

            $('.custom-container.cart-container').each(function(){
              if (!$(this).closest('body').attr('data-pagetype','cart')){
                $(this).closest('body').attr('data-pagetype','cart');
              }
            });

            $('.custom-container.cart-container .vendor-items-cont .card-header .vendor-heading .delivered-by').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('Fulfilled By');
                }
            });


            $('.cart-item.new .item-details .item-qty-cont').each(function(){
              if(!$(this).closest('.col-lg-2').hasClass('qty-col')){
                  $(this).closest('.col-lg-2').addClass('qty-col');
              }
            });

            $('.cart-item.new .item-details .unit-total-price').each(function(){
              if(!$(this).closest('.col-lg-3').hasClass('sub-total-col')){
                  $(this).closest('.col-lg-3').addClass('sub-total-col');
              }
            });

            $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/sgo534ygqVc6EzhG8fxSB1Aa');

            $('.custom-container.checkout').each(function(){
              if (!$(this).closest('body').attr('data-pagetype','checkout')){
                $(this).closest('body').attr('data-pagetype','checkout');
              }
            });

            $('.custom-container.cart-container .btn.continue-btn-new').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('add more cards');
                }
            });

            $('.shipping-form-container .exist-address-wrapper .card a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/T82dXsi6BExFik2ZLvP2zutF');

            $('.container.custom-container.checkout .btn#continue-to-delivery-and-billing').each(function(){
                if(!$(this).closest('.form-group').hasClass('submit-btn-cont')){
                    $(this).closest('.form-group').addClass('submit-btn-cont');
                    $(this).html('CONTINUE TO DELIVERY');
                }
            });

            $('.custom-container.checkout .address-form.new .form-control[formcontrolname="phone"]').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).attr('placeholder','(Country Code) Phone Number');
                }
            });

            $('.address-form.new select[formcontrolname="country"] option:disabled').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('Country/Region');
                }
            });

            $('.container.custom-container.checkout.physical .billing-heading:contains("Shipping Address")').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('Recipient Address');
                }
            });

            $('.delivery-methods-cont .vendor-cards-holder .card .order-item a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/T82dXsi6BExFik2ZLvP2zutF');

            $('.delivery-methods-cont .vendor-cards-holder .card .delivery-dropdowns-cont .label-email .recipent-info-edit img[alt="edit-recipient"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/T82dXsi6BExFik2ZLvP2zutF');

            $('.delivery-methods-cont .card .card-body .card-row a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/T82dXsi6BExFik2ZLvP2zutF');

            $('.custom-container.checkout .delivery-methods-cont .vendor-cards-holder h3.info-text:contains(Recipientâ€™s Info)').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Recipientâ€™s Info');
              }
            });

            $('.delivery-methods-cont .vendor-cards-holder .card .order-item a.edit-item-new').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newTxt = 'Edit Gift Card';
                $("a.edit-item-new:contains('Edit Product')").html(function(_, html) {
                   return  html.replace(/(Edit Product)/g, '<span class="updated-edit-string">'+newTxt+'</span>');
                });
              }
            });

            $(".custom-container.checkout .delivery-methods-cont .vendor-cards-holder .card .order-shipping-container label:contains('Gift Message')").each(function(){
              if(!$(this).closest('.col-12').hasClass('form-group')){
                $(this).closest('.col-12').addClass('form-group mb-0');
              }
            });

            $('app-checkout-delivery-v2 h1.breadcrumb-heading').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('Delivery Method');
                }
            });

            $('.custom-container.checkout .delivery-methods-cont .vendor-cards-holder .card .card-header .vendor-title span:contains("Delivered by")').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('Fulfilled By');
                }
            });

            $('.custom-container.checkout .delivery-methods-cont .vendor-cards-holder .card .shipping-totals-cont .total-caption:contains("Shipping")').each(function(){
              if(!$(this).closest('.total-row').hasClass('shipping-total-cont')){
                $(this).closest('.total-row').addClass('shipping-total-cont');
              }
            });

            $('.payment-form-container .card-details-cont').each(function(){
              if(!$(this).children('h2.section-heading').length){
                $(this).prepend('<h2 class="section-heading">Card Details</h2>');
                $(this).append('<div class="card-note">We accept Visa, Mastercard and American Express.</div>');
              }
            });

            $('.custom-container.checkout .payment-form-container .section-heading:contains("Card Details")').each(function(){
              if(!$(this).closest('.section-cont').hasClass('cc-details-cont')){
                $(this).closest('.section-cont').addClass('cc-details-cont');
              }
            });

            $('.custom-container.checkout .payment-form-container .section-heading:contains("Details")').each(function(){
              if(!$(this).closest('.section-cont').hasClass('billing-detail-cont')){
                $(this).closest('.section-cont').addClass('billing-detail-cont');
              }
            });

            //$('.payment-form-container .input-cont.password img.pass-eye:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/f2VPyxYkpvRZWKi5vUM3hp8N');

            //$('.checkout .password-note:not(.modified)').addClass('modified').html('Must be at least 6 characters including one special character');

            $('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc:contains("Shipping")').each(function(){
              if(!$(this).closest('.sub-total-item').hasClass('shipping-total-cont')){
                $(this).closest('.sub-total-item').addClass('shipping-total-cont');
              }
            });

            $('.custom-container.checkout .order-totals-container .promo-note').each(function(){
              if(!$(this).closest('.form-group').hasClass('promo-note-warpper')){
                $(this).closest('.form-group').addClass('promo-note-warpper');
              }
            });

            $(".custom-container.checkout .payment-form-container .btn.btn-submit:not(.modified)").addClass('modified').html('COMPLETE PURCHASE');

            $('.checkout .checkout-container.complete .card.order-confirm-wrapper').each(function(){
              if(!$(this).parent('.col-12').hasClass('order-confirm-outer')){
                $(this).parent('.col-12').addClass('order-confirm-outer');
              }
            });

            $('.checkout-container.complete .card-header img[alt="Rosewood Hotels logo"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/fBczckgz1TVHUUt9n7q5ipAZ');

            $('.checkout-container.complete .section-cont.customer-note').each(function(){
              if(!$(this).hasClass('total-delivery')){
                $(this).addClass('total-delivery');
                var firstPart = $(this).find('br')[0].previousSibling.nodeValue;

                var newTxt = 'Your order is being confirmed. You will receive an email with further information.';
                $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function(_, html) {
                 return  html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">'+newTxt+'</div>')
                });

                var secondPart = $(this).find('.del-content').html();

                $(this).html('<div class="salutation-cont d-none">'+firstPart+'</div><div class="del-content pt-0">'+secondPart+'</div>');
              }
            });

            $('.checkout-container.complete.physical .shipping-data .shipping-row .col-6:contains("Address")').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('Delivery Address:');
                }
            });

            $('.checkout-container.complete.digital .shipping-data .shipping-row .col-6:contains("Address")').each(function(){
                if(!$(this).closest('.shipping-row').hasClass('digital-product-address')){
                    $(this).closest('.shipping-row').addClass('digital-product-address');
                }
            });

            $('.checkout-container.complete:not(.digital) .msg-cont .msg-inner').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('You will receive a shipping confirmation when your Gift Card(s) are on the way.');
              }
            });

            $('.checkout-container.complete.digital .msg-cont .msg-inner').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Your E-Gift Cards will be delivered to the recipients inbox shortly.');
              }
            });

            $('.checkout .checkout-container.complete .card-footer-right').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('<span>RHR Gift Card Services, L.L.C., a Virginia limited liability company</span>');
              }
            });

            $('.checkout .checkout-container.complete .card-footer-left span:contains("Questions")').each(function(){
                if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Need help? Contact our');
                }
            });

            $('.checkout .checkout-container.complete .card-footer a:contains("Customer Support")').each(function(){
                if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).attr('href','mailto:rosewood@techsembly.com');
                }
            });

            $('.checkout-container.complete .card-footer').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).append('<div class="col-12 text-center home-btn-cont"><a class="btn btn-primary home-btn text-center text-uppercase" href="/">back to home</a></div>');
              }
            });

            $('body.auth-user .user-container .ts-balance-form').closest('.user-container:not(.d-none)').addClass('d-none');

            $('.user-container .user-heading:contains("Check TS Gift Card Balance")').each(function(){
              if(!$(this).hasClass('gift-card-heading')){
                $(this).addClass('gift-card-heading');
                $(this).html('Check Gift Card Balance');
              }
            });

            $('.user-container .user-heading:contains("My TS Card Balance")').each(function(){
              if(!$(this).hasClass('gift-card-heading')){
                $(this).addClass('gift-card-heading');
                $(this).html('My Gift Card Balance');
              }
            });
            $('.ts-balance-form label[for="gift-card-no"]:not(.modified)').addClass('modified').html('Gift card number');
            $('.ts-balance-form input[name="gift-card-no"]:not(.modified)').addClass('modified').attr('placeholder','Enter gift card number');

            $('.custom-container.login-signup').each(function(){
              if (!$(this).closest('body').attr('data-pagetype','login-signup')){
                $(this).closest('body').attr('data-pagetype','login-signup');
              }
            });

            $('form.signup-form label.newsletter-check span:not(.modified)').addClass('modified').html("I agree to receive news and special offers emails in relation to hotels, products and services from Rosewood Hotel Group, its affiliates and other businesses or properties it owns or manages.");

            $('.footer .footer-widget .widget-content ul li a').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('target', '_blank');
              }
            });

            $('.footer .mobile-footer .footer-widget ul li a').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('target', '_blank');
              }
            });

            $(".footer .footer-widget:last-child .widget-title:not(.modified)").addClass("modified").addClass('modified').html("FOLLOW US");
            $('.footer .footer-widget .social-links li a img[alt="fb"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/kVHWRiaCq6MCshDvQ775Lnjp');
            $('.footer .footer-widget .social-links li a img[alt="instagram"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/CCokyF4cPuvJMwsrktFtZYyS');
            $('.footer .footer-widget .social-links li a img[alt="twitter"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/94GN39JddxjD41TVJhpiFhWK');
            $('.footer .footer-widget .social-links li a img[alt="pinterest"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/mZ2LSsvAmjoPdhk7ahyzAhFQ');
            $('.footer .footer-widget .social-links:not(.modified)').append('<li><a href="https://www.youtube.com/channel/UClZ2O-efl09dZ2BypTw2kng" rel="nofollow"><img class="modified" alt="youtube" width="32" src="https://static.techsembly.com/1E4EQD8YiKiNMJKbiyicRTf3"></a></li>');
            $('.footer .footer-widget .social-links:not(.modified)').addClass('modified').append('<li><a href="https://www.rosewoodhotels.com/en/wechat" rel="nofollow"><img class="modified" alt="wechat" width="32" src="https://static.techsembly.com/1ADctJsvZbB5RM6oLN12h88F"></a></li>');
            $('header.header .menu-holder>li>div:not(.modified)').addClass('modified submenu');

            $('.footer a:contains("+1")').each(function(){
              var string_phone= $(this).html();
              var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
              var phone_no = "tel:" + result_phone;
              if(!$(this).hasClass('tel-link')){
                $(this).addClass('tel-link');
                $(this).attr('href', phone_no);
              }
            });

            $('.footer a:contains("Gift Card Support")').each(function(){
              var string_email= 'rosewood@techsembly.com';
              var email_link = "mailto:" + string_email;
              if(!$(this).hasClass('email-link')){
                $(this).addClass('email-link');
                $(this).attr('href', email_link);
              }
            });

            $('.footer .powered-by').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var new_txt = 'Â©';
                var new_txt2 = '2022 Rosewood Hotel Group';
                var new_txt3 = 'Gongan Beian: 31010102004896';
                $(this).find("div:contains('Â©')").html(function(_, html) {
                   return  html.replace(/(Â©)/g, '<span class="updated-string">'+new_txt+'</span>');
                });
                /*$(this).find("div:contains('|')").html(function(_, html) {
                   return  html.replace(/(|)/g, '<span class="pow-sep mx-lg-1">|</span>');
                });*/
                $(this).find("div:contains('2022 Rosewood Hotel Group')").html(function(_, html) {
                   //return  html.replace(/(2022 Rosewood Hotel Group)/g, '<span class="updated-copyright">'+new_txt2+'<br class="d-lg-none"><span class="pow-sep mx-1 d-none d-lg-inline-block">|</span></span>');
                });
              }
            });

            $('footer .powered-by a').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('target', '_blank');
              }
            });

            $(".mat-form-field-label").each(function(){
              if($(this).attr('aria-owns')) {
                $(this).removeAttr('aria-owns');
              }
            });

            /*$('.checkout .checkout-container.complete').each(function(){
              if($(this).length && ($(document).scrollTop() != 0)) {
                $(document).scrollTop( $("#content").offset().top );
                console.log('scrolled to top');
              }
            })*/


          });
        });
    });
})();
