function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }
    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });
    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

function addMenuLabel() {
  var menuLabel = '<span class="mob-menu-label text-uppercase">Menu</span>';
  var elemMenuButton = $('header.header .mobile-menu#sidebarCollapse')[0];
  elemMenuButton.insertAdjacentHTML('beforeend', menuLabel);
}

function customizeGiftCard(sku, optionValue) {
  $(`.product-detail-container .product-holder .product-detail .product-info .product-heading .product-sku:contains("${sku}"):not(.sku-captured)`)
    .addClass('sku-captured')
    .each(function () {
      $(this).closest('.product-inner').addClass('customize-card-holder');
      $(`.customize-card-holder select[id="gift-card-value-Select gift card value"] option[value=${optionValue}]`).text("Customize your card value");
    });
}

function addShippingAddrHeading() {
  var addrHeading = "<div class='shippingHeading'><h1>Shipping Address</h1><div>";
  var shippingFormCont = $('.custom-container.checkout .shipping-form-cont')[0];
  shippingFormCont.insertAdjacentHTML('afterbegin', addrHeading);
}

function addcopyrightText() {
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="container-fluid"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-6 tc-rights-reserved pl-0 pr-0">©' + copyrightYear +
    ' Rosewood Hotel Group</div><div class="col-6 ts-copyright text-right pr-0"><span>Powered by</span><span class="ml-1"><a href="https://techsembly.com/" target="_blank">Techsembly</a></span></div></div></div>'
  var elem = $(".checkout-footer")[0]
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}

function addCheckoutNote() {
  var copyrightHtml = `<div class='row flex-column checkoutFooterNote'><p><span class='title'>Note:</span></p><p>Your purchase order is subject to Rosewood Munich’s <a href='https://shop.rosewoodhotels.com/munich/pages/terms-and-conditions-rosewood-munich' target='_blank'>Terms and Conditions</a> and our service provider Techsembly's
  <a href='https://shop.rosewoodhotels.com/munich/pages/terms-and-conditions' target='_blank'>Terms and Conditions</a>.</p><p>By confirming your order you agree to Rosewood Munich’s
  <a href='https://shop.rosewoodhotels.com/munich/pages/terms-and-conditions-rosewood-munich' target='_blank'>Terms and Conditions</a> and Techsembly's
  <a href='https://shop.rosewoodhotels.com/munich/pages/terms-and-conditions' target='_blank'>Terms and Conditions</a> and the processing of your personal data as described in Rosewood Munich’s
  <a href='https://www.rosewoodhotels.com/en/privacy-policy' target='_blank'>Privacy Policy</a>. This purchase will display on my statement as <<TBC by Rosewood Munich Finance team >>.</p>
  <p class='assistance-text'>For assistance please contact Customer Service, 7 days a week, 24 hours a day.</p>
  <p>US Toll-free: <a href='tel:+18882258452'>+1 888 2258 452</a><br>US Local: <a href='tel:+12035838588'>+1 203 583 8588</a></p></div>`
  var elem = $(".checkout  .payment-form-container .payment-form")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}

function addBackToHomeBtn() {
  var copyrightHtml = '<div class="d-flex justify-content-center items-center backToHomeBtn"><a href="https://rosewood-munich.techsembly.com/">BACK TO HOME</a></div>'
  var elem = $(".order-confirm-wrapper .card-footer")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}

function addSignUpSection() {
  var newSignupSection = '<a href="https://rwhg-mkt-prod1-m.adobe-campaign.com/lp/RosewoodWebSubscriptions?languageCode=en&amp;propertyCode=Brand&amp;source=WEB&amp;emailAddress=" class="btn btn-signup" type="button" target="_blank">Enter Your Email</a>';
  var elemFooter = $('.sign-up-section .sign-up-container')[0];
  elemFooter.insertAdjacentHTML('beforeend', newSignupSection);
}
function appendSocialLinks() {
  var socialLinksList = $("ul.social-links.pt-md-1");
  socialLinksList.wrap("<div class='social-links-wrapper'></div>");
  $(".social-links-wrapper").prepend("<h1>CONNECT WITH US</h1>");
  $(".social-links-wrapper").appendTo("section.sign-up-section");
}
function addAnnounceMentBar() {
  var announcementBar = '<div class="notificationbar notification-outer">' +
    '<div class="notification-inner">Our Commitment to Our Guests: <a href="" target="_blank">COVID-19</a>' +
    '<span class="cross-cont"><img width="12" src="https://static.techsembly.com/yApgKFNtUAA6Ae3oQVCNBMck"/></span>' +
    '</div>';
  var elemContent = $('body #content')[0];
  elemContent.insertAdjacentHTML('afterbegin', announcementBar);
}

function addSidebarCross() {
  var crossCont = '<div class="sidebar-cross-cont">' +
    '<div class="inner-cont"><img width="19" src="https://static.techsembly.com/yApgKFNtUAA6Ae3oQVCNBMck"/></div>' +
    '</div>';
  var sidebarContent = $('body #sidebar')[0];
  sidebarContent.insertAdjacentHTML('afterbegin', crossCont);
}

function refundForm() {
  var refundFormTemp = '<form id="refund-form" name="refund-form" accept-charset="utf-8" action="https://formspree.io/f/mnqkrajw" method="post">' +
    '<h1 class="refund-heading">RETURNS & REFUNDS</h1>' +
    '<p class="refund-details">If your purchase is not exactly what you are looking for, you have 14 days to return. Please fill out the form below.</p>' +
    '<!-- firstname and last name -->' +
    '<div class="feilds-cont d-flex justify-content-between flex-wrap px-0">' +
    '<div class="col-12 col-md-6 pl-0 pr-0 pr-md-2">' +
    '<div class="form-group pr-0 pr-md-1">' +
    '<input name="subject" type="hidden" value="Refund request from Munich website" />' +
    '<input class="form-control" name="firstName" id="first-name" type="text" placeholder="First Name" required>' +
    '</div>' +
    '</div>' +
    '<div class="col-12 col-md-6 pl-0 pl-md-2 pr-0">' +
    '<div class="form-group pl-0 pl-md-1">' +
    '<input class="form-control" name="lastName" id="last-name" type="text" placeholder="Last Name" required>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '<!-- email and order id -->' +
    '<div class="feilds-cont d-flex justify-content-between flex-wrap px-0">' +
    '<div class="col-12 col-md-8 pl-0 pr-0 pr-md-2">' +
    '<div class="form-group pr-0 pr-md-1">' +
    '<input class="form-control" name="email" id="email" type="email" placeholder="Email Address" required>' +
    '</div>' +
    '</div>' +
    '<div class="col-12 col-md-4 pl-0 pl-md-2 pr-0">' +
    '<div class="form-group pl-0 pl-md-1">' +
    '<input class="form-control" name="orderId" id="order-id" type="text" placeholder="Order ID" required>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '<!-- message box -->' +
    '<div class="feilds-cont d-flex justify-content-between flex-wrap px-0 msg-box">' +
    '<label>Please share the details of the items you would like to return.</label>' +
    '<div class="form-group pr-1 refund-msg-cont">' +
    '<textarea class="form-control" name="message" id="message" placeholder="If multiple products, please list in the space provided."></textarea>' +
    '</div>' +
    '</div>' +
    '<!-- reason select box -->' +
    '<div class="feilds-cont d-flex justify-content-between flex-wrap px-0 reason-select">' +
    '<label>Reason for returning your item(s)</label>' +
    '<div class="form-group pr-1 w-100">' +
    '<select  class="form-control" id="reason" name="reason">' +
    '<option disabled="disabled" selected="selected" value="">Please Select</option>' +
    '<option value="damaged">Product was damaged</option>' +
    '<option value="quality">Product quality</option>' +
    '<option value="different">Different from the photograph</option>' +
    '<option value="incorrect-size">Incorrect size</option>' +
    '<option value="Other">Other</option>' +
    '</select>' +
    '</div>' +
    '<div class="form-group pr-1 refund-msg-cont">' +
    '<textarea class="form-control" id="other-message" name="reason_desc" placeholder="If you selected other, please share your reason for returning your item(s)." required></textarea>' +
    '</div>' +
    '</div>' +
    '<!-- return method select -->' +
    '<div class="feilds-cont d-flex justify-content-between flex-wrap px-0 return-method-select">' +
    '<label>Select Return Method</label>' +
    '<div class="form-group pr-1 w-100">' +
    '<select  class="form-control" id="return-method" name="return_method">' +
    '<option disabled="disabled" selected="selected" value="">Please Select</option>' +
    '<option value="drop-off">Drop-off at Hotel Hona Village, A Rosewood Hotel</option>' +
    '<option value="by-post">Return by post</option>' +
    '</select>' +
    '</div>' +
    '</div>' +
    '<!-- refund note -->' +
    '<div class="refund-note">' +
    "<p class='note-msg'>Please note all return costs are at the customer's expense. Details for returns by post:</p>" +
    '<p class="return-details">Rosewood Munich, A Rosewood Resort</p>' +
    "<p class='return-details'>72-300 Maheawalu Drive, </p>" +
    "<p class='return-details'>Kailua-Kona, </p>" +
    "<p class='return-details'>HI 96740</p>" +
    // '<p class="return-details">USA</p>' +
    '<p class="return-details"><a href="tel:+49898000190">+49 89 8000 190</a></p>' +
    // '<p class="return-details">rosewood@techsembly.com</p>'+
    '</div>' +
    '<div class="form-group request-btn-cont">' +
    '<button class="btn btn-primary px-4" type="submit">REQUEST REFUND</button>' +
    '</div>' +
    '<p id="my-form-status"></p>' +
    '</form>';
  var elemRefundFormTemp = $('.refund-form-cont')[0];
  elemRefundFormTemp.insertAdjacentHTML('afterbegin', refundFormTemp);
}

function addPersonalDataNote() {
  var loginPersonalDataNote = "<div class='personal-data-note'>Your personal data will be processed in accordance with Rosewood Hotel Group's <a href='https://www.rosewoodhotels.com/en/privacy-policy' target='_blank'>Privacy Policy</a>.<div>";
  var elemHeading = $('.custom-container .login-signup-tabs-cont .right-panel .signup-form')[0];
  elemHeading.insertAdjacentHTML('afterend', loginPersonalDataNote);
}
function addCategoryBanner() {
  var segment_str = window.location.pathname; // return segment1/segment2/segment3/segment4
  var segment_array = segment_str.split("/");
  var last_segment = segment_array.pop();
  console.log(last_segment)
  // let categoryTitle='<h1></h1>';

  // if (last_segment=='lifestyle') {
  //   categoryTitle='<h1>lifestyle <span>&</span> Gifts</h1>';
  // }
  // if (last_segment=='experiences') {
  //   categoryTitle='<h1>Experiences</h1>';
  // }
  // if (last_segment=='food-drink') {
  //   categoryTitle='<h1>FOOD <span>&</span> DRINK</h1>';
  // }
  // if (last_segment=='wellness') {
  //   categoryTitle='<h1>SPA <span>&</span> WELLNESS</h1>';
  // }
  // if (last_segment=='gift-cards') {
  //   categoryTitle='<h1>GIFT CARDS</h1>';
  // }
  var categoryBanner = "<section class='category-banner'><img class='w-100' src='https://static.techsembly.com/6fvSqzckTVPQxGCKdHU5xPCN'/><h1>Experiences</h1></section>";
  var elemHeading = $('body[data-pagetype="category"] .app-header')[0];
  elemHeading.insertAdjacentHTML('afterend', categoryBanner);
}

// function addDeliveryParagraph() {
//   var deliveryPara = "<p>Our delivery time starts from the moment an order is accepted and includes a 24-hour period where your items will be processed and dispatched.</p>";
//   var elem = $('.delivery-page-cont')[0];
//   elem.insertAdjacentHTML('beforeend', deliveryPara);
// }
(function () {
  var script = document.createElement("SCRIPT");
  script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
  script.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script);

  var script_fonts = document.createElement("SCRIPT");
  script_fonts.src = 'https://bitbucket.org/TSTechsembly/jscss/raw/4c6a295796a068d0de0ffda519efa4d24c33b223/rosewood/global/fonts.js';
  script_fonts.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script_fonts);

  // Poll for jQuery to come into existence
  var checkReady = function (callback) {
    if (window.jQuery) {
      callback(jQuery);
    } else {
      window.setTimeout(function () {
        checkReady(callback);
      }, 20);
    }
  };

  // Now lets do something
  checkReady(function ($) {
    $(function () {


      waitForElm('footer.footer').then((elm) => {
        addSignUpSection();
        appendSocialLinks()
      }).catch((error) => { });

      waitForElm('body #content').then((elm) => {
        addAnnounceMentBar();
        $("nav#sidebar").addClass("announce-open");
      }).catch((error) => { });

      waitForElm('body #sidebar').then((elm) => {
        addSidebarCross();
      }).catch((error) => { });

      waitForElm('header.header .mobile-menu .mobile-menu-icon').then((elm) => {
        addMenuLabel();
      }).catch((error) => { });

      waitForElm('.shipping-form-cont').then((elm) => {
        addShippingAddrHeading();
      }).catch((error) => { });
      waitForElm('app-footer-checkout').then((elm) => {
        addcopyrightText()
      }).catch((error) => { });
      waitForElm('.checkout  .payment-form-container').then((elm) => {
        addCheckoutNote()
      }).catch((error) => { });

      waitForElm('.order-confirm-wrapper .card-footer').then((elm) => {
        addBackToHomeBtn()
      }).catch((error) => { });
      waitForElm('.refund-form-cont').then((elm) => {
        refundForm();
      }).catch((error) => { });
      waitForElm('.custom-container .login-signup-tabs-cont .right-panel').then((elm) => {
        addPersonalDataNote()
      }).catch((error) => { });
      waitForElm('body[data-pagetype="category"] header.header').then((elm) => {
        addCategoryBanner()
      }).catch((error) => { });
      // waitForElm('.delivery-page-cont').then((elm) => {
      //   addDeliveryParagraph()
      // }).catch((error) => { });

      var segment_str = window.location.pathname; // return segment1/segment2/segment3/segment4
      var segment_array = segment_str.split("/");
      var last_segment = segment_array.pop();

      var arr = last_segment.split('-');
      var firstver = arr[0];
      var twover = arr[1];
      var match = firstver + '-' + twover;

      // code for test form submission with prevent default
      waitForElm('#refund-form').then((elm) => {
        var form = document.getElementById("refund-form");

        async function handleSubmit(event) {
          event.preventDefault();
          var status = document.getElementById("my-form-status");
          var data = new FormData(event.target);
          fetch(event.target.action, {
            method: form.method,
            body: data,
            headers: {
              'Accept': 'application/json'
            }
          }).then(response => {
            if (response.ok) {
              status.innerHTML = "Thanks for your submission!";
              form.reset();
              location = currentUrlFull + '/' + 'thank-you';
            } else {
              response.json().then(data => {
                if (Object.hasOwn(data, 'errors')) {
                  status.innerHTML = data["errors"].map(error => error["message"]).join(", ")
                } else {
                  status.innerHTML = "Oops! There was a problem submitting your form"
                }
              })
            }
          }).catch(error => {
            status.innerHTML = "Oops! There was a problem submitting your form"
          });
        }
        form.addEventListener("submit", handleSubmit)

      }).catch((error) => { });

      waitForElm('.btn-back-home').then((elm) => {
        $('.btn-back-home').attr('href', currentUrlFull);
      }).catch((error) => { });

      // junaid started
      waitForElm('.container.custom-container.cart-container .cart-login-cont button#checkout-as-guest').then((elm) => {
        $(".container.custom-container.cart-container .cart-login-cont button#checkout-as-guest").click(function () {
          $(this).addClass("modified");
          $(".custom-container.cart-container .cart-login-cont form input#guest-email").focus();
        });
      }).catch((error) => { });

      waitForElm('.nav-search-field').then((elm) => {
        $('.nav-search-field:not(.modified)').addClass('modified').attr("placeholder", "");
      });

      waitForElm('header.header .menu-holder>li>div').then((elm) => {
        $('header.header .menu-holder>li>div').each(function () {
          if (!$(this).children().length) {
            $(this).addClass('empty');
          }
        });
      });

      waitForElm('header.header .toggle-menu-cont').then((elm) => {
        $('header.header .toggle-menu-cont').addClass('col-3').removeClass('col-1 col-sm-2 col-md-2');
      });

      waitForElm('header.header .menu-links-cont').then((elm) => {
        $('header.header .menu-links-cont').addClass('col-3').removeClass('col-md-6 col-sm-5 col-7');
      });

      waitForElm('header.header .logo').then((elm) => {
        $('header.header .logo').parent('div').removeClass('col-4 col-sm-5 col-md-4');
        $('header.header .logo').parent('div').addClass('col-6 logo-cont');
        $('header.header .logo img:not(body[data-pagetype="home"] header.header .logo img, body[data-pagetype="category"] header.header .logo img)').attr("src", "https://static.techsembly.com/srbtSLEiKMeTCi2o2yaSBg6V")
      });

      waitForElm('.banner-carousel-section.modified.main-mobile-banner .slide-tile .slide-title').then((elm) => {
        const heading = $('.banner-carousel-section .slide-tile .slide-title').html();
        const text = heading;
        const words = text.split(' ');
        const targetWordIndex = words.findIndex(word => word === 'at');
        if (targetWordIndex !== -1) {
          words[targetWordIndex] = `<span class="highlight">${words[targetWordIndex]}</span>`;
          const newText = words.join(' ');
          $('.banner-carousel-section.modified.main-mobile-banner .slide-tile .slide-title').html(newText);
        }
      });

      waitForElm('.banner-carousel-container .owl-carousel .owl-nav').then((elm) => {
        $(".banner-carousel-container .owl-carousel .owl-nav .owl-prev img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/rWwMRD1sMEwAoiD2ADuduaGD");
        $(".banner-carousel-container .owl-carousel .owl-nav .owl-next img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/vKE5pEqkw93HVUaVixPsW8Bh");
      });

      waitForElm('.sign-up-section .sign-up-container .sign-up-form input').then((elm) => {
        $(".sign-up-section .sign-up-container .sign-up-form input[type=email]:not(.modified)").addClass('modified').attr("placeholder", "Enter Your Email");
      });

      waitForElm('.footer .footer-widget:last-child .widget-title').then((elm) => {
        $(".footer .footer-widget:last-child .widget-title:not(.modified)").addClass("modified").addClass('modified').html("FOLLOW US");
      });

      waitForElm(".footer .footer-widget .social-links").then((elm) => {
        $('.social-links-wrapper .social-links li a img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/8jcy8rKfQUdYeZiLhZp7FkoX');
        $('.social-links-wrapper .social-links li a img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/MhRf494WcdM7Yyt8jrmbworz');
        $('.social-links-wrapper .social-links li a img[alt="wechat"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/cRb7BRQY9vXpzNhiP7SJ6FEd');
        $('.social-links-wrapper .social-links li a img[alt="pinterest"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/orb8d1xBK4p8y1tZXdty82Mb');
        $('.social-links-wrapper .social-links li a img[alt="youtube"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/2Hhibj7wnbX7UsmSSBq9W4EW');
        $(".social-links-wrapper .social-links li:last-child:not(.modified) a").addClass("modified").attr("href", "#");

        $(".social-links-wrapper .social-links li a").each(function () {
          var anchor = $(this);
          var img = anchor.find('img');
          if (img.length > 0) {
            var altValue = img.attr('alt');
            if (altValue && altValue.toLowerCase().includes('pinterest')) { anchor.addClass('modified').attr('href', 'https://www.pinterest.com/rosewoodhotels/'); }
            if (altValue && altValue.toLowerCase().includes('wechat')) { anchor.addClass('modified').attr('href', 'https://www.rosewoodhotels.com/en/wechat'); }
          }
        });
      }).catch((error) => { }); 

      waitForElm('.custom-container.checkout.digital .shipping-form-cont h1').then((elm) => {
        $(".custom-container.checkout.digital .shipping-form-cont h1:not(.modified)").addClass('modified').html("Billing Address");
      });
      waitForElm('header.header .menu-holder>li>div').then((elm) => {
        $('header.header .menu-holder>li>div:not(.modified)').addClass('modified submenu');
      });
      waitForElm('.sign-up-section .sign-up-container .sign-up-form .submit').then((elm) => {
        $(".sign-up-section .sign-up-container .sign-up-form .submit:not(.modified)").addClass('modified').html("<img src='https://static.techsembly.com/r8SZXjCs4chUymLYyT6fp8Mu' alt='submit'>");
      });
      waitForElm('.mobile-menu#sidebarCollapse').then((elm) => {
        $('.mobile-menu#sidebarCollapse:not(.modified)').addClass('modified').append('<img src="https://static.techsembly.com/q3NnfinrbuTTGvZdCjTUsijp" class="mobile-menu-icon" alt="mobile-menu">')
      });
      waitForElm('.product-detail-container .product-detail').then((elm) => {
        $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
      });
      waitForElm('.product-detail-container .product-carousel').then((elm) => {
        $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');
      });
      waitForElm('#continue-to-delivery-and-billing').then((elm) => {
        $('#continue-to-delivery-and-billing').closest('div.form-group:not(.modified)').addClass('continue-btn-cont modified');
      });
      waitForElm('.custom-container.checkout .shipping-form-container .btn.btn-submit').then((elm) => {
        $('.custom-container.checkout .shipping-form-container .btn.btn-submit').closest('div.form-group:not(.modified)').addClass('continue-btn-cont modified');
      });
      waitForElm('header.header .menu-holder').then((elm) => {
        $('header.header .menu-holder').each(function () {
          $(this).closest('.container').addClass('wrapped');
          $(this).closest('.container').wrap('<div class="container-fluid main-menu-wrapper"></div>');
        });
      });
      waitForElm('.cookie-policy-container').then((elm) => {
        $('.cookie-policy-container').each(function () {
          $(this).closest('.footer-page-container').addClass('cookie-policy-custom-container');
        });
      });
      waitForElm('.banner-carousel-section').then((elm) => {
        $($('.banner-carousel-section')[0]).each(function () {
          $(this).addClass('modified');
          $(this).addClass('main-mobile-banner');
        });
        $($('.banner-carousel-section')[1]).each(function () {
          $(this).addClass('modified');
          $(this).addClass('main-desktop-banner');
        });
        $($('.banner-carousel-section')[2]).each(function () {
          $(this).addClass('modified');
          $(this).addClass('first-multi-banner-carousel');
        });
        $($('.banner-carousel-section')[3]).each(function () {
          $(this).addClass('modified');
          $(this).addClass('second-multi-banner-carousel');
        });
      });
      waitForElm('.home-banner .banner-heading a>div').then((elm) => {
        $('.home-banner .banner-heading a>div').each(function () {
          $(this).closest('.home-banner').addClass('home-gift-section');
        });
      });
      waitForElm('.home-gift-section .banner-heading').then((elm) => {
        $('.home-gift-section .banner-heading').each(function () {
          $(this).addClass('modified-btn-added');
          // $(this).append('<a class="custom-btn btn-transparent" href="https://shop.rosewoodhotels.com/phnom-penh/phnom-penh/gift-cards">SHOP NOW</a>');
        });
      });
      waitForElm('.promo-section .promo-holder .promo-box .promo-content .promo-title').then((elm) => {
        $('.promo-section .promo-holder .promo-box .promo-content .promo-title:contains("DISCOVER EXPERIENCES")').each(function () {
          $(this).closest('.promo-section').addClass('promo-modified');
        });
      });
      waitForElm('a.wishlistImage img').then((elm) => {
        $('a.wishlistImage img').each(function () {
          $(this).addClass('appeared');
          $(this).attr('src', 'https://static.techsembly.com/jH1yxVUj6LaVtApPQdRCUZSK');
          if (($(this).attr('alt') == 'added-to-wishlist') && ($(this).attr('src') != 'https://static.techsembly.com/iRMtYGEWNck56pBFHs1XfkWb')) {
            $(this).attr('src', 'https://static.techsembly.com/iRMtYGEWNck56pBFHs1XfkWb');
          }
        });
      });
      waitForElm('app-product-detail app-related-products').then((elm) => {
        $('app-product-detail app-related-products[title="More items from"]').each(function () {
          $(this).addClass('modified');
          $(this).find('h3').html('your recommendations').addClass("productHeading").removeClass("text-center");
        });
      });
      waitForElm('.custom-container.complete .vendor-items-holder .vendor-order-details .order-item').then((elm) => {
        $('.custom-container.complete .vendor-items-holder .vendor-order-details .order-item .order-item-left').each(function () {
          $(this).addClass('col-md-5');
          $(this).removeClass('col-md-8');
        });
        $('.custom-container.complete .vendor-items-holder .vendor-order-details .order-item .order-item-right').each(function () {
          $(this).addClass('col-md-7');
          $(this).removeClass('col-md-4');
        });
      });
      waitForElm('.custom-container.cart-container').then((elm) => {
        $('.custom-container.cart-container').each(function () {
          $(this).closest('body').attr('data-pagetype', 'cart');
        });
      });
      waitForElm('.custom-container.checkout').then((elm) => {
        $('.custom-container.checkout').each(function () {
          $(this).closest('body').attr('data-pagetype', 'checkout');
        });
      });
      waitForElm('.custom-container .refund-form-cont').then((elm) => {
        $('.custom-container .refund-form-cont').each(function () {
          $(this).closest('body').attr('data-pagetype', 'refund-form');
        });
      });
      waitForElm('#continue-to-delivery-and-billing').then((elm) => {
        $('#continue-to-delivery-and-billing').each(function () {
          $(this).addClass('mb-0 modified');
        });
      });
      waitForElm('.container.footer-page-container .page-heading').then((elm) => {
        $('.container.footer-page-container .page-heading:contains(Contact Us)').each(function () {
          $(this).closest('body').attr('data-pagetype', 'contact-us');
        });
      });
      waitForElm('.product-detail-container .personalise-form .form-group .order-btn').then((elm) => {
        $('.product-detail-container .personalise-form .form-group .order-btn').each(function () {
          $(this).addClass('modified');
          $(this).html('ADD TO BAG');
        });
      });
      waitForElm('.custom-container.cart-container .cart-login-cont .login-cart-panel .panel-heading').then((elm) => {
        $('.custom-container.cart-container .cart-login-cont .login-cart-panel .panel-heading:contains(Sign In To Check Out Faster)').each(function () {
          $(this).addClass('modified');
          $(this).html('Sign in to check out faster');
        });
      });
      waitForElm('.product-detail-container .product-holder .share-link').then((elm) => {
        $('.product-detail-container .product-holder .share-link li:first-child span').each(function () {
          $(this).addClass('modified shareLable');
          $(this).html('Share via');
        });
      });

      waitForElm('.product-detail-container .product-holder .share-link').then((elm) => {
        // Find the image element
        var image = $('img[src="https://cdn-saas.techsembly.com/packs/./storefront/fonts/pinterest1.svg"]');
        // Find the parent <a> element
        var parentLink = image.parent('a');
        // Move the target attribute from the image to the parent <a> element
        parentLink.attr('target', image.attr('target'));
        // Remove the target attribute from the image
        image.removeAttr('target');
      });


      // Find the image element
      var image = $('img[src="https://cdn-saas.techsembly.com/packs/./storefront/fonts/pinterest1.svg"]');

      // Find the parent <a> element
      var parentLink = image.parent('a');

      // Move the target attribute from the image to the parent <a> element
      parentLink.attr('target', image.attr('target'));

      // Remove the target attribute from the image
      image.removeAttr('target');



      waitForElm('.product-detail-container .personalise#personalise>div.row').then((elm) => {
        $('.product-detail-container .personalise#personalise>div.row').each(function () {
          $(this).addClass('modified productDesc');
        });
      });
      waitForElm('.payment-form-container .card-details-cont').then((elm) => {
        $('.payment-form-container .card-details-cont:first-child').each(function () {
          $(this).prepend('<h2 class="section-heading">Card Details</h2>');
          $(this).append('<p class="card-note">We accept Visa, Mastercard and American Express.</p>');
        });
      });
      waitForElm('.shipping-form-container .breadcrumb-heading').then((elm) => {
        $('.shipping-form-container .breadcrumb-heading:contains("Shipping")').each(function () {
          $(this).addClass('modified').html("Shipping Details");
        });
        $('.shipping-form-container .breadcrumb-heading:contains("Billing")').each(function () {
          $(this).addClass('modified').html("Billing Details");
        });
      });
      waitForElm('app-checkout-delivery .shipping-form-container .breadcrumb-heading').then((elm) => {
        $('.shipping-form-container .breadcrumb-heading:contains("Select Delivery Method")').each(function () {
          $(this).addClass('modified').html("Delivery Details");
        });
      });
      waitForElm('.delivery-methods-cont .vendor-cards-holder .card.vendor-card .card-header .vendor-title>span').then((elm) => {
        $('.delivery-methods-cont .vendor-cards-holder .card.vendor-card .card-header .vendor-title>span:contains("Delivered by")').each(function () {
          $(this).removeClass('mr-1');
          $(this).addClass('modified').html("Fulfilled By");
        });
      });
      // waitForElm('.cart-container .vendor-items-cont .card-header .delivered-by').then((elm) => {
      //   $('.cart-container .vendor-items-cont .card-header .delivered-by:contains("Delivered By")').each(function () {
      //     $(this).removeClass('mr-1');
      //     $(this).addClass('modified').html("Fulfilled By");
      //   });
      // });
      waitForElm('.delivery-methods-cont .vendor-cards-holder .card .order-shipping-container label').then((elm) => {
        $(".delivery-methods-cont .vendor-cards-holder .card .order-shipping-container label:contains('Recipient's Info:')").each(function () {
          $(this).closest('.delivery-dropdowns-cont').addClass('modified digital-product-cont');
          $(this).closest('.order-shipping-container').addClass('modified digital-shipping-options-cont');
        });
      });
      waitForElm('.digital-product-cont .form-group input').then((elm) => {
        $('.digital-product-cont .form-group input[placeholder="Last Name"]').each(function () {
          $(this).closest('.col-12').addClass('modified digital-lastname-cont');
        });
        $('.digital-product-cont .form-group input[placeholder="First Name"]').each(function () {
          $(this).closest('.col-12').addClass('modified digital-firstname-cont');
        });
      });
      waitForElm('.delivery-methods-cont').then((elm) => {
        $('.delivery-methods-cont').each(function () {
          $(this).closest('.shipping-form-container').addClass("continue-to-payment-cont");
        });
      });
      waitForElm('.continue-to-payment-cont .btn.btn-submit').then((elm) => {
        $('.continue-to-payment-cont .btn.btn-submit').each(function () {
          $(this).addClass("continue-to-payment-modified");
          $(this).html("CONTINUE TO PAYMENT");
        });
      });
      waitForElm('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc').then((elm) => {
        $('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc:contains("Sales Tax")').each(function () {
          $(this).addClass('modified').html("Sales Tax:");
        });
      });
      waitForElm('.custom-container .breadcrumb-heading').then((elm) => {
        $('.custom-container .breadcrumb-heading:contains("Payment")').each(function () {
          $(this).addClass('modified payment-heading');
        });
      });
      waitForElm('.custom-container.checkout .payment-form-container .btn.btn-primary').then((elm) => {
        $(".custom-container.checkout .payment-form-container .btn.btn-primary:not(.modified, .apply-btn)").addClass('modified complete-purchase-btn').html('COMPLETE PURCHASE');
      });
      waitForElm('.complete-purchase-btn').then((elm) => {
        $('.complete-purchase-btn').each(function () {
          $(this).closest('.form-group').addClass('modified complete-purchase-btn-cont');
        });
      });
      waitForElm('.checkout-container.complete .section-cont.customer-note').then((elm) => {
        $('.checkout-container.complete .section-cont.customer-note').each(function () {
          if (!$(this).hasClass('total-delivery')) {
            $(this).addClass('total-delivery');
            var newTxt = 'Your order is being processed. You will receive an email with further information.';
            $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function (_, html) {
              return html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">' + newTxt + '</div>')
            });
          }
        });
        $('.checkout-container.complete .section-cont.customer-note').each(function () {
          if (!$(this).hasClass('total-delivery1')) {
            $(this).addClass('total-delivery1');
            var firstPart = $(this).find('br')[0].previousSibling.nodeValue;
            var secondPart = $(this).find('.updated-del-string').html();
            $(this).html('<div class="salutation-cont">' + firstPart + '</div><div class="updated-del-string pt-0">' + secondPart + '</div>');
          }
        });
      });
      waitForElm('.products-container .products-holder-main .products-holder .product-column .product-list .product-content .product-title span').then((elm) => {
        $('.products-container .products-holder-main .products-holder .product-column .product-list .product-content .product-title span:contains("by")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            var removedTxt = $(this).html();
            removedTxt = removedTxt.replace("by", "");
            $(this).html(removedTxt);
          }
        });
      });
      waitForElm('.product-detail-container .product-holder .product-detail .product-info .made-by a.vendor-link').then((elm) => {
        $('.product-detail-container .product-holder .product-detail .product-info .made-by a.vendor-link').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            var removedTxt = $(this).html();
            removedTxt = removedTxt.replace("by", "");
            $(this).html(removedTxt);
          }
        });
      });
      waitForElm('.related-products .related-products-holder .product-content .product-title a.vendor-link span').then((elm) => {
        $('.related-products .related-products-holder .product-content .product-title a.vendor-link span').each(function () {
          $(this).addClass('modified');
          var removedTxt = $(this).html();
          removedTxt = removedTxt.replace("by", "");
          $(this).html(removedTxt);
        });
      });
      waitForElm('.custom-container.checkout .shipping-form-container .btn.btn-submit').then((elm) => {
        $('.custom-container.checkout .shipping-form-container .btn.btn-submit:contains("Continue Checkout")').each(function () {
          $(this).addClass('modified');
          $(this).html('CONTINUE TO DELIVERY');
        });
      });
      waitForElm('.custom-container.checkout .shipping-form-container .address-heading').then((elm) => {
        $('.custom-container.checkout .shipping-form-container .address-heading:contains("Or Add New Billing Details")').each(function () {
          $(this).addClass('submit-btn-modified or-addNew-wrapper');
          $("#continue-to-delivery-and-billing").html('add & CONTINUE TO DELIVERY');
        });
      });
      waitForElm(".custom-container.checkout .shipping-form-container .address-heading").then((elm) => {
        $('.custom-container.checkout .shipping-form-container .address-heading:contains("Or Add New Shipping Details")').each(function () {
          $(this).addClass("submit-btn-modified or-addNew-wrapper");
          $("#continue-to-delivery-and-billing").html("add & CONTINUE TO DELIVERY");
        });
      }).catch((error) => { });
      waitForElm('.payment-form-container .checkbox-cont .checkbox-note').then((elm) => {
        $('.payment-form-container .checkbox-cont .checkbox-note:contains("Same as delivery address")').each(function () {
          $(this).addClass('modified');
          $(this).closest('form-group').addClass('modified same-as-shipping-cont');
          $(this).html('Same as shipping address');
        });
      });
      waitForElm('.checkout-container.complete .shipping-data .shipping-row div').then((elm) => {
        $('.checkout-container.complete .shipping-data .shipping-row div:contains("Address")').each(function () {
          $(this).addClass('modified');
          $(this).html('Delivery Address:');
        });
      });
      waitForElm('.checkout-container.complete .shipping-data .shipping-row div').then((elm) => {
        $('.checkout-container.complete .shipping-data .shipping-row div:contains("Shipping:")').each(function () {
          $(this).addClass('modified');
          $(this).html('Delivery:');
        });
      });
      waitForElm('.checkout-container.complete .card-body .total-cont .col-3.pr-3').then((elm) => {
        $('.checkout-container.complete .card-body .total-cont .col-3.pr-3:contains("Total:")').each(function () {
          $(this).addClass('modified');
          $(this).html('Total');
        });
      });
      waitForElm('.checkout-container.complete .card-body .msg-cont .msg-inner').then((elm) => {
        $('.checkout-container.complete .card-body .msg-cont .msg-inner').each(function () {
          $(this).addClass('modified');
          $(this).html('You will receive a shipping confirmation when your item(s) are on the way.');
        });
      });
      waitForElm('.order-confirm-wrapper .card-footer .card-footer-left>span').then((elm) => {
        $('.order-confirm-wrapper .card-footer .card-footer-left>span:contains("Questions? Contact our")').each(function () {
          $(this).addClass('modified');
          $(this).html('Need help? Contact our');
        });
      });
      waitForElm('.order-confirm-wrapper .card-footer .card-footer-left a').then((elm) => {
        $('.order-confirm-wrapper .card-footer .card-footer-left a:contains("Customer Support")').each(function () {
          $(this).addClass('modified');
          $(this).attr('href', 'mailto:rosewood@techsembly.com');
        });
      });
      waitForElm('.order-confirm-wrapper .card-footer .card-footer-right').then((elm) => {
        $('.order-confirm-wrapper .card-footer .card-footer-right').each(function () {
          $(this).addClass('modified');
          $(this).html('RHR Gift Card Services, L.L.C., a Virginia limited liability company');
        });
      });
      waitForElm('.footer .footer-widget .widget-content ul li a').then((elm) => {
        $('.footer .footer-widget .widget-content ul li a').each(function () {
          $(this).addClass('modified');
          $(this).attr('target', '_blank');
        });
        $(".powered-link-cont a").attr('target', '_blank');
      });
      waitForElm('.footer .mobile-footer .footer-widget ul li a').then((elm) => {
        $('.footer .mobile-footer .footer-widget ul li a').each(function () {
          $(this).addClass('modified');
          $(this).attr('target', '_blank');
        });
      });
      waitForElm('.footer a').then((elm) => {
        $('.footer a:contains("Tel:")').each(function () {
          var string_phone = $(this).html();
          var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
          var phone_no = "tel:" + result_phone;
          if (!$(this).hasClass('tel-link')) {
            $(this).addClass('tel-link');
            $(this).attr('href', phone_no);
          }
        });
        // $('.footer a:contains("Gift Card Support")').each(function () {
        //   var string_email = 'rosewood@techsembly.com';
        //   var email_link = "mailto:" + string_email;
        //   if (!$(this).hasClass('email-link')) {
        //     $(this).addClass('email-link');
        //     $(this).attr('href', email_link);
        //   }
        // });
      });
      waitForElm('.notificationbar .cross-cont').then((elm) => {
        $(".notificationbar .cross-cont").click(function () {
          if (!$(".notificationbar").hasClass("d-none")) {
            $(".notificationbar").addClass("d-none");
            $("nav#sidebar").removeClass("announce-open");
          }
        });
      });
      waitForElm('.sidebar-cross-cont img').then((elm) => {
        $(".sidebar-cross-cont img").click(function () {
          if ($(".mob-overlay").hasClass('active')) {
            $("body").removeClass('scroll-hidden');
            $("html").removeClass('scroll-hidden-html');
            $("nav#sidebar").removeClass('active');
            $(".mob-overlay").removeClass("active");
          }
        });
      });

      waitForElm('.product-detail-container .product-holder .product-detail div').then((elm) => {
        $('.product-detail-container .product-holder .product-detail div:contains(" BEST SELLER ")').each(function () {
          $(this).addClass('modified best-seller-tag');
        });
      });
      // waitForElm('.container.home-intro-container').then((elm) => {
      //   $('.container.home-intro-container').each(function () {
      //     $(this).addClass('modified');
      //     $(this).wrap('<a class="gift-sec-link" href="https://the-carlyle.techsembly.com/the-carlyle/gift-card"></a>');
      //   });
      // });
      waitForElm('.custom-container.login-signup').then((elm) => {
        $('.custom-container.login-signup').each(function () {
          $(this).closest('body').attr('data-pagetype', 'login-signup');
        });
      });
      waitForElm('.user-wrapper .user-container .user-heading').then((elm) => {
        $('.user-wrapper .user-container .user-heading:contains("My TS Card Balance")').each(function () {
          $(this).closest(".user-container").addClass('d-none');
        });
        $('.user-wrapper .user-container .user-heading:contains("My Balance")').each(function () {
          $(this).closest(".user-container").addClass('d-none');
        });
      });
      waitForElm('.order-summary-cont .btn').then((elm) => {
        $('.order-summary-cont .btn:contains("Proceed to Checkout")').each(function () {
          $(this).closest('.card-body').addClass('textHide-modified');
        });
      });
      waitForElm('.mat-form-field-label').then((elm) => {
        $(".mat-form-field-label").each(function () {
          $(this).removeAttr("aria-owns");
        });
      });
      waitForElm('.custom-container.cart-container .card .card-body .item-details-cont .personalize-row .item-desc .collapse-link span').then((elm) => {
        $('.custom-container.cart-container .card .card-body .item-details-cont .personalize-row .item-desc .collapse-link span:contains(details)').each(function () {
          $(this).addClass('modified');
          $(this).html('Details');
        });
      });
      waitForElm('#delivery-options-popup .modal-container .del-options-form .modal-body .radio-container').then((elm) => {
        $('#delivery-options-popup .modal-container .del-options-form .modal-body .radio-container').each(function () {
          $(this).parent().addClass('justify-content-center');
        });
      });
      // waitForElm('.tab-pane.personalise').then((elm) => {
      //   $('.tab-pane.personalise').each(function(){
      //     $(this).addClass('modified')
      //     let personaliseHtml = $(this).find('.personalise-form');
      //     $(personaliseHtml).insertBefore('.product-order #myTab');
      //   })
      // });
      waitForElm('.shipping-form-container .breadcrumb-heading').then((elm) => {
        $('.shipping-form-container .breadcrumb-heading:contains("Billing Details")').each(function () {
          $(".custom-container.checkout .shipping-form-cont h1:contains('Shipping Address')").addClass('modified').html("Billing Address");
        });
      });
      waitForElm('.cta-btn').then((elm) => {
        $('.cta-btn').attr('target', '_self');
      }).catch((error) => { });

      waitForElm('.sign-up-section').then((elm) => {
        $(".sign-up-section").wrap("<div class='max-sign-up' style='background-color:#D2C7C4;'></div>");
      }).catch((error) => { });
      // junaid END
      waitForElm('.product-detail-container .product-holder .product-detail .product-info .product-heading .product-sku')
      .then((elem) => {
        // ---------------'SKU NO', value 
        customizeGiftCard('034485', 45076);
        customizeGiftCard('034486', 45082);
      })
      .catch((error) => {});

      waitForElm(".vendor-order-details .items-atts-cont .qty").then((elm) => {
        $('.vendor-order-details .items-atts-cont .qty').each(function () {
          if (!$(this).hasClass('modified-quantity')) {
            $(this).addClass('modified-quantity');
            var orderQuantity = $(this);
            $(this).parents('.order-item').find('.item-desc').append(orderQuantity);
          }
        });
      }).catch((error) => { });


      waitForElm(".product-detail-container .product-holder .product-detail .product-info .product-sku").then((elm) => {
        $('.product-detail-container .product-holder .product-detail .product-info .product-sku').each(function () {
          let delivery_skus = [034486,034485];
          let sku_no = parseInt($(this).html());
          if (delivery_skus.includes(sku_no) && !$(this).closest('.product-holder').hasClass('del-opt-pro')) {
            $(this).closest('.product-holder').addClass('del-opt-pro');
            $('.product-detail-container .personalise-form .delivery-form .delivery-fields[formcontrolname="receipient_first_name"]').each(function () {
              // var infoText;
              var recipientInfo = "<div class='form-group recipient-info-top'><label class='control-label'>Recipient Details</label><div class='recipient-note'>Please share the name of the gift card recipient</div></div>";
              if (!$('.product-detail-container .personalise-form .delivery-form').find('.recipient-info-top').length) {
                $(recipientInfo).insertBefore($(this));
              }
            });
            // var navTabs = $('.product-detail .product-order .nav.nav-tabs'); // Select the 'nav nav-tabs' element
            // navTabs.addClass('d-none');
            $(".product-detail-container .personalise-form .delivery-form p:contains('Please select from one of the options below.')").each(function () {
              if (!$(this).hasClass('d-none')) {
                $(this).addClass('d-none')
              }
            });
            $(".product-detail-container .personalise-form .delivery-form h1:contains('Delivery Options')").each(function () {
              if (!$(this).hasClass('d-none')) {
                $(this).addClass('d-none')
              }
            });
            $(".product-detail-container .personalise-form .delivery-form h1:contains('Delivery Options')").each(function () {
              if (!$(this).hasClass('d-none')) {
                $(this).addClass('d-none')
              }
            });

            $(".product-order #myTab").each(function () {
              if (!$(this).hasClass('d-none')) {
                $(this).addClass('d-none')
              }
            });

          }


        });
      }).catch((error) => { });


      $('meta[name="viewport"]:not(.modified)').addClass('modified').attr("content", "width=device-width, initial-scale=1, maximum-scale=1");
      $(".header .container.p-0.d-none.d-lg-block.d-xl-block:not(.modified)").addClass('modified').attr("id", "myHeader");
      $("app-product-listing .mob-btn-group").parent().addClass('product-listing-head');
      $("button").closest(".form-group").addClass('continue-billing-holder');
      $("app-product-listing .product-listing-head .mob-btn-group #refineBtn").html("Filters")

      setTimeout(function () {
        $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');

        $('.mobile-carousel-section').each(function () {
          $(this).wrap("<section class='col-12 mobile-carousel-wrapper'></section>");
        });
      }, 2000);


      var timesRun = 0;
      var interval = setInterval(function () {
        if ($('.payment-form-container').length) {
          timesRun += 1;
          if (timesRun === 2) {
            clearInterval(interval);
          }
          $(window).scrollTop(0);
          console.log('scrolled');
        }
      }, 2000);

      setInterval(function () {

        if (navigator.platform.indexOf('Mac') > -1 || navigator.platform.indexOf('iPhone') || navigator.platform.indexOf('iPad')) {
          $('#delivery-options-popup .modal-container .radio-container .checkmark').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified mac-radio-holder');
            }
          });
        }

        if (!$('.promo-section').first().hasClass('promo-modified')) {
          $('.promo-section').first().addClass('promo-modify');
        }

        waitForElm('.payment-form-container').then((elm) => {
          if ($('.payment-form-container').length > 0) {
            if (!$('body').hasClass('scrolled')) {
              $(window).scrollTop(0);
              $('body').addClass('scrolled')
            }
          }
        }).catch((error) => { });

        // code for sticky header
        window.onscroll = function () {
          var header = document.getElementsByClassName("main-menu-wrapper");
          var sticky = header[0].offsetTop;
          if (window.pageYOffset > sticky) {
            header[0].classList.add("sticky");
          } else {
            header[0].classList.remove("sticky");
          }
        };
        // code for sticky header end


        var segment_str = window.location.pathname; // return segment1/segment2/segment3/segment4
        var segment_array = segment_str.split('/');
        var last_segment = segment_array.pop();

        if (last_segment == 'gift-card-033391' || last_segment == 'e-gift-card-033392') {
          $('.product-detail-container .product-holder').each(function () {
            if (!$(this).hasClass('gift-card-modified')) {
              $(this).addClass('gift-card-modified');
            }
          });
          $('.product-detail-container .personalise-form .delivery-form .radio-container input[formcontrolname="delivery_mode"]').each(function () {
            if (!$(this).closest('.radio-container').hasClass('d-none')) {
              $(this).closest('.radio-container').addClass('d-none');
            }
          });

          $(".product-detail-container .personalise-form .delivery-form p:contains('Please select from one of the options below.')").each(function () {
            if (!$(this).hasClass('d-none')) {
              $(this).addClass('d-none')
            }
          });
          $(".product-detail-container .personalise-form .delivery-form h1:contains('Delivery Options')").each(function () {
            if (!$(this).hasClass('d-none')) {
              $(this).addClass('d-none')
            }
          });
          $('.product-detail-container .personalise-form .delivery-form .delivery-fields[formcontrolname="receipient_first_name"]').each(function () {
            // var infoText;
            var recipientInfo = "<div class='form-group recipient-info-top'><label class='control-label'>Recipient Details</label><div class='recipient-note'>Please share the name of the gift card recipient</div></div>";
            if (!$('.product-detail-container .personalise-form .delivery-form').find('.recipient-info-top').length) {
              $(recipientInfo).insertBefore($(this));
            }
          });
        }
        $('.checkout .payment-form-container .card-details-cont .address-card .row-head:contains("Address")').each(function () {
          if (!$(this).closest('.card-details-cont').hasClass('modified')) {
            $(this).closest('.card-details-cont').addClass('modified address-card-cont')
          }
        });
        $('.checkout .address-card-cont .section-heading:contains("Details")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified')
            $(this).html('Billing Details')
          }
        });

        hrefurl = $(location).attr("href");
        last_part = hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
        currentUrlSecondPart = window.location.pathname.split('/')[1];
        currentUrlFull = document.location.origin + '/' + currentUrlSecondPart;
        if (currentUrlSecondPart == '') {
          if (!$('body').attr('data-pagetype', 'home')) {
            $('body').attr('data-pagetype', 'home');
          }
        }
        if (currentUrlSecondPart == 'catalogsearch') {
          if (!$('body').attr('data-pagetype', 'search_results')) {
            $('body').attr('data-pagetype', 'search_results');
          }
        }
        if (currentUrlSecondPart == 'vendors') {
          if (!$('body').attr('data-pagetype', 'vendors')) {
            $('body').attr('data-pagetype', 'vendors');
          }
        }
        if (last_part == 'wishlist') {
          if (!$('body').attr('data-pagetype', 'wishlist')) {
            $('body').attr('data-pagetype', 'wishlist');
          }
        }

        $("body[data-pagetype='product'] .related-products .custom-container:not(.modified)").addClass('modified').prepend('<div class="text-releate">your recommendations</div>');

        if ((navigator.userAgent.indexOf('Mac OS X') != -1) || (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
          $("header.header .head-links li .cart-qty:not(.modified-ios)").addClass('modified-ios');
        }

        if ((navigator.userAgent.indexOf('Mac OS X') != -1) || (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
          $("header .mob-top-links .cart-qty:not(.modified-ios)").addClass('modified-ios');
        }

        if (match == "gift-card") {
          $(".product-detail-container .product-holder .personalise-form .delivery-form .info-text").each(function () {
            var msg_txt;
            msg_txt = "If adding multiple gift cards to your cart, this message will apply to all gift cards sent to one single shipping address. If you would like unique messages for each card, you must add each one separately to the shopping cart.";
            if (!$(this).hasClass("modified-info-text")) {
              $(this).addClass("modified-info-text");
              $(this).html(msg_txt);
            }
          });
        }

        if (match == "e-gift") {
          $(".product-detail-container .product-holder .personalise-form .delivery-form .info-text").each(function () {
            var msg_txt;
            msg_txt = "If adding multiple gift cards to your cart, this message will apply to all gift cards sent to one single shipping address. If you would like unique messages for each card, you must add each one separately to the shopping cart.";
            if (!$(this).hasClass("modified-info-text")) {
              $(this).addClass("modified-info-text");
              $(this).html(msg_txt);
            }
          });
        }
      }, 2000);
    });
  });
})();