function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }
    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });
    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

function addMenuLabel() {
  var menuLabel = '<span class="mob-menu-label text-uppercase">Menu</span>';
  var elemMenuButton = $('header.header .mobile-menu#sidebarCollapse')[0];
  elemMenuButton.insertAdjacentHTML('beforeend', menuLabel);
}

function addLoginCartNote() {
  var loginCartNote = "<div class='login-note'>This login is for Rosewood London, A Rosewood Hotel's online shop.<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}

function addGuestCheckoutNote() {
  var checkoutNote = "<div class='checkout-note text-center w-100 pt-4'>If you do not have an account please use guest checkout. You will be given the option to save account information later in the checkout process<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}

function addShippingAddrHeading() {
  var addrHeading = "<div class='shippingHeading'><h1>Shipping Address</h1><div>";
  var shippingFormCont = $('.custom-container.checkout .shipping-form-cont')[0];
  shippingFormCont.insertAdjacentHTML('afterbegin', addrHeading);
}

function addcopyrightText() {
  var copyrightHtml = '<div class="container-fluid"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-6 tc-rights-reserved pl-0 pr-0">Â©2022 Rosewood Hotel Group</div><div class="col-6 ts-copyright text-right pr-0"><span>Powered by</span><span class="ml-1"><a href="https://techsembly.com/" target="_blank">Techsembly</a></span></div></div></div>'
  var elem = $(".checkout-footer")[0]
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}

function addCheckoutNote() {
  var copyrightHtml = "<div class='row flex-column checkoutFooterNote'><p><span class='title'>Note:</span></p><p>Your purchase order is subject to Rosewood London <a href='https://www.rosewoodhotels.com/en/gift-cards/terms-and-conditions' target='_blank'>Terms and Conditions</a> and our service provider Techsembly's <a href='https://rosewood-london.techsembly.com/pages/terms-and-conditions' target='_blank'>Terms and Conditions</a>.</p><p>By confirming your order you agree to Rosewood London <a href='https://www.rosewoodhotels.com/en/gift-cards/terms-and-conditions'>Terms and Conditions</a> and Techsembly's <a href='https://rosewood-london.techsembly.com/pages/terms-and-conditions' target='_blank'>Terms and Conditions</a> and the processing of your personal data as described in Rosewood London <a href='https://www.rosewoodhotels.com/en/privacy-policy' target='_blank'>Privacy Policy</a>. This purchase will display on my statement as TECHS* Rosewood London.</p><p class='assistance-text'>For assistance please contact Customer Service, 7 days a week, 24 hours a day.</p><p>UK Toll-free: <a href='tel:+448000488077'>+44 800 0488077</a><br>US Local: <a href='tel:+12035838588'>+1 203 583 8588</a></p></div>"
  var elem = $(".checkout  .payment-form-container .payment-form")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}

function addBackToHomeBtn() {
  var copyrightHtml = '<div class="d-flex justify-content-center items-center backToHomeBtn"><a href="https://rosewood-london.techsembly.com/">BACK TO HOME</a></div>'
  var elem = $(".order-confirm-wrapper .card-footer")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}

function addSignUpSection() {
  var newSignupSection = '<section class="sign-up-section new">' +
  '<div class="sign-up-container"><h2 class="section-title">STAY CONNECTED</h2>' +
  '<div class="signup-text text-center">' +
  '<p>Be the first to receive Rosewood news and exclusive offers</p>' +
  '<a href="https://rwhg-mkt-prod1-m.adobe-campaign.com/lp/RosewoodWebSubscriptions?languageCode=en&propertyCode=Brand&source=WEB&emailAddress=" class="btn btn-primary btn-signup" type="button" target="_blank">SIGN UP</a>' +
  '</div>' +
  '</div>' +
  '</section>';
  var elemFooter = $('footer.footer')[0];
  elemFooter.insertAdjacentHTML('beforebegin', newSignupSection);
}

function addAnnounceMentBar() {
   var announcementBar = '<div class="notificationbar notification-outer">' +
  '<div class="notification-inner">Place your festive orders by 19 December to receive before Christmas Day.</div>'+
  '<span class="cross-cont"><img width="12" src="https://static.techsembly.com/yApgKFNtUAA6Ae3oQVCNBMck"/></span>'+
  '</div>';
  var elemContent = $('body #content')[0];
  elemContent.insertAdjacentHTML('afterbegin', announcementBar);
}

function addSidebarCross() {
   var crossCont = '<div class="sidebar-cross-cont">' +
   '<div class="inner-cont"><img width="19" src="https://static.techsembly.com/yApgKFNtUAA6Ae3oQVCNBMck"/></div>' +
   '</div>';
  var sidebarContent = $('body #sidebar')[0];
  sidebarContent.insertAdjacentHTML('afterbegin', crossCont);
}

function refundForm() {
  var refundFormTemp = '<form id="refund-form" name="refund-form" accept-charset="utf-8" action="https://formspree.io/f/mknebzbw" method="post">' +
    '<h1 class="refund-heading">RETURNS & REFUNDS</h1>' +
    '<p class="refund-details">If your purchase is not exactly what you are looking for, you have 14 days to return. Please fill out the form below.</p>' +
    '<!-- firstname and last name -->' +
    '<div class="feilds-cont d-flex justify-content-between flex-wrap px-0">' +
    '<div class="col-12 col-md-6 pl-0 pr-0 pr-md-2">' +
    '<div class="form-group pr-0 pr-md-1">' +
    '<input name="subject" type="hidden" value="Refund request from carlyl website" />' +
    '<input class="form-control" name="firstName" id="first-name" type="text" placeholder="First Name" required>' +
    '</div>' +
    '</div>' +
    '<div class="col-12 col-md-6 pl-0 pl-md-2 pr-0">' +
    '<div class="form-group pl-0 pl-md-1">' +
    '<input class="form-control" name="lastName" id="last-name" type="text" placeholder="Last Name" required>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '<!-- email and order id -->' +
    '<div class="feilds-cont d-flex justify-content-between flex-wrap px-0">' +
    '<div class="col-12 col-md-8 pl-0 pr-0 pr-md-2">' +
    '<div class="form-group pr-0 pr-md-1">' +
    '<input class="form-control" name="email" id="email" type="email" placeholder="Email Address" required>' +
    '</div>' +
    '</div>' +
    '<div class="col-12 col-md-4 pl-0 pl-md-2 pr-0">' +
    '<div class="form-group pl-0 pl-md-1">' +
    '<input class="form-control" name="orderId" id="order-id" type="text" placeholder="Order ID" required>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '<!-- message box -->' +
    '<div class="feilds-cont d-flex justify-content-between flex-wrap px-0 msg-box">' +
    '<label>Please share the details of the items you would like to return.</label>' +
    '<div class="form-group pr-1 refund-msg-cont">' +
    '<textarea class="form-control" name="message" id="message" placeholder="If multiple products, please list in the space provided."></textarea>' +
    '</div>' +
    '</div>' +
    '<!-- reason select box -->' +
    '<div class="feilds-cont d-flex justify-content-between flex-wrap px-0 reason-select">' +
    '<label>Reason for returning your item(s)</label>' +
    '<div class="form-group pr-1 w-100">' +
    '<select  class="form-control" id="reason" name="reason">' +
    '<option disabled="disabled" selected="selected" value="">Please Select</option>' +
    '<option value="damaged">Product was damaged</option>' +
    '<option value="quality">Product quality</option>' +
    '<option value="different">Different from the photograph</option>' +
    '<option value="incorrect-size">Incorrect size</option>' +
    '<option value="Other">Other</option>' +
    '</select>' +
    '</div>' +
    '<div class="form-group pr-1 refund-msg-cont">' +
    '<textarea class="form-control" id="other-message" name="reason_desc" placeholder="If you selected other, please share your reason for returning your item(s)." required></textarea>' +
    '</div>' +
    '</div>' +
    '<!-- return method select -->' +
    '<div class="feilds-cont d-flex justify-content-between flex-wrap px-0 return-method-select">' +
    '<label>Select Return Method</label>' +
    '<div class="form-group pr-1 w-100">' +
    '<select  class="form-control" id="return-method" name="return_method">' +
    '<option disabled="disabled" selected="selected" value="">Please Select</option>' +
    '<option value="drop-off">Drop-off at Rosewood London</option>' +
    '<option value="by-post">Return by post</option>' +
    '</select>' +
    '</div>' +
    '</div>' +
    '<!-- refund note -->' +
    '<div class="refund-note">' +
    '<p class="note-msg">Please note all returns are at your own expense. Details for returns by post:</p>' +
    '<p class="return-details">Rosewood London - Returns</p>' +
    '<p class="return-details">252 High Holborn,</p>' +
    '<p class="return-details">London,</p>' +
    '<p class="return-details">WC1V 7EN</p>' +
    '<p class="return-details"><a href="tel:02077818888">020 7781 8888</a></p>' +
    // '<p class="return-details">rosewood@techsembly.com</p>'+
    '</div>' +
    '<div class="form-group request-btn-cont">' +
    '<button class="btn btn-primary px-4" type="submit">REQUEST REFUND</button>' +
    '</div>' +
    '<p id="my-form-status"></p>' +
  '</form>';
  var elemRefundFormTemp = $('.refund-form-cont')[0];
  elemRefundFormTemp.insertAdjacentHTML('afterbegin', refundFormTemp);
}

function addPersonalDataNote() {
  var loginPersonalDataNote = "<div class='personal-data-note'>Your personal data will be processed in accordance with Rosewood Hotel Group's <a href='https://www.rosewoodhotels.com/en/privacy-policy' target='_blank'>Privacy Policy</a>.<div>";
  var elemHeading = $('.custom-container .login-signup-tabs-cont .right-panel .signup-form')[0];
  elemHeading.insertAdjacentHTML('afterend', loginPersonalDataNote);
}

function addDeliveryParagraph() {
  var deliveryPara = "<p>Our delivery time starts from the moment an order is accepted and includes a 24-hour period where your items will be processed and dispatched.</p>";
  var elem = $('.delivery-page-cont')[0];
  elem.insertAdjacentHTML('beforeend', deliveryPara);
}
(function () {
  var script = document.createElement("SCRIPT");
  script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
  script.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script);

  var script_fonts = document.createElement("SCRIPT");
  script_fonts.src = 'https://bitbucket.org/TSTechsembly/jscss/raw/4c6a295796a068d0de0ffda519efa4d24c33b223/rosewood/global/fonts.js';
  script_fonts.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script_fonts);

    // Poll for jQuery to come into existence
    var checkReady = function (callback) {
      if (window.jQuery) {
        callback(jQuery);
      } else {
        window.setTimeout(function () {
           checkReady(callback);
          }, 20);
      }
    };

    // Now lets do something
    checkReady(function ($) {
      $(function () {


        waitForElm('footer.footer').then((elm) => {
          addSignUpSection();
        }).catch((error) => {});

        waitForElm('body #content').then((elm) => {
          addAnnounceMentBar();
          $("nav#sidebar").addClass("announce-open");
        }).catch((error) => {});

        waitForElm('body #sidebar').then((elm) => {
          addSidebarCross();
        }).catch((error) => {});

        waitForElm('header.header .mobile-menu .mobile-menu-icon').then((elm) => {
          addMenuLabel();
        }).catch((error) => {});


        waitForElm('.cart-right-cont').then((elm) => {
            addLoginCartNote();
            addGuestCheckoutNote();
          }).catch((error) => {});

        waitForElm('.shipping-form-cont').then((elm) => {
           addShippingAddrHeading();
          }).catch((error) => {});
        waitForElm('app-footer-checkout').then((elm) => {
            addcopyrightText()
          }).catch((error) => {});
        waitForElm('.checkout  .payment-form-container').then((elm) => {
            addCheckoutNote()
          }).catch((error) => {});

        waitForElm('.order-confirm-wrapper .card-footer').then((elm) => {
            addBackToHomeBtn()
          }).catch((error) => {});
        waitForElm('.refund-form-cont').then((elm) => {
          refundForm();
        }).catch((error) => {});
        waitForElm('.custom-container .login-signup-tabs-cont .right-panel').then((elm) => {
            addPersonalDataNote()
          }).catch((error) => {});
        waitForElm('.delivery-page-cont').then((elm) => {
            addDeliveryParagraph()
          }).catch((error) => {});
        // code for test form submission with prevent default
          waitForElm('#refund-form').then((elm) => {
           var form = document.getElementById("refund-form");

           async function handleSubmit(event) {
            event.preventDefault();
            var status = document.getElementById("my-form-status");
            var data = new FormData(event.target);
            fetch(event.target.action, {
              method: form.method,
              body: data,
              headers: {
                'Accept': 'application/json'
              }
            }).then(response => {
              if (response.ok) {
                status.innerHTML = "Thanks for your submission!";
                form.reset();
                location = 'https://rosewood-london.techsembly.com/pages/thank-you';
              } else {
                response.json().then(data => {
                  if (Object.hasOwn(data, 'errors')) {
                    status.innerHTML = data["errors"].map(error => error["message"]).join(", ")
                  } else {
                    status.innerHTML = "Oops! There was a problem submitting your form"
                  }
                })
              }
            }).catch(error => {
              status.innerHTML = "Oops! There was a problem submitting your form"
            });
          }
          form.addEventListener("submit", handleSubmit)

        }).catch((error) => {});
        $("body").children().each(function () {});

        $(".header .container.p-0.d-none.d-lg-block.d-xl-block:not(.modified)").addClass('modified').attr("id", "myHeader");


        $("app-product-listing .mob-btn-group").parent().addClass('product-listing-head');
        $("button").closest(".form-group").addClass('continue-billing-holder');
        // console.log($("#continue-to-delivery-and-billing").parent())
        $("app-product-listing .product-listing-head .mob-btn-group #refineBtn").html("Filters");

        setTimeout(function () {
          $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
        }, 2000);

        setInterval(function () {
          $(".container.custom-container.cart-container .cart-login-cont button#checkout-as-guest").click(function () {
            if (!$(this).hasClass("modified")) {
              $(this).addClass("modified");
              $(".custom-container.cart-container .cart-login-cont form input#guest-email").focus();
            }
          });

          $('.nav-search-field:not(.modified)').addClass('modified').attr("placeholder", "");
          $('header.header .menu-holder>li>div').each(function () {
            if (!$(this).children().length) {
              $(this).addClass('empty');
            }
          });
          if (!$('header.header .toggle-menu-cont').hasClass('col-3')) {
                $('header.header .toggle-menu-cont').addClass('col-3').removeClass('col-1 col-sm-2 col-md-2');
            }
            if (!$('header.header .menu-links-cont').hasClass('col-3')) {
                $('header.header .menu-links-cont').addClass('col-3').removeClass('col-md-6 col-sm-5 col-7');
            }
            if ($('header.header .logo').parent('div').hasClass('col-sm-5')) {
                $('header.header .logo').parent('div').removeClass('col-4 col-sm-5 col-md-4');
                $('header.header .logo').parent('div').addClass('col-6 logo-cont');
            }

          // To change copyright text for mobile screen
          // $('.footer .copyright-cont.d-lg-none .powered-by>div:first-child').each(function(){
          //   if(!$(this).hasClass('modified')){
          //     var newStr=$(".copyright-cont .powered-by>div:first-child").html().replace(/|/g, "<br>");
          //     console.log(newStr);
          //     $(this).addClass('modified');
          //   }
          // });
          // To change copyright text for mobile screen end

            $(".head-links .profile-icon img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/gSKYD9Y6cMQH3NaEBUPkGFXr");
            $(".head-links .wishlist-heart img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/3WkfvrNVHgazZq8TytpGjAk6");
            $(".head-links .cart-bag img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/6jYH3dCJQpMuSxCH7ZMJL5u2");
            // to change icon for banner carousel banner arrows
            $(".banner-carousel-container .owl-carousel .owl-nav .owl-prev img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/rWwMRD1sMEwAoiD2ADuduaGD");
            $(".banner-carousel-container .owl-carousel .owl-nav .owl-next img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/vKE5pEqkw93HVUaVixPsW8Bh");

          // head icons for mobile
          $(".mob-top-links .mob-search-link img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/x7M4SRDBSYVU6EJqoTZ8Jt9M");
          $(".mob-top-links .cart-bag img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/tX7ATTP9ShgxeKbzRhXJR9F5");
          // head icons for mobile end
          $(".sign-up-section .sign-up-container .sign-up-form input[type=email]:not(.modified)").addClass('modified').attr("placeholder", "Enter Your Email");
          $(".footer .footer-widget:last-child .widget-title:not(.modified)").addClass("modified").addClass('modified').html("FOLLOW US");
          $("header.header .head-links li .add-to-basket .dropdown-menu h4:not(.modified)").addClass('modified').html("SHOPPING BAG");
          $('.footer .footer-widget .social-links li a img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/FScPW8bhc66WrhcPJCyJMGCv');
          $('.footer .footer-widget .social-links li a img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/Pgxzdv1RSQtLYgkNYWgqTb15');
          $('.footer .footer-widget .social-links li a img[alt="twitter"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/4U5bXTuCmvtumMEKVLngJBQC');
          $('.footer .footer-widget .social-links li a img[alt="pinterest"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/6HMeoQZsV5z6EBWK2GKvprya');

          $('.footer .footer-widget .social-links:not(.modified)').addClass('modified').append('<li><a href="https://www.youtube.com/channel/UCwY4qBPhnnnzf6HKMJ2JMhg" rel="nofollow"><img class="modified" alt="youtube" width="32" src="https://static.techsembly.com/2Hhibj7wnbX7UsmSSBq9W4EW"></a></li>');
          // $('.footer .footer-widget .social-links:not(.modified)').append('<li><a href="https://www.youtube.com" rel="nofollow"><img class="modified" alt="youtube" width="32" src="https://static.techsembly.com/6gNxfDDfL8UN12pbanHeWr4p"></a></li>');
          // $('.footer .footer-widget .social-links:not(.modified)').addClass('modified').append('<li><a href="https://www.rosewoodhotels.com/en/the-carlyle-new-york/wechat" rel="nofollow"><img class="modified" alt="wechat" width="32" src="https://static.techsembly.com/TemksL89wenKCGAVmTrGTJar"></a></li>');

          $('header.header .menu-holder>li>div:not(.modified)').addClass('modified submenu');
          $(".header .menu-links-cont .search-item-form .btn-search img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/2qXjCnVdazr1KPhkeN1t3n3p");
          $(".sign-up-section .sign-up-container .sign-up-form .submit:not(.modified)").addClass('modified').html("<img src='https://static.techsembly.com/dT7gZrQPLcQMLTsbKgZUxMLL' alt='submit'>");
          $('.mobile-menu#sidebarCollapse:not(.modified)').addClass('modified').append('<img src="https://static.techsembly.com/q3NnfinrbuTTGvZdCjTUsijp" class="mobile-menu-icon" alt="mobile-menu">')
          $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
          $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');
          $('.product-detail-container .product-detail .wishlistImage img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/jH1yxVUj6LaVtApPQdRCUZSK");
          $('.custom-container.cart-container .card.vendor-items-holder .card-body .edit-item-new img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/YxPJAMPgqxXvH5fRahZcRoS9");
          $('.custom-container.checkout .shipping-form-container .card-header .edit-address img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/YxPJAMPgqxXvH5fRahZcRoS9");
          $('.delivery-methods-cont .contact-card .card-body .card-row .edit-contact img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/YxPJAMPgqxXvH5fRahZcRoS9");
          $('.delivery-methods-cont .contact-card .card-body .card-row .edit-address img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/YxPJAMPgqxXvH5fRahZcRoS9");
          // $('.custom-container.cart-container .btn.continue-btn-new').closest('.col-12:not(.modified)').addClass('pl-lg-0 modified');
          $('#continue-to-delivery-and-billing, .custom-container.checkout .shipping-form-container .btn.btn-submit').closest('div.form-group:not(.modified)').addClass('continue-btn-cont modified');
          $('meta[name="viewport"]:not(.modified)').addClass('modified').attr("content", "width=device-width, initial-scale=1, maximum-scale=1");
          $('header.header .menu-holder').each(function () {
            if (!$(this).closest('.container').hasClass('wrapped')) {
              $(this).closest('.container').addClass('wrapped');
              $(this).closest('.container').wrap('<div class="container-fluid main-menu-wrapper"></div>');
            }
          });
          $('.cookie-policy-container').each(function () {
          if (!$(this).closest('.footer-page-container').hasClass('cookie-policy-custom-container')) {
            $(this).closest('.footer-page-container').addClass('cookie-policy-custom-container');
          }
        });
          // $('.banner-carousel-section .slide-title:contains("london edit")').each(function () {
          //   if(!$(this).closest('.banner-carousel-section').hasClass('main-desktop-banner')){
          //     $(this).closest('.banner-carousel-section').addClass('modified main-desktop-banner');
          //   }
          // });
          // $('.banner-carousel-section:nth-child(2)').each(function () {
          //   if(!$(this).hasClass('main-mobile-banner')) {
          //     // $(this).removeClass('mobile-carousel-section');
          //     $(this).addClass('modified main-mobile-banner');
          //   }
          // });
          $($('.banner-carousel-section')[0]).each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              $(this).addClass('main-desktop-banner');
            }
          });

          $($('.banner-carousel-section')[1]).each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              $(this).addClass('main-mobile-banner');
            }
          });
          $('.main-mobile-banner').each(function () {
            if (!$(this).hasClass('wrapped')) {
              $(this).addClass('wrapped')
             $(this).wrap("<section class='mobile-carousel-wrapper'></section>");
           }
         });
          $($('.banner-carousel-section')[2]).each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              $(this).addClass('first-multi-banner-carousel');
            }
          });
          $($('.banner-carousel-section')[3]).each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              $(this).addClass('second-multi-banner-carousel');
            }
          });

          $('.home-banner .banner-heading>div:contains("THE ULTIMATE GIFT")').each(function () {
            if (!$(this).closest('.home-banner').hasClass('home-gift-section')) {
              $(this).closest('.home-banner').addClass('home-gift-section');
            }
          });

        $('.home-gift-section .banner-heading').each(function () {
          if (!$(this).hasClass('modified-btn-added')) {
            $(this).addClass('modified-btn-added');
            $(this).append('<a class="custom-btn btn-transparent" href="https://shop.rosewoodhotels.com/london/gift-card-030703">SHOP NOW</a>');
          }
        });

        $('.promo-section .promo-holder .promo-box .promo-content .promo-title:contains("A DAY AT THE MUSEUM")').each(function () {
          if (!$(this).closest('.promo-section').hasClass('promo-modified')) {
              $(this).closest('.promo-section').addClass('promo-modified');
            }
          });
            // Product detail page Wishlist heart icons change upon click
        // $('.product-detail-container .wishlistImage:has(img[alt="add-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
        //   $('.product-detail-container .wishlistImage img').attr('src', 'https://static.techsembly.com/jH1yxVUj6LaVtApPQdRCUZSK');
        // });

        // $('.product-detail-container .wishlistImage:has(img[alt="added-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
        //   $('.product-detail-container .wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://static.techsembly.com/iRMtYGEWNck56pBFHs1XfkWb');
        // });

        // Product detail page related products Wishlist heart icons change upon click
        // $('.related-products  a.wishlistImage img').each(function () {
        //   if ($(this).attr('src') != 'https://static.techsembly.com/jH1yxVUj6LaVtApPQdRCUZSK') {
        //     $(this).attr('src', 'https://static.techsembly.com/jH1yxVUj6LaVtApPQdRCUZSK');
        //   }
        //   if (($(this).attr('alt') == 'added-to-wishlist') && ($(this).attr('src') != 'https://static.techsembly.com/iRMtYGEWNck56pBFHs1XfkWb')) {
        //     $(this).attr('src', 'https://static.techsembly.com/iRMtYGEWNck56pBFHs1XfkWb');
        //   }
        // });

        // Category page Wishlist heart icons change upon click
        // $('.products-container .products-holder-main .products-holder .product-column .product-list .product-content .product-rating a.wishlistImage img').each(function () {
        //   if ($(this).attr('src') != 'https://static.techsembly.com/jH1yxVUj6LaVtApPQdRCUZSK') {
        //     $(this).attr('src', 'https://static.techsembly.com/jH1yxVUj6LaVtApPQdRCUZSK');
        //   }
        //   if (($(this).attr('alt') == 'added-to-wishlist') && ($(this).attr('src') != 'https://static.techsembly.com/iRMtYGEWNck56pBFHs1XfkWb')) {
        //     $(this).attr('src', 'https://static.techsembly.com/iRMtYGEWNck56pBFHs1XfkWb');
        //   }
        // });
        // wishlist page heart icon
        // $('.wishlist-container .wishlistImage:has(img[alt="added-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
        //   $('.wishlist-container .wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://static.techsembly.com/iRMtYGEWNck56pBFHs1XfkWb');
        // });
        $('a.wishlistImage img').each(function () {
              if (!$(this).hasClass('appeared')) {
                $(this).addClass('appeared');
                $(this).attr('src', 'https://static.techsembly.com/jH1yxVUj6LaVtApPQdRCUZSK');
                if (($(this).attr('alt') == 'added-to-wishlist') && ($(this).attr('src') != 'https://static.techsembly.com/iRMtYGEWNck56pBFHs1XfkWb')) {
                  $(this).attr('src', 'https://static.techsembly.com/iRMtYGEWNck56pBFHs1XfkWb');
                }
              }
            });
          $('.promo-section .promo-holder .promo-box .promo-content .promo-title:contains("ULTIMATE EXPERIENCE")').each(function () {
            if(!$(this).closest('.promo-section').hasClass('promo-bottom')) {
              $(this).closest('.promo-section').addClass('promo-bottom');
            }
          });

          $('.promo-section.promo-bottom .promo-box .promo-content').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              let bannerLink=$(this).closest("a").attr("href")
              $(this).append('<a class="custom-btn btn-transparent" href="'+bannerLink+'">DISCOVER</a>');
            }
          });

          $('.promo-section.promo-bottom .promo-box:nth-child(2) a.custom-btn').each(function () {
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              let bannerLink=$(this).closest("a").attr("href")
              $(this).html('SHOP NOW').attr("href",bannerLink);
            }
          });

          // multi banner after gift card
          $('.promo-section .promo-holder .promo-box .promo-content .promo-title:contains("SENSE, A ROSEWOOD SPA")').each(function(){
              if(!$(this).closest('.promo-section').hasClass('promo-bottom')){
                $(this).closest('.promo-section').addClass('promo-bottom');
              }
            });

            $('.promo-section.promo-bottom .promo-box .promo-content').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                let bannerLink=$(this).closest("a").attr("href")
                $(this).append('<a class="custom-btn btn-transparent" href="'+bannerLink+'">DISCOVER</a>');
              }
            });
            $('.promo-section.promo-bottom .promo-box:nth-child(2) a.custom-btn').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                let bannerLink=$(this).closest("a").attr("href")
                $(this).html('DISCOVER').attr("href",bannerLink);
              }
            });
          // end
          $('.promo-section.promo-modified .promo-box .promo-content').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).append('<a class="custom-btn btn-transparent" href="#">DISCOVER</a>');
            }
          });
          $('.promo-section.promo-modified .promo-box:nth-child(2) a.custom-btn').each(function () {
            if(!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              $(this).html('SHOP NOW').attr("href", "https://rosewood-london.techsembly.com/catalogsearch/result/Bemelmans");
            }
          });

          $('app-product-detail app-related-products[title="More items from"]').each(function () {
              if (!$(this).hasClass('modified')) {
                  $(this).addClass('modified');
                  $(this).find('h3').html('your recommendations').addClass("productHeading").removeClass("text-center");
              }
            });
          $('.custom-container.complete .vendor-items-holder .vendor-order-details .order-item .order-item-left').each(function () {
              if (!$(this).hasClass('col-md-5')) {
                  $(this).addClass('col-md-5');
                  $(this).removeClass('col-md-8');
              }
            });
          $('.custom-container.complete .vendor-items-holder .vendor-order-details .order-item .order-item-right').each(function () {
              if (!$(this).hasClass('col-md-7')) {
                  $(this).addClass('col-md-7');
                  $(this).removeClass('col-md-4');
              }
            });

          $('.custom-container.cart-container').each(function(){
            if (!$(this).closest('body').attr('data-pagetype','cart')){
              $(this).closest('body').attr('data-pagetype','cart');
            }
          });
          $('.custom-container.checkout').each(function(){
            if (!$(this).closest('body').attr('data-pagetype','checkout')){
              $(this).closest('body').attr('data-pagetype','checkout');
            }
          });
           $('.custom-container .refund-form-cont').each(function(){
            if (!$(this).closest('body').attr('data-pagetype','refund-form')){
              $(this).closest('body').attr('data-pagetype','refund-form');
            }
          });
          $('#continue-to-delivery-and-billing').each(function(){
            if (!$(this).closest('form-group')){
              $(this).addClass('mb-0 modified');
            }
          });

          $('.container.footer-page-container .page-heading:contains(Contact Us)').each(function(){
            if (!$(this).closest('body').attr('data-pagetype','contact-us')){
              $(this).closest('body').attr('data-pagetype','contact-us');
            }
          });

          $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('ADD TO BAG');
            }
          });
           $('.custom-container.cart-container .cart-login-cont .login-cart-panel .panel-heading:contains(Sign In To Check Out Faster)').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Sign in to check out faster');
            }
          });

          $('.product-detail-container .product-holder .share-link li:first-child span').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified shareLable');
              $(this).html('Share via');
            }
          });

          $('.product-detail-container .personalise#personalise>div.row').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified productDesc');
            }
          });

          $('.payment-form-container .card-details-cont').each(function(){
                if(!$(this).children('h2.section-heading').length){
                  $(this).prepend('<h2 class="section-heading">Card Details</h2>');
                  $(this).append('<p class="card-note">We accept Visa, Mastercard and American Express.</p>');
                }
          });
          $('.shipping-form-container .breadcrumb-heading:contains("Shipping")').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified').html("Shipping Details");
            }
          });
          $('.shipping-form-container .breadcrumb-heading:contains("Billing")').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified').html("Billing Details");
            }
          });
          $('.shipping-form-container .breadcrumb-heading:contains("Billing Details")').each(function () {
            if (!$(".custom-container.checkout .shipping-form-cont h1:contains('Billing Details')").hasClass('modified')) {
              $(".custom-container.checkout .shipping-form-cont h1:contains('Shipping Address')").addClass('modified').html("Billing Address");
            }
          });
          $('.shipping-form-container .breadcrumb-heading:contains("Select Delivery Method")').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified').html("Delivery Details");
            }
          });
          $('.delivery-methods-cont .vendor-cards-holder .card.vendor-card .card-header .vendor-title>span:contains("Delivered by")').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).removeClass('mr-1');
              $(this).addClass('modified').html("Fulfilled By");
            }
          });
           $('.delivery-methods-cont .vendor-cards-holder .card .order-shipping-container label:contains("RecipientÃ¢â‚¬â„¢s Info:")').each(function () {
            if (!$(this).closest('.delivery-dropdowns-cont').hasClass('modified')) {
              $(this).closest('.delivery-dropdowns-cont').addClass('modified digital-product-cont');
            }
          });
           $('.delivery-methods-cont .vendor-cards-holder .card .order-shipping-container label:contains("RecipientÃ¢â‚¬â„¢s Info:")').each(function () {
            if (!$(this).closest('.order-shipping-container').hasClass('modified')) {
              $(this).closest('.order-shipping-container').addClass('modified digital-shipping-options-cont');
            }
          });
           $('.digital-product-cont .form-group input[placeholder="Last Name"]').each(function () {
            if (!$(this).closest('.col-12').hasClass('modified')) {
              $(this).closest('.col-12').addClass('modified digital-lastname-cont');
            }
          });
           $('.digital-product-cont .form-group input[placeholder="First Name"]').each(function () {
            if (!$(this).closest('.col-12').hasClass('modified')) {
              $(this).closest('.col-12').addClass('modified digital-firstname-cont');
            }
          });
          $('.delivery-methods-cont').each(function () {
            if (!$(this).closest('.shipping-form-container').hasClass('continue-to-payment-cont')) {
              $(this).closest('.shipping-form-container').addClass("continue-to-payment-cont");
            }
          });
          $('.continue-to-payment-cont .btn.btn-submit').each(function (){
              if (!$(this).hasClass("continue-to-payment-modified")) {
                $(this).addClass("continue-to-payment-modified");
                $(this).html("CONTINUE TO PAYMENT");
              }
          });

          $('header .mob-top-links .add-to-basket .dropdown-menu h4').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified').html("Shopping Bag");
            }
          });
          $('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc:contains("Subtotal")').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified').html("Subtotal:");
            }
          });
          $('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc:contains("Shipping")').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified').html("Shipping:");
            }
          });
          $('.order-totals-container .sub-total-holder .sub-total-item .sub-total-desc:contains("Sales Tax")').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified').html("Sales Tax:");
            }
          });
          $('.custom-container .breadcrumb-heading:contains("Payment")').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified payment-heading');
            }
          });
          // $('.checkout .password-note:not(.modified)').addClass('modified').html('Must be at least 6 characters including one special character');

          $('.product-detail-container .product-holder .share-link li:nth-child(2) img:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/8jcy8rKfQUdYeZiLhZp7FkoX');
          $('.product-detail-container .product-holder .share-link li:nth-child(3) img:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/WGzjuhovd6gcivNoCMGJC4MV');
          $('.product-detail-container .product-holder .share-link li:nth-child(4) img:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/orb8d1xBK4p8y1tZXdty82Mb');
          // $('.product-detail-container .product-holder .share-link li').each(function(){
          //   if ($(this).children('.vendor-point-desc').attr(i'd') != 'tnc-desc') {
          //     $(this).children('.vendor-point-desc').attr('id', 'tnc-desc');
          //   }
          // });

          $(".custom-container.checkout .payment-form-container .btn.btn-primary:not(.modified, .apply-btn)").addClass('modified complete-purchase-btn').html('COMPLETE PURCHASE');
          $('.complete-purchase-btn').each(function(){
              if(!$(this).closest('.form-group').hasClass('modified')){
                $(this).closest('.form-group').addClass('modified complete-purchase-btn-cont');
              }
            });
          $('.checkout-container.complete .order-confirm-top img:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/fBczckgz1TVHUUt9n7q5ipAZ').attr('width','120');


          $('.checkout-container.complete .section-cont.customer-note').each(function(){
            if(!$(this).hasClass('total-delivery')){
              $(this).addClass('total-delivery');
              var newTxt = 'Your order is being processed. You will receive an email with further information.';
              $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function (_, html) {
              return html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">' + newTxt + '</div>')
            });
            }
          });
          $('.checkout-container.complete .section-cont.customer-note').each(function() {
              if(!$(this).hasClass('total-delivery')) {
                $(this).addClass('total-delivery');
                var firstPart = $(this).find('br')[0].previousSibling.nodeValue;

                var secondPart = $(this).find('.updated-del-string').html();

                $(this).html('<div class="salutation-cont d-none">' + firstPart + '</div><div class="updated-del-string pt-0">' + secondPart + '</div>');
              }
            });
          $('.products-container .products-holder-main .products-holder .product-column .product-list .product-content .product-title span:contains("by")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            var removedTxt = $(this).html();
            removedTxt = removedTxt.replace("by", "");
            $(this).html(removedTxt);
          }
        });
          $('.product-detail-container .product-holder .product-detail .product-info .made-by a.vendor-link').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            var removedTxt = $(this).html();
            removedTxt = removedTxt.replace("by", "");
            $(this).html(removedTxt);
          }
        });
          $('.related-products .related-products-holder .product-content .product-title a.vendor-link span').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            var removedTxt = $(this).html();
            removedTxt = removedTxt.replace("by", "");
            $(this).html(removedTxt);
          }
        });
          $('.custom-container.checkout .shipping-form-container .btn.btn-submit:contains("Continue Checkout")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('CONTINUE TO DELIVERY');
          }
        });
          $('.custom-container.checkout .shipping-form-container .address-heading:contains("Or Add New Shipping Details")').each(function () {
          if (!$(this).hasClass('submit-btn-modified')) {
            $(this).addClass('submit-btn-modified or-addNew-wrapper');
            $("#continue-to-delivery-and-billing").html('add & CONTINUE TO DELIVERY');
          }
        });
           $('.payment-form-container .checkbox-cont .checkbox-note:contains("Same as delivery address")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).closest('form-group').addClass('modified same-as-shipping-cont');
            $(this).html('Same as shipping address');
          }
        });
           $('.checkout-container.complete .shipping-data .shipping-row div:contains("Address")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Delivery Address:');
            }
          });
           $('.checkout-container.complete .shipping-data .shipping-row div:contains("Shipping:")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Delivery:');
            }
          });

           $('.checkout-container.complete .card-body .total-cont .col-3.pr-3:contains("Total:")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Total');
            }
          });

           $('.checkout-container.complete .card-body .msg-cont .msg-inner').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('You will receive a shipping confirmation when your item(s) are on the way.');
            }
          });

           $('.order-confirm-wrapper .card-footer .card-footer-left>span:contains("Questions? Contact our")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Need help? Contact our');
            }
          });
           $('.order-confirm-wrapper .card-footer .card-footer-left a:contains("Customer Support")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).attr('href','mailto:rosewood@techsembly.com');
            }
          });
           $('.order-confirm-wrapper .card-footer .card-footer-right').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('RHR Gift Card Services, L.L.C., a Virginia limited liability company');
            }
          });
           $('.footer .footer-widget .widget-content ul li a').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('target', '_blank');
              }
            });

            $('.footer .mobile-footer .footer-widget ul li a').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('target', '_blank');
              }
            });
            $('.footer a:contains("+44 800 048 8077")').each(function () {
              var string_phone = $(this).html();
              var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
              var phone_no = "tel:" + result_phone;
              if (!$(this).hasClass('tel-link')) {
                $(this).addClass('tel-link');
                $(this).attr('href', phone_no);
              }
            });
            $('.footer a:contains("Gift Card Support")').each(function () {
              var string_email = 'rosewood@techsembly.com';
              var email_link = "mailto:" + string_email;
              if (!$(this).hasClass('email-link')) {
                $(this).addClass('email-link');
                $(this).attr('href', email_link);
              }
            });
            $('.footer .powered-by').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var new_txt = 'ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©';
                var new_txt2 = '2022 Rosewood Hotel Group';
                var new_txt3 = 'Gongan Beian: 31010102004896';
                $(this).find("div:contains('ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©')").html(function(_, html) {
                   return  html.replace(/(ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©)/g, '<span class="updated-string">'+new_txt+'</span>');
                });

                // $(this).find("div:contains('2022 Rosewood Hotel Group')").html(function(_, html) {
                //    return  html.replace(/(2022 Rosewood Hotel Group)/g, '<span class="updated-copyright">'+new_txt2+'<br class="d-lg-none"><span class="pow-sep mx-1 d-none d-lg-inline-block">|</span></span>');
                // });
              }
            });
            $(".notificationbar .cross-cont").click(function(){
             if(!$(".notificationbar").hasClass('d-none')){
              $(".notificationbar").addClass('d-none');
              $("nav#sidebar").removeClass("announce-open");
            }
          });
            $(".sidebar-cross-cont img").click(function(){
             if(!$(".mob-overlay").hasClass('active')){
              $("body").removeClass('scroll-hidden');
              $("html").removeClass('scroll-hidden-html');
              $(".mob-overlay").removeClass("active");
            }
          });
            $(".sidebar-cross-cont img").click(function(){
             if($(".mob-overlay").hasClass('active')){
              $("body").removeClass('scroll-hidden');
              $("html").removeClass('scroll-hidden-html');
              $("nav#sidebar").removeClass('active');
              $(".mob-overlay").removeClass("active");
            }
          });
          $('.product-detail-container .product-holder .product-detail div:contains(" BEST SELLER ")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified best-seller-tag');
            }
          });
          $('.container.home-intro-container').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).wrap('<a class="gift-sec-link" href="#"></a>');
            }
          });
          $('.custom-container.login-signup').each(function () {
          if (!$(this).closest('body').attr('data-pagetype', 'login-signup')) {
            $(this).closest('body').attr('data-pagetype', 'login-signup');
          }
        });
          if (navigator.platform.indexOf('Mac') > -1 || navigator.platform.indexOf('iPhone') || navigator.platform.indexOf('iPad') ) {
          $('#delivery-options-popup .modal-container .radio-container .checkmark').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified mac-radio-holder');
            }
          });
        }
        $('.user-wrapper .user-container .user-heading:contains("My TS Card Balance")').each(function () {
          if (!$(this).closest(".user-container").hasClass('d-none')) {
            $(this).closest(".user-container").addClass('d-none');
          }
        });
        $('.user-wrapper .user-container .user-heading:contains("My Balance")').each(function () {
          if (!$(this).closest(".user-container").hasClass('d-none')) {
            $(this).closest(".user-container").addClass('d-none');
          }
        });
        $('.checkout .payment-form-container .card-details-cont .address-card .row-head:contains("Address")').each(function () {
          if (!$(this).closest('.card-details-cont').hasClass('modified')) {
            $(this).closest('.card-details-cont').addClass('modified address-card-cont')
          }
        });
        $('.checkout .address-card-cont .section-heading:contains("Details")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified')
            $(this).html('Billing Details')
          }
        });
          // code for sticky header
          window.onscroll = function () {
            var header = document.getElementsByClassName("main-menu-wrapper");
            var sticky = header[0].offsetTop;
            if (window.pageYOffset > sticky) {
              header[0].classList.add("sticky");
            } else {
              header[0].classList.remove("sticky");
            }
          };
          // code for sticky header end

          var segment_str = window.location.pathname; // return segment1/segment2/segment3/segment4
          var segment_array = segment_str.split('/');
          var last_segment = segment_array.pop();

          // customizations for product e-gift-card-031053
          if (last_segment == 'e-gift-card-031053') {
            $('.product-detail-container .product-holder').each(function(){
              if(!$(this).hasClass('gift-card-modified')){
                $(this).addClass('gift-card-modified');
              }
            });
            $('.product-detail-container .personalise-form .delivery-form .radio-container input[formcontrolname="delivery_mode"]').each(function(){
              if(!$(this).closest('.radio-container').hasClass('d-none')){
                $(this).closest('.radio-container').addClass('d-none');
              }
            });

            $(".product-detail-container .personalise-form .delivery-form p:contains('Please select from one of the options below.')").each(function(){
              if(!$(this).hasClass('d-none')){
                $(this).addClass('d-none')
              }
            });
            $(".product-detail-container .personalise-form .delivery-form h1:contains('Delivery Options')").each(function(){
              if(!$(this).hasClass('d-none')){
                $(this).addClass('d-none')
              }
            });
            $('.product-detail-container .personalise-form .delivery-form .delivery-fields[formcontrolname="receipient_first_name"]').each(function(){
              // var infoText;
              var recipientInfo = "<div class='form-group recipient-info-top'><label class='control-label'>Recipient Details</label><div class='recipient-note'>Please share the name of the gift card recipient</div></div>";
              if (!$('.product-detail-container .personalise-form .delivery-form').find('.recipient-info-top').length) {
                $(recipientInfo).insertBefore($(this));
              }
            });
          }

          if (last_segment == 'e-gift-card-031053') {
            $('.product-detail-container .product-holder .personalise-form .delivery-form .info-text').each(function () {
              var msg_txt;
              msg_txt = "<b>Maximum GBPÂ£2,000 per E-Gift card and GBPÂ£10,000 per transaction.</b> If you would like to add a unique message for each card, please add each gift card separately to the shopping bag.";
              if (!$(this).hasClass('modified-info-text')) {
                $(this).addClass('modified-info-text');
                $(this).html(msg_txt);
              }
            });
          }

          // customizations for product gift-card-030703
          if (last_segment == 'gift-card-030703') {
            $('.product-detail-container .product-holder').each(function(){
              if(!$(this).hasClass('gift-card-modified')){
                $(this).addClass('gift-card-modified');
              }
            });
            $('.product-detail-container .personalise-form .delivery-form .radio-container input[formcontrolname="delivery_mode"]').each(function(){
              if(!$(this).closest('.radio-container').hasClass('d-none')){
                $(this).closest('.radio-container').addClass('d-none');
              }
            });

            $(".product-detail-container .personalise-form .delivery-form p:contains('Please select from one of the options below.')").each(function(){
              if(!$(this).hasClass('d-none')){
                $(this).addClass('d-none')
              }
            });
            $(".product-detail-container .personalise-form .delivery-form h1:contains('Delivery Options')").each(function(){
              if(!$(this).hasClass('d-none')){
                $(this).addClass('d-none')
              }
            });
            $('.product-detail-container .personalise-form .delivery-form .delivery-fields[formcontrolname="receipient_first_name"]').each(function(){
              // var infoText;
              var recipientInfo = "<div class='form-group recipient-info-top'><label class='control-label'>Recipient Details</label><div class='recipient-note'>Please share the name of the gift card recipient</div></div>";
              if (!$('.product-detail-container .personalise-form .delivery-form').find('.recipient-info-top').length) {
                $(recipientInfo).insertBefore($(this));
              }
            });
          }

          if (last_segment == 'gift-card-030703') {
            $('.product-detail-container .product-holder .personalise-form .delivery-form .info-text').each(function () {
              var msg_txt;
              msg_txt = "<b>Maximum GBPÂ£2,000 per card and GBPÂ£10,000 per transaction.</b> Your message will apply to all Gift Cards to one single shipping address. If you would like to add a unique message for each card, please add each gift card separately to the shopping bag.";
              if (!$(this).hasClass('modified-info-text')) {
                $(this).addClass('modified-info-text');
                $(this).html(msg_txt);
              }
            });
          }


          hrefurl = $(location).attr("href");
          last_part = hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
          currentUrlSecondPart = window.location.pathname.split('/')[1];
          if (currentUrlSecondPart == '') {
            if (!$('body').attr('data-pagetype', 'home')) {
              $('body').attr('data-pagetype', 'home');
            }
          }
          if (currentUrlSecondPart == 'catalogsearch') {
          if (!$('body').attr('data-pagetype', 'search_results')) {
            $('body').attr('data-pagetype', 'search_results');
          }
        }
        if (currentUrlSecondPart == 'vendors') {
          if (!$('body').attr('data-pagetype', 'vendors')) {
            $('body').attr('data-pagetype', 'vendors');
          }
        }
        if (last_part == 'wishlist') {
          if (!$('body').attr('data-pagetype', 'wishlist')) {
            $('body').attr('data-pagetype', 'wishlist');
          }
        }
        $(".mat-form-field-label").each(function () {
          if ($(this).attr('aria-owns')) {
            $(this).removeAttr('aria-owns');
          }
        });
      });
  });
  });
})();
