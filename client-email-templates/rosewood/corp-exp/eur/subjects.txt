Order conf customer:
  Subject: Your Rosewood Hotels & Resorts Gift Receipt

Order conf vendor:
  Subject: New Gift Order {{ order.number }} from Rosewood Hotels & Resorts

Shipment:
  Subject: Your Rosewood Hotels & Resorts Gift has shipped

TS Gift Card Mon:
  Subject: {{sender_name}} Has Sent You A Rosewood Hotels & Resorts E-Gift Card

TS Gift Card Exp:
  Subject: {{sender_name}} Has Sent You A Rosewood Hotels & Resorts Experience

Givex Card:
  Subject: {{gift_sender_name}} Has Sent You A Rosewood Hotels & Resorts Gift E-Gift Card     
