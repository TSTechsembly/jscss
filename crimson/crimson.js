function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }

        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
function addHeroBannerBtn(){
  var bannerBtn = '<button class="btn btn-transparent hero-btn"><span class="text-uppercase">explore experiences</span></button>';
  var elem = $(".home-banner .banner-heading")[0];
  elem.insertAdjacentHTML('beforeend', bannerBtn);
}
function addExploreLink(){
  var expLink = '<a class="explore-link text-uppercase" href="">Explore</a>';
  var elem = $(".gift-section .gifts-content")[0];
  elem.insertAdjacentHTML('beforeend', expLink);
}
function addLoginCartNote(){
  var loginCartNote = "<div class='login-note'>Please note this login is for Crimson gifting platform only.<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}
function addGuestCheckoutNote(){
  var checkoutNote = "<div class='checkout-note text-center w-100'>If you do not have an account please use guest checkout. You will be given the option to save account information later in the checkout process<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}
function addcopyrightText(){
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-6 ts-copyright text-uppercase"><span><a href="https://techsembly.com/" target="_blank">Techsembly&copy;</a></span><span class="ml-1">'+copyrightYear+'</span></div><div class="col-6 tc-rights-reserved text-right pr-0"></div></div></div></div>'
  var elem = $(".checkout-footer")[0]
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}
function addSupportEmail() {
  var supportEmail = '<span class="supp-email-cont"><span class="mr-1">AT</span><a class="text-lowercase" href="mailto:boracayvoucher@crimsonhotel.com">boracayvoucher@crimsonhotel.com</a></span>';
  var elemFooterLeftSocial = $('.custom-container.checkout-container.complete .card.order-confirm-wrapper .card-footer-left')[0];
  elemFooterLeftSocial.insertAdjacentHTML('beforeend', supportEmail);
}
function addSuccessCopyRight() {
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var successCopyRight = '<span class="text-uppercase">© '+copyrightYear+' Crimson</span>';
  var elemFooterRightSocial = $('.custom-container.checkout-container.complete .card.order-confirm-wrapper .card-footer-right')[0];
  elemFooterRightSocial.insertAdjacentHTML('afterbegin', successCopyRight);
}
(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
          font-family: 'brandon_grotesqueregular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_reg-webfont.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_reg-webfont.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_reg-webfont.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_reg-webfont.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_reg-webfont.woff2') format('woff2');\
          font-weight: normal;\
      }\
      @font-face {\
          font-family: 'brandon_grotesquemedium';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_med-webfont.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_med-webfont.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_med-webfont.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_med-webfont.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_med-webfont.woff2') format('woff2');\
          font-weight: 450;\
      }\
      @font-face {\
          font-family: 'brandon_grotesquebold';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_bld-webfont.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_bld-webfont.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_bld-webfont.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_bld-webfont.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/b0dade92f0d4c4434f30a9d5ef86f610a2084a58/crimson/fonts/brandon-grotesque/brandon_bld-webfont.woff2') format('woff2');\
          \
      }\
      @font-face {\
          font-family: 'Libre_Baskerville';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/2b6a1e64865a3838d2c137f1f74ceffb07940dfb/crimson/fonts/Baskerville/Baskerville.eot');\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/2b6a1e64865a3838d2c137f1f74ceffb07940dfb/crimson/fonts/Baskerville/Baskerville.eot?#iefix') format('embedded-opentype'), \
               url('https://bitbucket.org/TSTechsembly/jscss/raw/2b6a1e64865a3838d2c137f1f74ceffb07940dfb/crimson/fonts/Baskerville/Baskerville.ttf') format('truetype'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/2b6a1e64865a3838d2c137f1f74ceffb07940dfb/crimson/fonts/Baskerville/Baskerville.woff') format('woff'),\
               url('https://bitbucket.org/TSTechsembly/jscss/raw/2b6a1e64865a3838d2c137f1f74ceffb07940dfb/crimson/fonts/Baskerville/Baskerville.woff2') format('woff2');\
          \
      }\
      @font-face {\
          font-family: 'Calibre-Regular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/2b6a1e64865a3838d2c137f1f74ceffb07940dfb/crimson/fonts/calibre/Calibre-Regular.woff') format('woff');\
          font-weight: 400;\
          font-style: normal;\
      }\
    "));

    document.head.appendChild(newStyle);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">');
          //$('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Libre+Baskerville:ital,wght@0,400;0,700;1,400&display=swap" rel="stylesheet">');

          waitForElm('.hero-banner').then((elm) => {
            addHeroBannerBtn();
          }).catch((error) => {});

          waitForElm('.gift-section').then((elm) => {
            addExploreLink()();
          }).catch((error) => {});

          waitForElm('.cart-right-cont').then((elm) => {
            addLoginCartNote();
            addGuestCheckoutNote();
          }).catch((error) => {});

          waitForElm('app-footer-checkout').then((elm) => {
            addcopyrightText()
          }).catch((error) => {});

          waitForElm('.custom-container.checkout-container.complete').then((elm) => {
            addSupportEmail();
            addSuccessCopyRight();
          }).catch((error) => {});

          setTimeout(function () {
            $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
          }, 2000);
          setInterval(function() {
            $('header.header .search .search-item-form button#search-bar-button img[alt="search image"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/N2iNbj6CJJ8r4rnJR37JRLce');

            $('.mobile-search-box .btn-search img[alt="search image"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/N2iNbj6CJJ8r4rnJR37JRLce');

            $('header.header .menu-holder').each(function(){
              if(!$(this).closest('.container').hasClass('wrapped')){
                $(this).closest('.container').addClass('wrapped');
                $(this).closest('.container').wrap('<div class="container-fluid main-menu-wrapper"></div>');
              }
            });

            $('header.header .menu-holder>li>div').each(function(){
              if(!$(this).children().length){
                $(this).addClass('empty');
              }
            });

            hrefurl=$(location).attr("href");
            last_part=hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
            currentUrlSecondPart = window.location.pathname.split('/')[1];
            currentUrlThirdPart = window.location.pathname.split('/')[2];

            if (currentUrlThirdPart == 'catalogsearch'){
               if (!$('body').attr('data-pagetype','search_results')){
                   $('body').attr('data-pagetype','search_results');
               }
            }
            if (currentUrlSecondPart == 'vendors'){
               if (!$('body').attr('data-pagetype','vendors')){
                   $('body').attr('data-pagetype','vendors');
               }
            }
            if (last_part == 'cart'){
               if (!$('body').attr('data-pagetype','cart')){
                   $('body').attr('data-pagetype','cart');
               }
            }
            if (last_part == 'wishlist'){
               if (!$('body').attr('data-pagetype','wishlist')){
                   $('body').attr('data-pagetype','wishlist');
               }
               if(!$('header.header .head-links li a img[alt="wishlist"]').attr('src','https://static.techsembly.com/nY1GggDGQ2wA3s25AmWpe9ow')){
                  $('header.header .head-links li a img[alt="wishlist"]').attr('src','https://static.techsembly.com/nY1GggDGQ2wA3s25AmWpe9ow');
               }
            }

            if ($(".wishlist-container .products-container .products-holder i[aria-hidden='true']").length < 2){
              $('.wishlist-container .products-container .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
            }

            $('.wishlistImage:has(img[alt="added-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
                $('.wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://static.techsembly.com/nY1GggDGQ2wA3s25AmWpe9ow');
            });

            $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
            $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');
            $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Buy Now');
              }
            });

            $('app-product-detail app-related-products[title="More items from"]').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).find('h3').html('More Like This');
              }
            });

            $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/8FXSC4PX9CFNd7bJBoToBsb4');
            $('.custom-container.cart-container .vendor-items-cont a img[alt="gift"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/AVaWU9rhRJtSWdKJAboo9BE7');
            $('.delivery-methods-cont .card .card-body .text-right a img[alt="edit"]:not(.edit-img)').addClass('edit-img').attr('src', 'https://static.techsembly.com/8FXSC4PX9CFNd7bJBoToBsb4');

            $('.custom-container.cart-container .cart-item.new .item-details .item-name').each(function(){
              if(!$(this).closest('.col-lg-7').hasClass('details-col')){
                  $(this).closest('.col-lg-7').addClass('details-col');
              }
            });

            $('.cart-item.new .item-details .item-qty-cont').each(function(){
              if(!$(this).closest('.col-lg-2').hasClass('qty-col')){
                  $(this).closest('.col-lg-2').addClass('qty-col');
              }
            });

            $('.cart-item.new .item-details .unit-total-price').each(function(){
              if(!$(this).closest('.col-lg-3').hasClass('sub-total-col')){
                  $(this).closest('.col-lg-3').addClass('sub-total-col');
              }
            });

            $('.custom-container.checkout').each(function(){
              if (!$(this).closest('body').attr('data-pagetype','checkout')){
                $(this).closest('body').attr('data-pagetype','checkout');
              }
            });

            $('.custom-container.login-signup').each(function(){
              if (!$(this).closest('body').attr('data-pagetype','login-signup')){
                $(this).closest('body').attr('data-pagetype','login-signup');
              }
            });

            $('.custom-container.login-signup .breadcrumb-heading').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('My gift cards');
              }
            });

            $('.custom-container .login-signup-tabs-cont form.login-form').each(function(){
              if (!$(this).closest('app-login').find('.panel-heading').hasClass('modified')){
                $(this).closest('app-login').find('.panel-heading').addClass('modified');
                $(this).closest('app-login').find('.panel-heading').html('Existing Gift Card Account');
              }
            });

            $('.custom-container .login-signup-tabs-cont form.signup-form').each(function(){
              if (!$(this).closest('app-signup').find('.panel-heading').hasClass('modified')){
                $(this).closest('app-signup').find('.panel-heading').addClass('modified');
                $(this).closest('app-signup').find('.panel-heading').html('New Gift Card Account');
              }
            });

            $('.signup-form label:contains("The password must contain")').each(function(){
              if(!$(this).closest('.form-group').hasClass('d-none')){
                $(this).closest('.form-group').addClass('d-none');
              }
            });

            $('.auth-user .checkout .shipping-form h1.breadcrumb-heading:contains("Shipping Details")').each(function() {
              if(!$(this).closest('.shipping-form').hasClass('shipp-details')){
                $(this).closest('.shipping-form').addClass('shipp-details');
                $(this).html('Choose Shipping Details');
              }
            });

            $('.custom-container.checkout a img[alt="edit"]:not(.edit-img)').addClass('edit-img').attr('src', 'https://static.techsembly.com/8FXSC4PX9CFNd7bJBoToBsb4');

            $('.custom-container.checkout .shipping-form-container .label-email label.info-text').each(function(){
              if(!$(this).closest('.delivery-dropdowns-cont').hasClass('recipient-cont')){
                $(this).closest('.delivery-dropdowns-cont').addClass('recipient-cont');
              }
            });

            $('.checkout .checkout-container.complete').each(function(){
              if (!$(this).closest('body').hasClass('success-page')){
                $(this).closest('body').addClass('success-page');
              }
            });

            $('.container.user-container').each(function(){
              if (!$(this).closest('body').attr('data-pagetype','user-page')){
                $(this).closest('body').attr('data-pagetype','user-page');
              }
            });

            $('.custom-container.checkout .page-heading:contains("Success")').each(function(){
              if(!$(this).hasClass('d-none')){
                $(this).addClass('d-none');
              }
            });

            $('.checkout-container.complete .section-cont.customer-note').each(function(){
              if(!$(this).hasClass('total-delivery')){
                $(this).addClass('total-delivery');
                var newTxt = 'Your order is being confirmed. You will receive email with further information.';
                $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function(_, html) {
                   return  html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">'+newTxt+'</div>')
                });
              }
            });

            $('.checkout .checkout-container.complete .card.order-confirm-wrapper').each(function(){
              if(!$(this).parent('.col-12').hasClass('order-confirm-outer')){
                $(this).parent('.col-12').addClass('order-confirm-outer');
              }
            });

            $('.checkout-container.complete .card-footer').each(function(){
              let home_link = $('header.header .logo a').attr('href');
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).append('<div class="col-12 text-center home-btn-cont"><a class="btn btn-primary home-btn text-center text-uppercase" href="'+home_link+'">back to home</a></div>');
              }
            });

            $('app-user-account .user-container nav[aria-label="breadcrumb"] .user-heading:not(.modified)').addClass('modified').html('My Gift Card Account');

            $('body.auth-user .user-container .balance-form').closest('.user-container:not(.d-none)').addClass('d-none');

            $('.user-container .user-heading:contains("Check TS Gift Card Balance")').each(function(){
              if(!$(this).hasClass('gift-card-heading')){
                $(this).addClass('gift-card-heading');
                $(this).html('Check Gift Card Balance');
              }
            });

            $('.user-container .user-heading:contains("My TS Card Balance")').each(function(){
              if(!$(this).hasClass('gift-card-heading')){
                $(this).addClass('gift-card-heading');
                $(this).html('My Gift Card Balance');
              }
            });

            $('.user-container .user-heading:contains("My Orders")').each(function(){
              if(!$(this).hasClass('orders-heading')){
                $(this).addClass('orders-heading');
                $(this).html('My Gift Cards');
              }
            });

            $('.user-wrapper .container.user-container .btn.add-address-btn').each(function(){
              if(!$(this).hasClass('wrapped')){
                $(this).addClass('wrapped');
                $(this).wrap('<div class="btn-add-wrapper"></div>');
              }
            });

            $('.user-wrapper .user-container .order-items-details .order-item .contact-btn:contains("GLO Support"):not(.d-none)').addClass('d-none');

            $('.user-wrapper .user-container .order-items-details .order-item button:contains("Refund")').each(function(){
              if(!$(this).closest('div').hasClass('refund-col')){
                $(this).closest('div').addClass('refund-col');
                $(this).closest('div').prev('div').addClass('sec-last-col');
                $(this).prev('input').addClass('form-control');
                $(this).addClass('btn btn-primary btn-refund');
              }
            });

            $('.ts-balance-form label[for="gift-card-no"]:not(.modified)').addClass('modified').html('Gift card number');
            $('.ts-balance-form input[name="gift-card-no"]:not(.modified)').addClass('modified').attr('placeholder','Enter gift card number');

            $('.footer a:contains("+63")').each(function(){
              var string_phone= $(this).html();
              var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
              var phone_no = "tel:" + result_phone;
              if(!$(this).hasClass('tel-link')){
                $(this).addClass('tel-link');
                $(this).attr('href', phone_no);
              }
            });
            $('.footer a:contains("@")').each(function(){
              var string_email= $(this).html();
              var result_email = string_email.substring(string_email.indexOf(':') + 1);
              var email_link = "mailto:" + result_email;
              if(!$(this).hasClass('email-link')){
                $(this).addClass('email-link');
                $(this).attr('href', email_link);
              }
            });
            $('.footer a:contains("Privacy")').each(function(){
              if(!$(this).hasClass('privacy-link')){
                $(this).addClass('privacy-link');
                $(this).attr('target', '_blank');
              }
            });
            $('.footer a:contains("Terms")').each(function(){
              if(!$(this).hasClass('tnc-link')){
                $(this).addClass('tnc-link');
                $(this).attr('target', '_blank');
              }
            });

            if(!$('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').hasClass('social-widget')){
                $('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').addClass('social-widget');
                $('.social-widget .widget-title').html('Follow Us On:');
            }
            $('.social-links img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/JvWLywSqKZxQ1AmnPq9H5QRf');
            $('.social-links img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/v5TMLADUxkoXf3SxGALrSyBN');
            $('.social-links img[alt="twitter"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/aDbkAp65t5pBjPvdZaPyvR5w');
            $('.social-links img[alt="twitter"]').closest('li:not(.tw-link-cont)').addClass('tw-link-cont');
            $('.social-links img[alt="instagram"]').closest('li:not(.insta-link-cont)').addClass('insta-link-cont');
            $('.insta-link-cont:not(.moved)').addClass('moved').insertAfter($(".tw-link-cont"));
          });
        });
    });
})();
