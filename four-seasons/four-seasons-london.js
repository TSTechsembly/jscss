(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
        @font-face {\
            font-family: Helvetica;\
            src: url('https://bitbucket.org/TSTechsembly/jscss/raw/f4ee6e544e7415aba80750339d41581b9a583c76/four-seasons/fonts/helvetica/Helvetica.ttf');\
        }\
        @font-face {\
            font-family: Helvetica-bold;\
            src: url('https://bitbucket.org/TSTechsembly/jscss/raw/f4ee6e544e7415aba80750339d41581b9a583c76/four-seasons/fonts/helvetica/Helvetica-Bold.ttf');\
            font-weight: 300;\
        }\
        @font-face {\
            font-family: Garamond;\
            src: url('https://bitbucket.org/TSTechsembly/jscss/raw/f4ee6e544e7415aba80750339d41581b9a583c76/four-seasons/fonts/garamond-std/Garamond%20MT%20Std.ttf');\
        }\
        @font-face {\
            font-family: Garamond-bold;\
            src: url('https://bitbucket.org/TSTechsembly/jscss/raw/f4ee6e544e7415aba80750339d41581b9a583c76/four-seasons/fonts/garamond-std/Garamond%20MT%20Std%20Bold.ttf');\
        }\
        @font-face {\
            font-family: Garamond-italic;\
            src: url('https://bitbucket.org/TSTechsembly/jscss/raw/f4ee6e544e7415aba80750339d41581b9a583c76/four-seasons/fonts/garamond-std/Garamond%20MT%20Std%20Italic.ttf');\
        }\
        @font-face {\
            font-family: Garamond-bold-italic;\
            src: url('https://bitbucket.org/TSTechsembly/jscss/raw/f4ee6e544e7415aba80750339d41581b9a583c76/four-seasons/fonts/garamond-std/Garamond%20MT%20Std%20Bold%20Italic.ttf');\
        }\
    "));

    document.head.appendChild(newStyle);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
          $('.nav-search-field').attr("placeholder", "");
          $('.sign-up-section .sign-up-container .sign-up-form input[type="email"]').attr("placeholder","");
          $('.powered-by > div:nth-child(1)').html('© Four Seasons Hotels Limited 2020. All Rights Reserved');
          setTimeout(function () {
            $('header .spacing .toggle-menu-cont').append('<ul class="hotels-attr d-none d-lg-flex flex-row">'+
             '<li class="hotel-name">The Four Seasons Hotels  |</li>'+
             '<li class="hotel-branch pl-3">'+
                '<div class="dropdown branch_dropdown">'+
                  '<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownBranchButton">London at Park Lane </a>'+
                  '<div aria-labelledby="dropdownBranchButton" class="dropdown-menu">'+
                    '<ul class="hotels-links pl-0">'+
                      '<li>'+
                        '<a href="https://shenzhen.techsembly.com/" class="dropdown-item">Shenzhen</a>'+
                      '</li>'+
                      '<li>'+
                        '<a href="https://miami-beach.techsembly.com/" class="dropdown-item">The Surf Club Miami</a>'+
                      '</li>'+
                    '</ul>'+
                  '</div>'+
                '</div>'+
              '</li>'+
            '</ul>');
            $('.footer .footer-widget .widget-content .social-links li a img[alt="fb"]').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/e565c40a9ccf49202ca5c3a6119187901410c58d/four-seasons/images/fb.svg');
            $('.footer .footer-widget .widget-content .social-links li img[alt="fb"]').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/e565c40a9ccf49202ca5c3a6119187901410c58d/four-seasons/images/fb.svg');
            $('.footer .footer-widget .widget-content .social-links li a img[alt="instagram"]').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/e565c40a9ccf49202ca5c3a6119187901410c58d/four-seasons/images/insta.svg');
            $('.footer .footer-widget .widget-content .social-links li img[alt="instagram"]').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/e565c40a9ccf49202ca5c3a6119187901410c58d/four-seasons/images/insta.svg');
            $('.footer .copyright-cont .pay-methods-avail').append('<div class="meth-link new"><img alt="amex" src="https://bitbucket.org/TSTechsembly/jscss/raw/9fffae4338ec7f0b7c6f517eff078126ea268749/four-seasons/images/amex.svg" width="36"></div>'+
            '<div class="meth-link new"><img alt="apple-pay" src="https://bitbucket.org/TSTechsembly/jscss/raw/9fffae4338ec7f0b7c6f517eff078126ea268749/four-seasons/images/apple-pay.svg" width="36"></div>'+
            '<div class="meth-link new"><img alt="master-card" src="https://bitbucket.org/TSTechsembly/jscss/raw/9fffae4338ec7f0b7c6f517eff078126ea268749/four-seasons/images/mc-blue.svg" width="36"></div>'+
            '<div class="meth-link new"><img alt="master-card" src="https://bitbucket.org/TSTechsembly/jscss/raw/9fffae4338ec7f0b7c6f517eff078126ea268749/four-seasons/images/mc-orange.svg" width="36"></div>'+
            '<div class="meth-link new"><img alt="o-pay" src="https://bitbucket.org/TSTechsembly/jscss/raw/9fffae4338ec7f0b7c6f517eff078126ea268749/four-seasons/images/o-pay.svg" width="36"></div>'+
            '<div class="meth-link new"><img alt="visa" src="https://bitbucket.org/TSTechsembly/jscss/raw/9fffae4338ec7f0b7c6f517eff078126ea268749/four-seasons/images/visa.svg" width="36"></div>');
            $('<div class="d-flex align-items-center pt-2"><a class="footer-cp-link" href="https://techsembly.com/" target="_blank"><img class="copy-right-img" src="https://bitbucket.org/TSTechsembly/jscss/raw/cb4eae06bdbd21fa90732441b24b3809bd77f769/four-seasons/images/logo-white.svg", width="122"/></a></div>').insertAfter($(".powered-link-cont"));
          }, 2000);
          setInterval(function () {
            hrefurl=$(location).attr("href");
            last_part=hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
            currentUrlSecondPart = window.location.pathname.split('/')[1];

            if (currentUrlSecondPart == ''){
               if (!$('body').attr('data-pagetype','home')){
                   $('body').attr('data-pagetype','home');
               }
            }
            if(!$('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').hasClass('social-widget')){
                $('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').addClass('social-widget');
            }
            $('.footer .copyright-cont .pay-methods-avail ').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).find('.meth-link:not(.new)').addClass('d-none');
                    $(this).parent('div').addClass('copyright-inner-cont');
                }
            });
          });
        });
    });
})();
