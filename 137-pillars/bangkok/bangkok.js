function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }
    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });
    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

function addLoginCartNote() {
  var loginCartNote = "<div class='login-note'>Please note this login is for 137 Pillars Hotels & Resorts gifting platform only.<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}

function addGuestCheckoutNote() {
  var checkoutNote = "<div class='checkout-note text-center w-100 pt-4'>If you do not have an account, please checkout as guest. You will be given the option to create an account later in the checkout process.<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}

function addcopyrightText() {
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="container-fluid"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-12 col-lg-6 tc-rights-reserved pl-0">Techsembly&#169; '+copyrightYear+'</div></div></div>'
  var elem = $(".checkout-footer")[0]
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}

function addBackToHomeBtn() {
  var copyrightHtml = '<div class="d-flex justify-content-center items-center backToHomeBtn"><a href="https://137-pillars.techsembly.com">BACK TO HOME</a></div>'
  var elem = $(".order-confirm-wrapper .card-footer")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}

function addcopyrightToComplete() {
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="card-footer-right"><a href="#">&#169; '+copyrightYear+' 137 Pillars Hotels & Resorts</a></div>'
  var elem = $(".order-confirm-wrapper .card-footer .col-12")[0]
  elem.insertAdjacentHTML('beforeend', copyrightHtml)
}

function addRelatedProductHead() {
  var heading = '<h1 class="productHeading">More Like This</h1>';
  var relProdCont = $('.related-products .custom-container .container')[0];
  relProdCont.insertAdjacentHTML('afterbegin', heading);
}


(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // document.head.appendChild(newStyle);
    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };
    checkReady(function($) {
      $(function() {

        waitForElm('.cart-right-cont').then((elm) => {
          addLoginCartNote();
          addGuestCheckoutNote();
        }).catch((error) => {});
        waitForElm('app-footer-checkout').then((elm) => {
          addcopyrightText()
        }).catch((error) => {});
        waitForElm('.order-confirm-wrapper .card-footer').then((elm) => {
          addBackToHomeBtn()
        }).catch((error) => {});
        waitForElm('.order-confirm-wrapper .card-footer .col-12').then((elm) => {
          addcopyrightToComplete()
        }).catch((error) => {});
        waitForElm('.related-products .custom-container .container').then((elm) => {
          addRelatedProductHead();
        }).catch((error) => {});

        setTimeout(function () {
          $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');


        }, 2000);
        setInterval(function() {
        	$(".head-links #profile-icon img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/Sx9xhPsTs4z1mY68M5tVUh7f");
	        $(".head-links #wishlist-heart img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/ESgrQYBtxwj7Vkpva4RokMYf");
	        $(".head-links #cart-bag img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/wnDsMqPPaCYjU2b8r5yzWFZr");

          if(!$('header.header .toggle-menu-cont').hasClass('col-3')){
            $('header.header .toggle-menu-cont').addClass('col-3 col-md-3').removeClass('col-1 col-sm-2 col-md-2');
          };
          if(!$('header.header .menu-links-cont').hasClass('col-3')){
            $('header.header .menu-links-cont').addClass('col-3 col-md-3').removeClass('col-md-6 col-sm-5 col-7');
          };
          if($('header.header .logo').parent('div').hasClass('col-sm-5')){
            $('header.header .logo').parent('div').removeClass('col-sm-5 pl-2').addClass('col-6 col-md-6');
          };

          $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
          $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');
          $('.product-detail-container .product-holder').removeClass('mb-5 pb-5').addClass('mb-3 pb-4 modified');
          $('.product-detail-container .product-holder .product-detail').removeClass('mb-md-4').addClass('modified');

          // add to wishlist icon working and image change
          $('.product-detail-container .wishlistImage:has(img[alt="add-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
            $('.product-detail-container .wishlistImage img').attr('src', 'https://static.techsembly.com/ESgrQYBtxwj7Vkpva4RokMYf');
          });
          $('.product-detail-container .wishlistImage:has(img[alt="added-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
            $('.product-detail-container .wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://static.techsembly.com/z9eUhEWZqtKVXkufJmjcSQ4A');
          });
          $('.products-container .products-holder-main .products-holder .product-column .product-list .product-content .product-rating a.wishlistImage img').each(function(){
            if($(this).attr('src') != 'https://static.techsembly.com/ESgrQYBtxwj7Vkpva4RokMYf') {
              $(this).attr('src','https://static.techsembly.com/ESgrQYBtxwj7Vkpva4RokMYf');
            }
            if(($(this).attr('alt')=='added-to-wishlist') && ($(this).attr('src') != 'https://static.techsembly.com/z9eUhEWZqtKVXkufJmjcSQ4A')) {
              $(this).attr('src','https://static.techsembly.com/z9eUhEWZqtKVXkufJmjcSQ4A');
            }
          });

          $('.custom-container.cart-container .card.vendor-items-holder .card-body .edit-item-new img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/5hRin8PmPLAqQ2HnB4fp7WKs");
          $('.custom-container.checkout .shipping-form-container .card-header .edit-address img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/5hRin8PmPLAqQ2HnB4fp7WKs");
          $('.delivery-methods-cont .contact-card .card-body .card-row .edit-contact img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/5hRin8PmPLAqQ2HnB4fp7WKs");
          $('.delivery-methods-cont .contact-card .card-body .card-row .edit-address img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/5hRin8PmPLAqQ2HnB4fp7WKs");
          $('.delivery-methods-cont .vendor-cards-holder .card .order-item .item-desc .edit-item-new img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/5hRin8PmPLAqQ2HnB4fp7WKs");
          $('.delivery-methods-cont .vendor-cards-holder .card .delivery-dropdowns-cont .label-email .recipent-info-edit img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/5hRin8PmPLAqQ2HnB4fp7WKs");

          $('.checkout .password-note:not(.modified)').addClass('modified').html('Must be at least 6 characters including one special character');
          $('.checkout-container.complete .order-confirm-top img:not(.modified)').addClass('modified completeLogo').attr('src', 'https://static.techsembly.com/rG1wT8wpkcDiaLs3zEFPGrKv').attr('width', '125');
          $("header.header .logo").closest(".col-4:not(.modified)").addClass('modified mobile-logo-cont');

          $('.custom-container.cart-container').each(function(){
            if (!$(this).closest('body').attr('data-pagetype','cart')){
              $(this).closest('body').attr('data-pagetype','cart');
            }
          });
          $('.shipping-form-container .form-control#last-name').each(function(){
            if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).closest('.col-md-6').addClass('last-name-cont');
            }
          });
          $('.products-container .products-holder-main .products-holder .product-column').each(function() {
            if($(this).closest('.products-holder').hasClass('pt-5')){
              $(this).closest('.products-holder').removeClass('pt-5').addClass('pt-2');
            }
          });
          // Food and digital products
          $('.pickup_and_digital .nav-checkout-delivery .shipping-form-container .breadcrumb-heading').each(function() {
            if(!$(this).hasClass('fnd')){
              $(this).addClass('fnd');
              $(this).html('order details');
            }
          });
          $('.pickup_and_digital .shipping-form-container .breadcrumb-heading:contains("Shipping Details")').each(function() {
            if(!$(this).hasClass('fnd')){
              $(this).addClass('fnd');
              $(this).html('billing details');
            }
          });
          $('.pickup_and_digital .shipping-form-container .delivery-methods-cont .billing-heading:contains("Shipping Address")').each(function() {
            if(!$(this).hasClass('fnd')){
              $(this).addClass('fnd');
              $(this).html('Billing Address');
            }
          });

          $('.pickup_and_digital .shipping-form-container .shipping-form-cont-inner .billing-heading:contains("Delivery Method")').each(function() {
            if(!$(this).hasClass('fnd')){
              $(this).addClass('fnd');
              $(this).html('shipment Method');
            }
          });

          $('.pickup_and_digital .shipping-form-container .shipping-form-cont-inner .billing-heading').each(function() {
            if(!$(this).hasClass('fnd')){
              $(this).addClass('fnd');
              $(this).html('billing Address');
            }
          });

          $('.product-detail-container .personalise-form .form-group button.order-btn').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Buy now');
            }
          })

          $('.custom-container.checkout .nav-checkout-delivery .shipping-form-container .btn.btn-submit').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Continue to Payment');
            }
          });
          $('.custom-container.checkout .shipping-form-container .btn.btn-submit').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Continue to Order');
            }
          });
          $('.delivery-methods-cont .billing-heading:contains("Delivery Method")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Shipment Method');
            }
          });
          $('.custom-container.cart-container .vendor-items-cont .card-header .vendor-heading .delivered-by').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified').removeClass('mr-1');
              $(this).html('Fulfilled By');
            }
          });
          $('.delivery-methods-cont .vendor-cards-holder .card .card-header .vendor-title span:contains("Delivered by")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Fulfilled By');
            }
          });
          $('.order-summary-cont .order-totals-container .sub-total-desc:contains("Subtotal")').each(function() {
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).html('Subtotal:');
            }
          });

          $('.custom-container.cart-container .btn.continue-btn-new').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              $(this).closest(".col-12").addClass('modified pl-md-0');
              $(this).html('Return to shopping');
            }
          });
          $('.home-banner h1.banner-heading div:contains("Outer view banner")').each(function(){
            if(!$(this).closest('.home-banner').hasClass('suit-banner')){
              $(this).closest('.home-banner').addClass('suit-banner');
            }
          });
          $('.home-slider .custom-container .section-title:contains("EXPERIENCE")').each(function(){
            if(!$(this).closest('.home-slider').hasClass('experience-slider')){
              $(this).closest('.home-slider').addClass('experience-slider');
            }
          });
          $('.home-banner h1.banner-heading div:contains("GIFTS FROM 137 PILLARS HOUSE")').each(function(){
            if(!$(this).closest('.home-banner').hasClass('pool-banner')){
              $(this).closest('.home-banner').addClass('pool-banner');
            }
          });

          $('.footer .footer-widget .widget-content .social-links a img[alt="linkedin"]').attr('src', 'https://static.techsembly.com/5vBvMZEqCFichdBFi8phfBvy');
          $('.footer .footer-widget .widget-content .social-links a img[alt="fb"]').attr('src', 'https://static.techsembly.com/xHXN5wigX97L3e4YkxSP6xAu');
          $('.footer .footer-widget .widget-content .social-links a img[alt="instagram"]').attr('src', 'https://static.techsembly.com/pPfmMZXofDQqmH1aBuVNtJFp');

          $('.footer .footer-widget .widget-title:contains("KEEP IN TOUCH")').each(function() {
            if(!$(this).closest('.footer-widget').hasClass('text-right')){
              $(this).closest('.footer-widget').addClass('text-right');
              $('.footer-widget.text-right .widget-title').html("Follow us");
            }
          });
          $('.footer .footer-widget .widget-content ul li a:contains("EXPLORE OUR WORLD")').each(function() {
            if(!$(this).closest('.footer-widget').hasClass('widget-col-1')){
              $(this).closest('.footer-widget').addClass('widget-col-1');
            }
          });
          $('.footer .footer-widget .widget-content ul li a:contains("INFORMATION")').each(function() {
            if(!$(this).closest('.footer-widget').hasClass('widget-col-2')){
              $(this).closest('.footer-widget').addClass('widget-col-2');
            }
          });

          $('.checkout-container.complete .msg-cont .msg-inner').each(function () {
            if (!$(this).hasClass('modified')) {
              $(this).addClass('modified');
              var newTxt = '137 Pillars Hotels & Resorts';
              $(".checkout-container.complete .msg-cont .msg-inner:contains(' We’ll send you shipping confirmation when your item(s) are on the way! We appreciate your business, and hope you enjoy your purchase. ')").html(function (_, html) {
                return html.replace(/(137 Pillars Bangkok)/g,newTxt)
              });
            }
          });
          $('.order-confirm-wrapper .card-footer .card-footer-left a:contains("Customer Support")').each(function() {
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).attr('href', 'mailto:contact@137pillarsbangkok.com');
            }
          })
       })
      })
    })
})();
