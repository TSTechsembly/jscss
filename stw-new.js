(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
            $('head').append('<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">');
            $('head').append('<link rel="preconnect" href="https://fonts.gstatic.com"><link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">');
            $('head').append('<link rel="preconnect" href="https://fonts.gstatic.com"><link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@600&display=swap" rel="stylesheet">');
            $('head').append('<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">');
            $('.toggle-menu-cont .shipping li:nth-child(1)').html('Shop in');
            $('<li><img alt="flag" src="https://media.graphcms.com/lqo5fvHxQmi6hMSIqZVG" width="25"></li>').insertAfter($('.toggle-menu-cont .shipping li:nth-child(1)'));
            $('.footer .copyright-cont .powered-by').prepend('<img class="copy-right-img-top mb-1" src="https://media.graphcms.com/ICXT2PGzRsSnuutBN4ge", width="69" ,height="83"/>');
            $('meta[name=viewport]').attr('content','width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0')

            setTimeout(function () {
                $('header.header .search .search-item-form .nav-search-field').attr('placeholder','Search');
                $('.mobile-search-box .nav-search-field').attr('placeholder','Search');
                $('header.header .search .search-item-form .btn-search img').attr('src','https://media.graphcms.com/IChRsxB7RwetoVEFUd1D');
                $('header .mob-top-links .mob-search-link img').attr('src','https://media.graphcms.com/54irfsu0ThOm1Ddmpnx3');
                $('.mobile-search-box .btn-search img').attr('src','https://media.graphcms.com/IChRsxB7RwetoVEFUd1D');
                $('<div  class="spacing mobile-nav-sub mob-search-cont d-lg-none py-1" id="mobile-nav-sub"><div  class="mobile-search-mini-form col-12 pl-2 pr-0 pt-3 pb-2 mb-1"></div></div>').insertBefore('nav#sidebar .mob-nav');
                $('#mobile_search_mini_form').appendTo('nav#sidebar #mobile-nav-sub .mobile-search-mini-form');
                $('.product-carousel-desktop .owl-nav .owl-prev').html('<i class="chevron chevron-left" aria-hidden="true"></i>');
                $('.product-carousel-desktop .owl-nav .owl-next').html('<i class="chevron chevron-right" aria-hidden="true"></i>');
                $('body:not([data-pagetype="home"]) .app-header').append('<div class="row home-characs inner border-top">'+
                '<div class="container custom-container">'+
                '<div class="col-12 characs-cont d-flex align-items-center px-0 px-lg-3">'+
                '<div class="col-4 d-flex justify-content-center align-items-center single-charac">'+
                '<span class="text-uppercase mr-2">MADE in Scotland</span><img src="https://media.graphcms.com/3TGIeEmTWe9QxrrU5auF" width="20" alt="flag">'+
                '</div>'+
                '<div class="col-4 d-flex justify-content-center align-items-center single-charac">'+
                '<span class="text-uppercase mr-2">INTERNATIONAL DELIVERY</span><img src="https://media.graphcms.com/N0ZLzeJtSJCcjw0OV32U" width="20" alt="van">'+
                '</div>'+
                '<div class="col-4 d-flex justify-content-center align-items-center single-charac">'+
                '<span class="text-uppercase mr-2">SUPPORTING SCOTTISH MAKERS</span><img src="https://media.graphcms.com/zf8g75FbSUGdxUqFw2pQ" width="20" alt="heart">'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div');
                $('.home-banner .container-fluid').append('<div class="row home-characs">'+
                '<div class="container custom-container">'+
                '<div class="col-12 characs-cont d-flex align-items-center px-0 px-lg-3">'+
                '<div class="col-4 d-flex justify-content-center align-items-center single-charac">'+
                '<span class="text-uppercase mr-2">MADE in Scotland</span><img src="https://media.graphcms.com/3TGIeEmTWe9QxrrU5auF" width="20" alt="flag">'+
                '</div>'+
                '<div class="col-4 d-flex justify-content-center align-items-center single-charac">'+
                '<span class="text-uppercase mr-2">INTERNATIONAL DELIVERY</span><img src="https://media.graphcms.com/N0ZLzeJtSJCcjw0OV32U" width="20" alt="van">'+
                '</div>'+
                '<div class="col-4 d-flex justify-content-center align-items-center single-charac">'+
                '<span class="text-uppercase mr-2">SUPPORTING SCOTTISH MAKERS</span><img src="https://media.graphcms.com/zf8g75FbSUGdxUqFw2pQ" width="20" alt="heart">'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div');
                $('.sign-up-section .sign-up-container .sign-up-form .btn.submit').html('Subscribe');
                $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
                $('.product-detail-container .product-holder .share-link').appendTo('.product-detail-container .product-holder .product-order');
                $('.product-detail-container .product-holder .share-link li:nth-child(2) a img').attr('src','https://media.graphcms.com/1bZJ9o7cQk66fRfDDlz6');
                $('.product-detail-container .product-holder .share-link li:nth-child(3) a img').attr('src','https://media.graphcms.com/2PHpEXKR8isNwEMrw01L');
                $('.product-detail-container .product-holder .share-link li:nth-child(4) a img').attr('src','https://media.graphcms.com/IQOb9q0eT3mDo9elEm9l');

                $('.footer .footer-widget .widget-content .social-links li a#facebook-icon img').attr('src','https://media.graphcms.com/1aggfdipQoKmKpq9VLXl');
                $('.footer .footer-widget .widget-content .social-links li img#facebook-icon').attr('src','https://media.graphcms.com/1aggfdipQoKmKpq9VLXl');
                $('.footer .footer-widget .widget-content .social-links li a#insta-icon img').attr('src','https://media.graphcms.com/iG9oN8eiRF5YE35IE1Cg');
                $('.footer .footer-widget .widget-content .social-links li img#insta-icon').attr('src','https://media.graphcms.com/iG9oN8eiRF5YE35IE1Cg');
                $('<img class="copy-right-img" src="https://media.graphcms.com/j6MMRu8SOiW8N8cmh5zk", width="34" ,height="32"/>').insertAfter($(".powered-link-cont"));
            }, 2000);
            setInterval(function () {
                $('header.header .mobile-menu#sidebarCollapse svg').each(function(){
                    $(this).attr('width','21');
                    $(this).find('rect:nth-child(2)').attr('y','35');
                    $(this).find('rect:nth-child(3)').attr('y','60');
                });
                if(!$('header.header .toggle-menu-cont').hasClass('col-2')){
                    $('header.header .toggle-menu-cont').addClass('col-2 col-md-4').removeClass('col-1 col-sm-2 col-md-2');
                }
                if(!$('header.header .menu-links-cont').hasClass('col-3')){
                    $('header.header .menu-links-cont').addClass('col-3 col-md-4').removeClass('col-md-6 col-sm-5 col-7');
                }
                if(!$('header.header .logo').parent('div').hasClass('col-7')){
                    $('header.header .logo').parent('div').addClass('col-7').removeClass('col-4 col-sm-5');
                }
                $('#dropdownccyButton').each(function(){
                  if(!$(this).hasClass('flag-cont')) {
                    $(this).addClass('flag-cont');
                    $(this).append('<img class="flag" alt="flag" src="https://media.graphcms.com/lqo5fvHxQmi6hMSIqZVG">');
                  }
                });
                $('app-vendor').each(function(){
                    if ($(this).length){
                        if (!$('body').attr('data-pagetype','vendor-page')){
                            $('body').attr('data-pagetype','vendor-page');
                        }
                    }
                });
                /*$('.product-detail-container .preview-thumbnail .owl-stage-outer .owl-stage:not(.counted)').addClass('counted').each(function(){
                   var items = $(this).children('.owl-item').length;
                   //console.log(items);
                   if (items < 5) {
                       $(this).closest('.preview-thumbnail').addClass('arrows-hidden');
                   }
                });*/
                $('.vendor-micro-cont.dt-view .vendor-logo').each(function(){
                    if (!$(this).closest('div').hasClass('vendor-logo-cont')){
                        $(this).closest('div').addClass('vendor-logo-cont');
                    }
                    $(this).closest('div').removeClass('pb-5');
                });
                $('.vendor-micro-cont .vendor-intro').each(function(){
                    if (!$(this).closest('.container').hasClass('vendor-intro-holder')){
                        $(this).closest('.container').addClass('vendor-intro-holder');
                    }
                });
                $('.vendor-micro-cont .vendor-menu-cont').each(function(){
                    if (!$(this).closest('.container').hasClass('d-none')){
                        $(this).closest('.container').addClass('d-none');
                    }
                });
                $('.vendor-micro-cont.dt-view .container#about div.name').each(function(){
                    if(!$(this).children('h6.micro-heading').length){
                        $(this).prepend('<h6 class="micro-heading">MEET</h6>');
                    }
                });
                $('.vendor-intro-mob .vendor-text div.name').each(function(){
                    if(!$(this).children('h6.micro-heading').length){
                        $(this).prepend('<h6 class="micro-heading">MEET</h6>');
                    }
                });
                $('.vendor-micro-cont.dt-view .container#about div.video').each(function(){
                    if(!$(this).children('.chat-btn').length){
                        $(this).append('<a href="#contact-form-cont" class="btn btn-primary chat-btn">Chat with Me</a>');
                    }
                });
                $('.vendor-intro-mob .vendor-text div.profile-info').each(function(){
                    if(!$(this).children('.chat-btn').length){
                        $(this).append('<a href="#contact-form-cont" class="btn btn-primary chat-btn">Chat with Me</a>');
                    }
                });
                $('.vendor-micro-cont.dt-view .container#about div.social-responsibilty').each(function(){
                    if(!$(this).children('h6.micro-heading').length){
                        $(this).prepend('<h6 class="micro-heading">SOCIAL RESPONSIBILITY</h6>');
                    }
                });
                $('.vendor-intro-mob .vendor-text div.social-responsibilty').each(function(){
                    if(!$(this).children('h6.micro-heading').length){
                        $(this).prepend('<h6 class="micro-heading">SOCIAL RESPONSIBILITY</h6>');
                    }
                });
                $('.vendor-micro-cont .container .vendor-heading b').each(function () {
                    if(!$(this).children('a.collapse-link').length){
                        $(this).prepend('<a class="collapse-link collapsed" data-toggle="collapse" aria-expanded="false"><i class="fa fa-plus" aria-hidden="true"></i></a>');
                    }
                });
                $('.vendor-micro-cont .container.vendor-contact-form-cont ').each(function(){
                    if($(this).attr('id') != 'contact-form-cont') {
                        $(this).attr('id', 'contact-form-cont');
                    }
                    if ( $(this).find('.collapse-link').attr('href') != '#cont-form-wrap' ) {
                        $(this).find('.collapse-link').attr('href','#cont-form-wrap');
                        $(this).find('.collapse-link').attr('aria-controls','cont-form-wrap');
                    }
                    if ( $(this).children('.vendor-form-inner').attr('id') != 'cont-form-wrap' ) {
                        $(this).children('.vendor-form-inner').attr('id','cont-form-wrap');
                    }
                });
                $('.vendor-micro-cont .container.vendor-point-cont#delivery ').each(function(){
                    if ( $(this).find('.collapse-link').attr('href') != '#del-desc' ) {
                        $(this).find('.collapse-link').attr('href','#del-desc');
                        $(this).find('.collapse-link').attr('aria-controls','del-desc');
                    }
                    if ( $(this).children('.vendor-point-desc').attr('id') != 'del-desc' ) {
                        $(this).children('.vendor-point-desc').attr('id','del-desc');
                    }
                });
                $('.vendor-micro-cont .container.vendor-point-cont#delivery p.mb-4').each(function(){
                    var addHtml = $(this).html();
                    var newAddHtml = 'Ships from <span class="address-line">'+addHtml.substring(13)+'</span>';
                    //console.log(newAddHtml);
                    if(!$(this).children('span.address-line').length){
                        $(this).html(newAddHtml);
                    }
                });
                $('.vendor-micro-cont .container .vendor-heading b:contains("Returns")').each(function(){
                    if (!$(this).closest('.container').hasClass('returns-cont')) {
                        $(this).closest('.container').addClass('returns-cont');
                    }
                });
                $('.vendor-micro-cont .container.vendor-point-cont.returns-cont ').each(function(){
                    if ( $(this).find('.collapse-link').attr('href') != '#return-desc' ) {
                        $(this).find('.collapse-link').attr('href','#return-desc');
                        $(this).find('.collapse-link').attr('aria-controls','return-desc');
                    }
                    if ( $(this).children('.vendor-point-desc').attr('id') != 'return-desc' ) {
                        $(this).children('.vendor-point-desc').attr('id','return-desc');
                    }
                });
                $('.vendor-micro-cont .container .vendor-heading b:contains("Terms")').each(function(){
                    if (!$(this).closest('.container').hasClass('tnc-cont')) {
                        $(this).closest('.container').addClass('tnc-cont');
                    }
                });
                $('.vendor-micro-cont .container.vendor-point-cont.tnc-cont ').each(function(){
                    if ( $(this).find('.collapse-link').attr('href') != '#tnc-desc' ) {
                        $(this).find('.collapse-link').attr('href','#tnc-desc');
                        $(this).find('.collapse-link').attr('aria-controls','tnc-desc');
                    }
                    if ( $(this).children('.vendor-point-desc').attr('id') != 'tnc-desc' ) {
                        $(this).children('.vendor-point-desc').attr('id','tnc-desc');
                    }
                });
                $('.vendor-micro-cont .container #return-desc div:last-child').each(function(){
                    var addReturnHtml = $(this).html();
                    var newAddReturnHtml = addReturnHtml.substring(3);
                    //console.log(newAddReturnHtml);
                    if(!$(this).children('span.address-line').length){
                        $(this).html('<span class="address-line">'+newAddReturnHtml+'</span>');
                    }
                });
                $('.vendor-micro-cont .container .vendor-heading b a.collapse-link').each(function(){
                    if(!$(this).hasClass('collapsed')){
                        $(this).find('i.fa').addClass('fa-minus').removeClass('fa-plus');
                    }
                    else {
                        $(this).find('i.fa').addClass('fa-plus').removeClass('fa-minus');
                    }
                });
                $('.custom-container.cart-container .vendor-heading').each(function () {
                    if (!$(this).children('span.delivered-by').length) {
                        $(this).prepend('<span class="delivered-by text-uppercase mr-2">DELIVERED BY</span>');
                    }
                });
                $('.custom-container.cart-container .cart-item .item-img:not(.appeared)').addClass('appeared').each(function () {
                    $(this).closest('.col-md-2').addClass('col-lg-2 item-img-cont');
                });
                $('.custom-container.cart-container .cart-item .item-details:not(.shrinked)').addClass('shrinked').each(function () {
                    $(this).closest('.col-md-10').addClass('col-lg-10 item-details-cont');
                });
                $('.custom-container.cart-container .mob-cart-sep').each(function(){
                    if(!$(this).closest('.cart-container').hasClass('mob-total-cont')){
                        $(this).closest('.cart-container').addClass('mob-total-cont');
                    }
                });
                $('.custom-container.checkout .page-heading').each(function(){
                    if(!$(this).closest('.row').hasClass('d-lg-none')){
                        $(this).closest('.row').addClass('d-lg-none');
                    }
                });
                $('.custom-container.cart-container .shopping-btns-last').parent('div:not(.pt-lg-0)').addClass('pt-lg-0');
                $('app-signup .right-panel').closest('div.col-md-12:not(.sign-up-form-cont)').addClass('sign-up-form-cont');
                $('.signup-form label:contains("The password must contain")').each(function(){
                    if(!$(this).closest('.form-group').hasClass('done')){
                      $(this).closest('.form-group').addClass('d-none');
                    }
                });
                $('form.signup-form label.newsletter-check span').html("Subscribe to newsletters");
                $('.address-form').find("input[formcontrolname=building]").each(function(ev){
                    $(this).parent('div').addClass('building-cont');
                });
                $('.address-form').find("input[formcontrolname=postCode]").each(function(ev){
                    $(this).parent('div').addClass('post-code-cont');
                });
                $('.address-form').find("input[formcontrolname=phone]").each(function(ev){
                    if(!$(this).val()) {
                      $(this).attr("placeholder", "Phone number");
                    }
                    $(this).parent('div').addClass('phone-no-cont');
                });
                $('.custom-container.checkout .checkout-tabs-cont .vendor-items-holder .vendor-title').each(function () {
                    if (!$(this).children('span.delivered-by').length) {
                        $(this).prepend('<span class="delivered-by mr-1 text-uppercase">Delivered by</span>');
                    }
                });
                $('.payment-form-container form .radio-container .pay-method-name').closest('.form-group:not(.pay-methods-cont)').addClass('pay-methods-cont');
                $('.custom-container.checkout .checkout-tabs-cont form.payment-form .btn.submit').closest('.col-md-12:not(.payment-btn-cont)').addClass('payment-btn-cont');
                $('.payment-form-container form.payment-form .card-images-holder').each(function(){
                    if (!$(this).children('div.cards-cont').length) {
                        $(this).append('<div class="cards-cont d-flex"><img class="card-image" src="https://media.graphcms.com/pBySMapRS6mPtiDD6xG7" alt="card"><img class="card-image" src="https://media.graphcms.com/6T0IbqbAT3emxQxg7KMy" alt="card"><img class="card-image" src="https://media.graphcms.com/hdXyWTCERq6gqZBBFw2v" alt="card"></div>');
                    }
                });
                $('.payment-form').find("input[formcontrolname=promoCode]").each(function(ev){
                    $(this).closest('div.form-group').addClass('promo-code-cont');
                });
                if(!$('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').hasClass('social-widget')){
                    $('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').addClass('social-widget');
                    $('.social-widget').find('.widget-title').html('OUR COMMUNITY');
                }
                $('.product-detail-container .product-order .tab-content .personalise-form').each(function(){
                    if(!$(this).hasClass('moved')){
                        $(this).addClass('moved');
                        $(this).insertBefore($('ul#myTab'));
                    }
                });
                var myVar = setInterval(myTimer, 1000);

                function myTimer() {
                    var bestSellerTitleInt;
                    var prosLinkInt;
                    $('.vendor-micro-cont .container#best-seller-cont .vendor-heading').each(function () {
                        var bestSellerHeadInt = {};
                        bestSellerTitleInt = "SHOP THE COLLECTION";
                        prosLinkInt = $(this).find("a:last-child").attr("href");
                        //console.log(prosLinkInt);
                            if(prosLinkInt != null && (!$(this).hasClass('modified-ajax'))){
                            $(this).html('<b>'+bestSellerTitleInt+'<a class="ml-2" href="'+prosLinkInt+'">[View all]</a></b>');
                            $(this).addClass('modified-ajax');
                            }
                    });
                    $('.vendor-micro-cont .container.vendor-contact-form-cont').each(function(){
                        if(!$(this).children('.vendor-form-inner').length){
                            $(this).wrapInner($("<div class='vendor-form-inner collapse'>"));
                        }
                    });
                    $('.vendor-micro-cont .container.vendor-contact-form-cont .vendor-form-inner').each(function(){
                    $(this).find('h2.vendor-heading').prependTo($(this).closest(".vendor-contact-form-cont"));
                    });
                    $('.vendor-micro-cont .container.vendor-point-cont ').each(function(){
                        if(!$(this).children('.vendor-point-desc').length){
                            //$(this).children().not('h2.vendor-heading ').wrapAll('<div class="vendor-point-desc collapse" />');
                            $(this).wrapInner($("<div class='vendor-point-desc collapse'>"));
                        }
                    });

                    $('.vendor-micro-cont .container.vendor-point-cont .vendor-point-desc').each(function(){
                    $(this).find('h2.vendor-heading').prependTo($(this).closest(".vendor-point-cont"));
                    });

                }

                function myStopFunction() {
                clearInterval(myVar);
                }
            });
        });
    });
})();
