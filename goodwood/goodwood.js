function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }
        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });
        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
function addPromoHead() {
  var promoHead = '<h2 class="section-title text-center text-uppercase">Discover Our #GoodwoodExperience Restaurants</h2>';
  var elemPromoHolder = $('.promo-section .promo-holder')[0];
  elemPromoHolder.insertAdjacentHTML('beforebegin', promoHead);
}
function addLoginCartNote(){
  var loginCartNote = "<div class='login-note'>Please note this login is for Goodwood Park Singapore gifting platform only.<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}
function addPhoneNote(){
  var phoneNote = "<div class='phone-note'>Example: +65XXXXXXXX (no spacing)<div>";
  var elemInput = $('.custom-container.checkout .shipping-form-container .form-control#phone')[0];
  elemInput.insertAdjacentHTML('afterend', phoneNote);
}
function addGuestCheckoutNote(){
  var checkoutNote = "<div class='checkout-note text-center w-100'>You can create account after checkout<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}
function addPromoDropDowns(){
  var promoDropDowns = '<div class="col-12 d-flex justify-content-between flex-wrap promo-options-cont px-0">'+
  '<div class="col-12 col-md-6 px-0 pr-md-3 form-group">'+
  '<select class="form-control" name="bank_name" id="bank_name">'+
  '<option disabled="disabled" selected="selected" value="">Select Bank</option>'+
  '<option value="citi">Citi Bank</option>'+
  '<option value="ocbc">OCBC Bank</option>'+
  '<option value="dbs">DBS/POSB Bank</option>'+
  //'<option value="gourmet">Hotel Gourmet</option>'+
  '<option value="uob">UOB Bank</option>'+
  '<option value="hsbc">HSBC Bank</option>'+
  //'<option value="scb">Standard Chartered Bank</option>'+
  '</select>'+
  '</div>'+
  '<div class="col-12 col-md-6 px-0 pl-md-3 form-group">'+
  '<select class="form-control" name="card_type" id="card_type">'+
  '<option disabled="disabled" selected="selected" value="">Select Card Type</option>'+
  '<option value="visa">Visa</option>'+
  '<option value="master-card">Master Card</option>'+
  '<option value="amex">AMEX</option>'+
  '<option value="others">Others</option>'+
  '</select>'+
  '</div>'+
  '</div>';
  var elem = $(".payments-methods-cont")[0];
  elem.insertAdjacentHTML('afterbegin', promoDropDowns);
}
function addcopyrightText(){
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-6 ts-copyright text-uppercase"><span><a href="https://techsembly.com/" target="_blank">Techsembly&copy;</a></span><span class="ml-1">'+copyrightYear+
  '</span></div><div class="col-6 tc-rights-reserved text-right pr-0"></div></div></div></div>';
  var elem = $(".checkout-footer")[0];
  elem.insertAdjacentHTML('afterend', copyrightHtml);
}
function addSuccessCopyRight() {
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var successCopyRight = '<span>© '+copyrightYear+
  ' Goodwood Park</span>';
  var elemFooterLeftSocial = $('.custom-container.checkout-container.complete .card.order-confirm-wrapper .card-footer-right')[0];
  elemFooterLeftSocial.insertAdjacentHTML('afterbegin', successCopyRight);
}
(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
          waitForElm('.promo-section').then((elm) => {
            addPromoHead();
          }).catch((error) => {});

          /*waitForElm('.cart-container .vendor-items-holder').then((elm) => {
            var sgFlagImg = '<img class="mr-2" alt="store flag" width="25" src="https://static.techsembly.com/jpPnbDVhuEw661XGrWbyEXwS">'
            var elem = $(".vendor-items-holder .flag-cont")[0];
            elem.insertAdjacentHTML('afterbegin', sgFlagImg)
          }).catch((error) => {});*/

          waitForElm('.cart-right-cont').then((elm) => {
            addLoginCartNote();
            addGuestCheckoutNote();
          }).catch((error) => {});

          waitForElm('.custom-container.checkout .shipping-form-container').then((elm) => {
            addPhoneNote();
            document.getElementById("phone").addEventListener('focusout', function (evt) {
              evt.target.setAttribute("value",evt.target.value);
              var phone = evt.target.value;
              const regex = /^\+([0-9]){10}$/;
              var result = regex.test(phone);
              console.log(result);
              if (result == false) {
                 document.getElementById("phone").classList.add('ng-invalid');
                 document.getElementById("phone").classList.remove('ng-valid');
              }
              else if (result == true) {
                 document.getElementById("phone").classList.remove('ng-invalid');
              }

              //console.log(evt.target.getAttribute('value'));
            });
            $(".shipping-form .btn-submit").click(function(event){

               var phone = $(".shipping-form input#phone").val();
               if (phone != ''){
                 const regex = /^\+([0-9]){10}$/;
                 var result = regex.test(phone);
                 console.log(result);
                 $('.shipping-form').addClass('submitted');
                 if (result == false) {
                   event.preventDefault();
                   $(".shipping-form input#phone").addClass('ng-invalid');
                   return false;
                 }
                 else if (result == true) {
                   $(".shipping-form input#phone").removeClass('ng-invalid');
                 }
               }
               //alert(phone);
             });
          }).catch((error) => {});

          waitForElm('.payment-form-container .payments-methods-cont').then((elm) => {
            addPromoDropDowns();
            $('select#bank_name').on('change', function(e) {
              $('select#card_type').val('');
            });

            $('select#card_type').on('change', function(e) {
              var bank_name = $('select#bank_name').val();
              var promo_input = $('.payment-form-container input#promo-code');
              var promo_code;
              if (bank_name == 'citi' && (this.value == 'visa' || this.value == 'master-card' || this.value == 'amex')) {
                promo_code = 'CITI15';
                alert('Please apply promo code ' + promo_code + ' by clicking apply button ');
                promo_input.val(promo_code);
              }
              else if (bank_name == 'dbs' && (this.value == 'visa' || this.value == 'master-card' || this.value == 'amex')) {
                promo_code = 'DBS/POSB15';
                alert('Please apply promo code ' + promo_code + ' by clicking apply button ');
                promo_input.val(promo_code);
              }
              else if (bank_name == 'uob' && (this.value == 'visa' || this.value == 'master-card' || this.value == 'amex')) {
                promo_code = 'UOB15';
                alert('Please apply promo code ' + promo_code + ' by clicking apply button ');
                promo_input.val(promo_code);
              }
              else if (bank_name == 'ocbc' && (this.value == 'visa' || this.value == 'master-card' || this.value == 'amex')) {
                promo_code = 'OCBC15';
                alert('Please apply promo code ' + promo_code + ' by clicking apply button ');
                promo_input.val(promo_code);
              }
              else if (bank_name == 'gourmet' && (this.value == 'visa' || this.value == 'master-card' || this.value == 'others')) {
                //promo_code = 'GOURMET20';
                alert('Please apply promo code ' + promo_code + ' by clicking apply button ');
                promo_input.val(promo_code);
              }
              else if ((bank_name == 'hsbc') && (this.value == 'visa' || this.value == 'master-card' || this.value == 'amex')) {
                promo_code = 'HSBC15';
                alert('Please apply promo code ' + promo_code + ' by clicking apply button ');
                promo_input.val(promo_code);
              }
              else if ((bank_name == 'scb') && (this.value != '')) {
                //alert('Sorry! No discount is available for this bank / card');
                promo_code = '';
              }
​
              if (promo_code != '') {
                promo_input.closest('.form-group').removeClass('collapse');
                promo_input.closest('form').removeClass('ng-pristine ng-untouched').addClass('ng-touched ng-dirty');
                promo_input.removeClass('ng-pristine ng-untouched').addClass('ng-touched ng-dirty');
​
                setTimeout(function() {
                  promo_input.focus();
​
                  var ev = new Event('input');
​
                  document.getElementById('promo-code').value = document.getElementById('promo-code').value + ' ';
                  document.getElementById('promo-code').dispatchEvent(ev);
​
                }, 200);
                //promo_input.closest('form').find('.apply-btn').removeAttr('disabled');
              }
            });
​
          }).catch((error) => {});


          waitForElm('app-footer-checkout').then((elm) => {
            addcopyrightText()
          }).catch((error) => {});

          waitForElm('.custom-container.checkout-container.complete').then((elm) => {
            addSuccessCopyRight();
          }).catch((error) => {});

          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">');
          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">');
          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">');

          $("body").children().each(function() {
               //document.body.innerHTML = document.body.innerHTML.replace(/\u2028/g, ' ');
           });

          $(".gift-section .gifts-content .gifts-heading").each(function() {
            $(this).html($(this).html().replace(/\u2028/g," "));
          });

          setTimeout(function () {
            $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
          }, 2000);
          setInterval(function() {
            $('.nav-search-field:not(.modified)').addClass('modified').attr("placeholder", "");
            $('header.header .menu-holder>li>div').each(function(){
              if(!$(this).children().length){
                $(this).addClass('empty');
              }
            });

            hrefurl=$(location).attr("href");
            last_part=hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
            currentUrlSecondPart = window.location.pathname.split('/')[1];

            if (currentUrlSecondPart == ''){
               if (!$('body').attr('data-pagetype','home')){
                 $('body').attr('data-pagetype','home');
               }
            }
            if (currentUrlSecondPart == 'catalogsearch'){
               if (!$('body').attr('data-pagetype','search_results')){
                   $('body').attr('data-pagetype','search_results');
               }
            }
            if (currentUrlSecondPart == 'vendors'){
               if (!$('body').attr('data-pagetype','vendors')){
                   $('body').attr('data-pagetype','vendors');
               }
            }
            if (last_part == 'wishlist'){
               if (!$('body').attr('data-pagetype','wishlist')){
                   $('body').attr('data-pagetype','wishlist');
               }
            }

            $('.promo-holder .promo-box .promo-content .promo-title').each(function(){
              var banner_title = $(this).closest('.promo-box').find('.banner-title');
              if(!$(this).closest('.promo-box').hasClass('modified')){
                $(this).closest('.promo-box').addClass('modified');
                banner_title.insertBefore($(this));
              }
            });


            $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
            $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');

            $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('BUY NOW');
              }
            });
            $('.related-products .custom-container').each(function(){
              if(!$(this).children('h3.text-center').length){
                $(this).prepend('<h3 class="text-center related-pros-heading">More Like This</h3>');
              }
            });

            $('app-product-detail app-related-products[title="More items from"]').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).find('h3').html('More Like This');
              }
            });

            $('.product-detail-container .product-holder .product-detail .product-info .product-sku').each(function(){
              let sku_no = $(this).html();
              if(sku_no.includes("029564") && !$(this).closest('.product-holder').hasClass('no-del-pro')) {
                $(this).closest('.product-holder').addClass('no-del-pro');
              }
              else if(sku_no.includes("029563") && !$(this).closest('.product-holder').hasClass('no-del-pro')) {
                $(this).closest('.product-holder').addClass('no-del-pro');
              }
            });

            $('.custom-container.cart-container').each(function(){
              if (!$(this).closest('body').attr('data-pagetype','cart')){
                $(this).closest('body').attr('data-pagetype','cart');
              }
            });

            $('.cart-item.new .item-details .item-qty-cont').each(function(){
              if(!$(this).closest('.col-lg-2').hasClass('qty-col')){
                  $(this).closest('.col-lg-2').addClass('qty-col');
              }
            });

            $('.custom-container.cart-container .vendor-items-cont .card .card-header .flag-cont span:contains("Shipping From Singapore")').each(function() {
                if(!$(this).closest('.flag-cont').hasClass('flag-icon')){
                    $(this).closest('.flag-cont').addClass('flag-icon');
                    $(this).html('<img class="mr-2" alt="store-flag" width="25" src="https://static.techsembly.com/jpPnbDVhuEw661XGrWbyEXwS"><span>Shipping From Singapore</span>');
                }
            });

            $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/X4kgD4YNzvphrBkKttNjyKWx');

            $('#delivery-options-popup').each(function(){
              var radios = document.querySelectorAll('#delivery-options-popup .radio-container input[type="radio"]');

              for (const radioButton of radios) {
                radioButton.addEventListener('change', showSelected);
              }

              function showSelected(e) {
                //console.log(e);
                if (this.checked) {
                  var val = this.value;
                  var clickEvent = new Event('click');
                  for (var j = 0; j < radios.length; j++) {
                      if (radios[j].value == val) {
                        radios[j].click();
                        radios[j].checked = true;

                          //console.log(radios[j]);
                      }
                      else {
                        radios[j].checked = false;
                      }
                  }
                  //console.log(val);
                  /*console.defaultLog = console.log.bind(console);
                  console.logs = [];
                  console.log = function(){
                      // default &  console.log()
                      console.defaultLog.apply(console, arguments);
                      // new & array data
                      console.logs.push(Array.from(arguments));
                  }
                  console.log(arguments);*/
                }
              }

            });

            $('.custom-container.cart-container .btn.continue-btn-new').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('Return to shopping');
                }
            });

            $('.shipping-form-container .exist-address-wrapper .card a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/X4kgD4YNzvphrBkKttNjyKWx');

            $('.delivery-methods-cont .card .card-body .card-row a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/X4kgD4YNzvphrBkKttNjyKWx');

            $('.payment-form-container .card-details-cont').each(function(){
              if(!$(this).children('h2.section-heading').length){
                $(this).prepend('<h2 class="section-heading">Card Details</h2>');
              }
            });

            $('.payment-form-container .input-cont img.pass-eye:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/vW4v2JxQ8kNc6Gx2FwpWyMMq');

            //$('.checkout-container.complete .card-header img[alt="logo"]:not(.modified)').addClass('.modified').attr('src','https://static.techsembly.com/2wijR78TH8YJdfmEaRczQmXs');

            $('.checkout .password-note:not(.modified)').addClass('modified').html('Must be at least 6 characters including one special character');

            /*$('app-stripe-auth .checkout h1.page-heading').each(function(){
              if (!$(this).closest('.row').hasClass('success-heading-cont')){
                $(this).closest('.row').addClass('success-heading-cont');
              }
            })*/

            $('.auth-user .checkout .shipping-form h1.breadcrumb-heading:contains("Shipping Details")').each(function() {
              if(!$(this).closest('.shipping-form').hasClass('shipp-details')){
                $(this).closest('.shipping-form').addClass('shipp-details');
                $(this).html('Choose Shipping Details');
              }
            });

            $('.custom-container.checkout .order-summary-container .vendor-items-holder .vendor-order-details .order-item .qty').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newTxt = 'QTY:';
                $(".qty:contains('Qty:')").html(function(_, html) {
                   return  html.replace(/(Qty:)/g, '<span class="updated-qty-string mr-1">'+newTxt+'</span>');
                });
              }
            })

            $('.custom-container.checkout .checkout-tabs-cont .vendor-items-holder .vendor-order-details .order-item .qty').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newTxt = 'Qty';
                $(".qty:contains('Qty:')").html(function(_, html) {
                   return  html.replace(/(Qty:)/g, '<span class="updated-qty-string mr-1">'+newTxt+'</span>');
                });
              }
            });

            $('.custom-container.checkout .page-heading:contains("Success")').each(function(){
              if(!$(this).hasClass('d-none')){
                $(this).addClass('d-none');
              }
            });

            $('.checkout .checkout-container.complete .card.order-confirm-wrapper').each(function(){
              if(!$(this).closest('.col-12').hasClass('order-confirm-outer')){
                $(this).closest('.col-12').addClass('order-confirm-outer');
              }
            });

            $('.checkout .checkout-container.complete a:contains("Customer Support")').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('href','mailto:Goodwood@techsembly.com');
              }
            });

            $('.checkout-container.complete .card-footer').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).append('<div class="col-12 text-center home-btn-cont"><a class="btn btn-primary home-btn text-center text-uppercase" href="/">back to home</a></div>');
              }
            });

            $('body.auth-user .user-container .balance-form').closest('.user-container:not(.d-none)').addClass('d-none');

            $('.user-container .user-heading:contains("Check TS Gift Card Balance")').each(function(){
              if(!$(this).hasClass('gift-card-heading')){
                $(this).addClass('gift-card-heading');
                $(this).html('Check Gift Card Balance');
              }
            });
            $('.user-container .user-heading:contains("My TS Card Balance")').each(function(){
              if(!$(this).hasClass('gift-card-heading')){
                $(this).addClass('gift-card-heading');
                $(this).html('My Gift Card Balance');
              }
            });
            $('.ts-balance-form label[for="gift-card-no"]:not(.modified)').addClass('modified').html('Gift card number');
            $('.ts-balance-form input[name="gift-card-no"]:not(.modified)').addClass('modified').attr('placeholder','Enter gift card number');

            $('.social-links img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/YLW4PbUs4zJ3UYSuGGrn217i');
            $('.social-links img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/tob77pJhqkzuydMbLVC9sDdG');
            $('.social-links img[alt="pinterest"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/CFRbUCscpqxzvy4oEdeDqvMY');
            $('.social-links img[alt="linkedin"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/RP5NaLpiYvhJZpC2ndRkrwoN');

            $('.footer a:contains("+65")').each(function(){
              var string_phone= $(this).html();
              var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
              var phone_no = "tel:" + result_phone;
              if(!$(this).hasClass('tel-link')){
                $(this).addClass('tel-link');
                $(this).attr('href', phone_no);
              }
            });
            $('.footer a:contains("@")').each(function(){
              var string_email= $(this).html();
              //var result_email = string_email.substring(string_email.indexOf(':') + 1);
              var email_link = "mailto:" + string_email;
              if(!$(this).hasClass('email-link')){
                $(this).addClass('email-link');
                $(this).attr('href', email_link);
              }
            });

            $('.footer a:contains("Gift Card Support")').each(function(){
              var string_email= 'goodwoodparkhotel@techsembly.com';
              //var result_email = string_email.substring(string_email.indexOf(':') + 1);
              var email_link = "mailto:" + string_email;
              if(!$(this).hasClass('email-link')){
                $(this).addClass('email-link');
                $(this).attr('href', email_link);
              }
            });

            $('.footer .mobile-footer .list-group .list-group-holder .footer-widget').each(function(){
              if(!$(this).children().length){
                $(this).closest('.list-group-holder').addClass('d-none');
              }
            });

          });
        });
    });
})();
