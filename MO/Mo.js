function waitForElm(selector) {
    return new Promise(resolve => {
      if (document.querySelector(selector)) {
        return resolve(document.querySelector(selector));
      }
      const observer = new MutationObserver(mutations => {
        if (document.querySelector(selector)) {
          resolve(document.querySelector(selector));
          observer.disconnect();
        }
      });
      observer.observe(document.body, {
        childList: true,
        subtree: true
      });
    });
  }

  function addcopyrightText() {
    var copyrightHtml = '<div class="copy-bottom container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-12 ts-copyright"><span><a href="https://techsembly.com/" target="_blank">Techsembly&copy;</a></span><span class="ml-1">2022</span></div></div></div></div>'
    var elem = $(".checkout-footer")[0]
    elem.insertAdjacentHTML('afterend', copyrightHtml)
  }
  function addCorporateText() {
    var corporate = `<div class="main-corporate">
          <div class="corporate-outer">
            <div class="corporate-img">
              <img src="https://static.techsembly.com/waKHKow4pzffwS2Wp2qEj1HP" alt="corporate gifting">
            </div>
            <div class="corporate-content">
              <h3>Corporate Gifting</h3>
              <p>Thank your clients, recognise your employees or reward your customers with a Mandarin Oriental Gift Card.
                Recipients enjoy the flexibility of use towards revitalising spa treatments, world-class dining experiences,
                luxurious accommodations and more.</p>
              <p> For all enquiries please email <a
                  href="mailto:mohg-giftcards@techsembly.support">mohg-giftcards@techsembly.support</a></p>
            </div>
          </div>
        </div>
        <div class="statics-main">
        <div class="statics-outer">
          <div class="statics-element">
            <h3>Conditions of Use</h3>
            <a href="https://www.mandarinoriental.com/en/shop/conditions-of-use" target="__blank">Read More</a>
          </div>
          <div class="statics-element">
            <h3>FAQs</h3>
            <a href="https://www.mandarinoriental.com/en/shop/faq" target="__blank">Read More</a>
          </div>
          <div class="statics-element">
            <h3>My Card</h3>
            <a href="https://mandarinoriental.givex.com/cws/mandarinoriental_hg/consumer/user/login.py" target="__blank">Read More</a>
          </div>

        </div>
      </div>`;
    var elemMenuButton = $('app-newsletter')[0];
    elemMenuButton.insertAdjacentHTML('afterbegin', corporate);
  }

  function addMogText() {
    var mogText = `<div class="top-main-footer">
          <div class="footer-outer">
            <div class="footer-img">
              <img src="https://static.techsembly.com/YfBjaiR4FGZQXKHn67Fyu9SW" alt="mandarin oriental logo">
            </div>
            <div class="footer-content">
              <h3>Mandarin Oriental Hotel Group</h3>
              <p>8th Floor, One Island East, Taikoo Place 18 Westlands Road, Quarry Bay, Hong Kong <span>View All Toll-Free Reservation Numbers</span></p>
            </div>
          </div>
        </div>`;
    var elemMenuButton = $('footer')[0];
    elemMenuButton.insertAdjacentHTML('afterbegin', mogText);
  }

  function deliveryCheckBox() {
    var deliveryOption = `<div class="delivery-opt delivery-physical">
    <a href="https://mandarin-oriental-hotel-group.techsembly.com/egift-card-031505"><span class="box-delvery"></span>eGift Card</a>
    <a href="https://mandarin-oriental-hotel-group.techsembly.com/gift-card-031506" class="delivery-physical"><span class="box-delvery-physical"></span>Gift Card</a>
  </div>
  <div>
  <div class="resipient-text">
      <h3>Recipient Details</h3>
      <p>Please share the name and email of the gift card recipient</p>
    </div>`;
    var elemMenuButton = $('.physical .delivery-form p')[0];
    elemMenuButton.insertAdjacentHTML('beforeend', deliveryOption);
  }
  function digitDeliveryCheckBox() {
    var digiDeliveryOption = ` <div class="delivery-opt delivery-digital">
    <a href="https://mandarin-oriental-hotel-group.techsembly.com/egift-card-031505" class="delivery-egift"><span class="box-delvery-digital"></span>eGift Card</a>
    <a href="https://mandarin-oriental-hotel-group.techsembly.com/gift-card-031506" ><span class="box-delvery"></span>Gift Card</a>
  </div>
  <div class="resipient-text">
      <h3>Recipient Details</h3>
      <p>Please share the name and email of the gift card recipient</p>
    </div>`;
    var elemMenuButton = $('.digital .delivery-form p')[0];
    elemMenuButton.insertAdjacentHTML('beforeend', digiDeliveryOption);
  }

  function addCCYToolTip() {
    var ccyToolTip = '<div class="ccy-tooltip-cont d-none d-lg-block"><div class="ccy-tooltip">' +
      '<div class="tooltip-desc">Simply choose your<br> preferred currency here.</div>' +
      '<span class="cross-cont">X</span>' +
      '</div></div>';
    var elemToggleMenuCont = $('header .row.spacing .toggle-menu-cont')[0];
    elemToggleMenuCont.insertAdjacentHTML('beforeend', ccyToolTip);
  }


  (function () {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    //example

    //example end


    // fonts add
    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
        font-family: 'Avenir';\
        src: url('https://bitbucket.org/RKTechsembly/frontendtechsembly/raw/e4b2c98373a2f43ae56f4c42ea839068fbff1ae6/fonts/Avenir/avenir-light/Avenir-Light.woff2') format('woff2'),\
        url('https://bitbucket.org/RKTechsembly/frontendtechsembly/raw/e4b2c98373a2f43ae56f4c42ea839068fbff1ae6/fonts/Avenir/avenir-light/Avenir-Light.woff') format('woff'),\
        url('https://bitbucket.org/RKTechsembly/frontendtechsembly/raw/e4b2c98373a2f43ae56f4c42ea839068fbff1ae6/fonts/Avenir/avenir-light/Avenir-Light.ttf') format('truetype');\
        font-weight: 300;\
        font-style: normal;\
      }\
      @font-face {\
        font-family: 'Avenir-Medium';\
        src: url('https://bitbucket.org/RKTechsembly/frontendtechsembly/raw/e4b2c98373a2f43ae56f4c42ea839068fbff1ae6/fonts/Avenir/Avenir-Medium/%40font-face/2090551770be22b09600a40b0b4673b7.woff2') format('woff2'),\
        url('https://bitbucket.org/RKTechsembly/frontendtechsembly/src/master/fonts/Avenir/Avenir-Medium/%40font-face/2090551770be22b09600a40b0b4673b7.woff') format('woff'),\
        url('https://bitbucket.org/RKTechsembly/frontendtechsembly/src/master/fonts/Avenir/Avenir-Medium/%40font-face/2090551770be22b09600a40b0b4673b7.ttf') format('truetype');\
        font-weight: 500;\
        font-style: normal;\
      }\
      @font-face {\
        font-family: 'Adobe Garamond Pro Regular';\
        src: url('https://bitbucket.org/RKTechsembly/frontendtechsembly/raw/3d3a72ddc76a6c69b825e50757ffbcd87881e5b1/fonts/adobe-garamond-pro/AGaramondPro-Regular.woff') format('woff');\
        font-weight: normal;\
        font-style: normal;\
      }\
      @font-face {\
        font-family: 'Adobe Garamond Pro Bold';\
        src: url('https://bitbucket.org/RKTechsembly/frontendtechsembly/src/master/fonts/adobe-garamond-pro/AGaramondPro-Bold.woff') format('woff');\
        font-weight: normal;\
        font-style: normal;\
      }\
  \
    "));
    document.head.appendChild(newStyle);

    // end fonts

    function addcopyrightToComplete() {
      var copyrightHtml = '<div class="card-footer-right">Â© 2022 Mandarin Oriental</div>'
      var elem = $(".order-confirm-wrapper .card-footer .col-12")[0]
      elem.insertAdjacentHTML('beforeend', copyrightHtml)
    }
    function addBackToHomeBtn() {
      var copyrightHtml = '<div class="d-flex justify-content-center items-center backToHomeBtn"><a href="http://giftcards.mandarinoriental.com/">Back To Home</a></div>'
      var elem = $(".order-confirm-wrapper .card-footer")[0]
      elem.insertAdjacentHTML('beforeend', copyrightHtml)
    }

    // Poll for jQuery to come into existence
    var checkReady = function (callback) {
      if (window.jQuery) {
        callback(jQuery);
      }
      else {
        window.setTimeout(function () { checkReady(callback); }, 20);
      }
    };

    // Now lets do something
    checkReady(function ($) {
      $(function () {

        waitForElm('app-newsletter').then((elm) => {
          addCorporateText();
        }).catch((error) => { });

        waitForElm('app-footer-checkout').then((elm) => {
          addcopyrightText()
        }).catch((error) => { });

        waitForElm('footer').then((elm) => {
          addMogText();
        }).catch((error) => { });

        waitForElm('.zain').then((elm) => {
          deliveryCheckBox();
        }).catch((error) => { });

        waitForElm('.zain').then((elm) => {
          digitDeliveryCheckBox();
        }).catch((error) => { });

        waitForElm('.order-confirm-wrapper .card-footer .col-12').then((elm) => {
          addcopyrightToComplete()
        }).catch((error) => { });

        waitForElm('.order-confirm-wrapper .card-footer').then((elm) => {
          addBackToHomeBtn()
        }).catch((error) => { });

        waitForElm('header.header .toggle-menu-cont').then((elm) => {
          addCCYToolTip();
        }).catch((error) => { });


        // end
      });



      setInterval(function () {

        $(".ccy-tooltip-cont .cross-cont").click(function () {
          if (!$(".ccy-tooltip-cont").hasClass('d-lg-none')) {
            $(".ccy-tooltip-cont").removeClass('d-lg-block').addClass('d-lg-none');
          }
        });
        $('meta[name="viewport"]:not(.modified)').addClass('modified').attr("content", "width=device-width, initial-scale=1, maximum-scale=1");

        $('header.header .shipping li .dropdown .dropdown-menu').each(function () {
          if (!$(this).hasClass('countries-dd')) {
            $(this).addClass('countries-dd');
            $(this).append('<div class="countries-links-cont">' +
              '<div class="hkd"><a class="dropdown-item hidestore text-uppercase" translate="" href="http://giftcards.mandarinoriental.com/egift-card-031505"><img src="https://static.techsembly.com/nrZ6QHHy8EJzDXCiLs23zSrm">USD</a></div>' +
              '<div class="usd"><a class="dropdown-item hidestore text-uppercase" translate="" href="http://giftcards.mandarinoriental.com/hkd/egift-card-031707"><img src="https://static.techsembly.com/R3kpeeVf6VMr6KABi5ArmUf7">HKD</a></div>' +
              '<div class="gbp"><a class="dropdown-item hidestore text-uppercase" translate="" href="http://giftcards.mandarinoriental.com/eur/egift-card-031706"><img src="https://static.techsembly.com/USTeiBQHiU7RBrtFn3dvzZr6">EUR</a></div>' +
              '<div class="eur"><a class="dropdown-item hidestore text-uppercase" translate="" href="http://giftcards.mandarinoriental.com/gbp/egift-card-031708"><img src="https://static.techsembly.com/NeSRBNB9c48MFZcZ5Rz24iwB">GBP</a></div>' +
              '</div>');
          }
        });

        $('#sidebar ul.list-unstyled.user-links').each(function () {
          if (!$(this).hasClass('user-mob-links')) {
            $(this).addClass('user-mob-links');
            var activeCCY = $('header.header .shipping li a.dropdown-toggle').html();
            $(this).append('<li>' +
              '<div class="d-flex align-items-center">' +
              '<div class="dropdown pref_dropdown w-100">' +
              '<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownMenuLink_mob">' + activeCCY + '</a>' +
              '<div aria-labelledby="dropdownMenuLink" class="dropdown-menu countries-links-cont pt-0 mt-0" x-placement="top-start">' +
              '<div class="hkd"><a class="dropdown-item hidestore text-uppercase" translate="" href="http://giftcards.mandarinoriental.com/egift-card-031505"><img src="https://static.techsembly.com/nrZ6QHHy8EJzDXCiLs23zSrm">USD</a></div>' +
              '<div class="usd"><a class="dropdown-item hidestore text-uppercase" translate="" href="http://giftcards.mandarinoriental.com/hkd/egift-card-031707"><img src="https://static.techsembly.com/R3kpeeVf6VMr6KABi5ArmUf7">HKD</a></div>' +
              '<div class="gbp"><a class="dropdown-item hidestore text-uppercase" translate="" href="http://giftcards.mandarinoriental.com/eur/egift-card-031706"><img src="https://static.techsembly.com/USTeiBQHiU7RBrtFn3dvzZr6">EUR</a></div>' +
              '<div class="eur"><a class="dropdown-item hidestore text-uppercase" translate="" href="http://giftcards.mandarinoriental.com/gbp/egift-card-031708"><img src="https://static.techsembly.com/NeSRBNB9c48MFZcZ5Rz24iwB">GBP</a></div>' +
              '</div>' +
              '</div>' +
              '</div>' +
              '</li>');
          }
        });

        $(".head-links .add-to-basket [alt='cart']img:not(.modified), .mob-top-links .mob-cart-btn [alt='cart']img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/sDwuTqLFT1Q4fBYkSuN5Ugkm");
        $(".head-links .profile-icon img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/Py6RfiCRpJE46tgb2n18XytX");
        $(".head-links .mob-search-link [alt='search image']img:not(.modified), .mob-top-links .mob-search-link [alt='search image']img:not(.modified) ").addClass('modified').attr("src", "https://static.techsembly.com/Qa1sMhTidFrRitJHA8n3TNRr");
        $('.digital .preview-pic:not(.modified)').addClass('modified').append('<h2 class="card-design">Select your eGift Card Design</h2>');
        // share image change
        $('.share-link li:nth-child(2) img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/LkJLFbQFYBwETs7qkWqkszNy");
        $('.share-link li:nth-child(3) img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/XeMhN6NX8PXFkzRQ7fjNQKmr");
        $('.share-link li:nth-child(4) img:not(.modified)').addClass('modified').attr("src", "https://static.techsembly.com/PTAUoGH9MnPUMhK5SyE9FhD9");
        $('.share-link li:nth-child(1) span:contains("Share this")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Share via');
          }
        });
        // footer
        $(".footer .footer-widget:last-child .widget-title:not(.modified)").addClass("modified").addClass('modified').html("Connect");
        $('.footer .footer-widget .social-links li a img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/UqEN6sd3ZXKZAQEMhnYjDzay');
        $('.footer .footer-widget .social-links li a img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/pkNdZ5pFRoNrjzwkQDfsYstz');
        $('.footer .footer-widget .social-links li a img[alt="twitter"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/GqB9HmNaXNkTGEchjLeDPDSv');

        $('.footer .footer-widget .social-links:not(.modified)').addClass('modified').append('<li><a href="https://www.youtube.com/MOHotels" rel="nofollow"><img class="modified" alt="youtube" width="18" src="https://static.techsembly.com/PKvoAtsXYu6iM1rp8tr4oeF6"></a></li>');

        $('.footer .footer-widget .social-links:not(.weiboModified)').addClass('weiboModified').append('<li><a href="http://weibo.com/mandarinoriental001" rel="nofollow"><img class="modified" alt="Weibo" width="18" src="https://static.techsembly.com/ADUfwuTPNEPdN7CbEZVrVjdC"></a></li>');

        $('.footer .footer-widget .social-links li a, .share-link li:last-child a').addClass('tagetModified').attr('target', '__blank');


        $('.sign-up-container h2').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Subscribe');
          }
        });
        $('.sign-up-container .signup-text p').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Join our mailing list for the latest news and member-only deals.');
          }
        });


        var $video = $("#click"), //jquery-wrapped video element
          mousedown = false;

        // $video.last().autoplay = true;

        $video.on('mousedown', function () {
          mousedown = true;
        });

        $(window).on('mouseup', function () {
          mousedown = false;
        });

        $video.on('play', function () {
          $video.attr('controls', '');
          $('.playpause').addClass('hide');
        });

        $video.on('pause', function () {
          if (!mousedown) {
            $video.removeAttr('controls');
          }
          $('.playpause').removeClass('hide');
        });


        $('.video').parent().click(function () {
          if ($(this).children(".video").get(0).paused) {
            $(this).children(".video").get(0).play();
          }
        });



        // video top after header
        $('[data-pagetype="product"] .app-header').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).append(`<div class="video-main ">
            <video width="100%" height="500" id="click" id="videoeingle" class="video desktop-video" controls muted autoplay="autoplay">
            <source src="https://media.graphassets.com/LxNHQxaVSyir2fdZmfE0" class="mobile-video" type="video/mp4">
          </video>

            <video width="100%" height="500" id="click" id="videoeingle" class="video mobile-video" controls muted autoplay="autoplay">
            <source src="https://media.graphassets.com/dkVndYIcSR21NyKU5Bvf" class="mobile-video" type="video/mp4">
          </video>
          <span class="videoicon playpause hide"><img src="https://static.techsembly.com/QkX6Hzbu2V6iqknHqzswDCWJ" alt="video"></span>
            </div>
            `);
          }
        });

        // edit icon change
        $(".edit-address [alt='edit']img:not(.modified), .edit-product-items a.edit-item-new [alt='edit']img:not(.modified), a.recipent-info-edit  [alt='edit-recipient']img:not(.modified), .unit-attributes-cont a.edit-item-new [alt='edit']img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/ZbNfqAMajM3B43xDH1LZMpMz");


        $(".item-desc .badge-stroke img:not(.modified), .pro-badge img.badge-img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/G1JSFz4QYRgjaYErhDA7V9a6");


        $('.order-summary-cont .card-body-inner').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).append(`<p class="order-summary-text">Please note this login is for the Mandarin Oriental gifting platform only.</p>`);
          }
        });


        $('.custom-container.cart-container .cart-login-cont app-login-cart .btn.submit').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html("sign in & checkout");
          }
        });

        $(".shipping-form-container .shipping-form .shipping-form-cont-inner button#continue-to-delivery-and-billing:not(.modifieded)").addClass('modifieded').html("continue to delivery");


        $('.payment-form-container .payments-methods-cont').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).append(`<h3 class="payment-heading">Card Details</h3>`);;
          }
        });


        $('.show-pay-details .form-group').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).append(`<h3 class="strip-bottom-text">We accept Visa, Mastercard and American Express.</h3>`);;
          }
        });



        $(".pass-eye [alt='eye']img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/qeRWEPZmvsTvqMGHMpCabpW4");




        $('.payment-form .left-panel').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).append(`<div class="complete-note"><h3>Note:</h3> <p><span>I agree to the MOHG Data Privacy Policy.</span>
            By completing the payment process to purchase my Gift Card, I acknowledge and agree that the order information I submit is accurate and that my Gift Card purchase will be subject to the Conditions of Use.</p>
            <h4>Customer Support:</h4>
            <p>US toll-free : <a href="tel:+1 888-559-0772">+1 888-559-0772</a></p>
            <p>UK : <a href="tel:+44 20 4571 3529">+44 20 4571 3529</a></p>
            <p>Hong Kong : <a href="tel:+852 2592 5942">+852 2592 5942</a></p>
            <p>Email: <a href="mailto:mohg-giftcards@techsembly.support" class="text-decoration-underline">mohg-giftcards@techsembly.support</a></p>
            </div>`);
          }
        });


        $(".product-detail-container .product-order .info-text:not(.modifieded)").addClass('modifieded').html("A Mandarin Oriental Gift Card can be used towards hotel accommodations, spa services, dining, boutique purchases and leisure activities at any of our participating properties and venues. Guests within the U.S. can also redeem within the Shop M.O. online store.");

        $('.checkout-container.complete .msg-cont .msg-inner').each(function () {
          if (!$(this).hasClass('msg-inner-modified')) {
            $(this).addClass('msg-inner-modified');
            $(this).html('Please donâ€™t hesitate to contact our Customer Support team if you have any questions in relation to your order. We hope you enjoy your purchase.');
          }
        });


        $('.personalise .delivery-form textarea.form-control.delivery-fields').each(function () {
          if (!$(this).hasClass('modified-msg-placeholder')) {
            $(this).addClass('modified-msg-placeholder');
            $(".personalise .delivery-form textarea.form-control.delivery-fields").attr("placeholder", "Write a Gift Message (optional)");
          }
        });

        $(".custom-container.cart-container .cart-right-cont .cart-login-cont form input.form-control[formcontrolname='email']").each(function () {
          if (!$(this).hasClass('modified-email-placeholder')) {
            $(this).addClass('modified-email-placeholder');
            $(".custom-container.cart-container .cart-right-cont .cart-login-cont form input.form-control[formcontrolname='email']").attr("placeholder", "user@example.com");
          }
        });


        $(".custom-container.cart-container .cart-right-cont .cart-login-cont form input.form-control [formcontrolname='email']#login-email").each(function () {
          if (!$(this).hasClass('modified-email-placeholder')) {
            $(this).addClass('modified-email-placeholder');
            $(".custom-container.cart-container .cart-right-cont .cart-login-cont form input.form-control [formcontrolname='email']").attr("placeholder", "user@example.com");
          }
        });



        $(".total-cont.section-cont .col-3.px-0.pr-3:not(.modified)").addClass('modified').html("Total");


        $("app-buy-now-login .modal-popup .modal-body [alt='Hotel logo']img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/3p5RJ1jJrohnZ3wgSjAJN7xV");
        $('.guestCheckout-form input#guest-email').each(function () {
          if (!$(this).hasClass('modified-paceholder')) {
            $(this).addClass('modified-paceholder');
            $(".guestCheckout-form input#guest-email").attr("placeholder", "user@gmail.com");
          }
        });

        $('.cart-login-form-modal input[type="email"]').each(function () {
          if (!$(this).hasClass('modified-paceholder')) {
            $(this).addClass('modified-paceholder');
            $(".cart-login-form-modal input[type='email']").attr("placeholder", "user@gmail.com");
          }
        });

        $('.cart-login-form-modal #login-btn').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('sign in & checkout');
          }
        });


        $('.cart-right-cont .cart-login-cont .guest-form-cont').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).append('<p class="guest-checkout-text">If you do not have an account, please checkout as guest. You will be given the option to create an account later in the checkout process.</p>');
          }
        });

        $('.options-container').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(".option-container:first-child .customcheck input[type='radio']")[0].click();
          }
        });

        $(".checkout .nav-checkout-delivery .shipping-form-container .left-panel form h1.breadcrumb-heading:not(.modified)").addClass('modified').html("Select Delivery Method");

        $('.checkout .nav-checkout-delivery .shipping-form-container .left-panel .shipping-totals-cont .total-row .total-caption:contains("Shipping")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Shipping');
          }
        });

        $('.checkout .nav-checkout-delivery .shipping-form-container .left-panel .shipping-totals-cont .total-row .total-caption:contains("Subtotal:")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Subtotal');
          }
        });


        $('.payment-form-container .card-details-cont h2.section-heading:contains("Details")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Billing Details');
          }
        });

        $('.checkout-container.complete .section-cont.customer-note').each(function () {
          if (!$(this).hasClass('total-delivery-modified')) {
            $(this).addClass('total-delivery-modified');
            var firstPart = $(this).find('br')[0].previousSibling.nodeValue;
            var newTxt = 'Thank you for your order. We will notify you by email when your item(s) have been dispatched.';
            $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function (_, html) {
              return html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">' + newTxt + '</div>')
            });

            var secondPart = $(this).find('.del-content').html();

            $(this).html('<div class="salutation-cont d-none">' + firstPart + '</div><div class="del-content pt-0">' + secondPart + '</div>');
          }
        });


        $('.checkout .checkout-container.complete .card-footer a:contains("Customer Support")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('href', 'mailto:mohg-giftcards@techsembly.support');
          }
        });

        $(".footer-widget:first-child ul:nth-child(4) li a:not(.modified)").addClass('modified').attr("href", "https://www.mandarinoriental.com/en/contact-information#regional-sales-offices?_ijcid=1670314378448|3.1383335731.1664259508620.5a52f063");

        $('.order-shipping-container .scheduled-dtime:not(.modified-schedule-time)').addClass('modified-schedule-time').prepend('<h4 class="mo-sechdule-heading">When would you like it sent?</h4>')


        // $('.btns-cont .btn-secondary.action-btn:contains("CANCEL")').each(function () {
        //   if (!$(this).hasClass('text-modified')) {
        //     $(this).addClass('text-modified');
        //     $(this).html('Cancel');
        //   }
        // });
        // $('.btns-cont .btn-primary.action-btn').each(function () {
        //   if (!$(this).hasClass('text-modified')) {
        //     $(this).addClass('text-modified');
        //     $(this).html('Yes');
        //   }
        // });

        $('.delivery-methods-cont .vendor-cards-holder .card .order-item a.edit-item-new').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            var newTxt = 'Edit Gift Card';
            $("a.edit-item-new:contains('Edit Product')").html(function (_, html) {
              return html.replace(/(Edit Product)/g, '<span class="updated-edit-string">' + newTxt + '</span>');
            });
          }
        });

        $('.custom-container.checkout .order-totals-container .sub-total-holder .sub-total-desc:contains(" Promo Total:")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html(' Promo Total');
          }
        });
        $('.card-details-cont.section-cont span.checkbox-note').each(function () {
          if (!$(this).hasClass('modified-checkBox-text')) {
            $(this).addClass('modified-checkBox-text');
            $(this).html(' Same as Shipping Address');
          }
        });


        // Phone Number Add
        $('.footer a:contains("+1 888-559-0772")').each(function () {
          var string_phone = $(this).html();
          var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
          var phone_no = "tel:" + result_phone;
          if (!$(this).hasClass('tel-link')) {
            $(this).addClass('tel-link');
            $(this).attr('href', phone_no);
          }
        });

        $('.footer a:contains("Gift Card Support")').each(function () {
            var string_phone = 'mohg-giftcards@techsembly.support';
            var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
            var phone_no = "mailto:" + result_phone;
            if (!$(this).hasClass('mail-link')) {
              $(this).addClass('mail-link');
              $(this).attr('href', phone_no);
          }
        });

        // mobile carousel text add
        $('.product-carousel-mob .owl-dots').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).prepend(`<div class="dot-top-heading"><p>Select your eGift Card Design</p>
            </div>
            `);
          }
        });

        // end
      });
    });
  })();
