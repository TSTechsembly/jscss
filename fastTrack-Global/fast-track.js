function waitForElm(selector) {
	return new Promise((resolve) => {
		if (document.querySelector(selector)) {
			return resolve(document.querySelector(selector));
		}
		const observer = new MutationObserver((mutations) => {
			if (document.querySelector(selector)) {
				resolve(document.querySelector(selector));
				observer.disconnect();
			}
		});
		observer.observe(document.body, {
			childList: true,
			subtree: true,
		});
	});
}
const d = new Date();
let copyrightYear = d.getFullYear();

function addcopyrightText() {
	var copyrightHtml = '<div class="copy-bottom container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-12 ts-copyright"><span><a href="https://techsembly.com/" target="_blank">Techsembly </a>&copy;</span><span class="ml-1">' + copyrightYear + "</span></div></div></div></div>";
	var elem = $(".checkout-footer")[0];
	elem.insertAdjacentHTML("afterend", copyrightHtml);
}

function addcopyrightToComplete() {
	var copyrightHtml = '<div class="card-footer-right">© ' + copyrightYear + " Triumph Hotel</div>";
	var elem = $(".order-confirm-wrapper .card-footer .col-12")[0];
	elem.insertAdjacentHTML("beforeend", copyrightHtml);
}

(function () {
	var script = document.createElement("SCRIPT");
	script.src = "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js";
	script.type = "text/javascript";
	document.getElementsByTagName("head")[0].appendChild(script);
	// Poll for jQuery to come into existence
	var checkReady = function (callback) {
		if (window.jQuery) {
			callback(jQuery);
		} else {
			window.setTimeout(function () {
				checkReady(callback);
			}, 20);
		}
	};
	checkReady(function ($) {
		$(function () {
			setTimeout(function () {
				$("meta[name=viewport]").attr("content", "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0");
			}, 800);
				

			waitForElm("app-footer-checkout").then((elm) => {
				addcopyrightText();
			}).catch((error) => { });

			waitForElm(".order-confirm-wrapper .card-footer .col-12").then((elm) => {
				addcopyrightToComplete();
			}).catch((error) => { });

		

			waitForElm(".footer .footer-widget").then((elem) => {
				$('.footer .footer-widget .widget-title:contains("KEEP IN TOUCH")').each(function () {
					$(this).html("Follow Us");
				});
			}).catch((error) => { });

			waitForElm('.mat-form-field-label').then((elm) => {
				$(".mat-form-field-label").each(function(){
				  if($(this).attr('aria-owns')) {
					$(this).removeAttr('aria-owns');
				  }
				}); 
			  }).catch((error) => { });

			  waitForElm('app-product-detail .add-to-basket-cont.basket-static').then((elm) => {
			  $("app-product-detail .add-to-basket-cont.basket-static").append('<p class="after-form">Monetary gift cards have no expiration date and can be used on-property. Please check Terms & Conditions for where redeemable.</p>');
			}).catch((error) => { });

			waitForElm('form.guestCheckout-form.v3').then((elm) => {
				$('form.guestCheckout-form.v3 .btn.submit').text('Continue to delivery');
			}).catch((error) => { });
			

			waitForElm('.cart-login-form-modal input[type="email"]').then((elem) => {
				$('.cart-login-form-modal input[type="email"]').each(function () {
					$(".cart-login-form-modal input[type='email']").attr("placeholder", "user@gmail.com");
				});
			}).catch((error) => { });



			

			waitForElm("app-confirm-buy-now-with-cart button.btn-guest").then((elem) => {
				$("app-confirm-buy-now-with-cart button.btn-guest").addClass("modifedText").text("BUY NOW");
			}).catch((error) => { });

			waitForElm(".order-shipping-container .scheduled-dtime").then((elem) => {
				$(".order-shipping-container .scheduled-dtime:not(.modified-schedule-time)").addClass("modified-schedule-time").prepend('<h4 class="mo-sechdule-heading">Choose a Specific Date and Time to Send or Deliver Instantly</h4>');
			}).catch((error) => { });

			waitForElm(".sign-up-section .sign-up-container .sign-up-form input").then((elem) => {
				$(".sign-up-section .sign-up-container .sign-up-form input[type='email']").attr('placeholder', 'Your email address');
			}).catch((error) => { });

			waitForElm(".checkout-container.complete .section-cont.customer-note").then((elem) => {
				$(".checkout-container.complete .section-cont.customer-note").each(function () {
					$(".checkout-container.complete .section-cont.customer-note").addClass("total-delivery-modified");
					var firstPart = $(".checkout-container.complete .section-cont.customer-note").find("br")[0].previousSibling.nodeValue;
					var newTxt = "Thank you for your order. We will notify you by email when your item(s) have been dispatched.";
					$(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function (_, html) {
						return html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">' + newTxt + "</div>");
					});
				});
				$(".customer-note").each(function () {
					var targetElement = $(".customer-note.section-cont");
					var textNode = targetElement.contents().filter(function () {
						return this.nodeType === 3 && this.nodeValue.includes("Hi");
					});
					if (textNode.length > 0) {
						var updatedText = textNode[0].nodeValue.replace("Hi", "Dear");
						textNode[0].nodeValue = updatedText;
					}
				});
			}).catch((error) => { });

			waitForElm(".complete .msg-inner").then((elem) => {
				$(".msg-inner").html("Please don’t hesitate to contact our Customer Support team if you have any questions in relation to your order. We hope you enjoy your purchase.");
			}).catch((error) => { });

			waitForElm("footer .powered-by .powered-link-cont").then((elm) => {
				$("footer .powered-by .powered-link-cont a:not(.tagetModified)").addClass("tagetModified").attr("target", "_blank");
			}).catch((error) => { });


			waitForElm(".personalise .delivery-form textarea.form-control.delivery-fields").then((elm) => {
				$(".personalise .delivery-form textarea.form-control.delivery-fields").attr('placeholder', 'Write a Gift Message (optional)');
			}).catch((error) => { });


			waitForElm(".left-panel .payment-form-container .input-cont input").then((elm) => {
				$('.left-panel .payment-form-container .input-cont input[type="password"]').attr('placeholder', 'Password');
			}).catch((error) => { });

			waitForElm(".left-panel .payment-form-container .section-heading").then((elm) => {
				$(".left-panel .payment-form-container .section-heading:contains(Details)").text('Billing Details')
			}).catch((error) => { });

			  waitForElm('.personalise-form select').then((elem) => {
				$('.personalise-form select option').each(function () {
				  if (!$(this).hasClass('modified')) {
					$(this).addClass('modified');
					$('.personalise-form select option:contains("Customize (add additional amount to card)")').text("Customize your card  value");
				  }
				});
			  }).catch((error) => { });

			  waitForElm('.custom-container.checkout-container.complete .card.order-confirm-wrapper .card-footer a').then((elem) => {
			  $('.custom-container.checkout-container.complete .card.order-confirm-wrapper .card-footer a:contains("Customer Support")').each(function(){
				var string_email_add = '#';
				var email_link_checkout = "mailto:" + string_email_add;
				if(!$(this).hasClass('email-link')){
				  $(this).addClass('email-link');
				  $(this).attr('href', email_link_checkout);
				}
			  });
			}).catch((error) => { });


		waitForElm(".product-detail-container .personalise-form .info-text").then((elem) => {
			$(".product-detail-container .personalise-form .info-text").addClass("textModified").text("If adding multiple gift cards to your cart, this message will apply to all gift cards to one single shipping address. If you would like unique messages for each card, you must add each one separately to the shopping cart.");
		}).catch((error) => { });

		waitForElm(".product-detail-container .personalise-form .delivery-form").then((elem) => {
		$('.product-detail-container .personalise-form .delivery-form .title').text('Recipient Details');
		$('.product-order .tab-pane h1.title+p').text('Gift cards are sent digitally, please share the name and email of the recipient.');
	}).catch((error) => { });

	waitForElm("button#guest-btn").then((elem) => {
	$('button#guest-btn').text('Continue to delivery');
}).catch((error) => { });
		
		
			// start set interval
			setInterval(function () {
				//     // type here code
				$(".guestCheckout-form input#guest-email").each(function () {
					if (!$(this).hasClass("modified-paceholder")) {
						$(this).addClass("modified-paceholder");
						$(".guestCheckout-form input#guest-email").attr("placeholder", "user@gmail.com");
					}
				});
				$(".custom-container.cart-container .cart-right-cont .cart-login-cont form input.form-control#login-email").each(function () {
					if (!$(this).hasClass("modified-paceholder-examp")) {
						$(this).addClass("modified-paceholder-examp");
						$(this).attr("placeholder", "user@example.com");
					}
				});
		

			}, 1000);
			// end set interval
		});
	});
})();