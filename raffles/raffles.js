function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }
    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });
    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

(function () {
  var script = document.createElement("SCRIPT");
  script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
  script.type = 'text/javascript';
  document.getElementsByTagName("head")[0].appendChild(script);

  // Poll for jQuery to come into existence
  var checkReady = function (callback) {

    if (window.jQuery) {
      callback(jQuery);
    }
    else {
      window.setTimeout(function () { checkReady(callback); }, 20);
    }
  };




  function addcopyrightText() {
    var copyrightHtml = '<div class="container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-6 ts-copyright"><span><a href="https://techsembly.com/" target="_blank">Techsembly&copy;</a></span><span class="ml-1">2022</span></div><div class="col-6 tc-rights-reserved text-right pr-0"></div></div></div></div>'
    var elem = $(".checkout-footer")[0]
    elem.insertAdjacentHTML('afterend', copyrightHtml)
  }


  function addcopyrightToComplete() {
    var copyrightHtml = '<div class="card-footer-right"><a href="#">© 2022 Raffles Hotels & Resorts</a></div>'
    var elem = $(".order-confirm-wrapper .card-footer .col-12")[0]
    elem.insertAdjacentHTML('beforeend', copyrightHtml)
  }
  function addBackToHomeBtn() {
    var copyrightHtml = '<div class="d-flex justify-content-center items-center backToHomeBtn"><a href="https://raffles.techsembly.com/">BACK TO HOME</a></div>'
    var elem = $(".order-confirm-wrapper .card-footer")[0]
    elem.insertAdjacentHTML('beforeend', copyrightHtml)
  }


  // Now lets do something
  checkReady(function ($) {
    $(function () {
      $('head').append('<link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;500;700;900&display=swap" rel="stylesheet"></link>');
      $('head').append('<link href="https://fonts.googleapis.com/css2?family=Noto+Serif+Display:ital,wght@0,300;0,400;1,300&display=swap" rel="stylesheet"></link>');

      waitForElm('app-footer-checkout').then((elm) => {
        addcopyrightText()
      }).catch((error) => { });

      waitForElm('.order-confirm-wrapper .card-footer .col-12').then((elm) => {
        addcopyrightToComplete()
      }).catch((error) => { });

      waitForElm('.order-confirm-wrapper .card-footer').then((elm) => {
        addBackToHomeBtn()
      }).catch((error) => { });


      $("app-product-listing .mob-btn-group").parent().addClass('product-listing-head');
      $("button").closest(".form-group").addClass('continue-billing-holder');
      // console.log($("#continue-to-delivery-and-billing").parent())
      $("app-product-listing .product-listing-head .mob-btn-group #refineBtn").html("Filters");


      var segment_str = window.location.pathname; // return segment1/segment2/segment3/segment4
      var segment_array = segment_str.split( '/' );
      var last_segment = segment_array.pop();
      console.log(last_segment);

      $("#curency").on('change', function() {
        $( "#update-preferences-header" ).click();
      });

      setInterval(function () {

        $('meta[name="viewport"]:not(.modified)').addClass('modified').attr("content", "width=device-width, initial-scale=1, maximum-scale=1");

        $('.btn#update-preferences-header:not(.modified)').addClass('modified').html('Update your preferred currency');


        $('.footer-page-container .default-breadcrumb').each(function () {
          if (!$(this).closest(".footer-page-container").hasClass('d-none')) {
            $(this).closest(".footer-page-container").addClass('d-none')
          }
        });

        // home page

        $("[data-pagetype='home'] .logo img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/ApEfwbDuBkS26Bn3mKhRGVQ2");
        $("[data-pagetype='home'] .head-links .wishlist-heart img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/vAEx3S5LuwtFg4dXV5gz9oV9");
        $("[data-pagetype='home'] .head-links .add-to-basket img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/bm3PqoY4B4mzVp8soVzHwTag");
        $("[data-pagetype='home'] .head-links .profile-icon img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/JP5EXCe2AqMnbnoqzKpKBVRN");

        $("[data-pagetype='home'] .head-links .mob-search-link [alt='search image']img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/jYFZvQruSz8yGG2vXt6DCzth");

        $(".btn-search [alt='search image']img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/fzSgsSnn2jMNpQJcXQWyQML5");
        $('.promo-section:nth-child(2)').addClass('celebration-main')

        $(".head-links .wishlist-heart img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/T1EsL5WTxB38gQzXWbRPG2W1");
        $(".add-to-basket a.cart-bag  [alt='cart']img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/n3nJAvt2sXykeoDrhyhSf23G");
        $(".head-links .profile-icon img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/p8cvSYNyaFMrmqKmYEiWK3f1");
        $('.home-banner .banner-heading:not(.modified)').addClass('modified').append('<a href="/Experiences" rel="nofollow">EXPLORE EXPERIENCES</a>');
        $('app-product-info .product-holder .product-inner-detail-wrapper button.btn.btn-default.btn-outline:not(.modified)').addClass('modified').html('Purchase');
        $('.sign-up-section .sign-up-container:not(.modified)').addClass('modified').append('<a href="https://www.raffles.com/offers/email-offers/" target="_blank" class="signup-footer">SIGN UP</a>');

        $('.footer .custom-container:not(.modified)').addClass('modified').append(' <div class="rf-footer-logo"><a href="https://www.raffles.com/offers/email-offers/"> <img src="https://static.techsembly.com/QgHKaDMCLDV1tB3Vz1YrZxtd" alt=""></a></div>');


        // rf-experence
        $('div').children().each(function () {
          if ($(this).hasClass('rf-experence')) {
            $(this).parent().addClass('modified rf-experence-main');
          }
        });

        // wellness
        $('div').children().each(function () {
          if ($(this).hasClass('wellness')) {
            $(this).parent().addClass('modified rf-wellness-main');
          }
        });

         // rf-celebration
         $('div').children().each(function () {
          if ($(this).hasClass('celebra-text')) {
            $(this).parent().addClass('modified celebra-text-main');
          }
        });

        // rf-art
        $('div').children().each(function () {
          if ($(this).hasClass('rf-art')) {
            $(this).parent().addClass('modified rf-art-main');
          }
        });
        // Gastronomy
        $('.home-intro-container').children().each(function () {
          if ($(this).hasClass('gastronomy-text')) {
            $(this).parent().addClass('modified gastronomy-text-main');
          }
        });

        // rf-celebration
        $('div').children().each(function () {
          if ($(this).hasClass('art-text')) {
            $(this).parent().addClass('modified art-text-main');
          }
        });

        //rf-footer-logo
        $('div').children().each(function () {
          if ($(this).hasClass('rf-footer-logo')) {
            $(this).parent().addClass('modified rf-footer-logo-main');
          }
        });

        $('div').children().each(function () {
          if ($(this).hasClass('rf-category')) {
            $(this).parent().addClass('modified rf-category-full');
          }
        });

        $('div').children().each(function () {
          if ($(this).hasClass('rf-services')) {
            $(this).parent().addClass('modified rf-category-full');
          }
        });

        $('.promo-section .promo-box .promo-content').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).append('<span class="rf-multi-discover">DISCOVER</span>');
          }
        });


        $('.nav-checkout-delivery .shipping-form-container .breadcrumb-heading').each(function () {
          if (!$(this).hasClass('fnd')) {
            $(this).addClass('fnd');
            $(this).html('Select Delivery Method');
          }
        });


        $('.footer .footer-widget .social-links li a img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/AxygFqSfRtd5mG6dvPgZdE6i');
        $('.footer .footer-widget .social-links li a img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/puneiwvc1HwShdcMC4vcq8Sb');
        // $('.footer .footer-widget .social-links li a img[alt="twitter"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/ZtToqqknNEEw847o1hjWNpA4');

        $('a.mob-search-link img[alt="search image"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/brMsGowbJyj5HGZWzubripDZ');

        $('[data-pagetype="home"] a.mob-search-link img[alt="search image"]:not(.modifiedhome)').addClass('modifiedhome').attr('src', 'https://static.techsembly.com/yNf1YFbkDZv9jjyYPeeQvS5L');



        $('[data-pagetype="home"] .mob-top-links img[alt="cart"]:not(.modifiedhomeCart)').addClass('modifiedhomeCart').attr('src', 'https://static.techsembly.com/mBVHBvze4MZvyTYxkswZStUq');

        // Listing Page text

        setTimeout(function () {
          if (last_segment=='gastronomy'){
            $('.products-holder-main:not(.modified)').addClass('modified').find('.banner-img').after('<div class="listing-banner-detail"><h4>Gastronomy by Raffles </h4><p>Journeys that travel to the heart of good food in good company – always a moment of theatre and a satisfying alchemy of flavour, ritual and occasion.</p></div>');
          }
          if (last_segment=='celebration'){
            $('.products-holder-main:not(.modified)').addClass('modified').find('.banner-img').after('<div class="listing-banner-detail"><h4>Celebration by Raffles </h4><p>Celebrating togetherness with tailored adventures, space to live abundantly and generous care that recognises every member of the tribe – welcome to the century-old tradition of revelling at Raffles</p></div>');
          }
          if (last_segment=='wellbeing'){
            $('.products-holder-main:not(.modified)').addClass('modified').find('.banner-img').after('<div class="listing-banner-detail"><h4>Wellness by Raffles </h4><p>Sumptuous wellness sojourns crafted with expert practitioners to restore life’s balance through treatments & therapies, movement, nutrition, cultural learnings and the opportunity to immerse in nature</p></div>');
          }
          if (last_segment=='arts-culture'){
            $('.products-holder-main:not(.modified)').addClass('modified').find('.banner-img').after('<div class="listing-banner-detail"><h4>Art & Culture by Raffles </h4><p>Destinations unveiled in your own time and way – with Raffles, where ideas are born and history is made, your guide and compass to true cultural immersion</p></div>');
          }

          $(".sign-up-container h2").each(function () {
            if ($(this).children().length == 0) {
              $(this).addClass('product-info-page')
              $(this).html($(this).text().replace('Inspiration to', '<span class="pro-modified-italic">INSPIRATION to</span>'));
            }
          });

          const expression = /(iPhone|iPod|iPad)/i;
          if (expression.test(navigator.platform)) {
            $('.product-info-container .product-video').addClass('product-apple-device')
          } else {
            $('.product-info-container .product-video').addClass('other-device')
          }

        }, 1000);

        // product detail page
        $('.product-holder .personalise .col-lg-10').addClass('col-lg-12 p-0').removeClass('col-lg-10');
        $('.modal-close.modal-close-btn.d-md-none').addClass('d-block').removeClass('d-md-none');

        $('#cart-notification .modal-container.login-modal .modal-body .footer-btns .btn-guest:contains("Continue to buy now")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Buy Now');
          }
        });

        // badge change
        $(".item-desc .badge-stroke img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/GBTjziBSpzKXGTWVo46bviCy");
        $(".pro-badge img.badge-img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/GBTjziBSpzKXGTWVo46bviCy");

        // edit icon change
        $(".edit-address [alt='edit']img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/U3NoDeNFASQsamZpX66KVQuS");

        $(".edit-product-items a.edit-item-new  [alt='edit']img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/U3NoDeNFASQsamZpX66KVQuS");


        $("a.recipent-info-edit  [alt='edit-recipient']img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/U3NoDeNFASQsamZpX66KVQuS");


        $(".unit-attributes-cont a.edit-item-new [alt='edit']img:not(.modified)").addClass('modified').attr("src", "https://static.techsembly.com/U3NoDeNFASQsamZpX66KVQuS");


        $('.checkout-container.complete .section-cont.customer-note').each(function () {
          if (!$(this).hasClass('total-delivery-modified')) {
            $(this).addClass('total-delivery-modified');
            var firstPart = $(this).find('br')[0].previousSibling.nodeValue;
            var newTxt = 'Thank you for your order. We will notify you by email when your item(s) have been dispatched. ';
            $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function (_, html) {
              return html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">' + newTxt + '</div>')
            });

            var secondPart = $(this).find('.del-content').html();

            $(this).html('<div class="salutation-cont d-none">'+firstPart+'</div><div class="del-content pt-0">'+secondPart+'</div>');
          }
        });

        $('.checkout-container.complete .shipping-data .shipping-row div:contains("Address")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Delivery Address:');
          }
        });

        $('.checkout-container.complete .msg-cont .msg-inner').each(function () {
          if (!$(this).hasClass('msg-inner-modified')) {
            $(this).addClass('msg-inner-modified');
            $(this).html('Please donapos;t hesitate to contact our Customer Support team if you have any questions in relation to your order. We hope you enjoy your purchase.');
          }
        });


        if (!$('.promo-section').last().hasClass('promo-bottom')) {
          $('.promo-section').last().addClass('promo-bottom');
        }

        if (!$('.home-intro-container').last().hasClass('rf-learn-main')) {
          $('.home-intro-container').last().addClass('rf-learn-main');
        }


        if (!$('.home-slider').first().hasClass('modified gastronomy')) {
          $('.home-slider').first().addClass('modified gastronomy');
        }


        if (!$('.home-slider').last().hasClass('modified Culture')) {
          $('.home-slider').last().addClass('modified Culture');
        }


        $('.powered-link-cont').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).prepend('<span class="mobile-footer d-lg-none">© Copyright 2022 Raffles Hotels & Resorts</span>');
          }
        });

        $('.footer .footer-widget .widget-content ul.social-links li a').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('target', '_blank');
          }
        });
        $('.footer .mobile-footer .footer-widget ul.social-links li a').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).attr('target', '_blank');
          }
        });

        $('app-product-detail .delivery-form p.info-text.text-left:contains("If adding multiple gift cards to your cart,")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Your message will apply to all Gift Cards to one single shipping address. If you would like unique messages for each card, please add each one separately to the shopping bag. ');
          }
        });

        $('app-product-detail #add-to-basket-btn:contains("Add to cart")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('ADD to bag');
          }
        });

        $('.delivery-methods-cont .vendor-cards-holder .card .order-item a.edit-item-new').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            var newTxt = 'Edit Gift Card';
            $("a.edit-item-new:contains('Edit Product')").html(function (_, html) {
              return html.replace(/(Edit Product)/g, '<span class="updated-edit-string">' + newTxt + '</span>');
            });
          }
        });

        $('.section-heading:contains("Details")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Billing Details');
          }
        });

        $('.container.user-container.pt-4 .user-heading:contains("My TS Card Balance")').each(function () {
          if (!$(this).closest('.user-container').hasClass('ts-card')) {
            $(this).closest('.user-container').addClass('ts-card d-none');
          }
        });

        // placeholder text change
        $('.guestCheckout-form input#guest-email').each(function () {
          if (!$(this).hasClass('modified-paceholder')) {
            $(this).addClass('modified-paceholder');
            $(".guestCheckout-form input#guest-email").attr("placeholder", "user@gmail.com");
          }
        });


        $('.cart-login-form-modal input[type="email"]').each(function () {
          if (!$(this).hasClass('modified-paceholder')) {
            $(this).addClass('modified-paceholder');
            $(".cart-login-form-modal input[type='email']").attr("placeholder", "user@gmail.com");
          }
        });

        // text add div tag
        $("h1.banner-heading div:first-child").each(function () {
          if ($(this).children().length == 0) {
            $(this).addClass('modified')
            $(this).html($(this).text().replace('by', '<span class="modified-lowerCase">by</span>'));
          }
        });

        // promo first and last
        if (!$('.promo-section').first().hasClass('modified promo-first')) {
          $('.promo-section').first().addClass('modified promo-first');
        }

        if (!$('.promo-section').last().hasClass('modified promo-last')) {
          $('.promo-section').last().addClass('modified promo-last');
        }


        $(".promo-section.promo-first .promo-box:first-child .promo-desc").each(function () {
          if ($(this).children().length == 0) {
            $(this).addClass('pro-modified')
            $(this).html($(this).text().replace('wellness', '<span class="pro-modified-italic">wellness</span>'));
          }
        });
        $(".promo-section.promo-first .promo-box:last-child .promo-desc").each(function () {
          if ($(this).children().length == 0) {
            $(this).addClass('pro-modified')
            $(this).html($(this).text().replace('meditation', '<span class="pro-modified-italic">meditation</span>'));
          }
        });

        $(".promo-section.promo-last .promo-box:first-child .promo-desc").each(function () {
          if ($(this).children().length == 0) {
            $(this).addClass('pro-modified')
            $(this).html($(this).text().replace('lost', '<span class="pro-modified-italic pro-dinner">lost</span>'));
          }
        });
        $(".promo-section.promo-last .promo-box:last-child .promo-desc").each(function () {
          if ($(this).children().length == 0) {
            $(this).addClass('pro-modified')
            $(this).html($(this).text().replace('affair', '<span class="pro-modified-italic d-inline-block">affair</span>'));
          }
        });

        $('.book-section .product-info-dream').each(function () {
          if (!$(this).closest('.book-section').hasClass('dreams-info')) {
            $(this).closest('.book-section').addClass('dreams-info');

          }
        });

        $('.checkout-container.complete .card-body .total-cont .col-3.pr-3:contains("Total:")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Total');
          }
        });

        $('.custom-container.checkout .order-totals-container .sub-total-holder .sub-total-desc:contains(" Promo Total:")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html(' Promo Total');
          }
        });

        $('.checkout .checkout-container.complete .card-footer a:contains("Customer Support")').each(function(){
            if(!$(this).hasClass('modified')){
              $(this).addClass('modified');
              $(this).attr('href','mailto:raffles@techsembly.com');
            }
        });

        $(".footer .footer-widget:last-child .widget-title:not(.modified)").addClass("modified").addClass('modified').html("STAY TUNED");
        $('a.wishlistImage img').each(function () {
          if (!$(this).hasClass('appeared')) {
            $(this).addClass('appeared');
            $(this).attr('src', 'https://static.techsembly.com/T1EsL5WTxB38gQzXWbRPG2W1');
            if (($(this).attr('alt') == 'added-to-wishlist') && ($(this).attr('src') != 'https://static.techsembly.com/23VYnnX7kdDvduzSu2KcSxan')) {
              $(this).attr('src', 'https://static.techsembly.com/23VYnnX7kdDvduzSu2KcSxan');
            }
          }
        });

        $('.product-video').attr('playsinline', '');
        $('.product-video').attr('preload', 'auto');
        // $('.product-video').attr('controls', 'true');
        $('.product-video source').attr('type', 'video/mp4');
        $('.product-video.product-apple-device').attr('poster', 'https://static.techsembly.com/BetSRnppNMmV38VHHGu2FupY');

        $('.auth-user h2.address-heading.mt-2:contains("Or Add New Shipping Details")').each(function () {
          if (!$(this).hasClass('modified')) {
            $(this).addClass('modified');
            $(this).html('Or Add New Shipping Address');
          }
        });

        $('.footer a:contains("+1")').each(function(){
          var string_phone= $(this).html();
          var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
          var phone_no = "tel:" + result_phone;
          if(!$(this).hasClass('tel-link')){
            $(this).addClass('tel-link');
            $(this).attr('href', phone_no);
          }
        });

        $('.footer a:contains("Gift Card Support")').each(function(){
          var string_email= 'raffles@techsembly.com';
          var email_link = "mailto:" + string_email;
          if(!$(this).hasClass('email-link')){
            $(this).addClass('email-link');
            $(this).attr('href', email_link);
          }
        });





        // end
      });
      setInterval(function() {
      	$('#personalise-form-id:not(.giftcardpresent)').addClass('giftcardpresent').each(function() {
          $('.radio-holder span:contains("Physical")').parent().parent().remove();
          $('select option:contains("Customize (add additional amount to card)")').text("Customise your card value");
        }, 250);
      });
    });
  });


})();
