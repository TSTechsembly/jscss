function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }

        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}
function addPromoHead() {
  var promoHead = '<h2 class="section-title text-center text-uppercase">How To Purchase</h2>';
  var elemPromoHolder = $('.promo-section .promo-holder')[0];
  elemPromoHolder.insertAdjacentHTML('beforebegin', promoHead);
}
function addGuestCheckoutNote(){
  var checkoutNote = "<div class='checkout-note text-center w-100'>If you do not have an account please use guest checkout. You will be given the option to save account information later in the checkout process.<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}
function addcopyrightText(){
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="container-fluid"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-7 col-md-6 tc-rights-reserved pl-0"><a class="mr-1" href="https://techsembly.com/" target="_blank">Techsembly©</a>'+copyrightYear+
  '</div><div class="col-5 col-md-6 ts-copyright text-right px-0">All Rights Reserved</div></div></div>';
  var elem = $(".checkout-footer")[0];
  elem.insertAdjacentHTML('afterend', copyrightHtml);
}
function addcopyrightToComplete() {
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="card-footer-right"><a rhef="#">&#169; '+copyrightYear+
  ' Belmond</a></div>';
  var elem = $(".order-confirm-wrapper .card-footer .col-12")[0];
  elem.insertAdjacentHTML('beforeend', copyrightHtml);
}
function addInspireSection(){
  var inspireSection = '<section class="inspired-section bg-white">'+
    '<div class="col-12 px-1 px-lg-3 py-2">'+
      '<div class="col-12 pt-4 pb-0 px-3 inspired-section-cont">'+
        '<div class="col-12 px-0 pt-4 d-flex flex-wrap align-items-center inspired-section-cont-inner">'+
          '<div class="col-12 col-lg-3 pl-lg-0 pt-2 pt-lg-0 text-center text-lg-left">'+
            '<h2 class="inspired-heading">Be The First To Know</h2>'+
          '</div>'+
          '<div class="col-12 col-lg-6 text-center text-lg-left px-0 px-lg-3 pr-lg-0">'+
            '<p>Sign up for exclusive news, travel inspiration and offers, delivered to your inbox</p>'+
          '</div>'+
          '<div class="col-12 col-lg-3 text-center text-lg-right px-3 pb-2 pr-lg-0 pb-lg-0 btn-signup-cont">'+
            '<a class="btn btn-primary btn-sign-up mt-4 mt-lg-0" target="_blank" href="https://www.belmond.com/newsletter">Sign Up</a>'+
          '</div>'+
        '</div>'+
      '</div>'+
    '</div>'+
    '<div class="col-12 px-1 px-lg-3">'+
      '<div class="col-12 px-3">'+
        '<hr class="border-stroke border-below-transparent margin-auto">'+
      '</div>'+
    '</div>'+
  '</section>';
  var elemFooter = $('footer.footer')[0];
  elemFooter.insertAdjacentHTML('beforebegin', inspireSection);
}
(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
          font-family: 'theano_didotregular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7afce5700cc855f492e39bb8161b72fe5ea2cd95/belmond/fonts/theano-didot/theanodidot-regular-webfont.woff2') format('woff2');\
      }\
      @font-face {\
          font-family: 'Abel':\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/54abca7d07696176ac62624aaa81b6827f4da1cc/belmond/fonts/abel/Abel-Regular.ttf');\
      }\
    "));

    document.head.appendChild(newStyle);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">');
          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">');

          waitForElm('.promo-section').then((elm) => {
            addPromoHead();
          }).catch((error) => {});

          waitForElm('.cart-right-cont').then((elm) => {
            addGuestCheckoutNote();
          }).catch((error) => {});

          waitForElm('app-footer-checkout').then((elm) => {
            addcopyrightText()
          }).catch((error) => {});
          waitForElm('.order-confirm-wrapper .card-footer .col-12').then((elm) => {
            //addcopyrightToComplete()
          }).catch((error) => {});
          waitForElm('footer.footer').then((elm) => {
            addInspireSection();
          }).catch((error) => {})
          setTimeout(function () {
            $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
            $('.promo-holder .promo-box').append('<div class="col-12 promo-desc-cont py-4 pl-lg-5">'+
            '<h2 class="promo-title-new"></h2>'+
            '<h3 class="promo-sub"></h3>'+
            '<br>'+
            '<a class="promo-link text-uppercase" href="#">purchase now</a>'+
            '</div>');
            var promoTitle;
            var promoDesc;
            var promoLink;
            var promos = [];

            $('.promo-holder .promo-box').each(function () {
                var promoItem = {};
                promoTitle = $(this).find(".promo-title").html();
                promoDesc = $(this).find(".promo-desc").html();
                promoLink = $(this).children("a").first().attr('href');
                promoItem.Title = promoTitle;
                promoItem.Text = promoDesc;
                promoItem.Link = promoLink;

                promos.push(promoItem);
                //console.log(JSON.stringify(promos));
                $(this).find(".promo-title-new").html(promoTitle);
                $(this).find(".promo-sub").html(promoDesc);
                $(this).find(".promo-link").attr('href', promoLink);
            });
          }, 2000);

          setInterval(function () {

            $('header.header .shipping li .dropdown .dropdown-menu').each(function(){
              if(!$(this).hasClass('countries-dd')){
                $(this).addClass('countries-dd');
                $(this).append('<div class="countries-links-cont">'+
                '<div class="usa"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.belmond.com/usd/">UNITED STATES</a></div>'+
                '<div class="eur"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.belmond.com/eur/">EUROPE</a></div>'+
                '<div class="uk"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.belmond.com/gbp/">UNITED KINGDOM</a></div>'+
                '</div>');
              }
            });

            $('#sidebar ul.list-unstyled.user-links').each(function(){
              if(!$(this).hasClass('user-mob-links')){
                $(this).addClass('user-mob-links');
                $(this).append('<li>'+
                '<div class="d-flex align-items-center">'+
                '<div class="dropdown pref_dropdown w-100">'+
                '<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownMenuLink_mob">USD</a>'+
                '<div aria-labelledby="dropdownMenuLink" class="dropdown-menu countries-links-cont pt-0 mt-0" x-placement="top-start">'+
                '<div class="usa"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.belmond.com/usd/">UNITED STATES</a></div>'+
                '<div class="eur"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.belmond.com/eur/">EUROPE</a></div>'+
                '<div class="uk"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.belmond.com/gbp/">UNITED KINGDOM</a></div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</li>');
              }
            });

            $('header.header .head-links li a img[alt="user-profile"]').closest('.nav-item:not(.profile-cont)').addClass('profile-cont');
            $('header.header .head-links li a img[alt="wishlist"]').closest('.nav-item:not(.wishlist-cont)').addClass('wishlist-cont');
            $('header .mob-top-links li a img[alt="wishlist"]').closest('li:not(.wishlist-cont)').addClass('wishlist-cont');
            $('nav#sidebar ul li a img[alt="wishlist"]').closest('li:not(.wishlist-cont)').addClass('wishlist-cont');
            $('header.header .head-links li a img[alt="cart"]').closest('.nav-item:not(.cart-cont)').addClass('cart-cont');

            if(!$('header.header .toggle-menu-cont').hasClass('col-3')){
                $('header.header .toggle-menu-cont').addClass('col-3').removeClass('col-1 col-sm-2 col-md-2');
            }
            if(!$('header.header .menu-links-cont').hasClass('col-4')){
                $('header.header .menu-links-cont').addClass('col-4').removeClass('col-md-6 col-sm-5 col-7');
            }
            if($('header.header .logo').parent('div').hasClass('col-sm-5')){
                $('header.header .logo').parent('div').removeClass('col-4 col-sm-5');
                $('header.header .logo').parent('div').addClass('col-5 logo-cont');
            }

            $('header .mob-top-links li .dropdown-flag').closest('li:not(.d-none)').addClass('d-none');

            $('header .mob-top-links li img[alt="user-profile"]').closest('li:not(.d-none)').addClass('d-none');

            $('header.header .menu-holder').each(function(){
              if(!$(this).closest('.container').hasClass('wrapped')){
                $(this).closest('.container').addClass('wrapped');
                $(this).closest('.container').wrap('<div class="container-fluid main-menu-wrapper"></div>');
              }
            });

            $('header.header .menu-holder>li>div').each(function(){
              if(!$(this).children().length){
                $(this).addClass('empty');
              }
            });

            $('.sign-up-section').each(function(){
              if(!$(this).children('.sign-up-inner-cont').length){
                $(this).find('.sign-up-container').addClass('wrapped');
                $(this).wrapInner('<div class="sign-up-inner-cont"><div class="sign-up-inner p-3"></div></div>');
              }
            });

            $('.sign-up-section .sign-up-container').each(function(){
              if(!$(this).children('.privacy-note-cont').length){
                $(this).append('<div class="privacy-note-cont">By subscribing you agree to with our <a href="#" target="_blank"><b>Privacy Policy</b></a></div>');
              }
            });

            $(".sign-up-section .sign-up-container .sign-up-form input[type='email']").each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).attr('placeholder','Enter your email')
              }
            });


            $('.wrapper .featured-client:not(.d-none)').addClass('d-none');

            $('.product-container .breadcrumb-heading:contains("Search results")').each(function(){
              if (!$(this).closest('body').hasClass('search-results-page')) {
                $(this).closest('body').addClass('search-results-page');
              }
            });
            $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified')
                $(this).html('Purchase');
              }
            });

            $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
            $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');

            $('.product-detail-container .product-holder form .form-group .form-control[id="Select gift card value"] + .invalid-feedback > div').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Select gift card value');
              }
            });


            $('app-product-detail app-related-products[title="More items from"]').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).find('h3').html('More Like This');
              }
            });

            $('.cart-item.new .item-details .item-qty-cont').each(function(){
              if(!$(this).closest('.col-lg-2').hasClass('qty-col')){
                  $(this).closest('.col-lg-2').addClass('qty-col');
              }
            });

            $('.custom-container.cart-container .vendor-items-cont a img[alt="gift"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/sMZ1J4kWUJFeUHUtL1aV4AD6');

            $('.auth-user .checkout .shipping-form h1.breadcrumb-heading:contains("Shipping Details")').each(function() {
              if(!$(this).closest('.shipping-form').hasClass('shipp-details')){
                $(this).closest('.shipping-form').addClass('shipp-details');
                $(this).html('Choose Shipping Details');
              }
            });

            $('.shipping-form-container .breadcrumb-heading:contains("Shipping")').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified').html("Shipping Details");
              }
            });

            $('.shipping-form-container .breadcrumb-heading:contains("Billing")').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified').html("Billing Details");
              }
            });

            $('.custom-container.checkout .order-summary-container .vendor-items-holder .vendor-order-details .order-item .qty').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newTxt = 'QTY:';
                $(".qty:contains('Qty:')").html(function(_, html) {
                   return  html.replace(/(Qty:)/g, '<span class="updated-qty-string mr-1">'+newTxt+'</span>');
                });
              }
            })

            $('.custom-container.checkout .checkout-tabs-cont .vendor-items-holder .vendor-order-details .order-item .qty').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newTxt = 'Qty';
                $(".qty:contains('Qty:')").html(function(_, html) {
                   return  html.replace(/(Qty:)/g, '<span class="updated-qty-string mr-1">'+newTxt+'</span>');
                });
              }
            });

            $('.checkout .shipping-form-container .btn.btn-submit:contains("Continue Checkout")').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).html('Continue checkout');
              }
            });

            //$('.shipping-form-container .exist-address-wrapper .card a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/hikfKo6mqTgkjCYz6trWQE5B');
            $('.delivery-methods-cont .card .card-body .card-row img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/hikfKo6mqTgkjCYz6trWQE5B');

            $('.custom-container.checkout .payment-form-container .section-heading:contains("Details")').each(function(){
              if(!$(this).closest('.section-cont').hasClass('billing-detail-cont')){
                $(this).closest('.section-cont').addClass('billing-detail-cont');
              }
            });

            $('.custom-container.checkout.digital .payment-form-container .section-heading:contains("Details")').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).html('Billing Details');
              }
            });

            //$('.checkout .password-note:not(.modified)').addClass('modified').html('Must be atleast 6 characters');

            $('.custom-container.login-signup h1.breadcrumb-heading').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).html('My gift cards');
              }
            });

            $('.custom-container.login-signup .left-panel .panel-heading').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).html('Existing Gift Card Account');
              }
            });

            $('.custom-container.login-signup .right-panel .panel-heading').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).html('New Gift Card Account');
              }
            });

            $('.custom-container .login-signup-tabs-cont form.login-form .forget-psw-link').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).html('Forgot Password?');
              }
            });

            $('form.signup-form .btn.submit').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).html('Create Gift card Account');
              }
            });

            $('.container.user-container h1.user-heading:contains("My Account")').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).html('My Gift Card Account');
              }
            });

            $('.user-container .user-heading:contains("Check TS Gift Card Balance")').each(function(){
              if(!$(this).hasClass('gift-card-heading')){
                $(this).addClass('gift-card-heading');
                $(this).html('Check Gift Card Balance');
              }
            });

            //$('.user-container .balance-form').closest('.user-container:not(.d-none)').addClass('d-none');
            $('.user-container .ts-balance-form').closest('.user-container:not(.d-none)').addClass('d-none');
            $('.user-container .user-heading:contains("My Addresses")').closest('.user-container:not(.d-none)').addClass('d-none');
            $('.user-container .user-heading:contains("My Orders")').closest('.user-container').removeClass('pt-5').addClass('pt-2');
            $('.user-container .user-heading .wishlist-link:contains("My Wishlists")').closest('.user-heading:not(.d-none)').addClass('d-none');
            $('.user-container .user-heading .wishlist-link:contains("My Wishlists")').closest('.user-container').removeClass('pt-5').addClass('pt-3');

            $('.signup-form label:contains("The password must contain")').each(function(){
              if(!$(this).closest('.form-group').hasClass('d-none')){
                $(this).closest('.form-group').addClass('d-none');
              }
            });

            $('.social-links img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/oeqvQTsaZPrCYZ2dpVh9hyhK');
            $('.social-links img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/k6SMZzs6Z5c8tKUYfiTqGeRn');
            $('.social-links img[alt="twitter"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/PHy1iMm4xRV21HNHEVF6mYBR');
            $('.social-links img[alt="linkedin"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/Y4knT5wDJuweozXuvYinqE89');

            if(!$('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').hasClass('social-widget')){
              $('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').addClass('social-widget');
            }

            $('.footer .footer-widget .widget-content ul li a').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('target', '_blank');
              }
            });

            $('.footer .mobile-footer .footer-widget ul li a').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('target', '_blank');
              }
            });

            // $('.pay-methods-avail').append('.payment-icons-right');
            $('.social-widget').each(function(){
              if (!$(this).find('.widget-title').hasClass('text')){
                $(this).find('.widget-title').addClass('text');
                $(this).find('.widget-title').html('follow us on');
              }
            });

            $('.footer a:contains("+44")').each(function(){
              var string_phone= $(this).html();
              var result_phone = string_phone.substring(string_phone.indexOf(':') + 1);
              var phone_no = "tel:" + result_phone;
              if(!$(this).hasClass('tel-link')){
                $(this).addClass('tel-link');
                $(this).attr('href', phone_no);
              }
            });

            $('.footer a:contains("Gift Card Support")').each(function(){
              var string_email= 'belmond@techsembly.com';
              var email_link = "mailto:" + string_email;
              if(!$(this).hasClass('email-link')){
                $(this).addClass('email-link');
                $(this).attr('href', email_link);
              }
            });
            // $('.pay-methods-avail').prepend('<h5 class="widget-title mb-0 text">follow us</h5>');

            $('.checkout-container.complete .section-cont.customer-note').each(function () {
              if (!$(this).hasClass('total-delivery')) {
                $(this).addClass('total-delivery');
                var newTxt = 'Your order is being confirmed and we will be shipping it soon';
                $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function (_, html) {
                  return html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">' + newTxt + '</div>')
                });
              }
            });

            // add me thank you text remove
            $('.checkout-container.complete .msg-cont .msg-inner').each(function () {
              if (!$(this).hasClass('modified')) {

                str = "Thank You!";
                newStr = str.replace('Thank You!', `We'll send you a shipping confirmation when your item(s) are on the way! We appreciate your business, and hope you enjoy your purchase.`);

                $(".checkout-container.complete .msg-cont .msg-inner:contains('Thank You!')").html(function (_, html) {
                  return (newStr);

                });
              }
            });

            $('.order-confirm-wrapper .card-footer .card-footer-left a:contains("Customer Support"):not(.modified)').addClass('modified').attr("href", "mailto:belmond@techsembly.com");



          });
        });
    });
})();
