function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }

        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}

function addcopyrightText(){
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-6 pl-0 ts-copyright">Techsembly&copy; '+copyrightYear+'</div><div class="col-6 tc-rights-reserved text-right pr-0">All Rights Reserved</div></div></div></div>'
  var elem = $(".checkout-footer")[0]
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}

function addLoginCartNote(){
  var loginCartNote = "Please note this login is for Le Manoir Aux Quat'Saisons gifting platform only. <br>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel .panel-heading')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}


(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
          font-family: 'theano_didotregular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7afce5700cc855f492e39bb8161b72fe5ea2cd95/belmond/fonts/theano-didot/theanodidot-regular-webfont.woff2') format('woff2');\
      }\
      @font-face {\
          font-family: 'Abel':\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/54abca7d07696176ac62624aaa81b6827f4da1cc/belmond/fonts/abel/Abel-Regular.ttf');\
      }\
    "));

    document.head.appendChild(newStyle);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
          $('.footer .copyright-cont div.flex-lg-row > div:last-child').addClass('payment-icons-right pr-0 d-flex justify-content-end');
          $('.payment-icons-right').append('<div class="meth-link ml-2"><img class="" alt="amex" width="48" src="https://staging-assets.techsembly.com/assets/new-home/amex-7602b8b6b64ed6a6a9210f49973c7b2f5a291457a30ea4ed3ec5e4cf0d2bafea.svg"></div>')
          $('.payment-icons-right').append('<div class="meth-link ml-2"><img class="" alt="amex" width="48" src="https://staging-assets.techsembly.com/assets/new-home/visa-28b343bd9740303bcfaa86932f8ec931cdaaf14f13709fc97a36eeb43c0d2452.svg"></div>')
          $('.payment-icons-right').append('<div class="meth-link ml-2"><img class="" alt="amex" width="48" src="https://staging-assets.techsembly.com/assets/new-home/mastercard-381a3455124485c692c0371db6a6df553f7eb74b86ec5cab6a38099700f469ee.svg"></div>')

          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">');
          setTimeout(function () {
            //$('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
          }, 2000);

          waitForElm('app-footer-checkout').then((elm) => {
            addcopyrightText()
          }).catch((error) => {});

          waitForElm('.cart-right-cont').then((elm) => {
            //addLoginCartNote();
          }).catch((error) => {});

          /*waitForElm('.payment-form-container').then((elm) => {
            addcopyrightText()
          }).catch((error) => {});*/

          waitForElm('#dropdownMenuButton').then((elm) => {
            $("#dropdownMenuButton")[0].innerHTML = $("#dropdownMenuButton")[0].innerHTML.split(" ")[0]
          }).catch((error) => {});


          waitForElm('.vendor-items-holder').then((elm) => {
            var gbFlagImg = '<img class="" alt="store flag" src="https://static.techsembly.com/4Qyt4iCYjvo6t217gXL6NPGC">'
            var elem = $(".vendor-items-holder .flag-cont")[0]
            elem.insertAdjacentHTML('afterbegin', gbFlagImg)
          }).catch((error) => {});


          waitForElm('.related-products .related-products-holder').then((elm) => {
            var elems = $(".related-products .related-products-holder .more-items-product-prices")
            elems.each(function(index, elem){
              elem.insertAdjacentHTML('afterbegin', "GBP ")
            })
          }).catch((error) => {});

          waitForElm('.footer .mobile-footer').then((elm) => {
            var elems_links = $($(".footer .mobile-footer .footer-widget")[0]).find("ul > li > a")
            elems_links.each(function(index, elem){
              var parent = $(elem).parent();
              var inner = elem.innerHTML
              var parts = inner.split(":")
              if(parts.length > 1){
                var newElem = "<strong><span>" + parts[0] + " · </span></strong>"
                parent[0].insertAdjacentHTML('afterbegin', newElem)
                elem.innerHTML = parts[1]
              }
            })
          }).catch((error) => {});

          setInterval(function () {
            $('header.header').each(function(){
              if(!$(this).children('.top-heading-cont').length){
                $('<div class="top-heading-cont"><h2 class="text-center">GIFT CARDS AND EXPERIENCES</h2></div>').insertBefore('header nav#sidebar');
              }
            });
            $('app-new-header').each(function(){
              if(!$(this).children('.top-heading-cont').length){
                $(this).prepend('<div class="top-heading-cont"><h2 class="text-center">GIFT CARDS AND EXPERIENCES</h2></div>');
              }
            });
            $('header.header .menu-holder').each(function(){
              if(!$(this).closest('.container').hasClass('wrapped')){
                $(this).closest('.container').addClass('wrapped');
                $(this).closest('.container').wrap('<div class="container-fluid main-menu-wrapper"></div>');
              }
            });
            $('.wrapper .product-container').each(function() {
              if(!$(this).closest('.container').hasClass('modified')){
                $(this).closest('.container').addClass('modified');
                $('<div class="hero-banner"><img class="w-100" src="https://static.techsembly.com/DsfmPqbnkeV7qSXdkspYLxCA"><div class="banner-desc-cont text-center"><h2 class="banner-header">LE MANOIR</h2><div class="banner-desc pt-lg-4">Give the gift of a legendary experience</div></div></div>').insertBefore($(this));
              }
            });
            $('.home-slider .section-title:contains("THE EXPERIENCES")').each(function(){
              if(!$(this).closest('.home-slider').hasClass('experience-slider')){
                $(this).closest('.home-slider').addClass('experience-slider');
                $('<div class="slider-top-heading text-center">CHAPTER ONE:</div>').insertBefore($(this));
              }
            });
            $('.chapter-cont').each(function(){
              if($(this).closest('.home-intro-container').hasClass('container')){
                $(this).closest('.home-intro-container').removeClass('container');
                $(this).closest('.home-intro-container').addClass('container-fluid');
              }
            });
            $('.promo-holder .promo-box img[alt="fine-dining"]').each(function(){
              if(!$(this).closest('.custom-container').hasClass('dine-promo-cont')){
                $(this).closest('.custom-container').addClass('dine-promo-cont');
                $(this).closest('.custom-container').prepend('<div class="row"><div class="col-12 dine-promo-top"><div class="slider-top-heading text-center">CHAPTER THREE:</div><h2 class="section-title text-center">TIME TO DINE</h2></div></div>');
              }
            });
            $('.home-slider .section-title:contains("MORE FROM THE WORLD")').each(function(){
              if(!$(this).closest('.home-slider').hasClass('speciality-slider')){
                $(this).closest('.home-slider').addClass('speciality-slider');
                $('<div class="slider-top-heading text-center">CHAPTER FOUR:</div>').insertBefore($(this));
              }
            });
            $('.wrapper .featured-client:not(.d-none)').addClass('d-none');
            $('.product-container .breadcrumb-heading:contains("Search results")').each(function(){
              if (!$(this).closest('body').hasClass('search-results-page')) {
                $(this).closest('body').addClass('search-results-page');
              }
            });
            $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
                $(this).html('Book');
            });
            //$('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#details-tab').closest('.nav-item').addClass('d-none');
            // $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').removeClass('col-lg-5').addClass('col-lg-6 product-inner modified');
            // $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').removeClass('col-lg-7').addClass('col-lg-6 modified');
            $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
            $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');
            var options = [];
            $('.product-detail-container .personalise-form .form-group select#Options').children().each(function(){
              var option_text = $(this).html();
              var result_text = option_text.substr(0, option_text.indexOf('+'));
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  if($(this).val()){
                    $(this).html(result_text);
                  }
                  /*options.push({
                    optionValue: $(this).html()
                  });
                  console.log(options);*/
              }
            });
            $('.related-products .custom-container').each(function(){
              if(!$(this).children('h3.text-center').length){
                $(this).prepend('<h3 class="text-center related-pros-heading">More Like This</h3>');
              }
            });
            $('app-product-detail app-related-products[title="More items from"]').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).find('h3').html('More Like This');
              }
            });
            /*checkout shipping details*/
            $('.shipping-form-container .shipping-form .left-panel').removeClass('col-lg-8').addClass('col-lg-7');
            $('.shipping-form-container .shipping-form .right-panel').removeClass('col-lg-4').addClass('col-lg-5');
            $('.shipping-form-container .left-panel').removeClass('col-lg-8').addClass('col-lg-7');
            $('.shipping-form-container .right-panel').removeClass('col-lg-4').addClass('col-lg-5');
            $('.delivery-methods-cont .card .card-body .row-head:contains("Ship to")').closest('.card-row:not(.d-none)').addClass('d-none').removeClass('d-flex');
            $('.delivery-methods-cont .vendor-cards-holder .card .shipping-options-cont .del-option').closest('.shipping-options-cont:not(.BorderBottom)').addClass('BorderBottom');
            $('.shipping-form-container .delivery-methods-cont .vendor-cards-holder .card .card-header .badge-stroke span:contains("Digital Product")').closest('.vendor-card:not(.digital-pros-card)').addClass('digital-pros-card');
            /*cart*/
            $('.order-shipping-container .shipping-totals-cont').removeClass('pt-4 mt-2');
            $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/361d96b4fccb4607c71bd05db80d19f3d8fa8060/belmond/images/edit.svg');
            $('.custom-container.cart-container .vendor-items-cont a img[alt="gift"]:not(.modified)').addClass('modified').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/361d96b4fccb4607c71bd05db80d19f3d8fa8060/belmond/images/gift.svg');
            $('.custom-container.cart-container').each(function(){
              if (!$(this).closest('body').attr('data-pagetype','cart')){
                $(this).closest('body').attr('data-pagetype','cart');
              }
            })
            $('.custom-container.cart-container .cart-item.new .pro-badges-cont .pro-badge span:contains("GiveX Product")').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Digital Product');
              }
            });
            $('#delivery-options-popup .pref-method-note').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Select your preferred delivery methods');
              }
            })
            $('.cart-item.new a .underline:contains("Add Gift Options")').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Add Gift Message');
              }
            });
            /*end cart*/
            $('.shipping-form-container .exist-address-wrapper .card a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/361d96b4fccb4607c71bd05db80d19f3d8fa8060/belmond/images/edit.svg');
            $('.delivery-methods-cont .card .card-body .card-row img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/361d96b4fccb4607c71bd05db80d19f3d8fa8060/belmond/images/edit.svg');
            $('.payment-form .complete-note').each(function () {
              if (!$(this).closest('.card').hasClass('complete-note-card')) {
                $(this).closest('.card').addClass('complete-note-card');
                $(this).closest('.card').parent('.col-12').addClass('complete-note-cont');
              }
            });
            $('.social-links img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/VkrVQYfJFgW9DtBwuyoshdy2');
            $('.social-links img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/qMkBF8nmZhzzUYQWQXSgTS6o');
            $('.social-links img[alt="twitter"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/XQR6njCWvX5MD6qZzNGuGdQX');
            $('.social-links img[alt="fb"]').closest('li:not(.fb-link-cont)').addClass('fb-link-cont');
            $('.social-links img[alt="twitter"]').closest('li:not(.twitter-link-cont)').addClass('twitter-link-cont').insertBefore($('.social-links .fb-link-cont'));
            $('.social-link .twitter-link-cont:not(:first-child)').remove();
            $('.footer .footer-widget').parent('.col-lg-12:not(.footer-inner-cont)').addClass('footer-inner-cont');
            if(!$('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').hasClass('social-widget')){
              $('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').addClass('social-widget');
            }
            // $('.pay-methods-avail').append('.payment-icons-right');
            $('.social-widget').each(function(){
              if (!$(this).find('.widget-title').hasClass('text')){
                $(this).find('.widget-title').addClass('text');
                $(this).find('.widget-title').html('follow us');
              }
            });
            // $('.pay-methods-avail').prepend('<h5 class="widget-title mb-0 text">follow us</h5>');

            $('.user-container .balance-form').closest('.user-container:not(.d-none)').addClass('d-none');
            $('.user-container .ts-balance-form').closest('.user-container:not(.d-none)').addClass('d-none');

            $('.copyright-cont').each(function(){
              if (!$(this).find('.pay-methods-avail').hasClass('modified-widget')){
                $(this).find('.pay-methods-avail').addClass('modified-widget');
                $('.copyright-cont .modified-widget').prepend('<div class="social-widget"><h5 class="widget-title mb-0">follow us</h5><ul class="social-links"><li class="twitter-link-cont"><a href="https://twitter.com/lemanoir?lang=en" rel="nofollow"><img class="modified" alt="twitter" width="32" src="https://static.techsembly.com/XQR6njCWvX5MD6qZzNGuGdQX"></a></li><li class="fb-link-cont"><a href="https://www.facebook.com/lemanoirauxquatsaisons" rel="nofollow"><img class="modified" alt="fb" width="32" src="https://static.techsembly.com/VkrVQYfJFgW9DtBwuyoshdy2"></a></li><li><a href="https://www.instagram.com/belmondlemanoir" rel="nofollow"><img class="modified" alt="instagram" width="32" src="https://static.techsembly.com/qMkBF8nmZhzzUYQWQXSgTS6o"></a></li></ul></div>');
              }
              if(!$(this).children('.copyright-text-cont').length){
                $(this).append('<div class="col-12 copyright-text-cont text-center"><div class="powered-link-cont"><span>Powered by</span><span class="ml-1"><a href="https://techsembly.com">Techsembly</a></span></div></div>');
              }
            });

            var elems = $($(".footer .footer-widget .widget-content")[0]).find("ul > li > a")
              elems.each(function(index, elem){
                var parent = $(elem).parent();
                var inner = elem.innerHTML
                var parts = inner.split(":")
                if(parts.length > 1){
                  var newElem = "<strong><span>" + parts[0] + " · </span></strong>"
                  parent[0].insertAdjacentHTML('afterbegin', newElem)
                  elem.innerHTML = parts[1]
                }
              })
              $('.footer a:contains("+44")').each(function(){
                var string_phone= $(this).html();
                var result_phone = string_phone.substring(string_phone.indexOf('.') + 1);
                var phone_no = "tel:" + result_phone;
                if(!$(this).hasClass('tel-link')){
                  $(this).addClass('tel-link');
                  $(this).attr('href', phone_no);
                }
              });
              $('.footer a:contains("800")').each(function(){
                var string_phone2= $(this).html();
                var result_phone2 = string_phone2.substring(string_phone2.indexOf('.') + 1);
                var phone_no2 = "tel:" + result_phone2;
                if(!$(this).hasClass('tel-link')){
                  $(this).addClass('tel-link');
                  $(this).attr('href', phone_no2);
                }
              });
              $('.footer a:contains("@belmond.com")').each(function(){
                var string_email= $(this).html();
                //var result_email = string_email.substr(0, string_email.indexOf('.'));
                var email_link = "mailto:" + string_email;
                if(!$(this).hasClass('email-link')){
                  $(this).addClass('email-link');
                  $(this).attr('href', email_link);
                }
              });
              $('.footer a:contains("@techsembly.com")').each(function(){
                var string_email2= $(this).html();
                var email_link2 = "mailto:" + string_email2;
                if(!$(this).hasClass('email-link')){
                  $(this).addClass('email-link');
                  $(this).attr('href', email_link2);
                }
              });
          });
        });
    });
})();
