function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }

        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}

function addHeroBannerButton(){
  var bannerBtn = '<button class="btn btn-default hero-btn text-uppercase">start shopping</button>';
  var elem = $(".home-banner .banner-heading")[0];
  elem.insertAdjacentHTML('beforeend', bannerBtn);
}

function addSignupBtn(){
  var signupBtn = '<a href="https://www.belmond.com/newsletter" class="btn btn-default btn-sign-up text-uppercase" target="_blank">SIGN UP</a>';
  var elem = $(".sign-up-section .sign-up-container")[0];
  elem.insertAdjacentHTML('beforeend', signupBtn);
}

function addcopyrightText(){
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-6 pl-0 ts-copyright">Techsembly&copy; '+copyrightYear+'</div><div class="col-6 tc-rights-reserved text-right pr-0">All Rights Reserved</div></div></div></div>'
  var elem = $(".checkout-footer")[0]
  elem.insertAdjacentHTML('afterend', copyrightHtml)
}

function addLoginCartNote(){
  var loginCartNote = "Please note this login is for Le Manoir Aux Quat'Saisons gifting platform only. <br>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel .panel-heading')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}


(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
          font-family: 'theano_didotregular';\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/7afce5700cc855f492e39bb8161b72fe5ea2cd95/belmond/fonts/theano-didot/theanodidot-regular-webfont.woff2') format('woff2');\
      }\
      @font-face {\
          font-family: 'Abel':\
          src: url('https://bitbucket.org/TSTechsembly/jscss/raw/54abca7d07696176ac62624aaa81b6827f4da1cc/belmond/fonts/abel/Abel-Regular.ttf');\
      }\
    "));

    document.head.appendChild(newStyle);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {

          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">');
          setTimeout(function () {
            //$('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
          }, 2000);

          waitForElm('.home-banner').then((elm) => {
            addHeroBannerButton();
          }).catch((error) => {});

          waitForElm('.sign-up-section').then((elm) => {
            addSignupBtn();
          }).catch((error) => {});

          waitForElm('app-footer-checkout').then((elm) => {
            addcopyrightText()
          }).catch((error) => {});

          waitForElm('.cart-right-cont').then((elm) => {
            //addLoginCartNote();
          }).catch((error) => {});

          /*waitForElm('.payment-form-container').then((elm) => {
            addcopyrightText()
          }).catch((error) => {});*/

          waitForElm('#dropdownMenuButton').then((elm) => {
            $("#dropdownMenuButton")[0].innerHTML = $("#dropdownMenuButton")[0].innerHTML.split(" ")[0]
          }).catch((error) => {});

          waitForElm('.footer .mobile-footer').then((elm) => {
            var elems_links = $($(".footer .mobile-footer .footer-widget")[0]).find("ul > li > a")
            elems_links.each(function(index, elem){
              var parent = $(elem).parent();
              var inner = elem.innerHTML
              var parts = inner.split(":")
              if(parts.length > 1){
                var newElem = "<strong><span>" + parts[0] + " · </span></strong>"
                parent[0].insertAdjacentHTML('afterbegin', newElem)
                elem.innerHTML = parts[1]
              }
            })
          }).catch((error) => {});

          setInterval(function () {
            $('header.header .search-item-form img[alt="search image"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/h3YJEm4MbRzwmAvat2vA6XbM');
            $('header.header .menu-holder').each(function(){
              if(!$(this).closest('.container').hasClass('wrapped')){
                $(this).closest('.container').addClass('wrapped');
                $(this).closest('.container').wrap('<div class="container-fluid main-menu-wrapper"></div>');
              }
            });
            $('.home-slider .section-title:contains("The Experiences")').each(function(){
              if(!$(this).closest('.home-slider').hasClass('experience-slider')){
                $(this).closest('.home-slider').addClass('experience-slider');
              }
            });
            $('.experience-slider').find('.section-title').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $('<div class="slider-top-heading text-center">CHAPTER ONE:</div>').insertBefore($(this));
              }
            });
            $('.home-slider.experience-slider .owl-item:first-child .post-link').each(function() {
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  var obj = $(this);
                  var text = obj.html();
                  var parts = text.split(' ');
                  var replace = '<span class="firstWord">'+parts[0]+'</span><br />';
                  parts.shift();
                  replace += parts.join(' ');
                  obj.html(replace);
               }
            });
            $('.chapter-cont').each(function(){
              if($(this).closest('.home-intro-container').hasClass('container')){
                $(this).closest('.home-intro-container').removeClass('container');
                $(this).closest('.home-intro-container').addClass('container-fluid');
              }
            });
            $('.promo-holder .promo-box img[alt="chef-table"]').each(function(){
              if(!$(this).closest('.custom-container').hasClass('dine-promo-cont')){
                $(this).closest('.custom-container').addClass('dine-promo-cont');
                $(this).closest('.custom-container').prepend('<div class="row"><div class="col-12 dine-promo-top"><div class="slider-top-heading text-center">CHAPTER THREE:</div><h2 class="section-title text-center">Time To Dine</h2></div></div>');
              }
            });
            $('.promo-holder .promo-box').each(function(){
              var bannerTitle = $(this).find('.banner-title');
              var promoTitle = $(this).find('.promo-title');
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  bannerTitle.insertBefore(promoTitle);
              }
            });
            $('.gift-section').each(function(){
              var bannerTitle = $(this).find('.banner-title');
              var giftsTitle = $(this).find('.gifts-heading');
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  bannerTitle.insertBefore(giftsTitle);
              }
            });
            $('.home-slider .section-title:contains("More From The World Of Belmond")').each(function(){
              if(!$(this).closest('.home-slider').hasClass('speciality-slider')){
                $(this).closest('.home-slider').addClass('speciality-slider');
              }
            });
            $('.speciality-slider').find('.section-title').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $('<div class="slider-top-heading text-center">CHAPTER FOUR:</div>').insertBefore($(this));
              }
            });
            $('.wrapper .featured-client:not(.d-none)').addClass('d-none');
            $('.product-container .breadcrumb-heading:contains("Search results")').each(function(){
              if (!$(this).closest('body').hasClass('search-results-page')) {
                $(this).closest('body').addClass('search-results-page');
              }
            });
            $('.wishlistImage:has(img[alt="add-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
                $('.wishlistImage img[alt="add-to-wishlist"]').attr('src', 'https://static.techsembly.com/iWokqTPXBqHBpKuJiGoF5cKn');
            });
            $('.wishlistImage:has(img[alt="added-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
                $('.wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://static.techsembly.com/h9Fvcv3PWs652cTiFxMbA1C4');
            });
            $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('Book');
              }
            });
            //$('.product-detail-container .product-holder .product-order .nav-tabs .nav-item .nav-link#details-tab').closest('.nav-item').addClass('d-none');
            // $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').removeClass('col-lg-5').addClass('col-lg-6 product-inner modified');
            // $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').removeClass('col-lg-7').addClass('col-lg-6 modified');
            $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
            $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');
            var options = [];
            $('.product-detail-container .personalise-form .form-group select#Options').children().each(function(){
              var option_text = $(this).html();
              var result_text = option_text.substr(0, option_text.indexOf('+'));
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  if($(this).val()){
                    $(this).html(result_text);
                  }
                  /*options.push({
                    optionValue: $(this).html()
                  });
                  console.log(options);*/
              }
            });
            $('.related-products .custom-container').each(function(){
              if(!$(this).children('h3.text-center').length){
                $(this).prepend('<h3 class="text-center related-pros-heading">More Like This</h3>');
              }
            });
            $('app-product-detail app-related-products[title="More items from"]').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).find('h3').html('More Like This');
              }
            });
            /*checkout shipping details*/
            $('.shipping-form-container .shipping-form .left-panel').removeClass('col-lg-8').addClass('col-lg-7');
            $('.shipping-form-container .shipping-form .right-panel').removeClass('col-lg-4').addClass('col-lg-5');
            $('.shipping-form-container .left-panel').removeClass('col-lg-8').addClass('col-lg-7');
            $('.shipping-form-container .right-panel').removeClass('col-lg-4').addClass('col-lg-5');
            $('.delivery-methods-cont .card .card-body .row-head:contains("Ship to")').closest('.card-row:not(.d-none)').addClass('d-none').removeClass('d-flex');
            $('.delivery-methods-cont .vendor-cards-holder .card .shipping-options-cont .del-option').closest('.shipping-options-cont:not(.BorderBottom)').addClass('BorderBottom');
            $('.shipping-form-container .delivery-methods-cont .vendor-cards-holder .card .card-header .badge-stroke span:contains("Digital Product")').closest('.vendor-card:not(.digital-pros-card)').addClass('digital-pros-card');
            /*cart*/
            $('.order-shipping-container .shipping-totals-cont').removeClass('pt-4 mt-2');
            $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/hxDmQ7Zo4JYSmioSUzM2CNGr');
            $('.custom-container.cart-container .vendor-items-cont a img[alt="gift"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/RyTzTM4ncinrJiPWiG1AerbU');
            $('.custom-container.cart-container').each(function(){
              if (!$(this).closest('body').attr('data-pagetype','cart')){
                $(this).closest('body').attr('data-pagetype','cart');
              }
            })
            $('.custom-container.cart-container .cart-item.new .pro-badges-cont .pro-badge span:contains("GiveX Product")').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Digital Product');
              }
            });
            $('#delivery-options-popup .pref-method-note').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Select your preferred delivery methods');
              }
            })
            $('.cart-item.new a .underline:contains("Add Gift Message")').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).html('Add Gift Options');
              }
            });
            $('.cart-item.new .item-details .item-qty-cont').each(function(){
              if(!$(this).closest('.col-lg-2').hasClass('qty-col')){
                  $(this).closest('.col-lg-2').addClass('qty-col');
              }
            });

            $('.cart-item.new .item-details .unit-total-price').each(function(){
              if(!$(this).closest('.col-lg-3').hasClass('sub-total-col')){
                  $(this).closest('.col-lg-3').addClass('sub-total-col');
              }
            });
            /*end cart*/
            $('.shipping-form-container .exist-address-wrapper .card a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/hxDmQ7Zo4JYSmioSUzM2CNGr');
            $('.container.custom-container.checkout .btn#continue-to-delivery-and-billing').each(function(){
                if(!$(this).closest('.form-group').hasClass('submit-btn-cont')){
                    $(this).closest('.form-group').addClass('submit-btn-cont');
                    $(this).html('Continue checkout');
                }
            });
            $('.auth-user .custom-container.checkout .shipping-form-container').each(function () {
              if (($(this).find('.exist-address-wrapper').length) && !$(this).hasClass('exist-add-exists')) {
                $(this).addClass('exist-add-exists');
                $("#continue-to-delivery-and-billing").html('Add & continue checkout');
              }
            });
            $('.custom-container.checkout .address-form.new .form-control[formcontrolname="phone"]').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).attr('placeholder','Phone Number');
                }
            });
            $('.delivery-methods-cont .card .card-body .card-row img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/hxDmQ7Zo4JYSmioSUzM2CNGr');
            $('.delivery-methods-cont .vendor-cards-holder .card .card-header').each(function () {
              if (!($(this).find('.badge-stroke').length) && !$(this).hasClass('d-none')) {
                $(this).addClass('d-none');
              }
            });
            $('.payment-form .complete-note').each(function () {
              if (!$(this).closest('.card').hasClass('complete-note-card')) {
                $(this).closest('.card').addClass('complete-note-card');
                $(this).closest('.card').parent('.col-12').addClass('complete-note-cont');
              }
            });
            $('.checkout-container.complete .section-cont.customer-note').each(function () {
              if (!$(this).hasClass('total-delivery')) {
                $(this).addClass('total-delivery');
                var newTxt = 'Your order is being confirmed and we will be shipping it soon';
                $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function (_, html) {
                  return html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">' + newTxt + '</div>')
                });
              }
            });
            $('.checkout .checkout-container.complete .card.order-confirm-wrapper').each(function(){
              if(!$(this).parent('.col-12').hasClass('order-confirm-outer')){
                $(this).parent('.col-12').addClass('order-confirm-outer');
              }
            });

            $('.custom-container.checkout .checkout-tabs-cont .vendor-items-holder .vendor-order-details .order-item .qty').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newTxt = 'Qty';
                $(".qty:contains('Qty:')").html(function(_, html) {
                   return  html.replace(/(Qty:)/g, '<span class="updated-qty-string mr-1">'+newTxt+'</span>');
                });
              }
            });
            $('.checkout .checkout-container.complete .card-footer a:contains("Customer Support")').each(function(){
                if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).attr('href','mailto:belmond@techsembly.com ');
                }
            });
            $('.social-links img[alt="fb"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/fBpVafvPPNaJrHYuG35wGiRt');
            $('.social-links img[alt="instagram"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/YAkPSJahjMGfWkXNLoTsMiud');
            $('.social-links img[alt="twitter"]:not(.modified)').addClass('modified').attr('src', 'https://static.techsembly.com/sdqFg6gFGMZW38dCaK8wZi37');
            $('.social-links img[alt="fb"]').closest('li:not(.fb-link-cont)').addClass('fb-link-cont');
            $('.social-links img[alt="twitter"]').closest('li:not(.twitter-link-cont)').addClass('twitter-link-cont').insertBefore($('.social-links .fb-link-cont'));
            $('.social-link .twitter-link-cont:not(:first-child)').remove();
            $('.footer .footer-widget').parent('.col-lg-12:not(.footer-inner-cont)').addClass('footer-inner-cont');
            if(!$('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').hasClass('social-widget')){
              $('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').addClass('social-widget');
            }
            $('.footer .footer-widget .widget-content ul li a').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('target', '_blank');
              }
            });

            $('.footer .mobile-footer .footer-widget ul li a').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).attr('target', '_blank');
              }
            });
            // $('.pay-methods-avail').append('.payment-icons-right');
            $('.social-widget').each(function(){
              if (!$(this).find('.widget-title').hasClass('text')){
                $(this).find('.widget-title').addClass('text');
                $(this).find('.widget-title').html('follow us');
              }
            });
            // $('.pay-methods-avail').prepend('<h5 class="widget-title mb-0 text">follow us</h5>');

            $('.user-container .balance-form').closest('.user-container:not(.d-none)').addClass('d-none');
            $('.user-container .ts-balance-form').closest('.user-container:not(.d-none)').addClass('d-none');

            /*$('.copyright-cont').each(function(){
              if(!$(this).children('.copyright-text-cont').length){
                $(this).append('<div class="col-12 copyright-text-cont text-center"><div class="powered-link-cont"><span>Powered by</span><span class="ml-1"><a href="https://techsembly.com">Techsembly</a></span></div></div>');
              }
            });*/

            $('.footer .mobile-footer .list-group .list-group-holder>a').each(function(){
              if ((!$(this).text().trim().length) && (!$(this).closest('.list-group-holder').hasClass('d-none'))){
                $(this).closest('.list-group-holder').addClass('d-none');
              }
            });

              $('.footer a:contains("+44")').each(function(){
                var string_phone= $(this).html();
                var result_phone = string_phone.substring(string_phone.indexOf('.') + 1);
                var phone_no = "tel:" + result_phone;
                if(!$(this).hasClass('tel-link')){
                  $(this).addClass('tel-link');
                  $(this).attr('href', phone_no);
                }
              });
              $('.footer a:contains("800")').each(function(){
                var string_phone2= $(this).html();
                var result_phone2 = string_phone2.substring(string_phone2.indexOf('.') + 1);
                var phone_no2 = "tel:" + result_phone2;
                if(!$(this).hasClass('tel-link')){
                  $(this).addClass('tel-link');
                  $(this).attr('href', phone_no2);
                }
              });
              $('.footer a:contains("@belmond.com")').each(function(){
                var string_email= $(this).html();
                //var result_email = string_email.substr(0, string_email.indexOf('.'));
                var email_link = "mailto:" + string_email;
                if(!$(this).hasClass('email-link')){
                  $(this).addClass('email-link');
                  $(this).attr('href', email_link);
                }
              });
              $('.footer a:contains("@techsembly.com")').each(function(){
                var string_email2= $(this).html();
                var email_link2 = "mailto:" + string_email2;
                if(!$(this).hasClass('email-link')){
                  $(this).addClass('email-link');
                  $(this).attr('href', email_link2);
                }
              });
          });
        });
    });
})();
