(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
            $('head').append('<link rel="preconnect" href="https://fonts.gstatic.com"><link href="https://fonts.googleapis.com/css2?family=Source+Serif+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap" rel="stylesheet">');
            $('head').append('<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">');
            $('header.header').prepend('<div class="container-fluid top-header pt-2"><div class="row spacing align-items-center justify-content-between flex-wrap pt-1"><div class="col-12 col-md-6 pl-0 top-header-left"><a href="/contact"><i class="fa fa-envelope-o" aria-hidden="true"></i><span class="ml-2">CONTACT US</span></a></div><div class="col-12 col-md-6 pl-0 top-header-right text-md-right text-uppercase"><a href="#">The Peninsula Signature Events</a><span class="mx-3">|</span><a href="#">Quail Lodge & Golf Club</a></div></div></div>');
            setTimeout(function () {
               $('header.header .search .search-item-form .nav-search-field').attr('placeholder','');
               $('.mobile-search-box .nav-search-field').attr('placeholder','');
               $('.home-banner .banner-heading').append('<br><span>Exclusively at Quail Lodge</span>');
               $('<h2 class="section-title text-center mt-0">Explore the Shop</h2>').insertBefore('.promo-section .promo-holder');
               $('.promo-holder .promo-box:first-child .promo-content').append('<a href="#">Book Now > </a>');
               $('.promo-holder .promo-box:last-child .promo-content').append('<a href="#">Explore More > </a>');
               $('.sign-up-section .sign-up-container .sign-up-form input[type="email"]').attr('placeholder','');
               $('.footer .custom-container .footer-widget:nth-child(4)').append('<div class="footer-pay-methods d-flex flex-wrap pb-4"><img class="mr-2" src="https://media.graphcms.com/ND8G0ynTY6s4aZgFgSMp" alt="master-card"/>'+
               '<img class="mr-2" src="https://media.graphcms.com/6V7F2JYzTAWZEjUXtAsg" alt="amex"/>'+
               '<img class="" src="https://media.graphcms.com/jHYDSIMQyKAxUjbqRw0w" alt="visa"/>'+
               '</div>' +
               '<div class="footer-pay-note text-white"><p>Quail Lodge & Golf Club have selected Techsembly to help you choose, purchase and receive your gift vouchers.</p><br><p>Techsembly,s secure checkout is provided by Stripe and your payment details are never stored.</p></div>');
               $('.footer .footer-widget .widget-content .social-links li a#facebook-icon img').attr('src','https://media.graphcms.com/dRzX97mSQHWAGlVe87wP');
                $('.footer .footer-widget .widget-content .social-links li img#facebook-icon').attr('src','https://media.graphcms.com/dRzX97mSQHWAGlVe87wP');
                $('.footer .footer-widget .widget-content .social-links li a#insta-icon img').attr('src',' https://media.graphcms.com/TGrMhSRjQXiVdx7xaOsO');
                $('.footer .footer-widget .widget-content .social-links li img#insta-icon').attr('src',' https://media.graphcms.com/TGrMhSRjQXiVdx7xaOsO');
                $('.footer .footer-widget .widget-content .social-links li a#linkedin-icon img').attr('src','https://media.graphcms.com/e4nQ3F1SSru7rX04eioy');
                $('.footer .footer-widget .widget-content .social-links li img#linkedin-icon').attr('src','https://media.graphcms.com/e4nQ3F1SSru7rX04eioy');
                $('.footer .footer-widget .widget-content .social-links li a#twitter-icon img').attr('src','https://media.graphcms.com/T4slDiFgSfKaYEI00Ez7');
                $('.footer .footer-widget .widget-content .social-links li img#twitter-icon').attr('src','https://media.graphcms.com/T4slDiFgSfKaYEI00Ez7');
                $('.footer .footer-widget .widget-content .social-links li a#pinterest-icon img').attr('src','https://media.graphcms.com/fN1OaJ18QmaTesGOToVy');
                $('.footer .footer-widget .widget-content .social-links li img#pinterest-icon').attr('src','https://media.graphcms.com/fN1OaJ18QmaTesGOToVy');
               $('.powered-by > div:nth-child(1)').html('© 2020 Quail Lodge & Golf Club. All Rights Reserved. ');
               $('<div class="d-flex align-items-center pt-3"><img class="copy-right-img" src="https://media.graphcms.com/UeVqpVFuQRSO4Vwgm6Ew", width="24"/><span class="ml-2"><a class="footer-cp-link" href="https://techsembly.com/" target="_blank">Techsembly</a></span></div>').insertAfter($(".powered-link-cont"));
            }, 2000);
            setInterval(function () {
                $('.home-intro h2').each(function(){
                    if(!$(this).hasClass('store-title')){
                        $(this).addClass('store-title');
                        $(this).html('ABOUT Quail lodge & golf club');
                    }
                });
                $('.footer #google_translate_element').each(function(){
                    if(!$(this).closest('.footer-widget').hasClass('d-none')){
                        $(this).closest('.footer-widget').addClass('d-none');
                    }
                });
                if(!$('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').hasClass('social-widget')){
                    $('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').addClass('social-widget');
                    $('.social-widget').append('<a class="btn btn-primary btn-contact d-flex justify-content-center align-items-center mt-2" href="/contact">CONTACT</a>')
                }
            });
        });
    });
})();
