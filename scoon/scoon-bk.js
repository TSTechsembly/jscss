(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
        @font-face {\
            font-family: 'Nimbus-San-regular';\
            src: url('https://bitbucket.org/TSTechsembly/jscss/raw/56d7ada040c75239243d3402978fea890e8afc51/scoon/fonts/nimbussans/NimbusSan-Reg.woff2') format('woff2');\
        }\
        @font-face {\
            font-family: 'Nimbus-San-light';\
            src: url('https://bitbucket.org/TSTechsembly/jscss/raw/56d7ada040c75239243d3402978fea890e8afc51/scoon/fonts/nimbussans/NimbusSan-Lig.woff2') format('woff2');\
            font-weight: 300;\
        }\
        @font-face {\
            font-family: 'Louize-Display';\
            src: url('https://bitbucket.org/TSTechsembly/jscss/raw/aa8b00b7cc1fbe2ca29d5296ecd63bd65a711b7a/scoon/fonts/Louize-Display/LouizeDisplay.woff2') format('woff2');\
        }\
        @font-face {\
            font-family: 'Louize-Display-italic';\
            src: url('https://bitbucket.org/TSTechsembly/jscss/raw/aa8b00b7cc1fbe2ca29d5296ecd63bd65a711b7a/scoon/fonts/Louize-Display/LouizeDisplay-Italic.woff2') format('woff2');\
        }\
    "));

    document.head.appendChild(newStyle);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
          // $('head').append('<link href="https://bitbucket.org/TSTechsembly/jscss/raw/709ec8612d0f3c9f21d19438b81ca0312a51fb6e/scoon/fonts/Louize-Display/stylesheet.css" rel="stylesheet">');
          // $('head').append('<link href="https://bitbucket.org/TSTechsembly/jscss/raw/709ec8612d0f3c9f21d19438b81ca0312a51fb6e/scoon/fonts/nimbussans/stylesheet.css" rel="stylesheet">');
          $('head').append('<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">');
          $('.nav-search-field').attr("placeholder", "");
          $('img[alt="user-profile"]').attr("src", "https://bitbucket.org/TSTechsembly/jscss/raw/3257abe9d3ca18a77c63ce3218f11d8bffeeeabc/scoon/images/user.svg");
          $('img[alt="wishlist"]').attr("src", "https://bitbucket.org/TSTechsembly/jscss/raw/3257abe9d3ca18a77c63ce3218f11d8bffeeeabc/scoon/images/heart.svg");
          $('img[alt="cart"]').attr("src", "https://bitbucket.org/TSTechsembly/jscss/raw/3257abe9d3ca18a77c63ce3218f11d8bffeeeabc/scoon/images/cart.svg");
          $('header .mob-top-links li .mob-search-link img[alt="search image"]').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/ce94d5735610ba6f6017995f0e3677dfe291afe4/scoon/images/search.svg');
          $('.sign-up-section .sign-up-container .sign-up-form input[type="email"]').attr("placeholder","your email address");
          $('.powered-by > div:nth-child(1)').html('© 2021 Scoon. All Rights Reserved');
          setTimeout(function () {
             $('header.header .menu-links-cont .nav.search').appendTo('.main-menu-container');
            /*$('.home-carousel.modified').owlCarousel({
                loop: false,
                autoplay:false,
                autoplayTimeout:3000,
                nav: true,
                autoplayHoverPause:true,
                stagePadding: 0,
                dots: false,
                mouseDrag: true,
                responsiveClass:true,
                responsive: {
                  1000: {
                    items: 6,
                    margin: 24
                  }
                },

              });*/
              var back = "https://bitbucket.org/TSTechsembly/jscss/raw/a385f171b94ab56bd1807f06c9044d1cabb4e25e/scoon/images/arrow-left.svg";
              var next = "https://bitbucket.org/TSTechsembly/jscss/raw/a385f171b94ab56bd1807f06c9044d1cabb4e25e/scoon/images/arrow-right.svg";
              $('.filters-carousel').owlCarousel({
                  loop: false,
                  autoplay:false,
                  autoplayTimeout:3000,
                  nav: true,
                  autoplayHoverPause:true,
                  stagePadding: 0,
                  dots: false,
                  mouseDrag: true,
                  responsiveClass:true,
                  responsive: {
                    0:{
                      items: 3,
                      stagePadding: 0,
                      nav: true,
                      dots: false
                    },
                    450:{
                      items: 5,
                      stagePadding: 0,
                      nav: true,
                      dots: false
                    },
                    1000: {
                      items: 11,
                      margin: 0
                    }
                  },
                  navText: ["<img class='carousel-nav-img img-prev' src="+ back +">","<img class='carousel-nav-img img-next' src="+ next +">"]
                });
                $('.clients-carousel').owlCarousel({
                    loop: false,
                    autoplay:false,
                    autoplayTimeout:3000,
                    nav: true,
                    autoplayHoverPause:true,
                    stagePadding: 0,
                    dots: false,
                    mouseDrag: true,
                    responsiveClass:true,
                    responsive: {
                      0:{
                        items: 3,
                        stagePadding: 0,
                        nav: true,
                        dots: false
                      },
                      1000: {
                        items: 5,
                        margin: 0
                      }
                    },
                    navText: ["<img class='carousel-nav-img img-prev' src="+ back +">","<img class='carousel-nav-img img-next' src="+ next +">"]
                  });
              $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
              $('.footer .footer-widget .widget-content .social-links li a img[alt="fb"]').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/e7ce2d51373ad8e654b1e4378fa7e94eea9b132c/scoon/images/fb.svg');
              $('.footer .footer-widget .widget-content .social-links li img[alt="fb"]').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/e7ce2d51373ad8e654b1e4378fa7e94eea9b132c/scoon/images/fb.svg');
              $('.footer .footer-widget .widget-content .social-links li a img[alt="instagram"]').attr('src',' https://bitbucket.org/TSTechsembly/jscss/raw/e7ce2d51373ad8e654b1e4378fa7e94eea9b132c/scoon/images/insta.svg');
              $('.footer .footer-widget .widget-content .social-links li img[alt="instagram"]').attr('src',' https://bitbucket.org/TSTechsembly/jscss/raw/e7ce2d51373ad8e654b1e4378fa7e94eea9b132c/scoon/images/insta.svg');
              $('.footer .copyright-cont .pay-methods-avail').append('<div class="meth-link new"><img alt="apple-pay" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/apple_pay-f6db0077dc7c325b436ecbdcf254239100b35b70b1663bc7523d7c424901fa09.svg" width="52"></div>'+
              '<div class="meth-link new"><img alt="master-card" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/master-173035bc8124581983d4efa50cf8626e8553c2b311353fbf67485f9c1a2b88d1.svg" width="52"></div>'+
              '<div class="meth-link new"><img alt="paypal" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/paypal-49e4c1e03244b6d2de0d270ca0d22dd15da6e92cc7266e93eb43762df5aa355d.svg" width="52"></div>'+
              '<div class="meth-link new"><img alt="visa" src="https://cdn.shopify.com/shopifycloud/shopify/assets/payment_icons/visa-319d545c6fd255c9aad5eeaad21fd6f7f7b4fdbdb1a35ce83b89cca12a187f00.svg" width="52"></div>');
              $('<div class="d-flex align-items-center pt-2"><a class="footer-cp-link" href="https://techsembly.com/" target="_blank"><img class="copy-right-img" src="https://media.graphcms.com/gwNQKBJfTHCU2KGOiUCH", width="160"/></a></div>').insertAfter($(".powered-link-cont"));
          }, 1500);
          setInterval(function () {
            /*mobile*/
            $('header.header .mobile-menu#sidebarCollapse svg').each(function(){
                $(this).attr('height','25');
                $(this).attr('width','37.5');
                $(this).find('rect').attr('height','5');
                $(this).find('rect:nth-child(2)').attr('y','50');
                $(this).find('rect:nth-child(3)').attr('y','90');
            });
            if(!$('header.header .toggle-menu-cont').hasClass('col-4')){
                $('header.header .toggle-menu-cont').addClass('col-4').removeClass('col-1 col-sm-2 col-md-2');
            }
            if(!$('header.header .toggle-menu-cont').hasClass('modified')){
              $('header.header .toggle-menu-cont').addClass('modified');
              $('header.header .toggle-menu-cont.modified').append('<ul class="nav navbar-nav navbar-right d-sm-flex flex-row d-lg-none mob-top-links left"></ul>');
            }
            $('header .mob-top-links li .mob-search-link').closest('li:not(.mob-search-link-cont)').addClass('mob-search-link-cont');
            $('header .mob-top-links li .dropdown-flag').closest('li:not(.dropdown-flag-cont)').addClass('dropdown-flag-cont');
            $('header .mob-search-link-cont:not(.moved)').addClass('moved').appendTo( ".mob-top-links.left" );
            $('header .dropdown-flag-cont:not(.moved)').addClass('moved').appendTo( ".mob-top-links.left" );
            if(!$('header.header .menu-links-cont').hasClass('col-4')){
                $('header.header .menu-links-cont').addClass('col-4').removeClass('col-md-6 col-sm-5 col-7');
            }
            if($('header.header .logo').parent('div').hasClass('col-sm-5')){
                $('header.header .logo').parent('div').removeClass('col-sm-5');
            }
            /*end mobile*/
            $('header.header .menu-holder').each(function(){
                $(this).closest('.container:not(.main-menu-container)').addClass('main-menu-container');
            });
            $('header.header .main-menu-container').each(function(){
              if (!$(this).find('.nav.search').length) {
                  $('header.header .menu-links-cont .nav.search').appendTo('.main-menu-container');
              }
              if (!$(this).find('.main-menu-inner-cont').length) {
                $(this).wrapInner( "<div class='main-menu-inner-cont w-100 d-flex justify-content-between'></div>");
              }
            });
            /*$('header.header').each(function(){
              if (!$(this).find('.main-menu-after').length) {
                $("<div class='container-fluid main-menu-after d-none d-lg-block'></div>").insertAfter(".main-menu-container");
              }
            });*/
            $('.hero-banner-cont').closest('.container:not(.home-banner-container)').addClass('home-banner-container');
            $('.home-slider .section-title:contains("New Arrivals")').closest('.home-slider:not(.new-arrival-slider)').addClass('new-arrival-slider');
            $('.filters-section').closest('.home-intro-container:not("filters-container")').addClass('filters-container');
            $('.clients-carousel').closest('.home-intro-container:not("clients-container")').addClass('clients-container');
            $('.home-carousel').each(function(){
               if(!$(this).hasClass('modified')){
                 $(this).addClass('modified');
               }
            });
            $('.bestsellers-slider .section-title:contains("Shop By Category")').each(function(){
              if(!$(this).closest('.bestsellers-slider').hasClass('home-category-slider')){
                $(this).closest('.bestsellers-slider').addClass('home-category-slider');
              }
            });
            $('.wishlistImage:has(img[alt="add-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
                $('.wishlistImage img').attr('src', 'https://bitbucket.org/TSTechsembly/jscss/raw/3257abe9d3ca18a77c63ce3218f11d8bffeeeabc/scoon/images/heart.svg');
            });
            $('.wishlistImage:has(img[alt="added-to-wishlist"]):not(.appeared)').addClass('appeared').each(function () {
                $('.wishlistImage img[alt="added-to-wishlist"]').attr('src', 'https://bitbucket.org/TSTechsembly/jscss/raw/2cd4c3c48caa0d31cd31f816b737d1727fc7889b/scoon/images/heart-filled.svg');
            });
            $('.products-container .sidebar-widget .list-group').each(function(){
              if (!$(this).find('.list-head').length) {
                  $(this).prepend('<h3 class="list-head text-uppercase">Filter By</h3>');
              }
            });
            $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
                $(this).html('Add To Bag');
            });
            hrefurl=$(location).attr("href");
            last_part=hrefurl.substr(hrefurl.lastIndexOf('/') + 1);
            currentUrlSecondPart = window.location.pathname.split('/')[1];

            if (currentUrlSecondPart == ''){
               if (!$('body').attr('data-pagetype','home')){
                   $('body').attr('data-pagetype','home');
               }
            }
            if (currentUrlSecondPart == 'catalogsearch'){
               if (!$('body').attr('data-pagetype','search_results')){
                   $('body').attr('data-pagetype','search_results');
               }
            }
            if (currentUrlSecondPart == 'vendors'){
               if (!$('body').attr('data-pagetype','vendors')){
                   $('body').attr('data-pagetype','vendors');
               }
            }
            if (last_part == 'wishlist'){
               if (!$('body').attr('data-pagetype','wishlist')){
                   $('body').attr('data-pagetype','wishlist');
               }
               if(!$('header.header .head-links li a img[alt="wishlist"]').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/2cd4c3c48caa0d31cd31f816b737d1727fc7889b/scoon/images/heart-filled.svg')){
                  $('header.header .head-links li a img[alt="wishlist"]').attr('src','https://bitbucket.org/TSTechsembly/jscss/raw/2cd4c3c48caa0d31cd31f816b737d1727fc7889b/scoon/images/heart-filled.svg');
               }
            }
            if ($(".wishlist-container .products-container .products-holder i[aria-hidden='true']").length < 2){
              $('.wishlist-container .products-container .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
            }
            $('.address-form').find("input[formcontrolname=building]").each(function(){
                $(this).parent('div').addClass('building-cont');
            });
            $('.address-form').find("input[formcontrolname=postCode]").each(function(){
                $(this).parent('div').addClass('post-code-cont');
            });
            $('.address-form').find("input[formcontrolname=phone]").each(function(){
                if(!$(this).val()) {
                  $(this).attr("placeholder", "Phone Number");
                }
                $(this).parent('div').addClass('phone-no-cont');
            });
            $('app-product-detail app-related-products[title="More items from"]').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).find('h3').html('you may also like');
              }
            });
            $('.custom-container.cart-container app-breadcrumbs').each(function(){
              if(!$(this).closest('.row').hasClass('breadcrumb-cont')){
                $(this).closest('.row').addClass('breadcrumb-cont');
              }
            });
            $('.custom-container.cart-container .breadcrumb-heading:not(.modified)').addClass('modified').html('My Shopping Bag');
            $('app-cart').each(function () {
                if (!$(this).children('.container-fluid.mob-sep').length) {
                    $(this).prepend('<div class="container-fluid d-md-none mob-sep"></div>')
                }
            });
            $('.custom-container.cart-container .vendor-heading').each(function () {
                if (!$(this).children('span.delivered-by').length) {
                    $(this).prepend('<span class="delivered-by mr-2">By</span>');
                }
            });
            $('.cart-item.dt .item-img').each(function(){
                if (!$(this).parent('.col-md-2').hasClass('item-img-holder')){
                    $(this).parent('.col-md-2').addClass('item-img-holder');
                }
            });
            $('.cart-item.dt .item-details').each(function(){
                if (!$(this).parent('.col-md-10').hasClass('item-det-holder')){
                    $(this).parent('.col-md-10').addClass('item-det-holder');
                }
            });
            $('.custom-container.cart-container .total-cart-wrapper').each(function(){
                if(!$(this).closest('.border-grey').hasClass('total-cart-sep')){
                    $(this).closest('.border-grey').addClass('total-cart-sep');
                }
            });
            $('.footer .copyright-cont .pay-methods-avail ').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).find('.meth-link:not(.new)').addClass('d-none');
                }
            });
            if(!$('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').hasClass('social-widget')){
                $('.footer .footer-widget .widget-content .social-links').closest('.footer-widget').addClass('social-widget');
                $('.social-widget').append('<a class="btn btn-primary btn-contact d-none d-lg-flex justify-content-center align-items-center mt-1" href="/contact">CONTACT</a>')
            }
            $('.social-widget').each(function(){
              if (!$(this).find('.widget-title').hasClass('modified')){
                $(this).find('.widget-title').addClass('modified');
                $(this).find('.widget-title').html('follow us');
              }
            });

            // script for Collapse
            $('app-vendor').each(function(){
                if ($(this).length){
                    if (!$('body').attr('data-pagetype','vendor-page')){
                        $('body').attr('data-pagetype','vendor-page');
                    }
                }
            });
            $('.dt-view.vendor-micro-cont .vendor-logo').each(function(){
                if (!$(this).closest('div').hasClass('vendor-logo-cont')){
                    $(this).closest('div').addClass('vendor-logo-cont');
                }
                $(this).closest('div').removeClass('pb-5');
            });
            $('.vendor-micro-cont .vendor-intro').each(function(){
                if (!$(this).closest('.container').hasClass('vendor-intro-holder')){
                    $(this).closest('.container').addClass('vendor-intro-holder');
                    $('#about').appendTo('.vendor-intro-holder');
                    $('#about p.mb-4').addClass('info-text');
                    //$('.vendor-name > b').replaceWith('<h2>Meet Bybi</h2>');
                    $(this).find('.vendor-name > b').prepend('<span class="mr-1">Meet</span>');
                }
            });
            $('.vendor-micro-cont .vendor-menu-cont').each(function(){
                if (!$(this).closest('.container').hasClass('d-none')){
                    $(this).closest('.container').addClass('d-none');
                }
            });

            // $('.vendor-micro-cont.dt-view .container#about div.name').each(function(){
            //     if(!$(this).children('h6.micro-heading').length){
            //         $(this).prepend('<h6 class="micro-heading">MEET</h6>');
            //     }
            // });
            // $('.vendor-intro-mob .vendor-text div.name').each(function(){
            //     if(!$(this).children('h6.micro-heading').length){
            //         $(this).prepend('<h6 class="micro-heading">MEET</h6>');
            //     }
            // });

            $('.vendor-micro-cont .container .vendor-heading').each(function () {
                if(!$(this).children('a.collapse-link').length){
                    $(this).prepend('<a class="collapse-link collapsed" data-toggle="collapse" aria-expanded="false"><i class="fa fa-plus" aria-hidden="true"></i></a>');
                }
            });
            $('.vendor-micro-cont .container.vendor-contact-form-cont ').each(function(){
                if($(this).attr('id') != 'contact-form-cont') {
                    $(this).attr('id', 'contact-form-cont');
                }
                if ( $(this).find('.collapse-link').attr('href') != '#cont-form-wrap' ) {
                    $(this).find('.collapse-link').attr('href','#cont-form-wrap');
                    $(this).find('.collapse-link').attr('aria-controls','cont-form-wrap');
                }
                if ( $(this).children('.vendor-form-inner').attr('id') != 'cont-form-wrap' ) {
                    $(this).children('.vendor-form-inner').attr('id','cont-form-wrap');
                }
            });
            $('.vendor-micro-cont .container.vendor-point-cont#delivery ').each(function(){
                if ( $(this).find('.collapse-link').attr('href') != '#del-desc' ) {
                    $(this).find('.collapse-link').attr('href','#del-desc');
                    $(this).find('.collapse-link').attr('aria-controls','del-desc');
                }
                if ( $(this).children('.vendor-point-desc').attr('id') != 'del-desc' ) {
                    $(this).children('.vendor-point-desc').attr('id','del-desc');
                }
            });
            $('.vendor-micro-cont .container.vendor-point-cont#delivery p.mb-4').each(function(){
                var addHtml = $(this).html();
                var newAddHtml = 'Ships from <span class="address-line">'+addHtml.substring(13)+'</span>';
                //console.log(newAddHtml);
                if(!$(this).children('span.address-line').length){
                    $(this).html(newAddHtml);
                }
            });
            $('.vendor-micro-cont .container .vendor-heading b:contains("Returns")').each(function(){
                if (!$(this).closest('.container').hasClass('returns-cont')) {
                    $(this).closest('.container').addClass('returns-cont');
                }
            });
            $('.vendor-micro-cont .container.vendor-point-cont.returns-cont ').each(function(){
                if ( $(this).find('.collapse-link').attr('href') != '#return-desc' ) {
                    $(this).find('.collapse-link').attr('href','#return-desc');
                    $(this).find('.collapse-link').attr('aria-controls','return-desc');
                }
                if ( $(this).children('.vendor-point-desc').attr('id') != 'return-desc' ) {
                    $(this).children('.vendor-point-desc').attr('id','return-desc');
                }
            });
            $('.vendor-micro-cont .container .vendor-heading b:contains("Terms")').each(function(){
                if (!$(this).closest('.container').hasClass('tnc-cont')) {
                    $(this).closest('.container').addClass('tnc-cont');
                }
            });
            $('.vendor-micro-cont .container.tnc-cont ').each(function(){
                if ( $(this).find('.collapse-link').attr('href') != '#tnc-desc' ) {
                    $(this).find('.collapse-link').attr('href','#tnc-desc');
                    $(this).find('.collapse-link').attr('aria-controls','tnc-desc');
                }
                if ( $(this).children('.vendor-point-desc').attr('id') != 'tnc-desc' ) {
                    $(this).children('.vendor-point-desc').attr('id','tnc-desc');
                }
            });
            $('.vendor-micro-cont .container #return-desc div:last-child').each(function(){
                var addReturnHtml = $(this).html();
                var newAddReturnHtml = addReturnHtml.substring(3);
                //console.log(newAddReturnHtml);
                if(!$(this).children('span.address-line').length){
                    $(this).html('<span class="address-line">'+newAddReturnHtml+'</span>');
                }
            });
            $('.vendor-micro-cont .container .vendor-heading b a.collapse-link').each(function(){
                if(!$(this).hasClass('collapsed')){
                    $(this).find('i.fa').addClass('fa-minus').removeClass('fa-plus');
                }
                else {
                    $(this).find('i.fa').addClass('fa-plus').removeClass('fa-minus');
                }
            });

            $('.vendor-micro-cont .container.vendor-contact-form-cont').each(function(){
                if(!$(this).children('.vendor-form-inner').length){
                    $(this).wrapInner($("<div class='vendor-form-inner collapse'>"));
                }
            });
            $('.vendor-micro-cont .container.vendor-contact-form-cont .vendor-form-inner').each(function(){
               $(this).find('h2.vendor-heading').prependTo($(this).closest(".vendor-contact-form-cont"));
            });
            $('.vendor-micro-cont .container.vendor-point-cont ').each(function(){
                if(!$(this).children('.vendor-point-desc').length){
                    //$(this).children().not('h2.vendor-heading ').wrapAll('<div class="vendor-point-desc collapse" />');
                    $(this).wrapInner($("<div class='vendor-point-desc collapse'>"));
                }
            });

            $('.vendor-micro-cont .container.vendor-point-cont .vendor-point-desc').each(function(){
               $(this).find('h2.vendor-heading').prependTo($(this).closest(".vendor-point-cont"));
            });

            $('.vendor-micro-cont .container.tnc-cont ').each(function(){
                if(!$(this).children('.vendor-point-desc').length){
                    //$(this).children().not('h2.vendor-heading ').wrapAll('<div class="vendor-point-desc collapse" />');
                    $(this).addClass('vendor-point-cont');
                    $(this).wrapInner($("<div class='vendor-point-desc collapse'>"));
                }
            });
            $('.vendor-micro-cont .container.tnc-cont .vendor-point-desc').each(function(){
               $(this).find('h2.vendor-heading').prependTo($(this).closest(".vendor-point-cont"));
            });
            $('.vendor-point-cont').removeClass('pt-lg-5 pt-4');
            $('.vendor-point-cont .vendor-heading').removeClass('mt-lg-5 mt-lg-3 mt-lg-4 pt-lg-1 mt-2');
            $('.vendor-contact-form-cont').removeClass('pt-4');
          });
        });
    });
})();
