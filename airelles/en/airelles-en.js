function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }
    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });
    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}
function addPromoHead() {
  var promoHead = '<h2 class="section-title text-center text-uppercase">discover airelles</h2>';
  var elemPromoHolder = $('.promo-section .promo-holder')[0];
  elemPromoHolder.insertAdjacentHTML('beforebegin', promoHead);
}
function addInstaLinkCont() {
  var instaLinkCont = '<div class="container home-intro-container">'+
  '<div class="row insta-link-cont justify-content-center py-5">'+
    '<a href="https://www.instagram.com/airellescollection/" target="_blank">'+
       '<img src="https://static.techsembly.com/PvfMxkHx5XaMZ5ZSuMZJiuof" width="107" alt="link">'+
    '</a>'+
  '</div>'+
  '</div>';
  var elemSignupSection = $('.sign-up-section')[0];
  elemSignupSection.insertAdjacentHTML('beforebegin', instaLinkCont);
}
function addLoginCartNote(){
  var loginCartNote = "<div class='login-note'>Please note this login is for the Airelles gifting website only.<div>";
  var elemHeading = $('.custom-container.cart-container .cart-login-cont .login-cart-panel')[0];
  elemHeading.insertAdjacentHTML('afterbegin', loginCartNote);
}
function addGuestCheckoutNote(){
  var checkoutNote = "<div class='checkout-note text-center w-100'>You can create account after checkout<div>";
  var elemGuestFormCont = $('.custom-container.cart-container .cart-login-cont .guest-form-cont')[0];
  elemGuestFormCont.insertAdjacentHTML('beforeend', checkoutNote);
}
function addcopyrightText(){
  const d = new Date();
  let copyrightYear = d.getFullYear();
  var copyrightHtml = '<div class="container-fluid"><div class="row"><div class="col-12 d-flex checkout-copyright-cont"><div class="col-7 col-md-6 tc-rights-reserved">Techsembly© '+copyrightYear+'</div><div class="col-5 col-md-6 ts-copyright text-right"></div></div></div></div>';
  var elem = $(".checkout-footer")[0];
  elem.insertAdjacentHTML('afterend', copyrightHtml);
}
(function() {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
      @font-face {\
        font-family: 'futura_pt';\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b49257e90b6a51577906b36c4c27c35f0f287a3e/common-fonts/futura-pt/futuraptbook-webfont.eot');\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/b49257e90b6a51577906b36c4c27c35f0f287a3e/common-fonts/futura-pt/futuraptbook-webfont.eot?#iefix') format('embedded-opentype'), \
             url('https://bitbucket.org/TSTechsembly/jscss/raw/b49257e90b6a51577906b36c4c27c35f0f287a3e/common-fonts/futura-pt/futuraptbook-webfont.ttf') format('truetype'),\
             url('https://bitbucket.org/TSTechsembly/jscss/raw/b49257e90b6a51577906b36c4c27c35f0f287a3e/common-fonts/futura-pt/futuraptbook-webfont.woff') format('woff'),\
             url('https://bitbucket.org/TSTechsembly/jscss/raw/b49257e90b6a51577906b36c4c27c35f0f287a3e/common-fonts/futura-pt/futuraptbook-webfont.woff2') format('woff2');\
      }\
      @font-face {\
        font-family: 'futura_ptmedium';\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptmedium-webfont.eot');\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptmedium-webfont.eot?#iefix') format('embedded-opentype'), \
             url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptmedium-webfont.ttf') format('truetype'),\
             url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptmedium-webfont.woff') format('woff'),\
             url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptmedium-webfont.woff2') format('woff2');\
      }\
      @font-face {\
        font-family: 'futura_ptbold';\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptbold-webfont.eot');\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptbold-webfont.eot?#iefix') format('embedded-opentype'), \
             url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptbold-webfont.ttf') format('truetype'),\
             url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptbold-webfont.woff') format('woff'),\
             url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptbold-webfont.woff2') format('woff2');\
      }\
      @font-face {\
        font-family: 'futura_ptlight';\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptlight-webfont.eot');\
        src: url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptlight-webfont.eot?#iefix') format('embedded-opentype'), \
             url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptlight-webfont.ttf') format('truetype'),\
             url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptlight-webfont.woff') format('woff'),\
             url('https://bitbucket.org/TSTechsembly/jscss/raw/9d8b4d495b5a4ee3110f91d1d86269bc941a3aee/common-fonts/futura-pt/futuraptlight-webfont.woff2') format('woff2');\
      }\
    "));
  document.head.appendChild(newStyle);

    // Poll for jQuery to come into existence
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Now lets do something
    checkReady(function($) {
        $(function() {
          $('head').append('<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">');

          waitForElm('.promo-section').then((elm) => {
            addPromoHead();
          }).catch((error) => {});

          waitForElm('.sign-up-section').then((elm) => {
            addInstaLinkCont();
          }).catch((error) => {});

          waitForElm('.cart-right-cont').then((elm) => {
            addLoginCartNote();
            addGuestCheckoutNote();
          }).catch((error) => {});

          waitForElm('app-footer-checkout').then((elm) => {
            addcopyrightText()
          }).catch((error) => {});

          setTimeout(function () {
            $('meta[name=viewport]').attr('content','width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0');
            $('.products-container .products-holder-main .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');

            var promoTitle;
            var promoheading;
            var promoHeadDesc;

            $('.promo-holder .promo-box').each(function () {
                promoTitle = $(this).find(".promo-title");
                promoheading = $(this).find(".banner-title").html();
                promoHeadDesc = $('<div class="promo-head-desc">'+promoheading+'</div>');
                promoHeadDesc.insertAfter(promoTitle);

            });
          }, 2000);
          var timesRun = 0;
          var interval = setInterval(function(){
              if($('.payment-form-container').length){
                timesRun += 1;
                if(timesRun === 2){
                    clearInterval(interval);
                }
                $(window).scrollTop(0);
                console.log('scrolled');
              }
          }, 2000);

          setInterval(function () {
            $('header.header .shipping li .dropdown .dropdown-menu').each(function(){
              if(!$(this).hasClass('countries-dd')){
                $(this).addClass('countries-dd');
                $(this).append('<div class="countries-links-cont">'+
                '<div class="en"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.airelles.com/airelles-en">EN</a></div>'+
                '<div class="fr"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.airelles.com/">FR</a></div>'+
                '</div>');
              }
            });

            $('#sidebar ul.list-unstyled.user-links').each(function(){
              if(!$(this).hasClass('user-mob-links')){
                $(this).addClass('user-mob-links');
                $(this).append('<li>'+
                '<div class="d-flex align-items-center">'+
                '<div class="dropdown pref_dropdown w-100">'+
                '<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownMenuLink_mob">EN</a>'+
                '<div aria-labelledby="dropdownMenuLink" class="dropdown-menu countries-links-cont pt-0 mt-0" x-placement="top-start">'+
                '<div class="en"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.airelles.com/airelles-en">EN</a></div>'+
                '<div class="fr"><a class="dropdown-item hidestore text-uppercase" translate="" href="https://gifts.airelles.com/">FR</a></div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</li>');
              }
            });
            $(".menu-links-cont img[alt='user-profile']:not(.modified)").addClass('modified').attr("src","https://static.techsembly.com/2C34LjCnPv9w8SrRVKoog4j9");
            $(".menu-links-cont img[alt='wishlist']:not(.modified)").addClass('modified').attr("src","https://static.techsembly.com/GwdQsGJMMci6juz8Xxgiwvsy");
            $(".menu-links-cont img[alt='cart']:not(.modified)").addClass('modified').attr("src","https://static.techsembly.com/5oWbtniJTnncGcygFZkQd5un");
            $(".header .menu-links-cont .search-item-form .btn-search img:not(.altered)").addClass('altered').attr("src","https://static.techsembly.com/rMt6Bia1sBUficCTsTvMFEu1");

            if(!$('header.header .toggle-menu-cont').hasClass('col-2')){
                $('header.header .toggle-menu-cont').addClass('col-2').removeClass('col-1 col-sm-2 col-md-2');
            }
            if(!$('header.header .menu-links-cont').hasClass('col-2')){
                $('header.header .menu-links-cont').addClass('col-2').removeClass('col-md-6 col-sm-5 col-7');
            }
            if($('header.header .logo').parent('div').hasClass('col-sm-5')){
                $('header.header .logo').parent('div').removeClass('col-4 col-sm-5');
                $('header.header .logo').parent('div').addClass('col-8 logo-cont');
            }

            $('header.header .menu-holder').each(function(){
              if(!$(this).closest('.container').hasClass('wrapped')){
                $(this).closest('.container').addClass('wrapped');
                $(this).closest('.container').wrap('<div class="container-fluid main-menu-wrapper"></div>');
              }
            });
            $('header.header .menu-holder>li>div').each(function(){
              if(!$(this).children().length){
                $(this).addClass('empty');
              }
            });
            $('.sign-up-section .sign-up-container .sign-up-form input[type="email"]').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).attr('placeholder','Your Email*');
              }
            });

            $('a.wishlistImage img').each(function () {
              if (!$(this).hasClass('appeared')) {
                $(this).addClass('appeared');
                $(this).attr('src', 'https://static.techsembly.com/GwdQsGJMMci6juz8Xxgiwvsy');
                if (($(this).attr('alt') == 'added-to-wishlist') && ($(this).attr('src') != 'https://static.techsembly.com/Ud7pVK9encjXZQ44DZg1XbcF')) {
                  $(this).attr('src', 'https://static.techsembly.com/Ud7pVK9encjXZQ44DZg1XbcF');
                }
              }
            });

            $('.product-detail-container .product-detail').closest('.col-lg-5:not(.modified)').addClass('product-inner modified');
            $('.product-detail-container .product-carousel').closest('.col-lg-7:not(.modified)').addClass('product-carousel-cont modified');

            $('.product-detail-container .personalise-form .form-group .order-btn').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).html('BUY NOW');
              }
            });

            $('app-product-detail app-related-products[title="More items from"]').each(function(){
              if(!$(this).hasClass('modified')){
                  $(this).addClass('modified');
                  $(this).find('h3').html('More Like This');
              }
            });

            $('.custom-container.cart-container .vendor-items-cont a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/ibdPB8qWRETPckx6Psjp5NTh');
            $('.custom-container.cart-container .vendor-items-cont a img[alt="gift"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/UQRXekXeA5KNyPRVnkvBwMrC');

            $('.custom-container.cart-container .vendor-items-cont .card-header .vendor-heading .delivered-by').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('Fulfilled By');
                }
            });


            $('.cart-item.new .item-details .item-qty-cont').each(function(){
              if(!$(this).closest('.col-lg-2').hasClass('qty-col')){
                  $(this).closest('.col-lg-2').addClass('qty-col');
              }
            });

            $('.cart-item.new .item-details .unit-total-price').each(function(){
              if(!$(this).closest('.col-lg-3').hasClass('sub-total-col')){
                  $(this).closest('.col-lg-3').addClass('sub-total-col');
              }
            });

            $('.cart-item.new .pro-badges-cont .pro-badge img.badge-img:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/akP3DPPvAmtXSvNigzqCxi8w');

            $('.cart-item.new .gift-msg-cont textarea').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).attr('placeholder','Max 200 characters');
                }
            });

            $('.custom-container.cart-container .cart-login-cont .checkout-desc-cont img[alt="lock"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/AxhyXx4e1PSYGfQ99FhXQUPH');

            $('.custom-container.cart-container .btn.continue-btn-new').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('Return To shopping');
                }
            });

            if ($(".wishlist-container .products-container .products-holder i[aria-hidden='true']").length < 2){
              $('.wishlist-container .products-container .products-holder').append('<i aria-hidden="true"></i><i aria-hidden="true"></i>');
            }

            $('.custom-container.checkout').each(function(){
              if (!$(this).closest('body').attr('data-pagetype','checkout')){
                $(this).closest('body').attr('data-pagetype','checkout');
              }
            });

            $('.shipping-form-container .breadcrumb-heading:contains("Shipping")').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified').html("Shipping Details");
              }
            });

            $('.auth-user .checkout .shipping-form h1.breadcrumb-heading:contains("Shipping Details")').each(function() {
              if(!$(this).closest('.shipping-form').hasClass('shipp-details')){
                $(this).closest('.shipping-form').addClass('shipp-details');
                $(this).html('Choose Shipping Details');
              }
            });

            $('.shipping-form-container .breadcrumb-heading:contains("Billing")').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified').html("Billing Details");
              }
            });

            $('.auth-user .checkout .shipping-form h1.breadcrumb-heading:contains("Billing")').each(function() {
              if(!$(this).closest('.shipping-form').hasClass('bill-details')){
                $(this).closest('.shipping-form').addClass('bill-details');
                $(this).html('Choose Billing Details');
              }
            });

            $('.shipping-form-container .exist-address-wrapper .card a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/ibdPB8qWRETPckx6Psjp5NTh');

            $('.container.custom-container.checkout .btn#continue-to-delivery-and-billing').each(function(){
                if(!$(this).closest('.form-group').hasClass('submit-btn-cont')){
                    $(this).closest('.form-group').addClass('submit-btn-cont mb-0');
                    $(this).html('Continue to delivery');
                }
            });

            $('.auth-user .custom-container.checkout .shipping-form-container').each(function () {
              if (($(this).find('.exist-address-wrapper').length) && !$(this).hasClass('exist-add-exists')) {
                $(this).addClass('exist-add-exists');
                $("#continue-to-delivery-and-billing").html('ADD & CONTINUE TO DELIVERY');
              }
            });

            $(' .custom-container.checkout .shipping-form-container .breadcrumb-heading:contains("Select Delivery Method")').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('Select Shipment Method');
                }
            });

            $('.delivery-methods-cont .vendor-cards-holder .card .order-item a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/ibdPB8qWRETPckx6Psjp5NTh');

            $('.delivery-methods-cont .vendor-cards-holder .card .delivery-dropdowns-cont .label-email .recipent-info-edit img[alt="edit-recipient"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/ibdPB8qWRETPckx6Psjp5NTh');

            $('.delivery-methods-cont .card .card-body .card-row a img[alt="edit"]:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/ibdPB8qWRETPckx6Psjp5NTh');

            $('.custom-container.checkout .delivery-methods-cont .vendor-cards-holder .card .card-header .vendor-title span:contains("Delivered by")').each(function(){
                if(!$(this).hasClass('modified')){
                    $(this).addClass('modified');
                    $(this).html('Fulfilled By');
                }
            });

            $('.delivery-methods-cont .vendor-cards-holder .card .card-header .badge-stroke img:not(.modified)').addClass('modified').attr('src','https://static.techsembly.com/akP3DPPvAmtXSvNigzqCxi8w');

            $('.custom-container.checkout .order-summary-container .vendor-items-holder .vendor-order-details .order-item .qty').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newTxt = 'QTY:';
                $(".qty:contains('Qty:')").html(function(_, html) {
                   return  html.replace(/(Qty:)/g, '<span class="updated-qty-string mr-1">'+newTxt+'</span>');
                });
              }
            });

            $('.delivery-methods-cont .vendor-cards-holder .card .order-item .qty').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newTxt = 'QTY';
                $(".qty:contains('Qty:')").html(function(_, html) {
                   return  html.replace(/(Qty:)/g, '<span class="updated-qty-string mr-1">'+newTxt+'</span>');
                });
              }
            });

            $('.custom-container.checkout .delivery-methods-cont .vendor-cards-holder .card .shipping-totals-cont .total-caption:contains("Shipping")').each(function(){
              if(!$(this).closest('.total-row').hasClass('shipping-total-cont')){
                $(this).closest('.total-row').addClass('shipping-total-cont');
              }
            });

            $('.checkout .shipping-form-container .btn.btn-submit:contains("Continue Checkout")').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).closest('.form-group').addClass('submit-btn-cont mb-lg-0');
                $(this).html('Continue to payment');
              }
            });

            $('.custom-container.checkout .payment-form-container .section-heading:contains("Card Details")').each(function(){
              if(!$(this).closest('.section-cont').hasClass('cc-details-cont')){
                $(this).closest('.section-cont').addClass('cc-details-cont');
              }
            });

            $('.custom-container.checkout .payment-form-container .section-heading:contains("Details")').each(function(){
              if(!$(this).closest('.section-cont').hasClass('billing-detail-cont')){
                $(this).closest('.section-cont').addClass('billing-detail-cont');
              }
            });

            $('.custom-container.checkout.digital .payment-form-container .section-heading:contains("Details")').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).html('Billing Details');
              }
            });

            $('.custom-container.checkout .payment-form-container label[for="sameBillShopAdd"] .checkbox-note').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');
                $(this).html('Same as Shipping Address');
              }
            });

            $('.custom-container.checkout .payment-form-container .form-group .checkbox-cont[for="enableCheckout"]').each(function () {
              if (!$(this).closest('.card').hasClass('blue-light-bg')) {
                $(this).closest('.card').addClass('blue-light-bg enable-checkout-card');
                $(this).closest('.col-12.pt-4').addClass('enable-checkout-cont');
              }
            });

            $('.order-summary-cont .order-totals-container .sub-total-holder .sub-total-desc').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newCol = '';
                $(this).html(function(_, html) {
                   return  html.replace(/(:)/g, '<span class="updated-string">'+newCol+'</span>');
                });
              }
            });

            $('.custom-container.checkout .payment-form-container .btn.btn-submit').each(function(){
                if(!$(this).closest('.form-group').hasClass('submit-btn-cont')){
                    $(this).closest('.form-group').addClass('submit-btn-cont mb-lg-0');
                }
            });

            $('.checkout .checkout-container.complete .card.order-confirm-wrapper').each(function(){
              if(!$(this).parent('.col-12').hasClass('order-confirm-outer')){
                $(this).parent('.col-12').addClass('order-confirm-outer');
              }
            });

            $('.checkout-container.complete .section-cont.customer-note').each(function(){
              if(!$(this).hasClass('total-delivery')){
                $(this).addClass('total-delivery');
                var newTxt = 'You order is being confirmed and we will be shipping it soon';
                $(".customer-note:contains('Your order is being confirmed and we will be shipping it soon')").html(function(_, html) {
                   return  html.replace(/(Your order is being confirmed and we will be shipping it soon)/g, '<div class="updated-del-string">'+newTxt+'</div>')
                });
              }
            });

            $('.checkout-container.complete .section-cont.shipping-data .shipping-row div.col-md-3').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newCol = '';
                $(this).html(function(_, html) {
                   return  html.replace(/(:)/g, '<span class="updated-string">'+newCol+'</span>');
                });
              }
            });

            $('.checkout .checkout-container.complete .total-cont .col-3').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                var newCol = '';
                $(this).html(function(_, html) {
                   return  html.replace(/(:)/g, '<span class="updated-string">'+newCol+'</span>');
                });
              }
            });

            $('.checkout-container.complete .msg-cont .msg-inner').each(function () {
              if (!$(this).hasClass('modified')) {
                $(this).addClass('modified');

                newStr = 'AIRELLES CHÂTEAU DE VERSAILLES';

                $(".checkout-container.complete .msg-cont .msg-inner:contains('Airelles - EN')").html(function (_, html) {
                  return  html.replace(/(Airelles - EN)/g, '<div class="updated-hotel-name">'+newStr+'</div>')

                });
              }
            });

            $('.checkout-container.complete .card-footer').each(function(){
              if(!$(this).hasClass('modified')){
                $(this).addClass('modified');
                $(this).append('<div class="col-12 text-center home-btn-cont"><a class="btn btn-primary home-btn text-center text-uppercase" href="/">back to home</a></div>');
              }
            });

            $('.signup-form label:contains("The password must contain")').each(function(){
              if(!$(this).closest('.form-group').hasClass('password-note-cont')){
                $(this).closest('.form-group').addClass('password-note-cont');
              }
            });

            $('.user-container .balance-form').closest('.user-container:not(.d-none)').addClass('d-none');

            $('.user-wrapper .container.user-container .order-items-details .order-item input[type="text"]:not(.form-control)').addClass('form-control mt-3 mt-md-0 mb-3');

            $('.user-wrapper .container.user-container .order-items-details .order-item button:not(.btn)').addClass('btn btn-primary');

            $('.footer .d-lg-flex .footer-widget').each(function(){
              if(!$(this).closest('.d-lg-flex').hasClass('footer-widgets-cont')){
                $(this).closest('.d-lg-flex').addClass('footer-widgets-cont');
              }
            });
          });
        });
    });
})();
